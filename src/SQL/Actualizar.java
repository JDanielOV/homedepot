package SQL;

import javax.swing.*;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Ochoa Fecha: 21/11/2021
 * compuse modificar trabajador
 **/
public class Actualizar {
    /**
     * Valores de sParametros:
     * 0 = sNombre_Nuevo
     * 1 = sApellidoP_Nuevo
     * 2 = sApellidoM_Nuevo
     * 3 = sCorreo_Nuevo
     * 4 = sTelefono_Nuevo
     * 5 = sFecha_Nuevo
     * 6 = sNombre_Anterior
     * 7 = sApellidoP_Anterior
     * 8 = sApellidoM_Anterior
     * 9 = idCliente
     **/
    public static boolean actualizarCliente(String[] sParametros) {
        try {
            Connection cnn;
            CallableStatement cst;
            String sNombreCompleto = sParametros[0].concat(" ").concat(sParametros[1]).concat(" ").concat(sParametros[2]);
            String[] sTextos = {sParametros[0], sParametros[1], sParametros[2], sParametros[4], sParametros[5], sParametros[6], sParametros[7], sParametros[8]};
            boolean bVerVacios = ValidacionesGenerales.validarVacios(sTextos);
            boolean bValidarNombre = ValidacionesGenerales.validarCadenasLetras(sParametros[0].concat(sParametros[1]).concat(sParametros[2]));
            boolean bValidarTelefono = ValidacionesGenerales.validarCadenaTelefono(sParametros[4]);
            boolean bValidarFechaNacimiento = ValidacionesGenerales.validarFechaNacimiento(sParametros[5], "Cliente");
            boolean bValidarCorreo = ValidacionesGenerales.validarCorreo(sParametros[3]);
            boolean bValidarNombreActualYViejo = (sParametros[0].equalsIgnoreCase(sParametros[6]) &&
                    sParametros[1].equalsIgnoreCase(sParametros[7]) &&
                    sParametros[2].equalsIgnoreCase(sParametros[8])
            );
            if (bVerVacios && bValidarNombre && bValidarTelefono && bValidarFechaNacimiento) {
                if ((!sParametros[3].isBlank() && bValidarCorreo) || sParametros[3].isBlank()) {
                    boolean bValidarExistencia = ValidacionesGenerales.validarExistencia(sNombreCompleto, "ClienteNombre", sParametros[5]);
                    if (bValidarNombreActualYViejo || (!bValidarExistencia)) {
                        cnn = Conexion.getConexion();
                        cst = cnn.prepareCall("call actualizarCliente (?,?,?,?,?,?,?)");
                        cst.setString(1, sParametros[9]);
                        cst.setString(2, sParametros[0]);
                        cst.setString(3, sParametros[1]);
                        cst.setString(4, sParametros[2]);
                        cst.setString(5, sParametros[3]);
                        cst.setString(6, sParametros[4]);
                        cst.setString(7, sParametros[5]);
                        cst.execute();
                    } else {
                        JOptionPane.showMessageDialog(null, "El cliente ya existe ");
                        return false;
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Error con los datos proporcionados ");
                    return false;
                }
            } else {
                JOptionPane.showMessageDialog(null, "Error con los datos proporcionados ");
                return false;
            }
        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Problema con insertar usuario " + sqlex.getMessage());
            return false;
        }
        return true;
    }

    /**
     * Valores de sParametros:
     * 0 = sNombre_Nuevo
     * 1 = sApellidoP_Nuevo
     * 2 = sApellidoM_Nuevo
     * 3 = sCorreo_Nuevo
     * 4 = sTelefono_Nuevo
     * 5 = sFecha_Nuevo
     * 6 = sDireccion_Nuevo
     * 7 = sCargo_Nuevo
     * 8 = sSueldo_Nuevo
     * 9 = sContraseña_Nuevo
     * 10 = sNombre_Anterior
     * 11 = sApellidoP_Anterior
     * 12 = sApellidoM_Anterior
     * 13 = sContraseña_Anterior
     * 14 = rfcTrabajador
     **/
    public static boolean actualizarTrabajador(String[] sParametros) {
        try {
            Connection cnn;
            CallableStatement cst;
            String[] sTextos = {sParametros[0], sParametros[1], sParametros[2], sParametros[4], sParametros[5], sParametros[6], sParametros[7], sParametros[8],
                    sParametros[10], sParametros[11], sParametros[12], sParametros[13], sParametros[14]};
            boolean bVerVacios = ValidacionesGenerales.validarVacios(sTextos);
            boolean bValidarNombre = ValidacionesGenerales.validarCadenasLetras(sParametros[0].concat(sParametros[1]).concat(sParametros[2]));
            boolean bValidarTelefono = ValidacionesGenerales.validarCadenaTelefono(sParametros[4]);
            boolean bValidarFechaNacimiento = ValidacionesGenerales.validarFechaNacimiento(sParametros[5], "Trabajador");
            boolean bValidarCorreo = ValidacionesGenerales.validarCorreo(sParametros[3]);

            //boolean bValidarContraseñaActualYViejo = (sParametros[9].equalsIgnoreCase(sParametros[13]));
            boolean bValidarContraseñaActualYViejo = (ValidacionesGenerales.validarExistenciaCont(sParametros[14], sParametros[13], "TrabajadorContraseña"));
            if (bVerVacios && bValidarNombre && bValidarTelefono && bValidarFechaNacimiento) {

                if ((!sParametros[3].isBlank() && bValidarCorreo) || sParametros[3].isBlank()) {

                    String sNombreCompleto = sParametros[0].concat(" ").concat(sParametros[1]).concat(" ").concat(sParametros[2]);
                    ArrayList<String> sRFCs = ValidacionesGenerales.trabajadoresNombreFecha(sNombreCompleto, sParametros[5]);

                    if (bValidarContraseñaActualYViejo) {
                        System.out.println(sParametros[14].toLowerCase() + " " + sRFCs);
                        if (sRFCs.size() == 0 || sRFCs.contains(sParametros[14].toLowerCase())) {

                            if (sParametros[9].isBlank()) {
                                sParametros[9] = sParametros[13];
                            }
                            cnn = Conexion.getConexion();
                            cst = cnn.prepareCall("call actualizarTrabajador (?,?,?,?,?,?,?,?,?,?,?)");
                            cst.setString(1, sParametros[14]);
                            cst.setString(2, sParametros[0]);
                            cst.setString(3, sParametros[1]);
                            cst.setString(4, sParametros[2]);
                            cst.setString(5, sParametros[3]);
                            cst.setString(6, sParametros[4]);
                            cst.setString(7, sParametros[5]);
                            cst.setString(8, sParametros[6]);
                            cst.setString(9, sParametros[7]);
                            cst.setString(10, sParametros[8]);
                            cst.setString(11, sParametros[9]);
                            cst.execute();
                        } else {
                            JOptionPane.showMessageDialog(null, "El trabajador ya existe ");
                            return false;

                        }
                    } else {
                        JOptionPane.showMessageDialog(null, "Error con la contraseña proporcionada ");
                        return false;
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Error con los datos proporcionados ");
                    return false;
                }
            } else {
                JOptionPane.showMessageDialog(null, "Error con los datos proporcionados ");
                return false;
            }

        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Problema con actualizar trabajador " + sqlex.getMessage());
            return false;
        }
        return true;
    }


    /**
     * Valores de sParametros:
     * 0=Nombre
     * 1=Departamento
     * 2=IdProveedor
     * 3=precio
     * 4=cantidad
     * 5=id
     * 6=IdProveedorAnterior
     **/
    public static boolean actualizarProducto(String[] sParametros) {
        try {
            Connection cnn;
            CallableStatement cst;
            String[] sTextos = {sParametros[2], sParametros[0]};
            boolean bVerVacios = ValidacionesGenerales.validarVacios(sTextos);
            boolean bValidarProveedor = (sParametros[5].equals(sParametros[2])) ||
                    ValidacionesGenerales.validarExistencia(sParametros[2], "Proveedor");
            boolean bValidarProducto_Proveedor = ValidacionesGenerales.validarExistenciaProducto(sParametros[0], sParametros[2]);
            if (bVerVacios && bValidarProveedor) {
                ArrayList<String> sProductos = ValidacionesGenerales.ExistenciaProductoProveedores(sParametros[0], sParametros[2]);
                if (!bValidarProducto_Proveedor || sProductos.contains(sParametros[4])) {

                    System.out.println(sParametros[4]);
                    cnn = Conexion.getConexion();
                    cst = cnn.prepareCall("call actualizarProducto (?,?,?,?,?,?)");
                    cst.setString(1, sParametros[4]);
                    cst.setString(2, sParametros[0]);
                    cst.setString(3, sParametros[3]);
                    cst.setString(4, sParametros[2]);
                    cst.setString(5, sParametros[1]);
                    cst.setString(6, "1");
                    cst.execute();
                } else {
                    JOptionPane.showMessageDialog(null, "Este Producto ya existe con este proveedor");
                    return false;
                }

            } else {
                JOptionPane.showMessageDialog(null, "Error con los datos proporcionados ");
                return false;
            }
        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Problema con insertar usuario " + sqlex.getMessage());
            return false;
        }
        return true;
    }

    /**
     * Valores de sParametros:
     * 0 = sNombre_Nuevo
     * 1 = sApellidoP_Nuevo
     * 2 = sApellidoM_Nuevo
     * 3 = sCorreo_Nuevo
     * 4 = sTelefono_Nuevo
     * 5 = sEmpresaNuevo
     * 6 = sNombre_Anterior
     * 7 = sApellidoP_Anterior
     * 8 = sApellidoM_Anterior
     * 9 = idProveedor
     **/
    public static boolean actualizarProveedor(String[] sParametros) {
        try {
            Connection cnn;
            CallableStatement cst;
            String[] sTextos = {sParametros[0], sParametros[1], sParametros[2], sParametros[4], sParametros[5], sParametros[6], sParametros[7], sParametros[8]};
            String sNombreCompleto = sParametros[0].concat(" ").concat(sParametros[1]).concat(" ").concat(sParametros[2]);
            boolean bVerVacios = ValidacionesGenerales.validarVacios(sTextos);
            boolean bValidarNombre = ValidacionesGenerales.validarCadenasLetras(sParametros[0].concat(sParametros[1]).concat(sParametros[2]));
            boolean bValidarTelefono = ValidacionesGenerales.validarCadenaTelefono(sParametros[4]);
            //boolean bValidarEmpresa = ValidacionesGenerales.validarExistenciaEmpre(sParametros[9], sNombreCompleto, sParametros[5], "ProveedorEmpresa", 5);
            boolean bValidarCorreo = ValidacionesGenerales.validarCorreo(sParametros[3]);
            boolean bValidarNombreActualYViejo = (sParametros[0].equalsIgnoreCase(sParametros[6]) &&
                    sParametros[1].equalsIgnoreCase(sParametros[7]) &&
                    sParametros[2].equalsIgnoreCase(sParametros[8])
            );
            if (bVerVacios && bValidarNombre && bValidarTelefono ) {
                if ((!sParametros[3].isBlank() && bValidarCorreo) || sParametros[3].isBlank()) {
                    boolean bValidarExistencia = ValidacionesGenerales.validarExistencia(sNombreCompleto, "ProveedorNombre", sParametros[5]);
                    if (bValidarNombreActualYViejo || (!bValidarExistencia)) {
                        cnn = Conexion.getConexion();
                        cst = cnn.prepareCall("call actualizarProveedor (?,?,?,?,?,?,?)");
                        cst.setString(1, sParametros[9]);
                        cst.setString(2, sParametros[0]);
                        cst.setString(3, sParametros[1]);
                        cst.setString(4, sParametros[2]);
                        cst.setString(5, sParametros[3]);
                        cst.setString(6, sParametros[4]);
                        cst.setString(7, sParametros[5]);
                        cst.execute();
                    } else {
                        JOptionPane.showMessageDialog(null, "El Proveedor ya existe ");
                        return false;
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Error con los datos proporcionados ");
                    return false;
                }
            } else {
                JOptionPane.showMessageDialog(null, "Error con los datos proporcionados ");
                return false;
            }
        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Problema con insertar usuario " + sqlex.getMessage());
            return false;
        }
        return true;
    }

    /**
     * Valores de sParametros:
     * 0 = NpedFechaEntrega_Nuevo
     * 1 = NpedTotal_Nuevo
     * 2 = idPedido
     * 3= entregado
     **/
    public static boolean actualizarPedido(String[] sParametros) {
        try {
            Connection cnn;
            CallableStatement cst;
            String[] sTextos = {sParametros[0]};
            boolean bVerVacios = ValidacionesGenerales.validarVacios(sTextos);
            boolean bValidarFechaEntrega = (ValidacionesGenerales.validarFechaPedEntregaActualizacion(sParametros[0]));
            if (bVerVacios) {
                if (bValidarFechaEntrega) {
                    cnn = Conexion.getConexion();
                    cst = cnn.prepareCall("call actualizarPedido (?,?,?,?)");
                    cst.setString(1, sParametros[2]);
                    cst.setString(2, sParametros[0]);
                    cst.setString(3, sParametros[1]);
                    cst.setString(4, sParametros[3]);
                    cst.execute();
                } else {
                    JOptionPane.showMessageDialog(null, "La fecha de entrega no respeta los limites  ");
                    return false;
                }
            } else {
                JOptionPane.showMessageDialog(null, "Error con los datos proporcionados ");
                return false;
            }
        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Problema con insertar usuario " + sqlex.getMessage());
            return false;
        }
        return true;
    }

    /**
     * Valores de sParametros:
     * 0 =  NpedTotal_Nuevo
     * 1 = idPedido
     **/
    public static boolean actualizarPedidoTotal(String[] sParametros) {
        try {
            Connection cnn;
            CallableStatement cst;
            String[] sTextos = {sParametros[0]};
            boolean bVerVacios = ValidacionesGenerales.validarVacios(sTextos);
            if (bVerVacios) {
                    cnn = Conexion.getConexion();
                    cst = cnn.prepareCall("call actualizarPedidoTotal (?,?)");
                    cst.setString(1, sParametros[1]);
                    cst.setString(2, sParametros[0]);
                    cst.execute();
            } else {
                JOptionPane.showMessageDialog(null, "Error con los datos proporcionados ");
                return false;
            }
        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Problema con insertar usuario " + sqlex.getMessage());
            return false;
        }
        return true;
    }
    
    /**
     * Valores de sParametros:
     * 0 = NproductoId_Nuevo
     * 1 = NpedPrecio_Nuevo
     * 2 = NpedCant_Nuevo
     * 3 = idPedido
     * 4= entrega
     **/
    public static boolean actualizarFacturaPedido(String[] sParametros) {
        try {
            Connection cnn;
            CallableStatement cst;
            String[] sTextos = {sParametros[0], sParametros[1], sParametros[2]};
            boolean bVerVacios = ValidacionesGenerales.validarVacios(sTextos);
            if (bVerVacios) {
                cnn = Conexion.getConexion();
                cst = cnn.prepareCall("call actualizarFacturaPedido (?,?,?,?,?,?)");
                cst.setString(1, sParametros[3]);
                cst.setString(2, sParametros[0]);
                cst.setString(3, sParametros[1]);
                cst.setString(4, sParametros[2]);
                cst.setString(5, "1");
                cst.setString(6, sParametros[4]);
                cst.execute();
            } else {
                JOptionPane.showMessageDialog(null, "Error con los datos proporcionados ");
                return false;
            }
        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Problema con insertar usuario1 " + sqlex.getMessage());
            return false;
        }
        return true;
    }

    /**
     * Valores de sParametros:
     * 0 = NproductoId_Nuevo
     * 1 = idPedido
     **/
    public static boolean eliminarFacturaPedido(String id, String producto) {
        try {
            Connection cnn;
            CallableStatement cst;
            String[] sTextos = {id, producto};
            boolean bVerVacios = ValidacionesGenerales.validarVacios(sTextos);
            if (bVerVacios) {
                cnn = Conexion.getConexion();
                cst = cnn.prepareCall("call eliminarFacturaPedido (?,?)");
                cst.setString(1, id);
                cst.setString(2, producto);
                cst.execute();
            } else {
                JOptionPane.showMessageDialog(null, "Error con los datos proporcionados ");
                return false;
            }
        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Problema con insertar usuario1 " + sqlex.getMessage());
            return false;
        }
        return true;
    }
    
    public static boolean actualizarTarjeta(String[] sParametros) {
	try {
	    Connection cnn;
	    CallableStatement cst;
	    String[] sTextos = {sParametros[0], sParametros[1]};
	    boolean bVerVacios = ValidacionesGenerales.validarVacios(sTextos);
	    if (bVerVacios && !sParametros[0].equals(sParametros[1])) {
		    cnn = Conexion.getConexion();
		    cst = cnn.prepareCall("call combinarTarjetas (?,?)");
		    cst.setString(1, sParametros[0]);
		    cst.setString(2, sParametros[1]);
		    cst.execute();	
	    } else {
		JOptionPane.showMessageDialog(null, "Error con los datos proporcionados ");
		return false;
	    }
	} catch (SQLException sqlex) {
	    JOptionPane.showMessageDialog(null, "Problema con combinar tarjeta " + sqlex.getMessage());
	    return false;
	}
	return true;
    }
}
