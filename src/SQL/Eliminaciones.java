package SQL;

import javax.swing.*;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;

public class Eliminaciones {
    public static boolean eliminarTrabajador(String sRFC) {
        try {
            Connection cnn;
            CallableStatement cst;
            String[] sValores = {sRFC};
            if (ValidacionesGenerales.validarVacios(sValores)) {
                if (ValidacionesGenerales.validarExistencia(sRFC, "Trabajador")) {
                    cnn = Conexion.getConexion();
                    cst = cnn.prepareCall("call eliminarTrabajador (?)");
                    cst.setString(1, sRFC);
                    cst.execute();
                } else {
                    JOptionPane.showMessageDialog(null, "El trabajador no existe");
                    return false;
                }
            } else {
                JOptionPane.showMessageDialog(null, "Error con los datos proporcionados ");
                return false;
            }


        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Problema con Eliminar Trabajador " + sqlex.getMessage());
            return false;
        }
        return true;
    }

    public static boolean eliminarTarjeta(String sID) {
        try {
            Connection cnn;
            CallableStatement cst;
            String[] sValores = {sID};
            if (ValidacionesGenerales.validarVacios(sValores)) {
                if (ValidacionesGenerales.validarExistencia(sID, "Tarjetas")) {
                    cnn = Conexion.getConexion();
                    cst = cnn.prepareCall("call eliminarTarjeta (?)");
                    cst.setString(1, sID);
                    cst.execute();
                } else {
                    JOptionPane.showMessageDialog(null, "la tarjeta no existe");
                    return false;
                }
            } else {
                JOptionPane.showMessageDialog(null, "Error con los datos proporcionados ");
                return false;
            }


        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Problema con Eliminar Trabajador " + sqlex.getMessage());
            return false;
        }
        return true;
    }
    
    public static boolean eliminarDevolucion(String sID,String sIdProduto, String sCantidad, String sNVenta,String sIdCliente,String sTarjeta) {
        try {
            Connection cnn;
            CallableStatement cst;
            String[] sValores = {sID};
            if (ValidacionesGenerales.validarVacios(sValores)) {
                if (ValidacionesGenerales.validarExistencia(sID, "Devolucion")) {
                    cnn = Conexion.getConexion();
                    cst = cnn.prepareCall("call eliminarDevolucion (?,?,?,?,?,?)");
                    cst.setString(1, sID);
                    cst.setString(2, sIdProduto);
                    cst.setString(3, sCantidad);
                    cst.setString(4, sNVenta);
                    cst.setString(5, sIdCliente);
                    System.out.println("es "+sTarjeta);
                    if(sTarjeta.equals("null")){
                        System.out.println("es "+sTarjeta);
                        cst.setNull(6, Types.INTEGER);
                    }else{
                    cst.setString(6, sTarjeta);
                    }
                    cst.execute();
                } else {
                    JOptionPane.showMessageDialog(null, "la devolucion no existe");
                    return false;
                }
            } else {
                JOptionPane.showMessageDialog(null, "Error con los datos proporcionados ");
                return false;
            }


        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Problema con Eliminar Devolucion " + sqlex.getMessage());
            return false;
        }
        return true;
    }
    
    public static boolean eliminarPedido(String sRFC) {
        try {
            Connection cnn;
            CallableStatement cst;
            String[] sValores = {sRFC};
            if (ValidacionesGenerales.validarVacios(sValores)) {
                if (ValidacionesGenerales.validarExistencia(sRFC, "Pedidos")) {
                    cnn = Conexion.getConexion();
                    cst = cnn.prepareCall("call eliminarPedido (?)");
                    cst.setString(1, sRFC);
                    cst.execute();
                } else {
                    JOptionPane.showMessageDialog(null, "El pedido no existe");
                    return false;
                }
            } else {
                JOptionPane.showMessageDialog(null, "Error con los datos proporcionados ");
                return false;
            }


        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Problema con Eliminar Pedido " + sqlex.getMessage());
            return false;
        }
        return true;
    }
    
    public static boolean eliminarProveedor(String sID) {
        try {
            Connection cnn;
            CallableStatement cst;
            String[] sValores = {sID};
            if (ValidacionesGenerales.validarVacios(sValores)) {
                if (ValidacionesGenerales.validarExistencia(sID, "Proveedor")) {
                    cnn = Conexion.getConexion();
                    cst = cnn.prepareCall("call eliminarProveedor (?)");
                    cst.setString(1, sID);
                    cst.execute();
                } else {
                    JOptionPane.showMessageDialog(null, "El proveedor no existe");
                    return false;
                }
            } else {
                JOptionPane.showMessageDialog(null, "Error con los datos proporcionados ");
                return false;
            }
        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Problema con Eliminar Proveedor " + sqlex.getMessage());
            return false;
        }
        return true;
    }
    
    public static boolean eliminarProducto(String sID) {
        try {
            Connection cnn;
            CallableStatement cst;
            String[] sValores = {sID};
            if (ValidacionesGenerales.validarVacios(sValores)) {
                if (ValidacionesGenerales.validarExistencia(sID, "Producto")) {
                    cnn = Conexion.getConexion();
                    cst = cnn.prepareCall("call eliminarProducto (?)");
                    cst.setString(1, sID);
                    cst.execute();
                } else {
                    JOptionPane.showMessageDialog(null, "El producto no existe");
                    return false;
                }
            } else {
                JOptionPane.showMessageDialog(null, "Error con los datos proporcionados ");
                return false;
            }


        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Problema con Eliminar Producto " + sqlex.getMessage());
            return false;
        }
        return true;
    }
}
