package SQL;

import rojerusan.RSDateChooser;

import javax.swing.*;
import java.sql.*;

/**
 * @author Daniel Ochoa
 * <p>
 * Ochoa Fecha: 12/11/2021
 * Modificaciones: hice que el meotdo funcionara con el procedimiento almacenado
 */
public class ActualizarCargarDatos {

    public static void cargarDatos(String sIDCliente, int iConsulta, JComponent[] jcComponents, int[] iCampos) {
        Connection cnn;
        try {
            cnn = Conexion.getConexion();
            CallableStatement cst = cnn.prepareCall("call selectPantallasActualizaciones (?,?)");
            cst.setInt(1, iConsulta);
            boolean bValidarExistencia = false;
            if (iConsulta == 0) {
                bValidarExistencia = ValidacionesGenerales.validarExistencia(sIDCliente, "Cliente");
            } else if (iConsulta == 1) {
                bValidarExistencia = ValidacionesGenerales.validarExistencia(sIDCliente, "Trabajador");
            } else if (iConsulta == 2) {
                bValidarExistencia = ValidacionesGenerales.validarExistencia(sIDCliente, "Proveedor");
            } else if (iConsulta == 3) {
                bValidarExistencia = ValidacionesGenerales.validarExistencia(sIDCliente, "Producto");
            } else if (iConsulta == 4) {
                bValidarExistencia = ValidacionesGenerales.validarExistencia(sIDCliente, "Pedidos");
            }else if (iConsulta == 5) {
                bValidarExistencia = ValidacionesGenerales.validarExistencia(sIDCliente, "Venta");
            }
            System.out.println("cargardatos: " + bValidarExistencia);
            if (!bValidarExistencia) {
                ActualizarCargarDatos.limpiarCampos(jcComponents);
                ActualizarCargarDatos.bloquear(jcComponents);
                return;
            }

            cst.setString(2, sIDCliente);
            cst.execute();
            ResultSet rs = cst.getResultSet();
            boolean bControl = false;
            System.out.println(sIDCliente);
            while (rs.next()) {
                if (jcComponents.length == iCampos.length) {
                    for (int i = 0; i < jcComponents.length; i++) {
                        ActualizarCargarDatos.ponerValor(jcComponents[i], rs.getString(iCampos[i]));
                    }
                } else {
                    String[] sNombrePartes = ActualizarCargarDatos.obtenerNombrePartes(rs.getString(2));
                    JTextField j1 = (JTextField) jcComponents[0], j2 = (JTextField) jcComponents[1], j3 = (JTextField) jcComponents[2];
                    j1.setText(sNombrePartes[0]);
                    j2.setText(sNombrePartes[1]);
                    j3.setText(sNombrePartes[2]);
                    int iCounter = 1;
                    for (int i = 3; i < jcComponents.length; i++) {
                        ActualizarCargarDatos.ponerValor(jcComponents[i], rs.getString(iCampos[iCounter]));
                        iCounter++;
                    }
                }
                ActualizarCargarDatos.desbloquear(jcComponents);
                bControl = true;
                break;
            }
            if (!bControl) {
                ActualizarCargarDatos.limpiarCampos(jcComponents);
                ActualizarCargarDatos.bloquear(jcComponents);
            }
            cnn.close();
        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Problema ActualizarCargarDatos: CargarDatos " + sqlex.getMessage());
        }
    }

    private static String[] obtenerNombrePartes(String sNombreCompleto) {
        return sNombreCompleto.split(" ");
    }

    private static void ponerValor(JComponent jComponent, String sValor) {
        if (jComponent instanceof JTextField) {
            JTextField jt = (JTextField) jComponent;
            jt.setText(sValor);
        } else if (jComponent instanceof JLabel) {
            JLabel jt = (JLabel) jComponent;
            jt.setText(sValor);
        } else if (jComponent instanceof RSDateChooser) {
            RSDateChooser jt = (RSDateChooser) jComponent;
            jt.setTextoFecha(sValor);
        } else if (jComponent instanceof JSlider) {
            JSlider jt = (JSlider) jComponent;
            float fSalario = Float.parseFloat(sValor);
            jt.setValue((int) Math.round(fSalario));
        } else if (jComponent instanceof JComboBox) {
            JComboBox jt = (JComboBox) jComponent;
            jt.setSelectedItem(sValor);
        } else if (jComponent instanceof JSpinner) {
            JSpinner jt = (JSpinner) jComponent;
            float fSalario = Float.parseFloat(sValor);
            jt.setValue((int) Math.round(fSalario));
        }
    }

    private static void limpiarCampos(JComponent[] jcComponents) {
        for (int i = 0; i < jcComponents.length; i++) {
            if (jcComponents[i] instanceof JTextField) {
                JTextField jt = (JTextField) jcComponents[i];
                jt.setText("");
            } else if (jcComponents[i] instanceof RSDateChooser) {
                RSDateChooser jt = (RSDateChooser) jcComponents[i];
                jt.setTextoFecha("");
                jt.setVisible(false);
            } else if (jcComponents[i] instanceof JSlider) {
                JSlider jt = (JSlider) jcComponents[i];
                jt.setValue(jt.getMajorTickSpacing());
            } else if (jcComponents[i] instanceof JComboBox) {
                JComboBox jt = (JComboBox) jcComponents[i];
                jt.setEnabled(true);
            } else if (jcComponents[i] instanceof JSpinner) {
                JSpinner jt = (JSpinner) jcComponents[i];
                jt.setValue(jt.getValue());
            }
        }
    }

    private static void bloquear(JComponent[] jComponents) {
        for (JComponent jc : jComponents) {
            jc.setEnabled(false);
        }
    }

    private static void desbloquear(JComponent[] jComponents) {
        for (JComponent jc : jComponents) {
            jc.setEnabled(true);
            jc.setVisible(true);
        }
    }
}
