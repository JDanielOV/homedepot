package SQL;

import javax.swing.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
/**
 * @author Daniel Ochoa
 * <p>
 * Ochoa Fecha: 06/11/2021
 * Modificaciones: Clase creada para hacer la conexion a la base de datos
 */
public class Conexion {

    public static String Usuario = "UsuaAdmi";
    public static String Contraseña = "contrasena123";
    public static boolean Estado;
    public static String url;
    public static Connection con;

    public static Connection getConexion() {
        Estado = false;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            url = "jdbc:mysql://localhost:3306/HomeDepot";
            
        } catch (ClassNotFoundException e) {
            JOptionPane.showMessageDialog(null, "La conexion no ha sido establecida con el servidor:" + e.getMessage());
        }
        try {
            
            con = DriverManager.getConnection(url, Usuario, Contraseña);
            Estado = true;
            System.out.println(Conexion.Estado);

        } catch (SQLException e) {
            
            JOptionPane.showMessageDialog(null, "La conexion no ha sido establecida con el servidor:" + e.getMessage());
        }

        return con;

    }

}
