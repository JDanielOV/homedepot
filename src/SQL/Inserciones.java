package SQL;

import javax.swing.*;
import java.sql.*;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 * Ochoa Fecha: 19/11/2021
 * Modificaciones: compuse agregarVenta y producto
 **/
public class Inserciones {

    public static boolean insertarCliente(String sNombre, String sApellidoP, String sApellidoM, String sCorreo, String sTelefono, String sFecha) {
        try {
            Connection cnn;
            CallableStatement cst;
            String[] sTextos = {sNombre, sApellidoM, sApellidoP, sTelefono, sFecha};
            boolean bVerVacios = ValidacionesGenerales.validarVacios(sTextos);
            boolean bValidarNombre = ValidacionesGenerales.validarCadenasLetras(sNombre.concat(sApellidoP).concat(sApellidoM));
            boolean bValidarTelefono = ValidacionesGenerales.validarCadenaTelefono(sTelefono);
            boolean bValidarFechaNacimiento = ValidacionesGenerales.validarFechaNacimiento(sFecha, "Cliente");
            boolean bValidarCorreo = ValidacionesGenerales.validarCorreo(sCorreo);
            if (bVerVacios && bValidarNombre && bValidarTelefono && bValidarFechaNacimiento) {
                if ((!sCorreo.isBlank() && bValidarCorreo) || sCorreo.isBlank()) {
                    String sNombreCompleto = sNombre.concat(" ").concat(sApellidoP).concat(" ").concat(sApellidoM);
                    boolean bValidarExistencia = ValidacionesGenerales.validarExistencia(sNombreCompleto, "ClienteNombre", sFecha);
                    if (!bValidarExistencia) {
                        cnn = Conexion.getConexion();
                        cst = cnn.prepareCall("call agregarCliente (?,?,?,?,?,?)");
                        cst.setString(1, sNombre);
                        cst.setString(2, sApellidoP);
                        cst.setString(3, sApellidoM);
                        cst.setString(4, sCorreo);
                        cst.setString(5, sTelefono);
                        cst.setString(6, sFecha);
                        cst.execute();
                    } else {
                        JOptionPane.showMessageDialog(null, "El cliente ya existe ");
                        return false;
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Error con los datos proporcionados ");
                    return false;
                }
            } else {
                JOptionPane.showMessageDialog(null, "Error con los datos proporcionados ");
                return false;
            }
        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Problema con insertar usuario " + sqlex.getMessage());
            return false;
        }
        return true;
    }

    public static boolean insertarTrabajador(String sNombre, String sApellidoP, String sApellidoM, String sCorreo, String sTelefono, String sRfc, String sFecha, String sDireccion, String sCargo, String sSueldo, String sContraseña) {
        try {
            Connection cnn;
            CallableStatement cst;
            String[] sTextos = {sNombre, sApellidoM, sApellidoP, sTelefono, sRfc, sFecha, sDireccion, sCargo, sSueldo, sContraseña};
            boolean bVerVacios = ValidacionesGenerales.validarVacios(sTextos);
            boolean bValidarNombre = ValidacionesGenerales.validarCadenasLetras(sNombre.concat(sApellidoP).concat(sApellidoM));
            boolean bValidarTelefono = ValidacionesGenerales.validarCadenaTelefono(sTelefono);
            boolean bValidarFechaNacimiento = ValidacionesGenerales.validarFechaNacimiento(sFecha, "Trabajador");
            boolean bValidarCorreo = ValidacionesGenerales.validarCorreo(sCorreo);
            if (bVerVacios && bValidarNombre && bValidarTelefono && bValidarFechaNacimiento) {
                if ((!sCorreo.isBlank() && bValidarCorreo) || sCorreo.isBlank()) {
                    String sNombreCompleto = sNombre.concat(" ").concat(sApellidoP).concat(" ").concat(sApellidoM);
                    System.out.println(sFecha);
                    boolean bValidarExistenciaRFC = ValidacionesGenerales.validarExistencia(sRfc, "Trabajador");
                    boolean bValidarExistenciaRFCBorrado = ValidacionesGenerales.estaBorrado(sRfc, "TrabajadorTodos");
                    boolean bValidarExistencia = ValidacionesGenerales.validarExistenciaTrabajador(sRfc, sNombreCompleto, sFecha);
                    ArrayList<String> aPersonasMismoNombre = ValidacionesGenerales.trabajadoresNombreFechaExistentes(sNombreCompleto, sFecha);
                    if (!bValidarExistenciaRFC || bValidarExistenciaRFCBorrado) {
                        System.out.println("If para validar el rfc");
                        System.out.println(aPersonasMismoNombre.size() + " cuantos se llaman " + sNombreCompleto + "y cumplen el " + sFecha);
                        if (aPersonasMismoNombre.size() == 0) {
                            System.out.println("cuanrtas personas se llaman igual y cumplen el mismo dia");
                            if (!bValidarExistencia) {
                                System.out.println("veo si existe el rfc-nombre-fecha");
                                boolean bValidarDespedido = ValidacionesGenerales.estaBorradoTrabajador(sRfc, sNombreCompleto, sFecha);
                                cnn = Conexion.getConexion();
                                if (bValidarDespedido || bValidarExistenciaRFCBorrado) {
                                    cst = cnn.prepareCall("call contratarDeNuevoTrabajador (?,?,?,?,?,?,?,?,?,?,?)");
                                    cst.setString(1, sRfc);
                                    cst.setString(2, sNombre);
                                    cst.setString(3, sApellidoP);
                                    cst.setString(4, sApellidoM);
                                    cst.setString(5, sCorreo);
                                    cst.setString(6, sTelefono);
                                    cst.setString(7, sFecha);
                                    cst.setString(8, sDireccion);
                                    cst.setString(9, sCargo);
                                    cst.setString(10, sSueldo);
                                    cst.setString(11, sContraseña);
                                } else {
                                    cst = cnn.prepareCall("call agregarTrabajador (?,?,?,?,?,?,?,?,?,?,?,?)");
                                    cst.setString(1, sNombre);
                                    cst.setString(2, sApellidoP);
                                    cst.setString(3, sApellidoM);
                                    cst.setString(4, sCorreo);
                                    cst.setString(5, sTelefono);
                                    cst.setString(6, sRfc);
                                    cst.setString(7, sFecha);
                                    cst.setString(8, sDireccion);
                                    cst.setString(9, sCargo);
                                    cst.setString(10, sSueldo);
                                    cst.setString(11, sContraseña);
                                    cst.setString(12, "1");
                                }
                                cst.execute();
                            } else {
                                JOptionPane.showMessageDialog(null, "El Trabajador ya existe ");
                                return false;
                            }
                        } else {
                            JOptionPane.showMessageDialog(null, "El Trabajador ya existe ");
                            return false;
                        }
                    } else {
                        JOptionPane.showMessageDialog(null, "El RFC ya existe ");
                        return false;
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Error con los datos proporcionados ");
                    return false;
                }
            } else {
                JOptionPane.showMessageDialog(null, "Error con los datos proporcionados ");
                return false;
            }
        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Problema con insertar usuario " + sqlex.getMessage());
            return false;
        }
        return true;
    }

    public static boolean insertarProducto(String sNombre, String sPrecio, String sDepartamento, String sCantidad, String sProveedor) {
        try {
            Connection cnn;
            CallableStatement cst;
            String[] sTextos = {sNombre, sPrecio, sDepartamento, sCantidad, sProveedor};
            boolean bVerVacios = ValidacionesGenerales.validarVacios(sTextos);
            boolean bValidarNombre = ValidacionesGenerales.validarCadenasLetrasNumeros(sNombre);
            boolean bValidarProveedor = ValidacionesGenerales.validarNumero(sProveedor);
            System.out.println(bVerVacios + " " + bValidarNombre + " " + bValidarProveedor);
            if (bVerVacios && bValidarNombre && bValidarProveedor) {
                boolean bValidarExistencia = ValidacionesGenerales.validarExistenciaProducto(sNombre, sProveedor);
                if (bValidarExistencia) {
                    JOptionPane.showMessageDialog(null, "El producto ya existe ");
                    return false;
                } else {
                    boolean bValidarExistenciaProveedor = ValidacionesGenerales.validarExistencia(sProveedor, "Proveedor");
                    if (bValidarExistenciaProveedor) {
                        boolean bValidarProductoBorrado = ValidacionesGenerales.estaBorrado(sNombre, "ProductoNombreCualquiera");
                        if (!bValidarProductoBorrado) {
                            cnn = Conexion.getConexion();
                            cst = cnn.prepareCall("call agregarProducto (?,?,?,?,?,?)");
                            cst.setString(1, sNombre);
                            cst.setString(2, sPrecio);
                            cst.setString(3, sProveedor);
                            cst.setString(4, sDepartamento);
                            cst.setString(5, sCantidad);
                            cst.setBoolean(6, true);
                            cst.execute();
                            return true;
                        } else if (bValidarProductoBorrado) {
                            cnn = Conexion.getConexion();
                            cst = cnn.prepareCall("call actualizarProducto (?,?,?,?,?,?,?)");
                            cst.setString(1, CargarDatos.obtenerRegistros(sNombre, "ProductoNombreCualquiera").get(0).get(0));
                            cst.setString(2, sNombre);
                            cst.setString(3, sPrecio);
                            cst.setString(4, sProveedor);
                            cst.setString(5, sDepartamento);
                            cst.setString(6, sCantidad);
                            cst.setBoolean(7, true);
                            cst.execute();
                            return true;
                        }
                    } else {
                        JOptionPane.showMessageDialog(null, "El proveedor no existe");
                        return false;
                    }
                }
            } else {
                JOptionPane.showMessageDialog(null, "Error con los datos proporcionados ");
                return false;
            }
        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Problema con insertar producto " + sqlex.getMessage());
            return false;
        }
        return true;
    }

    public static boolean insertarTarjeta(String sId, String sRFC, String sFechaAlta, String sFechaExpiracion) {
        try {
            Connection cnn;
            CallableStatement cst;
            String[] sTextos = {sId};
            boolean bVerVacios = ValidacionesGenerales.validarVacios(sTextos);
            boolean bValidarIDCliente = ValidacionesGenerales.validarNumero(sId);
            boolean bValidarExistenciaCliente = ValidacionesGenerales.validarExistencia(sId, "Cliente", 1);
            if (bValidarIDCliente && bVerVacios) {
                if (bValidarExistenciaCliente) {
                    cnn = Conexion.getConexion();
                    cst = cnn.prepareCall("call agregarTarjeta (?,?,?,?,?,?)");
                    cst.setString(1, sId);
                    cst.setString(2, sRFC);
                    cst.setString(3, sFechaAlta);
                    cst.setString(4, sFechaExpiracion);
                    cst.setString(5, "0");
                    cst.setBoolean(6, true);
                    cst.execute();
                } else {
                    JOptionPane.showMessageDialog(null, "El cliente no existe ");
                    return false;
                }
            } else {
                JOptionPane.showMessageDialog(null, "Error con los datos proporcionados ");
                return false;
            }
        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Problema con insertar usuario " + sqlex.getMessage());
            return false;
        }
        return true;
    }

    public static boolean agregarVenta(String sIdCliente, String sRFC, String sFecha, String sVentaTotal, String sIdTarjeta, String sPuntosGastados, String sEfectivo, String sCambio) {
        try {
            Connection cnn;
            CallableStatement cst;
            String[] sTextos = {sIdCliente, sRFC};
            boolean bVerVacios = ValidacionesGenerales.validarVacios(sTextos);

            boolean bValidarCliente = (ValidacionesGenerales.validarNumero(sIdCliente) &&
                    ValidacionesGenerales.validarExistencia(sIdCliente, "Cliente"));

            boolean bValidarTarjeta = (ValidacionesGenerales.validarNumero(sIdTarjeta) &&
                    ValidacionesGenerales.validarExistencia(sIdTarjeta, "Tarjetas"));

            boolean bValidarPertenencia = (bValidarTarjeta
                    && bValidarCliente &&
                    ValidacionesGenerales.validarPertenenciaTarjeta(sIdTarjeta, sIdCliente, "Tarjetas"));

            int iPuntosTarjeta = (bValidarPertenencia) ? Integer.parseInt(CargarDatos.puntosTajerta(sIdTarjeta)) : 0;
            int iPuntosGastados = Integer.parseInt(sPuntosGastados);
            int iTotalAPagar = (int) (Math.ceil(Double.parseDouble(sVentaTotal)));
            double dEfectivoRecibido = Double.parseDouble(sEfectivo);
            double dCambio = (Double.parseDouble(sCambio));
            double dTotalAPagar = (Double.parseDouble(sVentaTotal));

            String sProcedimiento = "agregarVenta";
            System.out.println(bVerVacios + " " + bValidarCliente + " " + sIdTarjeta.isBlank() + " " + bValidarPertenencia);
            if ((bVerVacios && bValidarCliente && sIdTarjeta.isBlank()) || (bValidarPertenencia && bVerVacios)) {
                if (sIdTarjeta.isBlank()) {
                    sIdTarjeta = null;
                    System.out.println("No hay tarjeta selecionada");
                    if (iPuntosGastados > 0) {
                        JOptionPane.showMessageDialog(null, "No puede usar puntos si no seleciona una tarjeta");
                        return false;
                    }
                }
                if (bValidarPertenencia) {
                    System.out.println(iPuntosTarjeta);
                    if (iPuntosTarjeta < iPuntosGastados) {
                        JOptionPane.showMessageDialog(null, "No tiene puntos suficientes ");
                        return false;
                    }
                    if (iPuntosGastados > iTotalAPagar) {
                        dCambio = 0;
                        dCambio += dEfectivoRecibido;
                        iPuntosGastados = iTotalAPagar;
                    }
                }

                System.out.println(iPuntosGastados + "#" + dCambio);
                if ((double) (iPuntosGastados) + dEfectivoRecibido < dTotalAPagar) {
                    JOptionPane.showMessageDialog(null, "Los puntos mas el efectio no son suficientes");
                    return false;
                }

                cnn = Conexion.getConexion();
                cst = cnn.prepareCall("call " + sProcedimiento + " (?,?,?,?,?,?,?,?)");
                cst.setString(1, sIdCliente);
                cst.setString(2, sRFC);
                cst.setString(3, sFecha);
                cst.setString(4, sVentaTotal);
                cst.setString(5, sIdTarjeta);
                cst.setString(6, String.valueOf(iPuntosGastados));
                cst.setString(7, sEfectivo);
                cst.setString(8, String.valueOf(dCambio));
                cst.execute();
                cst.close();
                cnn.close();
                DecimalFormat df = new DecimalFormat("#.00");
                JOptionPane.showMessageDialog(null,
                        "Total a pagar: " + sVentaTotal + "\n" +
                                "Efectivo Recibido: " + sEfectivo + "\n" +
                                "Puntos Usados: " + df.format(iPuntosGastados) + "\n" +
                                "Cambio: " + df.format(dCambio));
            } else {
                JOptionPane.showMessageDialog(null, "Error con los datos proporcionados ");
                return false;
            }
        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Problema con agregarVenta " + sqlex.getMessage());
            return false;
        }
        return true;
    }

    public static boolean agregarFacturaVenta(String sIdProducto, String sIdVentaPrecio, String sCantidadPiezas) {
        try {
            Connection cnn;
            CallableStatement cst;
            cnn = Conexion.getConexion();
            cst = cnn.prepareCall("call agregarFacturaVenta (?,?,?,?)");
            cst.setString(1, sIdProducto);
            cst.setString(2, sIdVentaPrecio);
            cst.setString(3, sCantidadPiezas);
            cst.setString(4, "1");
            cst.execute();
        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Problema con insertar usuario " + sqlex.getMessage());
            return false;
        }
        return true;
    }

    public static boolean insertarDevolucion(String sVentaNum, String sIdCliente, String sIdProducto, String sFecha, String sMotivo, String sCantidad, String sTarjeta) {
        try {
            Connection cnn;
            CallableStatement cst;
            String[] sTextos = {sVentaNum, sIdCliente, sIdProducto, sFecha};
            boolean bVerVacios = ValidacionesGenerales.validarVacios(sTextos);

            boolean bValidarCliente = (ValidacionesGenerales.validarNumero(sIdCliente) &&
                    ValidacionesGenerales.validarExistencia(sIdCliente, "Cliente"));

            boolean bValidarVentaNum = (ValidacionesGenerales.validarNumero(sVentaNum) &&
                    ValidacionesGenerales.validarExistencia(sVentaNum, "Venta"));

            boolean bValidarProducto = (ValidacionesGenerales.validarNumero(sIdProducto)/* &&
                    ValidacionesGenerales.validarExistencia(sIdProducto, "Producto")*/
                    && ValidacionesGenerales.validarPertenenciaDevolucioProducto(sIdProducto, sVentaNum, "FacturaVenta"));

            boolean bValidarTarjeta = (ValidacionesGenerales.validarNumero(sTarjeta) &&
                    ValidacionesGenerales.validarExistencia(sTarjeta, "Tarjetas"));

            boolean bValidarCantidad = (ValidacionesGenerales.validarCantidadDevolver(sVentaNum, sCantidad));
            
            /*boolean bValidarPertenencia = (bValidarTarjeta
                    && bValidarCliente &&
                    ValidacionesGenerales.validarPertenenciaTarjeta(sTarjeta, sIdCliente, "Tarjetas") &&
                    ValidacionesGenerales.validarPertenenciaDevolucioTarjeta(sTarjeta,sVentaNum, "Venta"));
            String sProcedimiento = "";*/
            if ((bVerVacios && bValidarCliente)) {
                if (bValidarVentaNum) {
                    if (bValidarProducto) {
                        if (bValidarCantidad) {
                            if (sTarjeta.isBlank()) {
                                sTarjeta = null;
                            }
                            cnn = Conexion.getConexion();
                            cst = cnn.prepareCall("call agregarDevolucion (?,?,?,?,?,?,?,?)");
                            cst.setString(1, sVentaNum);
                            cst.setString(2, sIdCliente);
                            cst.setString(3, sIdProducto);
                            cst.setString(4, sFecha);
                            cst.setString(5, sMotivo);
                            cst.setString(6, sCantidad);
                            cst.setString(7, sTarjeta);
                            cst.setString(8, "1");
                            cst.execute();

                        } else {
                            JOptionPane.showMessageDialog(null, "Error con la cantidad a devolver");
                            return false;
                        }
                    } else {
                        JOptionPane.showMessageDialog(null, "Error con el ID Producto");
                        return false;
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Error con el numero de venta");
                    return false;
                }
            } else {
                JOptionPane.showMessageDialog(null, "Error con los datos proporcionados ");
                return false;
            }
        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Problema con insertar devolucion " + sqlex.getMessage());
            return false;
        }
        return true;
    }

    public static boolean agregarPedido(String NtrabRFC, String NproveedorId, String NpedFecha, String NpedFechaEntrega, String NpedTotal) {
        try {
            Connection cnn;
            CallableStatement cst;
            String[] sTextos = {NtrabRFC, NproveedorId, NpedFechaEntrega};
            boolean bVerVacios = ValidacionesGenerales.validarVacios(sTextos);

            boolean bValidarTrabajador = (ValidacionesGenerales.validarNumero(NtrabRFC) &&
                    ValidacionesGenerales.validarExistencia(NtrabRFC, "Trabajador"));

            boolean bValidarProveedor = (ValidacionesGenerales.validarNumero(NproveedorId) &&
                    ValidacionesGenerales.validarExistencia(NproveedorId, "Proveedor"));

            boolean bValidarFechaEntrega = (ValidacionesGenerales.validarFechaPedEntrega(NpedFechaEntrega));
            if (bVerVacios && bValidarTrabajador && bValidarProveedor) {
                if (bValidarFechaEntrega) {
                    cnn = Conexion.getConexion();
                    cst = cnn.prepareCall("call agregarPedido (?,?,?,?,?,?,?)");
                    cst.setString(1, NtrabRFC);
                    cst.setString(2, NproveedorId);
                    cst.setString(3, NpedFecha);
                    cst.setString(4, NpedFechaEntrega);
                    cst.setString(5, NpedTotal);
                    cst.setString(6, "1");
                    cst.setString(7, "0");
                    cst.execute();
                } else {
                    JOptionPane.showMessageDialog(null, "La fecha de entrega no respeta los limites  ");
                    return false;
                }
            } else {
                JOptionPane.showMessageDialog(null, "Error con los datos proporcionados ");
                return false;
            }
        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Problema con insertar pedido " + sqlex.getMessage());
            return false;
        }
        return true;
    }

    public static boolean agregarFacturaPedido(String sIdProducto, String sPedPrecio, String sCantidadPiezas) {
        try {
            boolean bValidarCantidad = ValidacionesGenerales.validarCantidadPedido(sIdProducto, sCantidadPiezas);
            if (bValidarCantidad) {
                Connection cnn;
                CallableStatement cst;
                cnn = Conexion.getConexion();
                cst = cnn.prepareCall("call agregarFacturaPedido (?,?,?,?)");
                cst.setString(1, sIdProducto);
                cst.setString(2, sPedPrecio);
                cst.setString(3, sCantidadPiezas);
                cst.setString(4, "1");
                cst.execute();
            } else {
                JOptionPane.showMessageDialog(null, "La cantidad debe ser mayor a 0");
                return false;
            }
        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Problema con insertar pedido " + sqlex.getMessage());
            return false;
        }
        return true;
    }

    public static boolean agregarFacturaPedidoActualizar(String idPed, String sIdProducto, String sPedPrecio, String sCantidadPiezas) {
        try {
            boolean bValidarCantidad = ValidacionesGenerales.validarCantidadPedido(sIdProducto, sCantidadPiezas);
            if (bValidarCantidad) {
                Connection cnn;
                CallableStatement cst;
                cnn = Conexion.getConexion();
                cst = cnn.prepareCall("call agregarFacturaPedidoActualizar (?,?,?,?,?)");
                cst.setString(1, idPed);
                cst.setString(2, sIdProducto);
                cst.setString(3, sPedPrecio);
                cst.setString(4, sCantidadPiezas);
                cst.setString(5, "1");
                cst.execute();
            } else {
                JOptionPane.showMessageDialog(null, "La cantidad debe ser mayor a 0");
                return false;
            }
        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Problema con insertar pedido1 " + sqlex.getMessage());
            return false;
        }
        return true;
    }

    public static boolean agregarProveedores(String sNombre, String sApellidoP, String sApellidoM, String sCorreo, String sTelefono, String sProveedorEmpre) {
        try {
            Connection cnn;
            CallableStatement cst;
            String[] sTextos = {sNombre, sApellidoP, sApellidoM, sCorreo, sTelefono, sProveedorEmpre};
            boolean bVerVacios = ValidacionesGenerales.validarVacios(sTextos);
            boolean bValidarNombre = ValidacionesGenerales.validarCadenasLetras(sNombre.concat(sApellidoP).concat(sApellidoM));
            boolean bValidarTelefono = ValidacionesGenerales.validarCadenaTelefono(sTelefono);
            boolean bValidarCorreo = ValidacionesGenerales.validarCorreo(sCorreo);
            //boolean bvalidarExistencia = ValidacionesGenerales.validarExistenciaEmpre(sParametros[9], sNombreCompleto, sParametros[5], "ProveedorEmpresa", 5);
            if (bVerVacios && bValidarNombre && bValidarTelefono) {
                if ((!sCorreo.isBlank() && bValidarCorreo) || sCorreo.isBlank()) {
                    String sNombreCompleto = sNombre.concat(" ").concat(sApellidoP).concat(" ").concat(sApellidoM);
                    boolean bValidarExistencia = ValidacionesGenerales.validarExistencia(sNombreCompleto, "ProveedorNombre", sProveedorEmpre);
                    if ((!bValidarExistencia)) {
                        cnn = Conexion.getConexion();
                        cst = cnn.prepareCall("call agregarProveedor (?,?,?,?,?,?,?)");
                        cst.setString(1, sNombre);
                        cst.setString(2, sApellidoP);
                        cst.setString(3, sApellidoM);
                        cst.setString(4, sCorreo);
                        cst.setString(5, sTelefono);
                        cst.setString(6, sProveedorEmpre);
                        cst.setString(7, "1");
                        cst.execute();
                    } else {
                        JOptionPane.showMessageDialog(null, "El Proveedor ya existe ");
                        return false;
                    }

                } else {
                    JOptionPane.showMessageDialog(null, "Error con los datos proporcionados ");
                    return false;
                }
            } else {
                JOptionPane.showMessageDialog(null, "Error con los datos proporcionados ");
                return false;
            }
        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Problema con insertar usuario " + sqlex.getMessage());
            return false;
        }
        return true;
    }
}
