/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SQL;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 * @author cristina 14-11-2021
 * Ochoa Fecha: 21/11/2021
 * Modificaciones: crre el metodo trabajadoresNombreFecha para ver quines don guales
 */
public class ValidacionesGenerales {

    protected static boolean validarCadenasLetras(String sCadena) {
        String[] sRegex = sCadena.split("[a-zA-Z\\s]+");
        return sRegex.length == 0;
    }

    protected static boolean validarCadenasLetrasNumeros(String sCadena) {
        String[] sRegex = sCadena.split("[A-Za-z0-9]+");
        return sRegex.length == 0;
    }

    protected static boolean validarCadenaTelefono(String sTelofono) {
        String[] sRegex = sTelofono.split("[0-9\\-]+");
        return sRegex.length == 0;
    }

    protected static boolean validarCorreo(String sCorreo) {
        String[] sRegex = sCorreo.split("[\\w-]+(\\.[\\w-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})");
        return sRegex.length == 0;
    }

    protected static boolean validarVacios(String[] sTextos) {
        for (String sTexto : sTextos) {
            if (sTexto.isBlank())
                return false;
        }
        return true;
    }

    /*-----------------------------Para Clientes y Tabajadores---------------------------*/
    protected static boolean validarFechaNacimiento(String fecha, String personaEntidad) {
        if (!fecha.isBlank()) {
            int año = Integer.parseInt(fecha.substring(0, 4));
            int mes = Integer.parseInt(fecha.substring(5, 7));
            int dia = Integer.parseInt(fecha.substring(8));
            LocalDate fechaHoy = LocalDate.now();
            LocalDate limite = fechaHoy.minusYears(18);
            LocalDate limiteCliente = fechaHoy.minusYears(10);
            LocalDate fechaNac = LocalDate.of(año, mes, dia);
            LocalDate minimo = LocalDate.of(1921, 1, 1);
            if (personaEntidad.equals("Trabajador")) {
                if (fechaNac.isBefore(limite) && fechaNac.isAfter(minimo)) {
                    return true;
                }
            } else if (personaEntidad.equals("Cliente")) {
                if (fechaNac.isBefore(limiteCliente) && fechaNac.isAfter(minimo)) {
                    return true;
                }
            }
        }
        return false;
    }


    /*-----------------------------Para Clientes y Tabajadores---------------------------*/
    protected static boolean validarExistencia(String sNombre, String sVista, String sFechaNacimiento) {
        try {
            Connection cnn;
            cnn = Conexion.getConexion();
            CallableStatement cst = cnn.prepareCall("call selectBuscar (?,?)");
            cst.setString(1, sNombre);
            cst.setString(2, sVista);
            cst.execute();
            ResultSet rs = cst.getResultSet();
            while (rs.next()) {
                String sFechaSQL = rs.getString(5);
                if (sFechaSQL.equals(sFechaNacimiento))
                    return true;
            }
        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Problema con la base de datos " + sqlex.getMessage());
        }
        return false;
    }

    protected static boolean validarExistenciaNombre(String sNombre, String sVista) {
        try {
            Connection cnn;
            cnn = Conexion.getConexion();
            CallableStatement cst = cnn.prepareCall("call selectBuscar (?,?)");
            cst.setString(1, sNombre);
            cst.setString(2, sVista);
            cst.execute();
            ResultSet rs = cst.getResultSet();
            while (rs.next()) {
                String sNombreSQL = rs.getString(2);
                if (sNombreSQL.equals(sNombre))
                    return true;
            }
        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Problema con la base de datos " + sqlex.getMessage());
        }
        return false;
    }

    protected static boolean validarExistenciaCont(String sId, String sNContrasena, String sVista) {
        try {
            Connection cnn;
            cnn = Conexion.getConexion();
            CallableStatement cst = cnn.prepareCall("call selectBuscar (?,?)");
            cst.setString(1, sId);
            cst.setString(2, sVista);
            cst.execute();
            ResultSet rs = cst.getResultSet();
            while (rs.next()) {
                String sContrasena = rs.getString(2);
                System.out.println(sContrasena + "#" + sNContrasena);
                if (sContrasena.equals(sNContrasena) && sId.equalsIgnoreCase(rs.getString(1))) {
                    System.out.println(sContrasena + " " + sNContrasena);
                    return true;
                }
            }
        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Problema con la base de datos " + sqlex.getMessage());
        }
        return false;
    }

    protected static boolean validarExistencia(String sValor, String sVista, int iColumna) {
        try {
            Connection cnn;
            cnn = Conexion.getConexion();
            CallableStatement cst = cnn.prepareCall("call selectBuscar (?,?)");
            cst.setString(1, sValor);
            cst.setString(2, sVista);
            cst.execute();
            ResultSet rs = cst.getResultSet();
            while (rs.next()) {
                String sNombreSQL = rs.getString(iColumna);
                if (sNombreSQL.equals(sValor))
                    return true;
            }
        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Problema con la base de datos " + sqlex.getMessage());
        }
        return false;
    }

    public static boolean validarNumero(String sNumero) {
        String[] sRegex = sNumero.split("[0-9]+");
        return sRegex.length == 0;
    }


    public static boolean validarExistencia(String idEntidad, String sVista) {
        try {
            Connection cnn;
            cnn = Conexion.getConexion();
            CallableStatement cst = cnn.prepareCall("call selectBuscar (?,?)");
            cst.setString(1, idEntidad);
            cst.setString(2, sVista);
            cst.execute();
            ResultSet rs = cst.getResultSet();
            while (rs.next()) {
                if (!(sVista.equals("Cliente") || sVista.equals("ClienteNombre")
                        || sVista.equals("Venta"))) {
                    if (rs.getString("Activo").equals("1")) {
                        cst.close();
                        cnn.close();
                        return true;
                    }
                } else {
                    cst.close();
                    cnn.close();
                    return true;
                }
            }
        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Problema con la base de datos " + sqlex.getMessage());
        }
        return false;
    }

    public static boolean validarPertenenciaTarjeta(String sIdTarjeta, String sIdCliente, String sVista) {
        try {
            Connection cnn;
            cnn = Conexion.getConexion();
            CallableStatement cst = cnn.prepareCall("call selectBuscar (?,?)");
            cst.setString(1, sIdTarjeta);
            cst.setString(2, sVista);
            cst.execute();
            ResultSet rs = cst.getResultSet();
            while (rs.next()) {
                String sIdClienteSQL = rs.getString(2);
                if (sIdClienteSQL.equals(sIdCliente))
                    return true;
            }
        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Problema con la base de datos " + sqlex.getMessage());
        }
        return false;
    }

    protected static boolean validarCantidadDevolver(String id, String cantidad) {
        try {
            int cant = Integer.parseInt(cantidad);
            Connection cnn;
            cnn = Conexion.getConexion();
            CallableStatement cst = cnn.prepareCall("call selectBuscar (?,?)");
            cst.setString(1, id);
            cst.setString(2, "FacturaVenta");
            cst.execute();
            ResultSet rs = cst.getResultSet();
            while (rs.next()) {
                String sNombreSQL = rs.getString(4);
                int cantSQL = Integer.parseInt(sNombreSQL);
                if (cantSQL == 0) {
                    return false;
                } else if (cant != 0 && cantSQL >= cant) {
                    return true;
                }
            }
        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Problema con la base de datos " + sqlex.getMessage());
        }
        return false;
    }

    protected static boolean validarFechaPedEntrega(String fecha) {
        if (!fecha.isBlank()) {
            int año = Integer.parseInt(fecha.substring(0, 4));
            int mes = Integer.parseInt(fecha.substring(5, 7));
            int dia = Integer.parseInt(fecha.substring(8));
            LocalDate fechaHoy = LocalDate.now();
            LocalDate limite = fechaHoy.plusYears(1);
            LocalDate fechaPed = LocalDate.of(año, mes, dia);
            if (fechaPed.isAfter(fechaHoy) && fechaPed.isBefore(limite)) {
                return true;
            }
        }
        return false;
    }

    public static boolean estaBorrado(String idEntidad, String sVista) {
        try {
            Connection cnn;
            cnn = Conexion.getConexion();
            CallableStatement cst = cnn.prepareCall("call selectBuscar (?,?)");
            cst.setString(1, idEntidad);
            cst.setString(2, sVista);
            cst.execute();
            ResultSet rs = cst.getResultSet();
            while (rs.next()) {
                if (rs.getString("Activo").equals("0")) {
                    return true;
                }
            }
        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Problema con la base de datos (estaBorrado)" + sqlex.getMessage());
        }
        return false;
    }

    public static boolean validarPertenenciaDevolucioTarjeta(String sIdTarjeta, String sVentaNum, String sVista) {
        try {
            Connection cnn;
            cnn = Conexion.getConexion();
            CallableStatement cst = cnn.prepareCall("call selectBuscar (?,?)");
            cst.setString(1, sVentaNum);
            cst.setString(2, sVista);
            cst.execute();
            ResultSet rs = cst.getResultSet();
            while (rs.next()) {
                String sIdTarjetaSQL = rs.getString(6);
                if (sIdTarjetaSQL.equals(sIdTarjeta))
                    return true;
            }
        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Problema con la base de datos " + sqlex.getMessage());
        }
        return false;
    }

    public static boolean validarPertenenciaDevolucioProducto(String sIdProducto, String sVentaNum, String sVista) {
        try {
            Connection cnn;
            cnn = Conexion.getConexion();
            CallableStatement cst = cnn.prepareCall("call selectBuscar (?,?)");
            cst.setString(1, sVentaNum);
            cst.setString(2, sVista);
            cst.execute();
            ResultSet rs = cst.getResultSet();
            while (rs.next()) {
                String sIdProductoSQL = rs.getString(5);
                if (sIdProductoSQL.equals(sIdProducto))
                    return true;
            }
        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Problema con la base de datos " + sqlex.getMessage());
        }
        return false;
    }

    public static boolean validarProveedorPedido(String sIdProveedor, String sIdProducto, String sVista) {
        try {
            Connection cnn;
            cnn = Conexion.getConexion();
            CallableStatement cst = cnn.prepareCall("call selectBuscar (?,?)");
            cst.setString(1, sIdProducto);
            cst.setString(2, sVista);
            cst.execute();
            ResultSet rs = cst.getResultSet();
            while (rs.next()) {
                String sIdProveedorSQL = rs.getString(6);
                if (sIdProveedorSQL.equals(sIdProveedor))
                    return true;
            }
        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Problema con la base de datos " + sqlex.getMessage());
        }
        return false;
    }


    protected static ArrayList<String> trabajadoresNombreFecha(String sNombre, String sFecha) {
        ArrayList<String> aRFCs = new ArrayList<>();
        try {
            Connection cnn;
            cnn = Conexion.getConexion();
            CallableStatement cst = cnn.prepareCall("call TrabajdorFechaNombre (?,?)");
            cst.setString(1, sNombre);
            cst.setString(2, sFecha);
            cst.execute();
            ResultSet rs = cst.getResultSet();
            while (rs.next()) {
                aRFCs.add(rs.getString(1).toLowerCase());
            }
        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Problema con la base de datos " + sqlex.getMessage());
        }
        return aRFCs;
    }

    protected static boolean validarExistenciaEmpre(String id, String sNombre, String sValor, String sVista, int iColumna) {
        try {
            Connection cnn;
            cnn = Conexion.getConexion();
            CallableStatement cst = cnn.prepareCall("call selectBuscar (?,?)");
            cst.setString(1, sValor);
            cst.setString(2, sVista);
            cst.execute();
            ResultSet rs = cst.getResultSet();
            String sID = "";
            while (rs.next()) {
                String sEmpresaSQL = rs.getString(iColumna);
                String sNombreSQL = rs.getString(2);
                sID = rs.getString(1);
                if (!sID.equals(id)) {
                    if (sEmpresaSQL.equalsIgnoreCase(sValor))
                        return false;
                }
            }
        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Problema con la base de datos " + sqlex.getMessage());
        }
        System.out.println("Es el mismo");
        return true;
    }

    protected static boolean validarFechaPedEntregaActualizacion(String fecha) {
        if (!fecha.isBlank()) {
            int año = Integer.parseInt(fecha.substring(0, 4));
            int mes = Integer.parseInt(fecha.substring(5, 7));
            int dia = Integer.parseInt(fecha.substring(8));
            LocalDate fechaHoy = LocalDate.now();
            LocalDate limite = fechaHoy.plusYears(1);
            LocalDate fechaPed = LocalDate.of(año, mes, dia);
            if (fechaPed.isAfter(fechaHoy) && fechaPed.isBefore(limite) || fechaPed.isEqual(fechaHoy)) {
                return true;
            }
        }
        return false;
    }

    public static boolean validarExistenciaPedidoPro(String idPed, String sValor, String sVista, int iColumna) {
        try {
            Connection cnn;
            cnn = Conexion.getConexion();
            CallableStatement cst = cnn.prepareCall("call selectBuscar (?,?)");
            cst.setString(1, idPed);
            cst.setString(2, sVista);
            cst.execute();
            ResultSet rs = cst.getResultSet();
            while (rs.next()) {
                String sNombreSQL = rs.getString(iColumna);
                if (sNombreSQL.equals(sValor))
                    return true;
            }
        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Problema con la base de datos " + sqlex.getMessage());
        }
        return false;
    }


    public static boolean validarExistenciaProducto(String sNombreProducto, String sIdProveedor) {
        try {
            Connection cnn;
            cnn = Conexion.getConexion();
            CallableStatement cst = cnn.prepareCall("call ExistenciaProducto (?,?)");
            cst.setString(1, sNombreProducto);
            cst.setString(2, sIdProveedor);
            cst.execute();
            ResultSet rs = cst.getResultSet();
            while (rs.next()) {
                return true;
            }
        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Problema con la base de datos " + sqlex.getMessage());
        }
        return false;
    }


    public static boolean validarExistenciaTrabajador(String sRFC, String sNombre, String sFecha) {
        try {
            Connection cnn;
            cnn = Conexion.getConexion();
            CallableStatement cst = cnn.prepareCall("call ExistenciaTrabajador (?,?,?)");
            cst.setString(1, sRFC);
            cst.setString(2, sNombre);
            cst.setString(3, sFecha);
            cst.execute();
            ResultSet rs = cst.getResultSet();
            while (rs.next()) {
                return true;
            }
        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Problema con la base de datos " + sqlex.getMessage());
        }
        return false;
    }

    public static boolean estaBorradoTrabajador(String sRFC, String sNombre, String sFecha) {
        try {
            Connection cnn;
            cnn = Conexion.getConexion();
            CallableStatement cst = cnn.prepareCall("call DespidoTrabajador (?,?,?)");
            cst.setString(1, sRFC);
            cst.setString(2, sNombre);
            cst.setString(3, sFecha);
            cst.execute();
            ResultSet rs = cst.getResultSet();
            while (rs.next()) {
                return true;
            }
        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Problema con la base de datos (estaBorradoTrabajador)" + sqlex.getMessage());
        }
        return false;
    }

    /*public static boolean validarCantidadPedido(String cantidad) {
        int cant = Integer.parseInt(cantidad);
        if (cant > 0) {
            return true;
        }
        return false;
    }*/


    protected static ArrayList<String> trabajadoresNombreFechaExistentes(String sNombre, String sFecha) {
        ArrayList<String> aRFCs = new ArrayList<>();
        try {
            Connection cnn;
            cnn = Conexion.getConexion();
            CallableStatement cst = cnn.prepareCall("call TrabajdorFechaNombreActivos (?,?)");
            cst.setString(1, sNombre);
            cst.setString(2, sFecha);
            cst.execute();
            ResultSet rs = cst.getResultSet();
            while (rs.next()) {
                aRFCs.add(rs.getString(1));
            }
        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Problema con la base de datos " + sqlex.getMessage());
        }
        return aRFCs;
    }


    protected static ArrayList<String> ExistenciaProductoProveedores(String sNombre, String sProveedor) {
        ArrayList<String> aRFCs = new ArrayList<>();
        try {
            Connection cnn;
            cnn = Conexion.getConexion();
            CallableStatement cst = cnn.prepareCall("call ExistenciaProducto (?,?)");
            cst.setString(1, sNombre);
            cst.setString(2, sProveedor);
            cst.execute();
            ResultSet rs = cst.getResultSet();
            while (rs.next()) {
                aRFCs.add(rs.getString(1));
            }
        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Problema con la base de datos " + sqlex.getMessage());
        }
        return aRFCs;
    }


    public static boolean validarNombreID(String sNombre, String sId, String sVista) {
        try {
            Connection cnn;
            cnn = Conexion.getConexion();
            CallableStatement cst = cnn.prepareCall("call " + sVista + " (?,?)");
            cst.setString(1, sNombre);
            cst.setString(2, sId);
            cst.execute();
            ResultSet rs = cst.getResultSet();
            while (rs.next()) {
                cnn.close();
                cst.close();
                return true;
            }
        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Problema con la base de datos (validarNombreID)" + sqlex.getMessage());
        }
        return false;
    }
    
    public static boolean validarCantidadPedido(String idProducto, String cantidad) {
        try {
            int cant = Integer.parseInt(cantidad);
            Connection cnn;
            cnn = Conexion.getConexion();
            CallableStatement cst = cnn.prepareCall("call selectBuscar (?,?)");
            cst.setString(1, idProducto);
            cst.setString(2, "ProductoIdCualquiera");
            cst.execute();
            ResultSet rs = cst.getResultSet();
            while (rs.next()) {
                String sNombreSQL = rs.getString(5);
                int cantSQL = Integer.parseInt(sNombreSQL);
                int suma=cantSQL+cant;
                if (suma <= 1000) {
                    return true;
                } else{
                    return false;
                }
            }
        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Problema con la base de datos " + sqlex.getMessage());
        }
        return false;
    }
}
