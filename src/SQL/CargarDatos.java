package SQL;

import rojerusan.RSComboMetro;
import rojerusan.RSTableMetro;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.JTextComponent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * @author Daniel Ochoa
 * <p>
 * Ochoa Fecha: 08/11/2021
 * Modificacion: cambie los metodos para que funcionanran con los metodos almacenados
 */
public class CargarDatos {

    public static void rellenarTabla(RSTableMetro rsTabla, String sVista) {
        Connection cnn;
        DefaultTableModel dtmTabla = new DefaultTableModel();
        try {
            cnn = Conexion.getConexion();

            CallableStatement cst = cnn.prepareCall("call selectVistas (?)");
            cst.setString(1, sVista);
            cst.execute();

            ResultSet rs = cst.getResultSet();
            ResultSetMetaData md = rs.getMetaData();

            int columnas = md.getColumnCount();
            for (int i = 1; i <= columnas; i++) {
                dtmTabla.addColumn(md.getColumnLabel(i));
            }
            while (rs.next()) {
                Object[] fila = new Object[columnas];
                for (int i = 0; i < columnas; i++) {
                    fila[i] = rs.getObject(i + 1);
                }
                dtmTabla.addRow(fila);
            }
            rsTabla.setModel(dtmTabla);
            cnn.close();
        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Problema CargarDatos" + sqlex.getMessage());
        }
    }

    public static void buscarTabla(RSTableMetro rsTabla, String buscar, String sConsulta, String sVista) {
        Connection cnn;
        DefaultTableModel modelo = (DefaultTableModel) rsTabla.getModel();

        while (modelo.getRowCount() > 0) {
            modelo.removeRow(0);
        }

        if (buscar.equals("")) {
            CargarDatos.rellenarTabla(rsTabla, sVista);
            return;
        }

        try {
            cnn = Conexion.getConexion();
            CallableStatement cst = cnn.prepareCall("call selectBuscar (?,?)");
            if (sConsulta.equals("PedidoProductos")) {
                cst.setString(1, buscar);
            } else {
                cst.setString(1, buscar.concat("%"));
            }
            cst.setString(2, sConsulta);
            cst.execute();

            ResultSet rs = cst.getResultSet();
            ResultSetMetaData md = rs.getMetaData();
            int columnas = md.getColumnCount();
            while (rs.next()) {
                Object[] fila = new Object[columnas];
                for (int i = 0; i < columnas; i++) {
                    fila[i] = rs.getObject(i + 1);
                }
                modelo.addRow(fila);
            }
        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Problema CargarDatos" + sqlex.getMessage());
        }
    }

    public static ArrayList<ArrayList<String>> obtenerRegistros(String sValor, String sVista) {
        ArrayList<ArrayList<String>> aRegistros = new ArrayList<>();
        try {
            Connection cnn;
            cnn = Conexion.getConexion();
            CallableStatement cst = cnn.prepareCall("call selectBuscar (?,?)");
            cst.setString(1, sValor);
            cst.setString(2, sVista);
            cst.execute();
            ResultSet rs = cst.getResultSet();
            ResultSetMetaData md = rs.getMetaData();
            int columnas = md.getColumnCount();
            while (rs.next()) {
                ArrayList<String> aFila = new ArrayList<>();
                for (int i = 0; i < columnas; i++) {
                    aFila.add(rs.getString(i + 1));
                }
                aRegistros.add(aFila);
            }
        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Problema CargarDatos" + sqlex.getMessage());
        }
        return aRegistros;
    }


    public static DefaultComboBoxModel cargarComboBox(String sValor, String sVista) {
        Connection cnn;
        DefaultComboBoxModel dtmCombo = new DefaultComboBoxModel();
        try {
            cnn = Conexion.getConexion();
            CallableStatement cst = cnn.prepareCall("call " + sVista + " (?)");
            if (sVista.equals("idTarjeta") || sVista.equals("nombreProductoIdProdDevolucion") || sVista.equals("nombreProductoIdProveedorV") || sVista.equals("idProveedorPed")) {
                cst.setString(1, sValor);
            } else {
                cst.setString(1, sValor.concat("%"));
            }
            cst.execute();

            ResultSet rs = cst.getResultSet();
            dtmCombo.removeAllElements();
            while (rs.next()) {
                dtmCombo.addElement(rs.getString(1));
            }
            cnn.close();
        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Problema CargarDatos" + sqlex.getMessage());
        }
        return dtmCombo;
    }

    public static void comboBoxPonerDatos(RSComboMetro rscmCombo, String sVista) {
        rscmCombo.getEditor().getEditorComponent().addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent evt) {
                String cadenaEscrita = rscmCombo.getEditor().getItem().toString();
                if (evt.getKeyCode() >= 65 && evt.getKeyCode() <= 90 || evt.getKeyCode() >= 96 && evt.getKeyCode() <= 105 || evt.getKeyCode() == 8) {
                    rscmCombo.setModel(CargarDatos.cargarComboBox(cadenaEscrita, sVista));
                    if (rscmCombo.getItemCount() > 0) {
                        rscmCombo.showPopup();

                        if (evt.getKeyCode() != 8) {
                            ((JTextComponent) rscmCombo.getEditor().getEditorComponent()).select(cadenaEscrita.length(), rscmCombo.getEditor().getItem().toString().length());

                        } else {
                            rscmCombo.getEditor().setItem(cadenaEscrita);
                        }

                    } else {
                        rscmCombo.addItem(cadenaEscrita);
                    }
                }
            }
        });
    }

    public static String idProducto(String sNombre, String sEmpresa) {
        Connection cnn;
        try {
            cnn = Conexion.getConexion();
            CallableStatement cst = cnn.prepareCall("call idProducto (?,?)");
            cst.setString(1, sNombre);
            cst.setString(2, sEmpresa);
            cst.execute();

            ResultSet rs = cst.getResultSet();
            while (rs.next()) {
                return (rs.getString(1));
            }
            cnn.close();
        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Problema CargarDatos idProducto " + sqlex.getMessage());
        }
        return " ";
    }

    public static String puntosTajerta(String sTarjeta) {
        Connection cnn;
        try {
            cnn = Conexion.getConexion();
            CallableStatement cst = cnn.prepareCall("call puntosTarjeta (?)");
            cst.setString(1, sTarjeta);
            cst.execute();

            ResultSet rs = cst.getResultSet();
            while (rs.next()) {
                return (rs.getString(1));
            }
            cnn.close();
        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Problema CargarDatos puntosTajerta " + sqlex.getMessage());
        }
        return " ";
    }

    public static HashMap<String, Object> datosReporteVenta(String sIDVenta) {
        HashMap<String, Object> map = new HashMap<String, Object>();
        Connection cnn;
        try {
            cnn = Conexion.getConexion();
            CallableStatement cst = cnn.prepareCall("call reporteVenta (?)");
            cst.setString(1, sIDVenta);
            cst.execute();

            ResultSet rs = cst.getResultSet();
            while (rs.next()) {
                map.put("IdVenta", rs.getString(3));
                map.put("NombreCliente", rs.getString(1));
                map.put("NombreTrabajador", rs.getString(2));
                map.put("IdTarjeta", rs.getString(4));
                map.put("Total", rs.getString(5));
                map.put("Efectivo", rs.getString(6));
                map.put("Cambio", rs.getString(7));
                map.put("DePuntos", rs.getString(8));
                map.put("PuUti", rs.getString(9));
                map.put("SalPu", rs.getString(10));
            }
            cnn.close();
        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Problema CargarDatos datosReporteVenta " + sqlex.getMessage());
        }
        return map;
    }



    public static String ventaMaxReciente() {
        Connection cnn;
        try {
            cnn = Conexion.getConexion();
            CallableStatement cst = cnn.prepareCall("call ventaMasReciente");
            cst.execute();

            ResultSet rs = cst.getResultSet();
            while (rs.next()) {
                return (rs.getString(1));
            }
            cnn.close();
        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Problema CargarDatos ventaMaxReciente " + sqlex.getMessage());
        }
        return " ";
    }
}