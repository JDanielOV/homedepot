package SQL;

import javax.swing.*;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;

/**
 * @author Daniel Ochoa
 * <p>
 * Ochoa Fecha: 06/11/2021
 * Modificaciones: Clase Creada para obtener informacion del trabajador y validar su existencia
 * <p>
 * Atencion al cliente RFC ='1' and Contraseña='ZR2XdwLksRM5';
 * Cajero RFC ='10' and Contraseña='ldsEyDx';
 * Oficinista RFC ='11' and Contraseña='zWELu1gxj';
 * Almacenista RFC ='14' and Contraseña='77g09IOQS3';
 * Gerente RFC ='24' and Contraseña='XAatTBi';
 */
public class Login {
    private String sRFC, sNombre, sEmail, sTelefono, sDireccion, sCargo;
    private BigDecimal bdSueldo;
    private LocalDate ldNacimiento;
    private boolean bExistencia = false;

    public Login(String sUsuario, String sContrasena) {
        this.ingresar(sUsuario, sContrasena);
    }

    private boolean ingresar(String sUsuario, String sContrasena) {
        Connection objecto = SQL.Conexion.getConexion();
        this.bExistencia = false;
        Statement Consulta;
        ResultSet recorrido;
        try {
            Consulta = objecto.createStatement();
            recorrido = Consulta.executeQuery("SELECT * FROM Vista_TrabajadorCont where (RFC ='" + sUsuario + "') and (Contraseña='" + sContrasena + "')");
            while (recorrido.next()) {
                this.sRFC = recorrido.getNString(1);
                this.bExistencia = true;
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Error al buscar al usuario por: " + e.getMessage());
        }
        return this.bExistencia;
    }

    public boolean existenciaUsuario() {
        return this.bExistencia;
    }

    public void datosUsuario() {
        Connection objecto = SQL.Conexion.getConexion();
        Statement Consulta;
        ResultSet recorrido;
        try {
            Consulta = objecto.createStatement();
            recorrido = Consulta.executeQuery("SELECT * FROM Vista_Trabajador where  RFC_Trabajador='".concat(this.sRFC).concat("';"));
            while (recorrido.next()) {
                this.sNombre = recorrido.getString(2);
                this.sEmail = recorrido.getString(3);
                this.sTelefono = recorrido.getString(4);
                this.ldNacimiento = LocalDate.parse(recorrido.getString(5));
                this.sDireccion = recorrido.getString(6);
                this.sCargo = recorrido.getString(7);
                this.bdSueldo = BigDecimal.valueOf(Double.parseDouble(recorrido.getString(8)));
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Error al buscar los datos del usuario: " + e.getMessage());
        }
    }

    public String getsRFC() {
        return sRFC;
    }

    public String getsNombre() {
        return sNombre;
    }

    public String getsEmail() {
        return sEmail;
    }

    public String getsTelefono() {
        return sTelefono;
    }

    public String getsDireccion() {
        return sDireccion;
    }

    public String getsCargo() {
        return sCargo;
    }

    public BigDecimal getBdSueldo() {
        return bdSueldo;
    }

    public LocalDate getLdNacimiento() {
        return ldNacimiento;
    }

}
