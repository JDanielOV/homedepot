package Ventanas;

import Recursos.Colores;
import Recursos.Fuentes;
import Recursos.Imagenes;
import SQL.Login;
import keeptoo.KGradientPanel;
import rojeru_san.RSButtonRiple;
import rojerusan.RSPasswordTextPlaceHolder;

import javax.swing.*;
import javax.swing.border.MatteBorder;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;

/**
 * @author Daniel Ochoa
 * <p>
 * Ochoa Fecha: 05/11/2021
 * Modificaciones: Hice que trbajara con la base de datos
 */
public class VentanaIngreso extends JFrame implements ActionListener {
    private KGradientPanel kgPanelFondo, kgPanelChico;
    private JTextField txtUsuario;
    private RSPasswordTextPlaceHolder txtContrasena;
    private JButton btnCerrar;
    private RSButtonRiple btnIngresar;

    public VentanaIngreso() {
        this.frameConfiguracion();
        this.panelConfiguracion();
        this.botonesConfiguracion();
        this.labelsConfiguracion();
        this.textosConfiguracion();
    }

    private void panelConfiguracion() {
        this.kgPanelFondo = new KGradientPanel();
        this.kgPanelFondo.setBounds(100, 0, 300, 450);
        this.kgPanelFondo.setBackground(Colores.C_COLOR_TRANSPARENTE.getColor());
        this.kgPanelFondo.setkEndColor(Colores.C_COLOR_GRIS4.getColor());
        this.kgPanelFondo.setkStartColor(Colores.C_COLOR_GRIS4.getColor());
        this.kgPanelFondo.setkBorderRadius(60);
        this.kgPanelFondo.setLayout(null);
        this.getContentPane().add(this.kgPanelFondo);

        /*--------------------------------------------------------*/
        this.kgPanelChico = new KGradientPanel();
        this.kgPanelChico.setBounds(0, 100, 500, 250);
        this.kgPanelChico.setBackground(Colores.C_COLOR_TRANSPARENTE.getColor());
        this.kgPanelChico.setkEndColor(Colores.C_COLOR_GRIS2.getColor());
        this.kgPanelChico.setkStartColor(Colores.C_COLOR_GRIS3.getColor());
        this.kgPanelChico.setkBorderRadius(40);
        this.kgPanelChico.setLayout(null);
        this.getContentPane().add(this.kgPanelChico);

    }

    private void botonesConfiguracion() {
        this.btnCerrar = new JButton(Imagenes.I_IMAGEN_CERRAR.getImagen());
        this.btnCerrar.setBounds(260, 0, 32, 32);
        this.btnCerrar.setBorderPainted(false);
        this.btnCerrar.setContentAreaFilled(false);
        this.btnCerrar.setFocusPainted(false);
        this.btnCerrar.setActionCommand("btnCerrar");
        this.btnCerrar.addActionListener(this);
        this.kgPanelFondo.add(this.btnCerrar);

        this.btnIngresar = new RSButtonRiple();
        this.btnIngresar.setBounds(100, 400, 100, 50);
        this.btnIngresar.setBorderPainted(false);
        this.btnIngresar.setFocusPainted(false);
        this.btnIngresar.setBackground(Colores.C_COLOR_GRIS2.getColor());
        this.btnIngresar.setColorHover(Colores.C_COLOR_GRIS3.getColor());
        this.btnIngresar.setColorText(Colores.C_COLOR_NEGRO.getColor());
        this.btnIngresar.setColorTextHover(Colores.C_COLOR_GRIS2.getColor());
        this.btnIngresar.setText("Ingresar");
        this.btnIngresar.setActionCommand("btnDevoluciones");
        this.btnIngresar.addActionListener(this);
        this.kgPanelFondo.add(this.btnIngresar);

    }

    private void frameConfiguracion() {
        this.setSize(500, 450);
        this.setResizable(false);
        this.setLocationRelativeTo(null);
        this.setUndecorated(true);
        this.getContentPane().setLayout(null);
        this.setBackground(Colores.C_COLOR_TRANSPARENTE.getColor());
    }

    private void labelsConfiguracion() {
        JLabel jlImgTitulo = new JLabel(Imagenes.I_IMAGEN_LOGO.getImagen());
        jlImgTitulo.setBounds(0, 10, 300, 128);
        jlImgTitulo.setVerticalAlignment(SwingConstants.CENTER);
        jlImgTitulo.setHorizontalAlignment(SwingConstants.CENTER);
        this.kgPanelFondo.add(jlImgTitulo);

        JLabel jlUsuario = new JLabel("Usuario");
        jlUsuario.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlUsuario.setBounds(100, 150, 100, 30);
        jlUsuario.setVerticalAlignment(SwingConstants.CENTER);
        jlUsuario.setHorizontalAlignment(SwingConstants.CENTER);
        this.kgPanelFondo.add(jlUsuario);

        JLabel jlContrasena = new JLabel("Contraseña");
        jlContrasena.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlContrasena.setBounds(80, 250, 140, 30);
        jlContrasena.setVerticalAlignment(SwingConstants.CENTER);
        jlContrasena.setHorizontalAlignment(SwingConstants.CENTER);
        this.kgPanelFondo.add(jlContrasena);

        String sTituloIzq = "I" + "<br>" + "N" + "<br>" + "G" + "<br>" + "R" + "<br>" + "E" + "<br>" + "S" + "<br>" + "O";
        JLabel jlTituloIzq = new JLabel("<html><div style='text-align: center;'>" + sTituloIzq + "</div></html>");
        jlTituloIzq.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlTituloIzq.setBounds(0, 1, 100, 248);
        jlTituloIzq.setVerticalAlignment(SwingConstants.CENTER);
        jlTituloIzq.setHorizontalAlignment(SwingConstants.CENTER);
        this.kgPanelChico.add(jlTituloIzq);

        String sTituloDer = "H" + "<br>" + "O" + "<br>" + "M" + "<br>" + "E" + "<br>" + "D" + "<br>" + "E" + "<br>" + "P" + "<br>" + "O" + "<br>" + "T";
        JLabel jlTituloDer = new JLabel("<html><div style='text-align: center;'>" + sTituloDer + "</div></html>");
        jlTituloDer.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlTituloDer.setBounds(400, 1, 100, 248);
        jlTituloDer.setVerticalAlignment(SwingConstants.CENTER);
        jlTituloDer.setHorizontalAlignment(SwingConstants.CENTER);
        this.kgPanelChico.add(jlTituloDer);

        JLabel jlImgUsuario = new JLabel(Imagenes.I_IMAGEN_USUARIO.getImagen());
        jlImgUsuario.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlImgUsuario.setBounds(48, 190, 32, 32);
        jlImgUsuario.setVerticalAlignment(SwingConstants.CENTER);
        jlImgUsuario.setHorizontalAlignment(SwingConstants.CENTER);
        this.kgPanelFondo.add(jlImgUsuario);

        JLabel jlImgContrasena = new JLabel(Imagenes.I_IMAGEN_CONTRASENA.getImagen());
        jlImgContrasena.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlImgContrasena.setBounds(48, 280, 32, 32);
        jlImgContrasena.setVerticalAlignment(SwingConstants.CENTER);
        jlImgContrasena.setHorizontalAlignment(SwingConstants.CENTER);
        this.kgPanelFondo.add(jlImgContrasena);
    }

    private void textosConfiguracion() {
        this.txtUsuario = new JTextField();
        this.txtUsuario.setBounds(80, 190, 140, 30);
        this.txtUsuario.setBackground(Colores.C_COLOR_GRIS4.getColor());
        this.txtUsuario.setBorder(new MatteBorder(0, 0, 2, 0, Colores.C_COLOR_GRIS1.getColor()));
        this.txtUsuario.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.txtUsuario.setHorizontalAlignment(SwingConstants.CENTER);
        this.kgPanelFondo.add(this.txtUsuario);

        

        this.txtContrasena = new RSPasswordTextPlaceHolder();
        this.txtContrasena.setBounds(80, 280, 140, 30);
        this.txtContrasena.setBackground(Colores.C_COLOR_GRIS4.getColor());
        this.txtContrasena.setBorder(new MatteBorder(0, 0, 2, 0, Colores.C_COLOR_GRIS1.getColor()));
        this.txtContrasena.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.txtContrasena.setHorizontalAlignment(SwingConstants.CENTER);
        this.kgPanelFondo.add(this.txtContrasena);


    }

    public static void main(String[] args) {
        VentanaIngreso ventana = new VentanaIngreso();
        ventana.setVisible(true);
    }

    private void btnCerrarActionPerformed() {
        System.exit(0);
    }

    private void btnIngresarActionPerformed(String btnAction) {
        String sUsuario = this.txtUsuario.getText();
        String sContrasena = this.txtContrasena.getText();
        Login lEntrar = new Login(sUsuario, sContrasena);
        if (!lEntrar.existenciaUsuario()) {
            JOptionPane.showMessageDialog(this, "Credenciales erroneas");
            this.txtUsuario.setText("");
            this.txtContrasena.setText("");
        } else {
            VentanaPrincipal v = new VentanaPrincipal(lEntrar);
            v.setVisible(true);
            this.dispose();
        }
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        String sButon = ae.getActionCommand();
        if (sButon.equals(this.btnCerrar.getActionCommand())) {
            this.btnCerrarActionPerformed();
        } else if (sButon.equals(this.btnIngresar.getActionCommand())) {
            this.btnIngresarActionPerformed(sButon);
        }
    }

}
