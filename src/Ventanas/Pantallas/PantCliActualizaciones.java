package Ventanas.Pantallas;

import Recursos.Colores;
import Recursos.Fuentes;
import Recursos.Imagenes;
import SQL.Actualizar;
import SQL.ActualizarCargarDatos;
import SQL.CargarDatos;
import SQL.Conexion;
import SQL.ValidacionesGenerales;
import keeptoo.KGradientPanel;
import rojeru_san.RSButtonRiple;
import rojerusan.RSDateChooser;

import javax.swing.*;
import javax.swing.border.MatteBorder;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import javax.swing.text.JTextComponent;
import rojerusan.RSComboMetro;

/**
 * @author Daniel Ochoa
 * Ochoa Fecha: 14/11/2021
 * Modificaciones: puse el codigo correspondiente para actualizar el cliente
 */
public class PantCliActualizaciones {
    private KGradientPanel kgPantalla;
    private JTextField txtNombre, txtApePaterno, txtApeMaterno, txtCorreoE, txtTelefono, txtIDCliente;
    private RSDateChooser rdFechas;
    private RSButtonRiple btnCrearUsuario;
    private String sNombre, sApellidoP, sApellidoM,cadena,id;
    private RSComboMetro rscmCliente;

    public PantCliActualizaciones() {
        this.panelConfiguracion();
        this.labelsConfiguracion();
        this.textosConfiguracion();
        this.dateChooserConfiguracion();
        this.botonesConfiguracion();
        this.comboBoxConfiguracion();
    }

    private void panelConfiguracion() {
        this.kgPantalla = new KGradientPanel();
        this.kgPantalla = new KGradientPanel();
        this.kgPantalla.setSize(1090, 550);
        this.kgPantalla.setBackground(Colores.C_COLOR_TRANSPARENTE.getColor());
        this.kgPantalla.setkEndColor(Colores.C_COLOR_BLANCO.getColor());
        this.kgPantalla.setkStartColor(Colores.C_COLOR_BLANCO.getColor());
        this.kgPantalla.setkBorderRadius(40);
        this.kgPantalla.setLayout(null);
    }
    
    private void comboBoxConfiguracion() {
        this.rscmCliente = new RSComboMetro();
        this.rscmCliente.setColorFondo(Colores.C_COLOR_GRIS4.getColor());
        this.rscmCliente.setColorBorde(Colores.C_COLOR_GRIS3.getColor());
        this.rscmCliente.setColorArrow(Colores.C_COLOR_GRIS3.getColor());
        this.rscmCliente.setForeground(Colores.C_COLOR_NEGRO.getColor());
        this.rscmCliente.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.rscmCliente.setBounds(90, 150, 300, 30);
        this.rscmCliente.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
        this.rscmCliente.setEditable(true);
        CargarDatos.comboBoxPonerDatos(this.rscmCliente, "nombreClienteIDCliente");
        this.kgPantalla.add(this.rscmCliente);
        this.rscmCliente.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String sNombreClienteIDCliente = rscmCliente.getSelectedItem() == null ? " " : rscmCliente.getSelectedItem().toString();
                ;
                int iComa = sNombreClienteIDCliente.indexOf(", ");
                int iDosPuntosEspacio = sNombreClienteIDCliente.indexOf("ID: ");

                String sNombrea = (sNombreClienteIDCliente.contains(", ")) ? (sNombreClienteIDCliente.substring(0, iComa)) : (" ");
                String sId = (sNombreClienteIDCliente.contains("ID: ")) ? (sNombreClienteIDCliente.substring(iDosPuntosEspacio + 4)) : (" ");
                id=sId;
                txtIDCliente.setText(id);
                JComponent[] jtMisEntradas = {txtNombre, txtApePaterno, txtApeMaterno, txtCorreoE, txtTelefono, rdFechas};
                    String sIdCliente = id;
                    int[] iCampos = {2, 3, 4, 5};
                    ActualizarCargarDatos.cargarDatos(sIdCliente, 0, jtMisEntradas, iCampos);
                    sNombre = txtNombre.getText();
                    sApellidoP = txtApePaterno.getText();
                    sApellidoM = txtApeMaterno.getText();
            }
        });
    }

    private void textosConfiguracion() {
        this.txtNombre = new JTextField();
        this.txtNombre.setBounds(530, 150, 200, 30);
        this.txtNombre.setBackground(Colores.C_COLOR_GRIS1.getColor());
        this.txtNombre.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
        this.txtNombre.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.txtNombre.setEnabled(false);
        this.kgPantalla.add(this.txtNombre);


        this.txtApePaterno = new JTextField();
        this.txtApePaterno.setBounds(865, 150, 200, 30);
        this.txtApePaterno.setBackground(Colores.C_COLOR_GRIS1.getColor());
        this.txtApePaterno.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
        this.txtApePaterno.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.txtApePaterno.setEnabled(false);
        this.kgPantalla.add(this.txtApePaterno);


        this.txtApeMaterno = new JTextField();
        this.txtApeMaterno.setBounds(110, 300, 200, 30);
        this.txtApeMaterno.setBackground(Colores.C_COLOR_GRIS1.getColor());
        this.txtApeMaterno.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
        this.txtApeMaterno.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.txtApeMaterno.setEnabled(false);
        this.kgPantalla.add(this.txtApeMaterno);


        this.txtCorreoE = new JTextField();
        this.txtCorreoE.setBounds(420, 300, 200, 30);
        this.txtCorreoE.setBackground(Colores.C_COLOR_GRIS1.getColor());
        this.txtCorreoE.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
        this.txtCorreoE.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.txtCorreoE.setEnabled(false);
        this.kgPantalla.add(this.txtCorreoE);


        this.txtTelefono = new JTextField();
        this.txtTelefono.setBounds(760, 300, 200, 30);
        this.txtTelefono.setBackground(Colores.C_COLOR_GRIS1.getColor());
        this.txtTelefono.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
        this.txtTelefono.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.txtTelefono.setEnabled(false);
        this.kgPantalla.add(this.txtTelefono);


        this.txtIDCliente = new JTextField();
        this.txtIDCliente.setBounds(110, 150, 200, 30);
        this.txtIDCliente.setBackground(Colores.C_COLOR_GRIS1.getColor());
        this.txtIDCliente.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
        this.txtIDCliente.setFont(Fuentes.FUENTE_TEXTOS.getFont());
    }

    private void labelsConfiguracion() {
        JLabel labelTitutlo = new JLabel("Actualizar Cliente");
        labelTitutlo.setFont(Fuentes.FUENTE_TITULOS.getFont());
        labelTitutlo.setBounds(350, 20, 350, 30);
        this.kgPantalla.add(labelTitutlo);

        JLabel jlImgTitulo = new JLabel(Imagenes.I_IMAGEN_CLIENTE.getImagen());
        jlImgTitulo.setFont(Fuentes.FUENTE_TITULOS.getFont());
        jlImgTitulo.setBounds(280, 10, 64, 64);
        this.kgPantalla.add(jlImgTitulo);

        JLabel jlNombreCliente = new JLabel("Nombre: ");
        jlNombreCliente.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlNombreCliente.setBounds(450, 150, 100, 30);
        this.kgPantalla.add(jlNombreCliente);

        JLabel jApellidoP = new JLabel("Apellido P: ");
        jApellidoP.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jApellidoP.setBounds(760, 150, 130, 30);
        this.kgPantalla.add(jApellidoP);

        JLabel jApellidoM = new JLabel("Apellido M: ");
        jApellidoM.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jApellidoM.setBounds(0, 300, 130, 30);
        this.kgPantalla.add(jApellidoM);

        JLabel jlCorreoE = new JLabel("Email: ");
        jlCorreoE.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlCorreoE.setBounds(340, 300, 130, 30);
        this.kgPantalla.add(jlCorreoE);

        JLabel jlTelefono = new JLabel("Telefono: ");
        jlTelefono.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlTelefono.setBounds(670, 300, 130, 30);
        this.kgPantalla.add(jlTelefono);

        JLabel jlFechaN = new JLabel("Fecha Nacimiento: ");
        jlFechaN.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlFechaN.setBounds(0, 400, 180, 30);
        this.kgPantalla.add(jlFechaN);

        JLabel jlClienteID = new JLabel("Cliente: ");
        jlClienteID.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlClienteID.setBounds(0, 150, 200, 30);
        this.kgPantalla.add(jlClienteID);
    }

    private void dateChooserConfiguracion() {
        this.rdFechas = new RSDateChooser();
        this.rdFechas.setColorBackground(Colores.C_COLOR_GRIS4.getColor());
        this.rdFechas.setColorButtonHover(Colores.C_COLOR_GRIS4.getColor());
        this.rdFechas.setColorTextDiaActual(Colores.C_COLOR_NEGRO.getColor());
        this.rdFechas.setColorForeground(Colores.C_COLOR_NEGRO.getColor());
        this.rdFechas.setFormatoFecha("YYYY-MM-dd");
        this.rdFechas.setFuente(Fuentes.FUENTE_TEXTOS.getFont());
        this.rdFechas.setBounds(180, 400, 180, 30);
        this.rdFechas.setVisible(false);
        this.kgPantalla.add(this.rdFechas);
    }

    private void botonesConfiguracion() {
        /*----------------------------------------*/
        this.btnCrearUsuario = new RSButtonRiple();
        this.btnCrearUsuario.setBounds(400, 450, 120, 50);
        this.btnCrearUsuario.setBorderPainted(false);
        this.btnCrearUsuario.setFocusPainted(false);
        this.btnCrearUsuario.setBackground(Colores.C_COLOR_GRIS3.getColor());
        this.btnCrearUsuario.setColorHover(Colores.C_COLOR_GRIS2.getColor());
        this.btnCrearUsuario.setColorText(Colores.C_COLOR_NEGRO.getColor());
        this.btnCrearUsuario.setColorTextHover(Colores.C_COLOR_GRIS4.getColor());
        this.btnCrearUsuario.setText("Actualizar");
        this.btnCrearUsuario.setActionCommand("btnClientes");
        this.btnCrearUsuario.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                LocalDate fechaHoy = LocalDate.now();
                String formatoFecha = fechaHoy.format(DateTimeFormatter.ofPattern("YYYY-MM-dd"));
                String sNombreClienteIDCliente = rscmCliente.getSelectedItem() == null ? " " : rscmCliente.getSelectedItem().toString();
                ;
                int iComa = sNombreClienteIDCliente.indexOf(", ");
                int iDosPuntosEspacio = sNombreClienteIDCliente.indexOf("ID: ");

                String sNombrea = (sNombreClienteIDCliente.contains(", ")) ? (sNombreClienteIDCliente.substring(0, iComa)) : (" ");
                String sId = (sNombreClienteIDCliente.contains("ID: ")) ? (sNombreClienteIDCliente.substring(iDosPuntosEspacio + 4)) : (" ");

                System.out.println("#" + sNombrea + "#" + sId + "#" + sNombreClienteIDCliente + "#");
                if (txtNombre.isEnabled()) {
                    String[] sParametros = {
                            txtNombre.getText().trim(),
                            txtApePaterno.getText().trim(),
                            txtApeMaterno.getText().trim(),
                            txtCorreoE.getText().trim(),
                            txtTelefono.getText().trim(),
                            rdFechas.getFechaSeleccionada().trim(),
                            sNombre,
                            sApellidoP,
                            sApellidoM,
                            sId
                    };
                    boolean bInsercionCliente = Actualizar.actualizarCliente(sParametros);
                    if (bInsercionCliente) {
                        JOptionPane.showMessageDialog(null, "Cliente modificado correctamente");
                        rscmCliente.removeAllItems();
                        txtNombre.setText("");
                        txtApePaterno.setText("");
                        txtApeMaterno.setText("");
                        txtCorreoE.setText("");
                        txtTelefono.setText("");
                        rdFechas.setTextoFecha(formatoFecha);
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "No a selecionado un cliente o no existe");
                }
            }
        });
        this.kgPantalla.add(this.btnCrearUsuario);
    }

    public KGradientPanel getkgPantalla() {
        return this.kgPantalla;
    }
}
