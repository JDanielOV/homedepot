package Ventanas.Pantallas;

/**
 * @author crist
 * Ochoa Fecha: 08/11/2021
 * Modificacion: cambie el metodo buscarTabla
 */

import Recursos.Colores;
import Recursos.Fuentes;
import Recursos.Imagenes;
import SQL.CargarDatos;
import SQL.Eliminaciones;
import keeptoo.KGradientPanel;
import rojerusan.RSTableMetro;

import javax.swing.*;
import javax.swing.border.MatteBorder;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import rojeru_san.RSButtonRiple;

public class PantTraEli {
    private RSTableMetro rsTablaTrabajadores;
    private KGradientPanel kgPantalla;
    private JTextField txtRfc;
    private JLabel txtRfcSelect;
    private RSButtonRiple btnEliminarTrabajador;
    private String sRFC;

    public PantTraEli() {
        this.panelConfiguracion();
        this.labelsConfiguracion();
        this.textosConfiguracion();
        this.tablaConfiguracion();
        this.botonesConfiguracion();
    }

    private void panelConfiguracion() {
        this.kgPantalla = new KGradientPanel();
        this.kgPantalla = new KGradientPanel();
        this.kgPantalla.setSize(1090, 550);
        this.kgPantalla.setBackground(Colores.C_COLOR_TRANSPARENTE.getColor());
        this.kgPantalla.setkEndColor(Colores.C_COLOR_BLANCO.getColor());
        this.kgPantalla.setkStartColor(Colores.C_COLOR_BLANCO.getColor());
        this.kgPantalla.setkBorderRadius(40);
        this.kgPantalla.setLayout(null);
    }

    private void textosConfiguracion() {
        this.txtRfc = new JTextField();
        this.txtRfc.setBounds(410, 100, 200, 30);
        this.txtRfc.setBackground(Colores.C_COLOR_GRIS1.getColor());
        this.txtRfc.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
        this.txtRfc.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.txtRfc.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyPressed(KeyEvent e) {
            }

            @Override
            public void keyReleased(KeyEvent e) {
                CargarDatos.buscarTabla(rsTablaTrabajadores, txtRfc.getText(), "Trabajador", "Trabajador");
                sRFC = "";
                txtRfcSelect.setText("Rfc seleccionado: ");
            }
        });
        this.kgPantalla.add(this.txtRfc);
    }

    private void labelsConfiguracion() {
        JLabel labelTitutlo = new JLabel("Eliminar Trabajador");
        labelTitutlo.setFont(Fuentes.FUENTE_TITULOS.getFont());
        labelTitutlo.setBounds(400, 20, 400, 30);
        this.kgPantalla.add(labelTitutlo);

        JLabel jlImgTitulo = new JLabel(Imagenes.I_IMAGEN_TRABAJADOR.getImagen());
        jlImgTitulo.setFont(Fuentes.FUENTE_TITULOS.getFont());
        jlImgTitulo.setBounds(320, 10, 64, 64);
        this.kgPantalla.add(jlImgTitulo);

        JLabel jlRfcTrabajador = new JLabel("Buscar Rfc: ");
        jlRfcTrabajador.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlRfcTrabajador.setBounds(300, 100, 100, 30);
        this.kgPantalla.add(jlRfcTrabajador);

        this.txtRfcSelect = new JLabel("Rfc seleccionado: ");
        this.txtRfcSelect.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        this.txtRfcSelect.setBounds(300, 180, 500, 70);
        this.kgPantalla.add(this.txtRfcSelect);
    }

    private void botonesConfiguracion() {
        /*----------------------------------------*/
        this.btnEliminarTrabajador = new RSButtonRiple();
        this.btnEliminarTrabajador.setBounds(700, 100, 120, 50);
        this.btnEliminarTrabajador.setBorderPainted(false);
        this.btnEliminarTrabajador.setFocusPainted(false);
        this.btnEliminarTrabajador.setBackground(Colores.C_COLOR_GRIS3.getColor());
        this.btnEliminarTrabajador.setColorHover(Colores.C_COLOR_GRIS2.getColor());
        this.btnEliminarTrabajador.setColorText(Colores.C_COLOR_NEGRO.getColor());
        this.btnEliminarTrabajador.setColorTextHover(Colores.C_COLOR_GRIS4.getColor());
        this.btnEliminarTrabajador.setText("Eliminar");
        this.btnEliminarTrabajador.setActionCommand("btnEliTrabajadores");
        this.btnEliminarTrabajador.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (seleccionTrabajador() == true) {
                    boolean bEstado = Eliminaciones.eliminarTrabajador(sRFC.trim());
                    if (bEstado) {
                        JOptionPane.showMessageDialog(null, "Trabajador eliminado");
                        txtRfc.setText("");
                        CargarDatos.buscarTabla(rsTablaTrabajadores, txtRfc.getText(), "Trabajador", "Trabajador");
                        txtRfcSelect.setText("Rfc seleccionado: ");
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "No ha selecionado trabajador");
                }
            }
        });
        this.kgPantalla.add(this.btnEliminarTrabajador);
    }

    private boolean seleccionTrabajador() {
        int renglon = this.rsTablaTrabajadores.getSelectedRow();
        if (renglon != -1) {
            String rfc = this.rsTablaTrabajadores.getValueAt(renglon, 0) + "";
            this.sRFC = rfc;
            this.txtRfcSelect.setText("Rfc seleccionado: " + rfc);
        } else {
            System.out.println("No selecciono trabajador");
            this.sRFC = "";
            return false;
        }
        return true;
    }

    private void tablaConfiguracion() {
        this.rsTablaTrabajadores = new RSTableMetro();
        this.rsTablaTrabajadores.setAltoHead(60);
        this.rsTablaTrabajadores.setRowHeight(40);
        this.rsTablaTrabajadores.setGrosorBordeFilas(0);
        this.rsTablaTrabajadores.setColorBackgoundHead(Colores.C_COLOR_GRIS4.getColor());
        this.rsTablaTrabajadores.setColorFilasBackgound1(Colores.C_COLOR_GRIS1.getColor());
        this.rsTablaTrabajadores.setColorFilasBackgound2(Colores.C_COLOR_GRIS2.getColor());
        this.rsTablaTrabajadores.setColorFilasForeground1(Colores.C_COLOR_NEGRO.getColor());
        this.rsTablaTrabajadores.setColorFilasForeground2(Colores.C_COLOR_NEGRO.getColor());
        this.rsTablaTrabajadores.setColorSelBackgound(Colores.C_COLOR_GRIS3.getColor());
        this.rsTablaTrabajadores.setColorBordeFilas(Colores.C_COLOR_GRIS1.getColor());
        this.rsTablaTrabajadores.setFuenteFilas(Fuentes.FUENTE_TEXTOS.getFont());
        this.rsTablaTrabajadores.setFuenteHead(Fuentes.FUENTE_TEXTOS.getFont());
        this.rsTablaTrabajadores.setPreferredScrollableViewportSize(new Dimension(250, 100));
        JScrollPane scrollPane = new JScrollPane(this.rsTablaTrabajadores);
        scrollPane.setBounds(10, 250, 980, 250);
        CargarDatos.rellenarTabla(this.rsTablaTrabajadores, "Trabajador");
        this.rsTablaTrabajadores.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                seleccionTrabajador();
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }
        });
        this.kgPantalla.add(scrollPane);
    }


    public KGradientPanel getkgPantalla() {
        return this.kgPantalla;
    }
}
