package Ventanas.Pantallas;

import Recursos.Colores;
import Recursos.Fuentes;
import Recursos.Imagenes;
import SQL.ActualizarCargarDatos;
import SQL.CargarDatos;
import SQL.Conexion;
import SQL.Inserciones;
import SQL.ValidacionesGenerales;
import keeptoo.KGradientPanel;
import rojeru_san.RSButtonRiple;
import rojerusan.RSComboMetro;

import javax.swing.*;
import javax.swing.border.MatteBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import javax.swing.text.JTextComponent;


/**
 * @author cristina 04-11-2021
 * 05-11-2021
 * Modifique agregar en la etiqueta fecha de entrega
 */
public class PantDevAgregar {
    private KGradientPanel kgPantalla;
    private JTextField txtIDProducto,txtIDDevolucion, txtMotivo;
    private RSButtonRiple btnAgregarDevolucion;
    private JSlider jsCantidad;
    private RSComboMetro cbMotivo,cbCliente;
    private JLabel txtIDTarjeta,txtIDCliente;
    private JSpinner jspCantidad;
    private LocalDate fechaHoy;
    private String cadena,id;

    public PantDevAgregar() {
        this.panelConfiguracion();
        this.labelsConfiguracion();
        this.textosConfiguracion();
        this.botonesConfiguracion();
        this.comboBoxConfiguracion();
        this.sliderConfiguracion();
        this.spinnerConfiguracion();
    }

    private void panelConfiguracion() {
        this.kgPantalla = new KGradientPanel();
        this.kgPantalla = new KGradientPanel();
        this.kgPantalla.setSize(1090, 550);
        this.kgPantalla.setBackground(Colores.C_COLOR_TRANSPARENTE.getColor());
        this.kgPantalla.setkEndColor(Colores.C_COLOR_BLANCO.getColor());
        this.kgPantalla.setkStartColor(Colores.C_COLOR_BLANCO.getColor());
        this.kgPantalla.setkBorderRadius(40);
        this.kgPantalla.setLayout(null);
    }
   
    private void comboBoxConfiguracion() {
        this.cbMotivo = new RSComboMetro();
        this.cbMotivo.addItem("Producto roto");
        this.cbMotivo.addItem("No le gustó al cliente");
        this.cbMotivo.addItem("Una polea floja");
        this.cbMotivo.addItem("Gotea mucho ");
        this.cbMotivo.addItem("Mal olor");
        this.cbMotivo.addItem("No enfria el clima");
        this.cbMotivo.addItem("Motor quemado");
        this.cbMotivo.addItem("Cables mal ensamblados");
        this.cbMotivo.addItem("Temperatura incorrecta");
        this.cbMotivo.addItem("Motor deteriorado");
        this.cbMotivo.addItem("Calentamiento desigual");
        this.cbMotivo.addItem("Averia en las resistencias");
        this.cbMotivo.addItem("Flama muy baja");
        this.cbMotivo.addItem("Las teclas del panel no responden");
        this.cbMotivo.addItem("Falla del motor eléctrico");
        this.cbMotivo.addItem("El motor suena extraño");
        this.cbMotivo.addItem("Falta de llaves");
        this.cbMotivo.addItem("No es la forma deseada");
        this.cbMotivo.addItem("Venia cuarteado");
        this.cbMotivo.addItem("Los seguros venian rotos");
        this.cbMotivo.addItem("La PC disminuyó su rendimiento");
        this.cbMotivo.addItem("Retorno de líquido");
        this.cbMotivo.addItem("El martillo empleado no es el adecuado para su máquina");
        this.cbMotivo.addItem("Falta de seguros");
        this.cbMotivo.addItem("Mal soldado");
        this.cbMotivo.setSelectedIndex(2);
        this.cbMotivo.setColorFondo(Colores.C_COLOR_GRIS2.getColor());
        this.cbMotivo.setColorBorde(Colores.C_COLOR_GRIS3.getColor());
        this.cbMotivo.setColorArrow(Colores.C_COLOR_GRIS3.getColor());
        this.cbMotivo.setForeground(Colores.C_COLOR_NEGRO.getColor());
        this.cbMotivo.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.cbMotivo.setBounds(210, 320, 480, 30);
        this.kgPantalla.add(cbMotivo);
        
        this.cbCliente = new RSComboMetro();
        this.cbCliente.setColorFondo(Colores.C_COLOR_GRIS4.getColor());
        this.cbCliente.setColorBorde(Colores.C_COLOR_GRIS3.getColor());
        this.cbCliente.setColorArrow(Colores.C_COLOR_GRIS3.getColor());
        this.cbCliente.setForeground(Colores.C_COLOR_NEGRO.getColor());
        this.cbCliente.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.cbCliente.setBounds(750, 120, 280, 30);
        this.cbCliente.addItemListener(new ItemListener(){
            @Override
            public void itemStateChanged(ItemEvent e) {
                String ventaN = txtMotivo.getText().trim();
                if(!ventaN.equals("")){
                    if(cbCliente.getSelectedItem()!=null){
                cadena="";
                cadena=cbCliente.getSelectedItem().toString();
                id=cadena.substring(cadena.lastIndexOf(":")+2);
                System.out.println("ex:"+id);
                txtIDProducto.setText(id);
                }
                }
        }
        });
        this.kgPantalla.add(this.cbCliente);
    }

    private void textosConfiguracion() {
        this.txtIDProducto = new JTextField();
        this.txtIDProducto.setBounds(790, 120, 200, 30);
        this.txtIDProducto.setBackground(Colores.C_COLOR_GRIS1.getColor());
        this.txtIDProducto.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
	
	this.txtIDTarjeta = new JLabel();
        this.txtIDTarjeta.setBackground(Colores.C_COLOR_GRIS1.getColor());
        this.txtIDTarjeta.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
        this.txtIDTarjeta.setBounds(100, 220, 200, 30);
        this.txtIDTarjeta.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        this.kgPantalla.add(this.txtIDTarjeta);
	

        this.txtMotivo = new JTextField();
        this.txtMotivo.setBounds(125, 120, 200, 30);
        this.txtMotivo.setBackground(Colores.C_COLOR_GRIS1.getColor());
        this.txtMotivo.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
        this.txtMotivo.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.txtMotivo.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent keyEvent) {

            }

            @Override
            public void keyPressed(KeyEvent keyEvent) {

            }

            @Override
            public void keyReleased(KeyEvent keyEvent) {
                String sId = txtMotivo.getText().trim();
                boolean bValidarPedido = ValidacionesGenerales.validarExistencia(sId, "Venta");
                System.out.println("estado"+bValidarPedido+" "+sId);
                if(bValidarPedido){
                DefaultComboBoxModel dcbmNuevo = CargarDatos.cargarComboBox(sId, "nombreProductoIdProdDevolucion");
                dcbmNuevo.addElement(" ");
                cbCliente.setModel(dcbmNuevo);
                cbCliente.setSelectedIndex(cbCliente.getItemCount() - 1);
                JComponent[] jtMisEntradas = {txtIDCliente,txtIDTarjeta};
                String sIdCliente = txtMotivo.getText().trim();
                int[] iCampos = {3, 6};
                ActualizarCargarDatos.cargarDatos(sIdCliente, 5, jtMisEntradas, iCampos);
                jspCantidad.getEditor().getComponent(0).setEnabled(false);
                }
                if(sId.isBlank()){
                    cbCliente.removeAllItems();
                    txtIDCliente.setText("");
                    txtIDTarjeta.setText("");
                }if(!bValidarPedido){
                    cbCliente.removeAllItems();
                    txtIDCliente.setText("");
                    txtIDTarjeta.setText("");
                }
            }
        });
        this.kgPantalla.add(this.txtMotivo);


        this.txtIDCliente = new JLabel();
        this.txtIDCliente.setBackground(Colores.C_COLOR_GRIS1.getColor());
        this.txtIDCliente.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
        this.txtIDCliente.setBounds(430, 120, 190, 30);
        this.txtIDCliente.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        this.kgPantalla.add(this.txtIDCliente);

    }

    private void labelsConfiguracion() {
        JLabel labelTitutlo = new JLabel("Agregar Nueva Devolucion");
        labelTitutlo.setFont(Fuentes.FUENTE_TITULOS.getFont());
        labelTitutlo.setBounds(350, 20, 350, 30);
        this.kgPantalla.add(labelTitutlo);

        JLabel jlImgTitulo = new JLabel(Imagenes.I_IMAGEN_DEVOLUCION.getImagen());
        jlImgTitulo.setFont(Fuentes.FUENTE_TITULOS.getFont());
        jlImgTitulo.setBounds(280, 10, 64, 64);
        this.kgPantalla.add(jlImgTitulo);

        JLabel jlIDProducto = new JLabel("Producto: ");
        jlIDProducto.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlIDProducto.setBounds(650, 120, 150, 30);
        this.kgPantalla.add(jlIDProducto);
	
	JLabel jlIDTarjeta = new JLabel("ID Tarjeta: ");
        jlIDTarjeta.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlIDTarjeta.setBounds(0, 220, 100, 30);
        this.kgPantalla.add(jlIDTarjeta);
	

        JLabel jlIDProveedor = new JLabel("ID Cliente: ");
        jlIDProveedor.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlIDProveedor.setBounds(340, 120, 130, 30);
        this.kgPantalla.add(jlIDProveedor);


        JLabel jlCantidad = new JLabel("Cantidad: ");
        jlCantidad.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlCantidad.setBounds(0, 430, 130, 30);
        this.kgPantalla.add(jlCantidad);
        
        this.fechaHoy = LocalDate.now();
        String formatoFecha = fechaHoy.format(DateTimeFormatter.ofPattern("dd-MM-YYYY"));
        JLabel labelFechaActual = new JLabel("Fecha:  " + formatoFecha);
        labelFechaActual.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        labelFechaActual.setBounds(400, 220, 250, 30);
        this.kgPantalla.add(labelFechaActual);


        JLabel labelMotivoDevolucion = new JLabel("Motivo de Devolucion:  ");
        labelMotivoDevolucion.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        labelMotivoDevolucion.setBounds(0, 320, 260, 30);
        this.kgPantalla.add(labelMotivoDevolucion);

        JLabel labelVentaNumero = new JLabel("Venta Numero:");
        labelVentaNumero.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        labelVentaNumero.setBounds(0, 120, 240, 30);
        this.kgPantalla.add(labelVentaNumero);
        // Campo de texto


    }

    private void sliderConfiguracion() {
        this.jsCantidad = new JSlider(1, 100, 50);
        this.jsCantidad.setOrientation(SwingConstants.HORIZONTAL);
        this.jsCantidad.setMajorTickSpacing(10);
        this.jsCantidad.setMinorTickSpacing(2);
        this.jsCantidad.setPaintLabels(true);
        this.jsCantidad.setPaintTicks(true);
        this.jsCantidad.setBounds(90, 430, 500, 50);
        this.kgPantalla.add(jsCantidad);
        this.jsCantidad.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent evt) {
                cambiarValorSpinner();
            }
        });
    }

    private void spinnerConfiguracion() {
        this.jspCantidad = new JSpinner();
        this.jspCantidad.setBounds(110, 480, 80, 25);
        this.jspCantidad.setValue(this.jsCantidad.getValue());
        this.jspCantidad.getEditor().getComponent(0).setEnabled(false);
        this.jspCantidad.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        this.jspCantidad.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                cambiarValorSlider();
                int valor = (int) jspCantidad.getValue();
                if (valor <= 0) {
                    jspCantidad.setValue(0);
                } else if (valor >= 100) {
                    jspCantidad.setValue(100);
                }
            }

        });
        this.kgPantalla.add(this.jspCantidad);
    }

    private void botonesConfiguracion() {
        /*----------------------------------------*/
        this.btnAgregarDevolucion = new RSButtonRiple();
        this.btnAgregarDevolucion.setBounds(790, 420, 160, 50);
        this.btnAgregarDevolucion.setBorderPainted(false);
        this.btnAgregarDevolucion.setFocusPainted(false);
        this.btnAgregarDevolucion.setBackground(Colores.C_COLOR_GRIS3.getColor());
        this.btnAgregarDevolucion.setColorHover(Colores.C_COLOR_GRIS2.getColor());
        this.btnAgregarDevolucion.setColorText(Colores.C_COLOR_NEGRO.getColor());
        this.btnAgregarDevolucion.setColorTextHover(Colores.C_COLOR_GRIS4.getColor());
        this.btnAgregarDevolucion.setText("Agregar Devolucion");
        this.btnAgregarDevolucion.setActionCommand("btnDevoluciones");
        this.btnAgregarDevolucion.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                String sVentaNum = txtMotivo.getText().trim();
                String sIdCliente = txtIDCliente.getText().trim();
                String sIdProducto = txtIDProducto.getText().trim();
                String sFechaHoy = fechaHoy.format(DateTimeFormatter.ofPattern("YYYY-MM-dd"));
                String sMotivo = cbMotivo.getSelectedItem().toString();
                String sCantidad = jspCantidad.getValue().toString();
                String sTarjeta = txtIDTarjeta.getText();
                if(sTarjeta==null){
                    boolean bControl = Inserciones.insertarDevolucion(sVentaNum,sIdCliente,sIdProducto,sFechaHoy,sMotivo,sCantidad,"");
                    if (bControl) {
                    JOptionPane.showMessageDialog(null, "Devolucion realizada");
                    cbCliente.removeAllItems();
                    txtIDCliente.setText("");
                    txtIDProducto.setText("");
                    txtIDTarjeta.setText("");
                    txtMotivo.setText("");
                }
                }else{
                boolean bControl = Inserciones.insertarDevolucion(sVentaNum,sIdCliente,sIdProducto,sFechaHoy,sMotivo,sCantidad,sTarjeta);
                if (bControl) {
                    JOptionPane.showMessageDialog(null, "Devolucion realizada");
                    cbCliente.removeAllItems();
                    txtIDCliente.setText("");
                    txtIDProducto.setText("");
                    txtIDTarjeta.setText("");
                    txtMotivo.setText("");
                }
                }
            }
        });
        this.kgPantalla.add(this.btnAgregarDevolucion);

    }


    private void cambiarValorSpinner() {
        this.jspCantidad.setValue(this.jsCantidad.getValue());
    }

    private void cambiarValorSlider() {
        this.jsCantidad.setValue((int) this.jspCantidad.getValue());
    }

    public KGradientPanel getkgPantalla() {
        return this.kgPantalla;
    }
}
