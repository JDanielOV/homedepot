package Ventanas.Pantallas;

import Recursos.Colores;
import Recursos.Fuentes;
import Recursos.Imagenes;
import SQL.CargarDatos;

import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.MatteBorder;

import keeptoo.KGradientPanel;
import rojerusan.RSTableMetro;

/**
 * @author cristina 06-11-2021
 * Ochoa Fecha: 08/11/2021
 * Modificacion: cambie el metodo buscarTabla
 */
public class PantFacturaPedVer {
    private RSTableMetro rsTablaFactura;
    private KGradientPanel kgPantalla;
    private JTextField txtNombre;

    public PantFacturaPedVer() {
        this.panelConfiguracion();
        this.labelsConfiguracion();
        this.textosConfiguracion();
        this.tablaConfiguracion();
    }

    private void panelConfiguracion() {
        this.kgPantalla = new KGradientPanel();
        this.kgPantalla = new KGradientPanel();
        this.kgPantalla.setSize(1090, 550);
        this.kgPantalla.setBackground(Colores.C_COLOR_TRANSPARENTE.getColor());
        this.kgPantalla.setkEndColor(Colores.C_COLOR_BLANCO.getColor());
        this.kgPantalla.setkStartColor(Colores.C_COLOR_BLANCO.getColor());
        this.kgPantalla.setkBorderRadius(40);
        this.kgPantalla.setLayout(null);
    }

    private void textosConfiguracion() {
        this.txtNombre = new JTextField();
        this.txtNombre.setBounds(470, 150, 200, 30);
        this.txtNombre.setBackground(Colores.C_COLOR_GRIS1.getColor());
        this.txtNombre.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
        this.txtNombre.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.txtNombre.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyPressed(KeyEvent e) {
            }

            @Override
            public void keyReleased(KeyEvent e) {
                CargarDatos.buscarTabla(rsTablaFactura, txtNombre.getText(), "FacturaPedido", "FacturaPedido");
                System.out.println("d");
            }
        });
        this.kgPantalla.add(this.txtNombre);
    }

    private void labelsConfiguracion() {
        JLabel labelTitulo = new JLabel("Lista De Facturas Pedidos");
        labelTitulo.setFont(Fuentes.FUENTE_TITULOS.getFont());
        labelTitulo.setBounds(360, 20, 380, 30);
        this.kgPantalla.add(labelTitulo);

        JLabel jlImgTitulo = new JLabel(Imagenes.I_IMAGEN_PEDIDO.getImagen());
        jlImgTitulo.setFont(Fuentes.FUENTE_TITULOS.getFont());
        jlImgTitulo.setBounds(280, 10, 64, 64);
        this.kgPantalla.add(jlImgTitulo);

        JLabel jlNombre = new JLabel("Buscar ID Pedido: ");
        jlNombre.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlNombre.setBounds(290, 150, 190, 30);
        this.kgPantalla.add(jlNombre);
    }

    private void tablaConfiguracion() {

        this.rsTablaFactura = new RSTableMetro();
        this.rsTablaFactura.setAltoHead(60);
        this.rsTablaFactura.setRowHeight(40);
        this.rsTablaFactura.setGrosorBordeFilas(0);
        this.rsTablaFactura.setColorBackgoundHead(Colores.C_COLOR_GRIS4.getColor());
        this.rsTablaFactura.setColorFilasBackgound1(Colores.C_COLOR_GRIS1.getColor());
        this.rsTablaFactura.setColorFilasBackgound2(Colores.C_COLOR_GRIS2.getColor());
        this.rsTablaFactura.setColorFilasForeground1(Colores.C_COLOR_NEGRO.getColor());
        this.rsTablaFactura.setColorFilasForeground2(Colores.C_COLOR_NEGRO.getColor());
        this.rsTablaFactura.setColorSelBackgound(Colores.C_COLOR_GRIS3.getColor());
        this.rsTablaFactura.setColorBordeFilas(Colores.C_COLOR_GRIS1.getColor());
        this.rsTablaFactura.setFuenteFilas(Fuentes.FUENTE_TEXTOS.getFont());
        this.rsTablaFactura.setFuenteHead(Fuentes.FUENTE_TEXTOS.getFont());
        this.rsTablaFactura.setPreferredScrollableViewportSize(new Dimension(250, 100));
        JScrollPane scrollPane = new JScrollPane(this.rsTablaFactura);
        scrollPane.setBounds(50, 250, 910, 250);
        CargarDatos.rellenarTabla(this.rsTablaFactura, "FacturaPedido");
        this.kgPantalla.add(scrollPane);
    }

    public KGradientPanel getkgPantalla() {
        return this.kgPantalla;
    }
}
