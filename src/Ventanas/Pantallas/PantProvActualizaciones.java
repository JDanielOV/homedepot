package Ventanas.Pantallas;

import Recursos.Colores;
import Recursos.Fuentes;
import Recursos.Imagenes;
import SQL.Actualizar;
import SQL.ActualizarCargarDatos;
import SQL.CargarDatos;
import keeptoo.KGradientPanel;
import rojeru_san.RSButtonRiple;


import javax.swing.*;
import javax.swing.border.MatteBorder;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import rojerusan.RSComboMetro;

/**
 * @author Daniel Ochoa
 * Ochoa Fecha: 12/11/2021
 * Modificaciones: puse el cmapo de empresa 
 */
public class PantProvActualizaciones {
    private KGradientPanel kgPantalla;
    private JTextField txtNombre, txtApePaterno, txtApeMaterno, txtCorreoE, txtTelefono, txtIDProveedor, txtEmpresa;
    private String sNombre, sApellidoP, sApellidoM;
     private RSComboMetro rscmIDProveedor;
    private RSButtonRiple btnCrearUsuario;

    public PantProvActualizaciones() {
        this.panelConfiguracion();
        this.labelsConfiguracion();
        this.textosConfiguracion();
        this.botonesConfiguracion();
	this.comboBoxConfiguracion();
    }

    private void panelConfiguracion() {
        this.kgPantalla = new KGradientPanel();
        this.kgPantalla = new KGradientPanel();
        this.kgPantalla.setSize(1090, 550);
        this.kgPantalla.setBackground(Colores.C_COLOR_TRANSPARENTE.getColor());
        this.kgPantalla.setkEndColor(Colores.C_COLOR_BLANCO.getColor());
        this.kgPantalla.setkStartColor(Colores.C_COLOR_BLANCO.getColor());
        this.kgPantalla.setkBorderRadius(40);
        this.kgPantalla.setLayout(null);
    }

    private void textosConfiguracion() {
        this.txtNombre = new JTextField();
        this.txtNombre.setBounds(430, 170, 200, 30);
        this.txtNombre.setBackground(Colores.C_COLOR_GRIS1.getColor());
        this.txtNombre.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
        this.txtNombre.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.txtNombre.setEnabled(false);
        this.kgPantalla.add(this.txtNombre);


        this.txtApePaterno = new JTextField();
        this.txtApePaterno.setBounds(770, 170, 200, 30);
        this.txtApePaterno.setBackground(Colores.C_COLOR_GRIS1.getColor());
        this.txtApePaterno.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
        this.txtApePaterno.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.txtApePaterno.setEnabled(false);
        this.kgPantalla.add(this.txtApePaterno);


        this.txtApeMaterno = new JTextField();
        this.txtApeMaterno.setBounds(110, 300, 200, 30);
        this.txtApeMaterno.setBackground(Colores.C_COLOR_GRIS1.getColor());
        this.txtApeMaterno.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
        this.txtApeMaterno.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.txtApeMaterno.setEnabled(false);
        this.kgPantalla.add(this.txtApeMaterno);


        this.txtCorreoE = new JTextField();
        this.txtCorreoE.setBounds(420, 300, 200, 30);
        this.txtCorreoE.setBackground(Colores.C_COLOR_GRIS1.getColor());
        this.txtCorreoE.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
        this.txtCorreoE.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.txtCorreoE.setEnabled(false);
        this.kgPantalla.add(this.txtCorreoE);


        this.txtTelefono = new JTextField();
        this.txtTelefono.setBounds(760, 300, 200, 30);
        this.txtTelefono.setBackground(Colores.C_COLOR_GRIS1.getColor());
        this.txtTelefono.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
        this.txtTelefono.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.txtTelefono.setEnabled(false);
        this.kgPantalla.add(this.txtTelefono);


        this.txtEmpresa = new JTextField();
        this.txtEmpresa.setBounds(110, 450, 200, 30);
        this.txtEmpresa.setBackground(Colores.C_COLOR_GRIS1.getColor());
        this.txtEmpresa.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
        this.txtEmpresa.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.txtEmpresa.setEnabled(false);
        this.kgPantalla.add(this.txtEmpresa);


        
    }

    private void labelsConfiguracion() {
        JLabel labelTitutlo = new JLabel("Actualizar Proveedor");
        labelTitutlo.setFont(Fuentes.FUENTE_TITULOS.getFont());
        labelTitutlo.setBounds(360, 20, 350, 30);
        this.kgPantalla.add(labelTitutlo);

        JLabel jlImgTitulo = new JLabel(Imagenes.I_IMAGEN_PROVEEDOR.getImagen());
        jlImgTitulo.setFont(Fuentes.FUENTE_TITULOS.getFont());
        jlImgTitulo.setBounds(280, 10, 64, 64);
        this.kgPantalla.add(jlImgTitulo);

        JLabel jlNombreProveedor = new JLabel("Nombre: ");
        jlNombreProveedor.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlNombreProveedor.setBounds(340, 170, 100, 30);
        this.kgPantalla.add(jlNombreProveedor);

        JLabel jApellidoP = new JLabel("Apellido P: ");
        jApellidoP.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jApellidoP.setBounds(660, 170, 130, 30);
        this.kgPantalla.add(jApellidoP);

        JLabel jApellidoM = new JLabel("Apellido M: ");
        jApellidoM.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jApellidoM.setBounds(0, 300, 130, 30);
        this.kgPantalla.add(jApellidoM);

        JLabel jlCorreoE = new JLabel("Email: ");
        jlCorreoE.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlCorreoE.setBounds(340, 300, 130, 30);
        this.kgPantalla.add(jlCorreoE);

        JLabel jlTelefono = new JLabel("Telefono: ");
        jlTelefono.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlTelefono.setBounds(670, 300, 130, 30);
        this.kgPantalla.add(jlTelefono);

        JLabel jlEmpresa = new JLabel("Empresa: ");
        jlEmpresa.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlEmpresa.setBounds(0, 450, 130, 30);
        this.kgPantalla.add(jlEmpresa);

        JLabel jlProveedorID = new JLabel("Proveedor: ");
        jlProveedorID.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlProveedorID.setBounds(0, 120, 200, 30);
        this.kgPantalla.add(jlProveedorID);
    }


    private void botonesConfiguracion() {
        this.btnCrearUsuario = new RSButtonRiple();
        this.btnCrearUsuario.setBounds(400, 450, 120, 50);
        this.btnCrearUsuario.setBorderPainted(false);
        this.btnCrearUsuario.setFocusPainted(false);
        this.btnCrearUsuario.setBackground(Colores.C_COLOR_GRIS3.getColor());
        this.btnCrearUsuario.setColorHover(Colores.C_COLOR_GRIS2.getColor());
        this.btnCrearUsuario.setColorText(Colores.C_COLOR_NEGRO.getColor());
        this.btnCrearUsuario.setColorTextHover(Colores.C_COLOR_GRIS4.getColor());
        this.btnCrearUsuario.setText("Actualizar");
        this.btnCrearUsuario.setActionCommand("btnProveedores");
        this.btnCrearUsuario.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                String sProv = rscmIDProveedor.getSelectedItem() == null ? " " : rscmIDProveedor.getSelectedItem().toString();
                ;
                int iDosPuntosEspaciop = sProv.indexOf(": ");
                String sProveedor = (sProv.contains(": ")) ? (sProv.substring(iDosPuntosEspaciop + 2)) : ("");
                if (txtNombre.isEnabled()) {
                    String[] sParametros = {
                            txtNombre.getText().trim(),
                            txtApePaterno.getText().trim(),
                            txtApeMaterno.getText().trim(),
                            txtCorreoE.getText().trim(),
                            txtTelefono.getText().trim(),
                            txtEmpresa.getText().trim(),
                            sNombre,
                            sApellidoP,
                            sApellidoM,
                            sProveedor
                    };
                    boolean bInsercionCliente = Actualizar.actualizarProveedor(sParametros);
                    if (bInsercionCliente) {
                        JOptionPane.showMessageDialog(null, "Proveedor modificado correctamente");
                        txtNombre.setText("");
                        txtApePaterno.setText("");
                        txtApeMaterno.setText("");
                        txtCorreoE.setText("");
                        txtTelefono.setText("");
                        txtEmpresa.setText("");
                        rscmIDProveedor.removeAllItems();
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "No a selecionado un cliente o no existe");
                }
            }
        });
        this.kgPantalla.add(this.btnCrearUsuario);
    }

    public KGradientPanel getkgPantalla() {
        return this.kgPantalla;
    }



private void comboBoxConfiguracion() {

	this.rscmIDProveedor = new RSComboMetro();
	this.rscmIDProveedor.setColorFondo(Colores.C_COLOR_GRIS4.getColor());
	this.rscmIDProveedor.setColorBorde(Colores.C_COLOR_GRIS3.getColor());
	this.rscmIDProveedor.setColorArrow(Colores.C_COLOR_GRIS3.getColor());
	this.rscmIDProveedor.setForeground(Colores.C_COLOR_NEGRO.getColor());
	this.rscmIDProveedor.setFont(Fuentes.FUENTE_TEXTOS.getFont());
	this.rscmIDProveedor.setBounds(130, 120, 350, 30);
	this.rscmIDProveedor.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
	this.rscmIDProveedor.setEditable(true);
	CargarDatos.comboBoxPonerDatos(this.rscmIDProveedor, "nombreProveedor");
        this.rscmIDProveedor.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String sProv = rscmIDProveedor.getSelectedItem() == null ? " " : rscmIDProveedor.getSelectedItem().toString();
                ;
                int iDosPuntosEspaciop = sProv.indexOf(": ");
                String sProveedor = (sProv.contains(": ")) ? (sProv.substring(iDosPuntosEspaciop + 2)) : ("");
                JComponent[] jtMisEntradas = {txtNombre, txtApePaterno, txtApeMaterno, txtCorreoE, txtTelefono, txtEmpresa};
                int[] iCampos = {2, 3, 4, 5};
                ActualizarCargarDatos.cargarDatos(sProveedor, 2, jtMisEntradas, iCampos);
                sNombre = txtNombre.getText();
                sApellidoP = txtApePaterno.getText();
                sApellidoM = txtApeMaterno.getText();
            }
        });
	this.kgPantalla.add(this.rscmIDProveedor);
}
}