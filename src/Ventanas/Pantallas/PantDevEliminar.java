package Ventanas.Pantallas;

import Recursos.Colores;
import Recursos.Fuentes;
import Recursos.Imagenes;
import SQL.CargarDatos;
import SQL.Eliminaciones;
import keeptoo.KGradientPanel;
import rojeru_san.RSButtonRiple;
import rojerusan.RSTableMetro;

import javax.swing.*;
import javax.swing.border.MatteBorder;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.*;

/**
 * @author cristina 05-11-2021
 * Ochoa Fecha: 08/11/2021
 * Modificacion: cambie el metodo buscarTabla
 */

//ID Cliente fue modificado a "Cliente"

public class PantDevEliminar {
    private RSTableMetro rsTablaDevoluciones;
    private KGradientPanel kgPantalla;
    private JTextField txtID;
    private JLabel txtIDSelect;
    private String sID,sIdProduto,sCantidad,sNVenta,sIdCliente,sTarjeta;
    private RSButtonRiple btnEliminarProductos;
    private final String[] STITULOS_TABLA = {"ID Devolucion", "Numero Venta", "ID Cliente", "ID Producto", "Devolucion Fecha", "Devolucion Motivo", "Devolucion Cantidad"};
    private Object[][] oContenidoTabla = {

    };
    private RSButtonRiple btnEliminar;

    public PantDevEliminar() {
        this.panelConfiguracion();
        this.labelsConfiguracion();
        this.textosConfiguracion();
        this.botonesConfiguracion();
        this.tablaConfiguracion();
    }

    private void panelConfiguracion() {
        this.kgPantalla = new KGradientPanel();
        this.kgPantalla = new KGradientPanel();
        this.kgPantalla.setSize(1090, 550);
        this.kgPantalla.setBackground(Colores.C_COLOR_TRANSPARENTE.getColor());
        this.kgPantalla.setkEndColor(Colores.C_COLOR_BLANCO.getColor());
        this.kgPantalla.setkStartColor(Colores.C_COLOR_BLANCO.getColor());
        this.kgPantalla.setkBorderRadius(40);
        this.kgPantalla.setLayout(null);
    }

    private void botonesConfiguracion() {
        /*----------------------------------------*/
        this.btnEliminar = new RSButtonRiple();
        this.btnEliminar.setBounds(700, 100, 120, 50);
        this.btnEliminar.setBorderPainted(false);
        this.btnEliminar.setFocusPainted(false);
        this.btnEliminar.setBackground(Colores.C_COLOR_GRIS3.getColor());
        this.btnEliminar.setColorHover(Colores.C_COLOR_GRIS2.getColor());
        this.btnEliminar.setColorText(Colores.C_COLOR_NEGRO.getColor());
        this.btnEliminar.setColorTextHover(Colores.C_COLOR_GRIS4.getColor());
        this.btnEliminar.setText("Eliminar");
        this.btnEliminar.setActionCommand("btnEli");
        this.btnEliminar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (seleccionProveedor() == true) {
                    boolean bEstado = Eliminaciones.eliminarDevolucion(sID.trim(),sIdProduto.trim(),sCantidad.trim(),sNVenta.trim(),sIdCliente.trim(),sTarjeta.trim());
                    if (bEstado) {
                        JOptionPane.showMessageDialog(null, "Devolucion eliminada");
                        txtID.setText("");
                        CargarDatos.buscarTabla(rsTablaDevoluciones, txtID.getText(), "Devolucion", "Devolucion");
                        txtIDSelect.setText("Num de Devolucion seleccionado: ");
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "No ha selecionado Devolucion");
                }
            }
        });
        this.kgPantalla.add(this.btnEliminar);


    }

    private void textosConfiguracion() {
        this.txtID = new JTextField();
        this.txtID.setBounds(410, 100, 200, 30);
        this.txtID.setBackground(Colores.C_COLOR_GRIS1.getColor());
        this.txtID.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
        this.txtID.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.txtID.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyPressed(KeyEvent e) {
            }

            @Override
            public void keyReleased(KeyEvent e) {
                CargarDatos.buscarTabla(rsTablaDevoluciones, txtID.getText(), "Devolucion", "Devolucion");
                txtIDSelect.setText("Num de Devolucion seleccionado: ");
                sID = "";
            }
        });
        this.kgPantalla.add(this.txtID);
    }

    private void labelsConfiguracion() {
        JLabel labelTitutlo = new JLabel("Eliminar Devolucion");
        labelTitutlo.setFont(Fuentes.FUENTE_TITULOS.getFont());
        labelTitutlo.setBounds(350, 20, 400, 30);
        this.kgPantalla.add(labelTitutlo);

        JLabel jlImgTitulo = new JLabel(Imagenes.I_IMAGEN_DEVOLUCION.getImagen());
        jlImgTitulo.setFont(Fuentes.FUENTE_TITULOS.getFont());
        jlImgTitulo.setBounds(280, 10, 64, 64);
        this.kgPantalla.add(jlImgTitulo);

        JLabel jlIDProveedor = new JLabel("Buscar ID: ");
        jlIDProveedor.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlIDProveedor.setBounds(300, 100, 130, 30);
        this.kgPantalla.add(jlIDProveedor);

        this.txtIDSelect = new JLabel("Num de Devolucion seleccionado: ");
        this.txtIDSelect.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        this.txtIDSelect.setBounds(300, 180, 500, 70);
        this.kgPantalla.add(this.txtIDSelect);
    }


    private boolean seleccionProveedor() {
        int renglon = this.rsTablaDevoluciones.getSelectedRow();
        if (renglon != -1) {
            String ID = this.rsTablaDevoluciones.getValueAt(renglon, 0) + "";
            String IdProduto =this.rsTablaDevoluciones.getValueAt(renglon, 3) + "";
            String Cantidad=this.rsTablaDevoluciones.getValueAt(renglon, 7) + "";
            String NVenta=this.rsTablaDevoluciones.getValueAt(renglon, 1) + "";
            String IdCliente=this.rsTablaDevoluciones.getValueAt(renglon, 2) + "";
            String Tarjeta=this.rsTablaDevoluciones.getValueAt(renglon, 4) + "";
            this.sID = ID;
            this.sIdCliente=IdCliente;
            this.sIdProduto=IdProduto;
            this.sNVenta=NVenta;
            this.sTarjeta=Tarjeta;
            this.sCantidad=Cantidad;
            this.txtIDSelect.setText("Num de Devolucion seleccionado: " + ID);
        } else {
            System.out.println("No selecciono proveedor");
            this.sID = "";
            return false;
        }
        return true;
    }

    private void tablaConfiguracion() {
        DefaultTableModel dtmTabla = new DefaultTableModel(this.oContenidoTabla, this.STITULOS_TABLA);
        this.rsTablaDevoluciones = new RSTableMetro();
        this.rsTablaDevoluciones.setModel(dtmTabla);
        this.rsTablaDevoluciones.setAltoHead(60);
        this.rsTablaDevoluciones.setRowHeight(40);
        this.rsTablaDevoluciones.setGrosorBordeFilas(0);
        this.rsTablaDevoluciones.setColorBackgoundHead(Colores.C_COLOR_GRIS4.getColor());
        this.rsTablaDevoluciones.setColorFilasBackgound1(Colores.C_COLOR_GRIS1.getColor());
        this.rsTablaDevoluciones.setColorFilasBackgound2(Colores.C_COLOR_GRIS2.getColor());
        this.rsTablaDevoluciones.setColorFilasForeground1(Colores.C_COLOR_NEGRO.getColor());
        this.rsTablaDevoluciones.setColorFilasForeground2(Colores.C_COLOR_NEGRO.getColor());
        this.rsTablaDevoluciones.setColorSelBackgound(Colores.C_COLOR_GRIS3.getColor());
        this.rsTablaDevoluciones.setColorBordeFilas(Colores.C_COLOR_GRIS1.getColor());
        this.rsTablaDevoluciones.setFuenteFilas(Fuentes.FUENTE_TEXTOS.getFont());
        this.rsTablaDevoluciones.setFuenteHead(Fuentes.FUENTE_TEXTOS.getFont());
        this.rsTablaDevoluciones.setPreferredScrollableViewportSize(new Dimension(250, 100));
        JScrollPane scrollPane = new JScrollPane(this.rsTablaDevoluciones);
        scrollPane.setBounds(10, 250, 1000, 250);
        CargarDatos.rellenarTabla(this.rsTablaDevoluciones, "Devolucion");
        this.rsTablaDevoluciones.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                seleccionProveedor();
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }

        });
        this.kgPantalla.add(scrollPane);
    }


    public KGradientPanel getkgPantalla() {
        return this.kgPantalla;
    }
}

