package Ventanas.Pantallas;

import Recursos.Colores;
import Recursos.Fuentes;
import Recursos.Imagenes;
import SQL.Actualizar;
import SQL.ActualizarCargarDatos;
import SQL.CargarDatos;
import keeptoo.KGradientPanel;
import rojeru_san.RSButtonRiple;
import rojerusan.RSDateChooser;

import javax.swing.*;
import javax.swing.border.MatteBorder;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import rojerusan.RSComboMetro;
import rojerusan.RSPasswordTextPlaceHolder;

/**
 * @author cristina
 * Ochoa 21-11-2021
 * Modificaciones: limpie los campos rfc y contrasena
 */
public class PantTraActualizaciones {
    private KGradientPanel kgPantalla;
    private JTextField txtNombre, txtApePaterno, txtApeMaterno, txtEmail, txtTelefono, txtDireccion;
    private RSDateChooser rdFechas;
    private RSComboMetro cbCargos, rscmTrabajador;
    private JSlider slSueldo;
    private JSpinner Sueldo;
    private RSButtonRiple btnCrearTrabajador;
    private String sNombre, sApellidoP, sApellidoM;
    private RSPasswordTextPlaceHolder rsContrasena, txtContrasenaNew;
    private String sRFCTrabajador;

    public PantTraActualizaciones() {
        this.panelConfiguracion();
        this.labelsConfiguracion();
        this.textosConfiguracion();
        this.dateChooserConfiguracion();
        this.comboBoxConfiguracion();
        this.sliderConfiguracion();
        this.spinnerConfiguracion();
        this.botonesConfiguracion();
    }

    private void panelConfiguracion() {
        this.kgPantalla = new KGradientPanel();
        this.kgPantalla = new KGradientPanel();
        this.kgPantalla.setSize(1090, 550);
        this.kgPantalla.setBackground(Colores.C_COLOR_TRANSPARENTE.getColor());
        this.kgPantalla.setkEndColor(Colores.C_COLOR_BLANCO.getColor());
        this.kgPantalla.setkStartColor(Colores.C_COLOR_BLANCO.getColor());
        this.kgPantalla.setkBorderRadius(40);
        this.kgPantalla.setLayout(null);
    }

    private void textosConfiguracion() {
       /* this.txtRfc = new JTextField();
        this.txtRfc.setBounds(60, 100, 200, 30);
        this.txtRfc.setBackground(Colores.C_COLOR_GRIS1.getColor());
        this.txtRfc.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
        this.txtRfc.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.txtRfc.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent keyEvent) {

            }

            @Override
            public void keyPressed(KeyEvent keyEvent) {

            }

            @Override
            public void keyReleased(KeyEvent keyEvent) {
                JComponent[] jtMisEntradas = {txtNombre, txtApePaterno, txtApeMaterno, txtEmail, txtTelefono, rdFechas, txtDireccion, cbCargos, slSueldo, Sueldo};
                String sIdCliente = txtRfc.getText();
                int[] iCampos = {2, 3, 4, 5, 6, 7, 8, 8};
                ActualizarCargarDatos.cargarDatos(sIdCliente, 1, jtMisEntradas, iCampos);
                Sueldo.getEditor().getComponent(0).setEnabled(false);
                sNombre = txtNombre.getText();
                sApellidoP = txtApePaterno.getText();
                sApellidoM = txtApeMaterno.getText();
                if (txtNombre.isEnabled()) {
                    rsContrasena.setEnabled(true);
                    rsContrasena.setText("");
                    txtContrasenaNew.setEnabled(true);
                    txtContrasenaNew.setText("");
                } else {
                    rsContrasena.setEnabled(false);
                    rsContrasena.setText("");
                    txtContrasenaNew.setEnabled(false);
                    txtContrasenaNew.setText("");
                }
            }
        });
        this.kgPantalla.add(this.txtRfc);*/

        this.txtNombre = new JTextField();
        this.txtNombre.setBounds(390, 100, 200, 30);
        this.txtNombre.setBackground(Colores.C_COLOR_GRIS1.getColor());
        this.txtNombre.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
        this.txtNombre.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.txtNombre.setEnabled(false);
        this.kgPantalla.add(this.txtNombre);

        this.txtApePaterno = new JTextField();
        this.txtApePaterno.setBounds(770, 100, 200, 30);
        this.txtApePaterno.setBackground(Colores.C_COLOR_GRIS1.getColor());
        this.txtApePaterno.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
        this.txtApePaterno.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.txtApePaterno.setEnabled(false);
        this.kgPantalla.add(this.txtApePaterno);

        this.txtApeMaterno = new JTextField();
        this.txtApeMaterno.setBounds(770, 200, 200, 30);
        this.txtApeMaterno.setBackground(Colores.C_COLOR_GRIS1.getColor());
        this.txtApeMaterno.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
        this.txtApeMaterno.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.txtApeMaterno.setEnabled(false);
        this.kgPantalla.add(this.txtApeMaterno);

        this.txtEmail = new JTextField();
        this.txtEmail.setBounds(70, 200, 200, 30);
        this.txtEmail.setBackground(Colores.C_COLOR_GRIS1.getColor());
        this.txtEmail.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
        this.txtEmail.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.txtEmail.setEnabled(false);
        this.kgPantalla.add(this.txtEmail);

        this.txtTelefono = new JTextField();
        this.txtTelefono.setBounds(390, 200, 200, 30);
        this.txtTelefono.setBackground(Colores.C_COLOR_GRIS1.getColor());
        this.txtTelefono.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
        this.txtTelefono.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.txtTelefono.setEnabled(false);
        this.kgPantalla.add(this.txtTelefono);

        this.txtDireccion = new JTextField();
        this.txtDireccion.setBounds(500, 300, 200, 30);
        this.txtDireccion.setBackground(Colores.C_COLOR_GRIS1.getColor());
        this.txtDireccion.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
        this.txtDireccion.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.txtDireccion.setEnabled(false);
        this.kgPantalla.add(this.txtDireccion);

        this.rsContrasena = new RSPasswordTextPlaceHolder();
        this.rsContrasena.setBounds(250, 480, 200, 30);
        this.rsContrasena.setBackground(Colores.C_COLOR_GRIS1.getColor());
        this.rsContrasena.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
        this.rsContrasena.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.rsContrasena.setForeground(Colores.C_COLOR_NEGRO.getColor());
        this.rsContrasena.setEnabled(false);
        this.kgPantalla.add(this.rsContrasena);

        this.txtContrasenaNew = new RSPasswordTextPlaceHolder();
        this.txtContrasenaNew.setBounds(690, 480, 200, 30);
        this.txtContrasenaNew.setBackground(Colores.C_COLOR_GRIS1.getColor());
        this.txtContrasenaNew.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
        this.txtContrasenaNew.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.txtContrasenaNew.setForeground(Colores.C_COLOR_NEGRO.getColor());
        this.txtContrasenaNew.setEnabled(false);
        this.kgPantalla.add(this.txtContrasenaNew);
    }

    private void labelsConfiguracion() {
        JLabel labelTitutlo = new JLabel("Actualizar Trabajador");
        labelTitutlo.setFont(Fuentes.FUENTE_TITULOS.getFont());
        labelTitutlo.setBounds(400, 20, 350, 30);
        this.kgPantalla.add(labelTitutlo);

        JLabel jlImgTitulo = new JLabel(Imagenes.I_IMAGEN_TRABAJADOR.getImagen());
        jlImgTitulo.setFont(Fuentes.FUENTE_TITULOS.getFont());
        jlImgTitulo.setBounds(320, 10, 64, 64);
        this.kgPantalla.add(jlImgTitulo);

        JLabel jlRfcTrabajador = new JLabel("Trabajador: ");
        jlRfcTrabajador.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlRfcTrabajador.setBounds(0, 70, 150, 30);
        this.kgPantalla.add(jlRfcTrabajador);

        JLabel jlNombreTrabajador = new JLabel("Nombre: ");
        jlNombreTrabajador.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlNombreTrabajador.setBounds(300, 100, 100, 30);
        this.kgPantalla.add(jlNombreTrabajador);

        JLabel jlApePTrabajador = new JLabel("Apellido P: ");
        jlApePTrabajador.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlApePTrabajador.setBounds(620, 100, 100, 30);
        this.kgPantalla.add(jlApePTrabajador);

        JLabel jlApeMTrabajador = new JLabel("Apellido M: ");
        jlApeMTrabajador.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlApeMTrabajador.setBounds(620, 200, 100, 30);
        this.kgPantalla.add(jlApeMTrabajador);

        JLabel jlEmailTrabajador = new JLabel("Email: ");
        jlEmailTrabajador.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlEmailTrabajador.setBounds(0, 200, 100, 30);
        this.kgPantalla.add(jlEmailTrabajador);

        JLabel jlTelTrabajador = new JLabel("Telefono: ");
        jlTelTrabajador.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlTelTrabajador.setBounds(300, 200, 100, 30);
        this.kgPantalla.add(jlTelTrabajador);

        JLabel jlFechaNacTrabajador = new JLabel("Fecha nacimiento: ");
        jlFechaNacTrabajador.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlFechaNacTrabajador.setBounds(0, 300, 190, 30);
        this.kgPantalla.add(jlFechaNacTrabajador);

        JLabel jlDireccionTrabajador = new JLabel("Direccion: ");
        jlDireccionTrabajador.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlDireccionTrabajador.setBounds(400, 300, 190, 30);
        this.kgPantalla.add(jlDireccionTrabajador);

        JLabel jlCargoTrabajador = new JLabel("Cargo: ");
        jlCargoTrabajador.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlCargoTrabajador.setBounds(710, 300, 190, 30);
        this.kgPantalla.add(jlCargoTrabajador);

        JLabel jlSueldoTrabajador = new JLabel("Sueldo: ");
        jlSueldoTrabajador.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlSueldoTrabajador.setBounds(0, 390, 100, 30);
        this.kgPantalla.add(jlSueldoTrabajador);

        JLabel jlContraseñaTrabajador = new JLabel("Contraseña: ");
        jlContraseñaTrabajador.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlContraseñaTrabajador.setBounds(130, 480, 150, 30);
        this.kgPantalla.add(jlContraseñaTrabajador);

        JLabel jlContraseñaNuevaTrabajador = new JLabel("Contraseña Nueva: ");
        jlContraseñaNuevaTrabajador.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlContraseñaNuevaTrabajador.setBounds(470, 480, 250, 30);
        this.kgPantalla.add(jlContraseñaNuevaTrabajador);
    }

    private void dateChooserConfiguracion() {
        this.rdFechas = new RSDateChooser();
        this.rdFechas.setColorBackground(Colores.C_COLOR_GRIS3.getColor());
        this.rdFechas.setColorButtonHover(Colores.C_COLOR_GRIS3.getColor());
        this.rdFechas.setColorTextDiaActual(Colores.C_COLOR_NEGRO.getColor());
        this.rdFechas.setColorForeground(Colores.C_COLOR_NEGRO.getColor());
        this.rdFechas.setFormatoFecha("YYYY-MM-dd");
        this.rdFechas.setFuente(Fuentes.FUENTE_TEXTOS.getFont());
        this.rdFechas.setBounds(190, 300, 180, 30);
        this.rdFechas.setVisible(false);
        this.kgPantalla.add(rdFechas);
    }

    private void comboBoxConfiguracion() {
        this.cbCargos = new RSComboMetro();
        this.cbCargos.addItem("Cajero");
        this.cbCargos.addItem("Almacenista");
        this.cbCargos.addItem("Atencion a clientes");
        this.cbCargos.addItem("Gerente");
        this.cbCargos.addItem("Oficinista");
        this.cbCargos.setSelectedIndex(2);
        this.cbCargos.setColorFondo(Colores.C_COLOR_GRIS2.getColor());
        this.cbCargos.setColorBorde(Colores.C_COLOR_GRIS3.getColor());
        this.cbCargos.setColorArrow(Colores.C_COLOR_GRIS3.getColor());
        this.cbCargos.setForeground(Colores.C_COLOR_NEGRO.getColor());
        this.cbCargos.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.cbCargos.setBounds(780, 300, 190, 30);
        this.cbCargos.setEnabled(false);
        this.kgPantalla.add(cbCargos);

        this.rscmTrabajador = new RSComboMetro();
        this.rscmTrabajador.setColorFondo(Colores.C_COLOR_GRIS4.getColor());
        this.rscmTrabajador.setColorBorde(Colores.C_COLOR_GRIS3.getColor());
        this.rscmTrabajador.setColorArrow(Colores.C_COLOR_GRIS3.getColor());
        this.rscmTrabajador.setForeground(Colores.C_COLOR_NEGRO.getColor());
        this.rscmTrabajador.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.rscmTrabajador.setBounds(0, 100, 300, 30);
        this.rscmTrabajador.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
        this.rscmTrabajador.setEditable(true);
        CargarDatos.comboBoxPonerDatos(this.rscmTrabajador, "nombreTrabajadorRFC");
        this.rscmTrabajador.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JComponent[] jtMisEntradas = {txtNombre, txtApePaterno, txtApeMaterno, txtEmail, txtTelefono, rdFechas, txtDireccion, cbCargos, slSueldo, Sueldo};
                String sRFCNombreTrabajador = rscmTrabajador.getSelectedItem() == null ? " " : rscmTrabajador.getSelectedItem().toString();
                int iPosicionPatronRFC = sRFCNombreTrabajador.lastIndexOf("RFC: ");
                String sIdCliente = sRFCNombreTrabajador.contains("RFC: ") ? (sRFCNombreTrabajador.substring(iPosicionPatronRFC + 5)) : (" ");
                sRFCTrabajador = sIdCliente;
                int[] iCampos = {2, 3, 4, 5, 6, 7, 8, 8};
                ActualizarCargarDatos.cargarDatos(sIdCliente, 1, jtMisEntradas, iCampos);
                Sueldo.getEditor().getComponent(0).setEnabled(false);
                sNombre = txtNombre.getText();
                sApellidoP = txtApePaterno.getText();
                sApellidoM = txtApeMaterno.getText();
                if (txtNombre.isEnabled()) {
                    rsContrasena.setEnabled(true);
                    rsContrasena.setText("");
                    txtContrasenaNew.setEnabled(true);
                    txtContrasenaNew.setText("");
                } else {
                    rsContrasena.setEnabled(false);
                    rsContrasena.setText("");
                    txtContrasenaNew.setEnabled(false);
                    txtContrasenaNew.setText("");
                }
            }
        });
        this.kgPantalla.add(this.rscmTrabajador);
    }

    private void spinnerConfiguracion() {
        this.Sueldo = new JSpinner();
        this.Sueldo.setBounds(120, 440, 80, 25);
        this.Sueldo.setValue(this.slSueldo.getValue());
        this.Sueldo.setEnabled(false);
        this.Sueldo.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        this.Sueldo.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                cambiarValorSlider();
                int valor = (int) Sueldo.getValue();
                if (valor <= 2000) {
                    Sueldo.setValue(2000);
                } else if (valor >= 30000) {
                    Sueldo.setValue(30000);
                }
            }

        });
        this.kgPantalla.add(this.Sueldo);
    }

    private void cambiarValorSlider() {
        this.slSueldo.setValue((int) this.Sueldo.getValue());
    }

    private void sliderConfiguracion() {
        this.slSueldo = new JSlider(2000, 30000, 15000);
        this.slSueldo.setOrientation(SwingConstants.HORIZONTAL);
        this.slSueldo.setMajorTickSpacing(2000);
        this.slSueldo.setMinorTickSpacing(2);
        this.slSueldo.setPaintLabels(true);
        this.slSueldo.setPaintTicks(true);
        this.slSueldo.setBounds(90, 390, 700, 50);
        this.slSueldo.setEnabled(false);
        this.kgPantalla.add(slSueldo);
        this.slSueldo.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                barraValoresStateChanged(evt);
            }
        });
    }

    //Evento del slider para tomar valor
    private void barraValoresStateChanged(javax.swing.event.ChangeEvent evt) {
//        this.sueldo.setText("" + this.slSueldo.getValue());
        this.Sueldo.setValue(this.slSueldo.getValue());

    }

    private void botonesConfiguracion() {
        /*----------------------------------------*/
        this.btnCrearTrabajador = new RSButtonRiple();
        this.btnCrearTrabajador.setBounds(880, 400, 120, 50);
        this.btnCrearTrabajador.setBorderPainted(false);
        this.btnCrearTrabajador.setFocusPainted(false);
        this.btnCrearTrabajador.setBackground(Colores.C_COLOR_GRIS3.getColor());
        this.btnCrearTrabajador.setColorHover(Colores.C_COLOR_GRIS2.getColor());
        this.btnCrearTrabajador.setColorText(Colores.C_COLOR_NEGRO.getColor());
        this.btnCrearTrabajador.setColorTextHover(Colores.C_COLOR_GRIS4.getColor());
        this.btnCrearTrabajador.setText("Actualizar");
        this.btnCrearTrabajador.setActionCommand("btnTrabajadores");
        this.btnCrearTrabajador.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (txtNombre.isEnabled()) {
                    String[] sParametros = {
                            txtNombre.getText().trim(),
                            txtApePaterno.getText().trim(),
                            txtApeMaterno.getText().trim(),
                            txtEmail.getText().trim(),
                            txtTelefono.getText().trim(),
                            rdFechas.getFechaSeleccionada().trim(),
                            txtDireccion.getText().trim(),
                            cbCargos.getSelectedItem().toString(),
                            Sueldo.getValue().toString(),
                            txtContrasenaNew.getText().trim(),
                            sNombre,
                            sApellidoP,
                            sApellidoM,
                            rsContrasena.getText().trim(),
                            sRFCTrabajador
                    };
                    boolean bInsercionCliente = Actualizar.actualizarTrabajador(sParametros);
                    System.out.println(rsContrasena.getText().trim());
                    if (bInsercionCliente) {
                        JOptionPane.showMessageDialog(null, "Trabajador modificado correctamente");
                        rscmTrabajador.setModel(new DefaultComboBoxModel());
                        txtNombre.setText("");
                        txtApePaterno.setText("");
                        txtApeMaterno.setText("");
                        txtEmail.setText("");
                        txtTelefono.setText("");
                        txtDireccion.setText("");
                        rsContrasena.setText("");
                        txtContrasenaNew.setText("");
                        txtNombre.setEnabled(false);
                        txtApePaterno.setEnabled(false);
                        txtApeMaterno.setEnabled(false);
                        txtEmail.setEnabled(false);
                        txtTelefono.setEnabled(false);
                        txtDireccion.setEnabled(false);
                        rsContrasena.setEnabled(false);
                        txtContrasenaNew.setEnabled(false);
                        rdFechas.setVisible(false);
                        cbCargos.setEnabled(false);
                        slSueldo.setEnabled(false);
                        Sueldo.setEnabled(false);
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "No a selecionado un trabajador o no existe");
                }
            }
        });
        this.kgPantalla.add(this.btnCrearTrabajador);
    }

    public KGradientPanel getkgPantalla() {
        return this.kgPantalla;
    }
}
