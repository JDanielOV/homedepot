package Ventanas.Pantallas;

import Recursos.Colores;
import Recursos.Fuentes;
import Recursos.Imagenes;
import SQL.CargarDatos;
import keeptoo.KGradientPanel;
import rojeru_san.RSButtonRiple;
import rojerusan.RSTableMetro;

import javax.swing.*;
import javax.swing.border.MatteBorder;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * @author cristina 05-11-2021
 * Ochoa Fecha: 08/11/2021
 * Modificacion: cambie el metodo buscarTabla
 */
public class PantDevVer {
    private RSTableMetro rsTablaDevoluciones;
    private KGradientPanel kgPantalla;
    private JTextField txtNombre;
    private RSButtonRiple btnBuscar;
    private final String[] STITULOS_TABLA = {"ID Devolucion", "Numero Venta", "ID Cliente", "ID Producto", "Devolucion Fecha", "Devolucion Motivo", "Devolucion Cantidad"};
    private Object[][] oContenidoTabla = {
    };


    public PantDevVer() {
        this.panelConfiguracion();
        this.labelsConfiguracion();
        this.textosConfiguracion();
        this.botonesConfiguracion();
        this.tablaConfiguracion();
    }

    private void panelConfiguracion() {
        this.kgPantalla = new KGradientPanel();
        this.kgPantalla = new KGradientPanel();
        this.kgPantalla.setSize(1090, 550);
        this.kgPantalla.setBackground(Colores.C_COLOR_TRANSPARENTE.getColor());
        this.kgPantalla.setkEndColor(Colores.C_COLOR_BLANCO.getColor());
        this.kgPantalla.setkStartColor(Colores.C_COLOR_BLANCO.getColor());
        this.kgPantalla.setkBorderRadius(40);
        this.kgPantalla.setLayout(null);
    }

    private void botonesConfiguracion() {
        /*----------------------------------------*/
        this.btnBuscar = new RSButtonRiple();
        this.btnBuscar.setBounds(750, 150, 120, 50);
        this.btnBuscar.setBorderPainted(false);
        this.btnBuscar.setFocusPainted(false);
        this.btnBuscar.setBackground(Colores.C_COLOR_GRIS3.getColor());
        this.btnBuscar.setColorHover(Colores.C_COLOR_GRIS2.getColor());
        this.btnBuscar.setColorText(Colores.C_COLOR_NEGRO.getColor());
        this.btnBuscar.setColorTextHover(Colores.C_COLOR_GRIS4.getColor());
        this.btnBuscar.setText("Buscar");
        this.btnBuscar.setActionCommand("btnBuscar");
        this.btnBuscar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                {
                    System.out.println("Buscar");
                }
            }


        });
        this.kgPantalla.add(this.btnBuscar);
    }


    private void textosConfiguracion() {
        this.txtNombre = new JTextField();
        this.txtNombre.setBounds(500, 150, 200, 30);
        this.txtNombre.setBackground(Colores.C_COLOR_GRIS1.getColor());
        this.txtNombre.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
        this.txtNombre.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.txtNombre.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyPressed(KeyEvent e) {
            }

            @Override
            public void keyReleased(KeyEvent e) {
                CargarDatos.buscarTabla(rsTablaDevoluciones, txtNombre.getText(), "Devolucion", "Devolucion");
                System.out.println("d");
            }
        });
        this.kgPantalla.add(this.txtNombre);
    }

    private void labelsConfiguracion() {
        JLabel labelTitulo = new JLabel("Lista De Devoluciones");
        labelTitulo.setFont(Fuentes.FUENTE_TITULOS.getFont());
        labelTitulo.setBounds(350, 20, 300, 30);
        this.kgPantalla.add(labelTitulo);

        JLabel jlImgTitulo = new JLabel(Imagenes.I_IMAGEN_DEVOLUCION.getImagen());
        jlImgTitulo.setFont(Fuentes.FUENTE_TITULOS.getFont());
        jlImgTitulo.setBounds(280, 10, 64, 64);
        this.kgPantalla.add(jlImgTitulo);

        JLabel jlNombre = new JLabel("ID Devolucion: ");
        jlNombre.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlNombre.setBounds(350, 150, 190, 30);
        this.kgPantalla.add(jlNombre);
    }

    private void tablaConfiguracion() {

        DefaultTableModel dtmTabla = new DefaultTableModel(this.oContenidoTabla, this.STITULOS_TABLA);
        this.rsTablaDevoluciones = new RSTableMetro();
        this.rsTablaDevoluciones.setModel(dtmTabla);
        this.rsTablaDevoluciones.setAltoHead(60);
        this.rsTablaDevoluciones.setRowHeight(40);
        this.rsTablaDevoluciones.setGrosorBordeFilas(0);
        this.rsTablaDevoluciones.setColorBackgoundHead(Colores.C_COLOR_GRIS4.getColor());
        this.rsTablaDevoluciones.setColorFilasBackgound1(Colores.C_COLOR_GRIS1.getColor());
        this.rsTablaDevoluciones.setColorFilasBackgound2(Colores.C_COLOR_GRIS2.getColor());
        this.rsTablaDevoluciones.setColorFilasForeground1(Colores.C_COLOR_NEGRO.getColor());
        this.rsTablaDevoluciones.setColorFilasForeground2(Colores.C_COLOR_NEGRO.getColor());
        this.rsTablaDevoluciones.setColorSelBackgound(Colores.C_COLOR_GRIS3.getColor());
        this.rsTablaDevoluciones.setColorBordeFilas(Colores.C_COLOR_GRIS1.getColor());
        this.rsTablaDevoluciones.setFuenteFilas(Fuentes.FUENTE_TEXTOS.getFont());
        this.rsTablaDevoluciones.setFuenteHead(Fuentes.FUENTE_TEXTOS.getFont());
        this.rsTablaDevoluciones.setPreferredScrollableViewportSize(new Dimension(250, 100));
        JScrollPane scrollPane = new JScrollPane(this.rsTablaDevoluciones);
        scrollPane.setBounds(10, 250, 1000, 250);
        CargarDatos.rellenarTabla(this.rsTablaDevoluciones, "Devolucion");
        this.kgPantalla.add(scrollPane);
    }

    public KGradientPanel getkgPantalla() {
        return this.kgPantalla;
    }
}

