package Ventanas.Pantallas;

import Recursos.Colores;
import Recursos.Fuentes;
import Recursos.Imagenes;
import SQL.*;
import keeptoo.KGradientPanel;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.view.JasperViewer;
import rojeru_san.RSButtonRiple;
import rojerusan.RSComboMetro;
import rojerusan.RSTableMetro;

import javax.swing.*;
import javax.swing.border.MatteBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.*;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Daniel Ochoa
 * <p>
 * Ochoa Fecha: 19/11/2021
 * Modificaciones: cambien agregarVenta y crre un metodo para agrupar las ventas
 */
public class PantVentAgregar {
    private KGradientPanel kgPantalla;
    private RSButtonRiple btnAgregarVenta, btnAgregarProducto, btnEliminarProducto;
    private JTextField txtDineroRecibido;
    private JSlider jsCantidad, jsPuntosUsar;
    private RSTableMetro rsTablaVentas;
    private JLabel jlIdVenta, jlTotalCosto;
    private Login lTrabajador;
    private RSComboMetro rscmProductos, rscmCliente, rscmTarjeta;
    private JSpinner jspCantidadProducto, jspPuntosUsar;
    private final String[] STITULOS_TABLA = {"ID Producto", "Nombre", "Precio", "Departamento", "Cantidad"};
    public static String sProductoEmpresaCombo;

    public PantVentAgregar(Login lUsuario) {
        this.lTrabajador = lUsuario;
        this.panelConfiguracion();
        this.labelsConfiguracion();
        this.textosConfiguracion();
        this.botonesConfiguracion();
        this.sliderConfiguracion();
        this.tablaConfiguracion();
        this.comboBoxConfiguracion();
        this.spinerConfiguracion();
    }

    private void panelConfiguracion() {
        this.kgPantalla = new KGradientPanel();
        this.kgPantalla = new KGradientPanel();
        this.kgPantalla.setSize(1090, 550);
        this.kgPantalla.setBackground(Colores.C_COLOR_TRANSPARENTE.getColor());
        this.kgPantalla.setkEndColor(Colores.C_COLOR_BLANCO.getColor());
        this.kgPantalla.setkStartColor(Colores.C_COLOR_BLANCO.getColor());
        this.kgPantalla.setkBorderRadius(40);
        this.kgPantalla.setLayout(null);
    }

    private void textosConfiguracion() {

/*        this.txtRFC = new JTextField(this.lTrabajador.getsRFC());
        this.txtRFC.setBounds(790, 200, 200, 30);
        this.txtRFC.setBackground(Colores.C_COLOR_GRIS1.getColor());
        this.txtRFC.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
        this.txtRFC.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        this.txtRFC.setEnabled(false);
        this.kgPantalla.add(this.txtRFC);*/

        this.txtDineroRecibido = new JTextField();
        this.txtDineroRecibido.setBounds(650, 268, 150, 30);
        this.txtDineroRecibido.setBackground(Colores.C_COLOR_GRIS1.getColor());
        this.txtDineroRecibido.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
        this.txtDineroRecibido.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        this.kgPantalla.add(this.txtDineroRecibido);
        txtDineroRecibido.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {

            }

            @Override
            public void keyReleased(KeyEvent e) {
                cambio();
            }
        });
    }

    private void labelsConfiguracion() {
        JLabel labelTitutlo = new JLabel("Agregar Nueva Venta");
        labelTitutlo.setFont(Fuentes.FUENTE_TITULOS.getFont());
        labelTitutlo.setBounds(350, 20, 350, 30);
        this.kgPantalla.add(labelTitutlo);

        JLabel jlImgTitulo = new JLabel(Imagenes.I_IMAGEN_VENTA.getImagen());
        jlImgTitulo.setFont(Fuentes.FUENTE_TITULOS.getFont());
        jlImgTitulo.setBounds(280, 10, 64, 64);
        this.kgPantalla.add(jlImgTitulo);

        JLabel jlIDProducto = new JLabel("Producto: ");
        jlIDProducto.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlIDProducto.setBounds(0, 100, 150, 30);
        this.kgPantalla.add(jlIDProducto);

        JLabel jlIDTarjeta = new JLabel("Tarjeta: ");
        jlIDTarjeta.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlIDTarjeta.setBounds(830, 100, 80, 30);
        this.kgPantalla.add(jlIDTarjeta);


        JLabel jlIDCliente = new JLabel("Cliente: ");
        jlIDCliente.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlIDCliente.setBounds(425, 100, 130, 30);
        this.kgPantalla.add(jlIDCliente);

        JLabel jlRFC = new JLabel("RFC Trabajador: " + lTrabajador.getsRFC());
        jlRFC.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlRFC.setBounds(750, 0, 250, 30);
        this.kgPantalla.add(jlRFC);

        JLabel jlPeizas = new JLabel("Piezas: ");
        jlPeizas.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlPeizas.setBounds(0, 150, 130, 30);
        this.kgPantalla.add(jlPeizas);

        JLabel jlPuntos = new JLabel("Puntos: ");
        jlPuntos.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlPuntos.setBounds(0, 200, 130, 30);
        this.kgPantalla.add(jlPuntos);

        this.jlIdVenta = new JLabel("ID seleccionado: ");
        this.jlIdVenta.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        this.jlIdVenta.setBounds(0, 250, 500, 70);
        this.kgPantalla.add(this.jlIdVenta);

        LocalDate fechaHoy = LocalDate.now();
        String formatoFecha = fechaHoy.format(DateTimeFormatter.ofPattern("dd-MM-YYYY"));
        JLabel labelFechaActual = new JLabel("Fecha:  " + formatoFecha);
        labelFechaActual.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        labelFechaActual.setBounds(0, 0, 200, 30);
        this.kgPantalla.add(labelFechaActual);

        this.jlTotalCosto = new JLabel("Total a pagar: ");
        this.jlTotalCosto.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        this.jlTotalCosto.setBounds(220, 250, 250, 70);
        this.kgPantalla.add(this.jlTotalCosto);

        JLabel jlMontoRecibido = new JLabel("Efectivo Recibido: ");
        jlMontoRecibido.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlMontoRecibido.setBounds(485, 268, 200, 30);
        this.kgPantalla.add(jlMontoRecibido);
    }

    private void sliderConfiguracion() {
        this.jsCantidad = new JSlider(1, 1000, 500);
        this.jsCantidad.setOrientation(SwingConstants.HORIZONTAL);
        this.jsCantidad.setMajorTickSpacing((this.jsCantidad.getMaximum() / 10));
        this.jsCantidad.setMinorTickSpacing(10);
        this.jsCantidad.setPaintLabels(true);
        this.jsCantidad.setPaintTicks(true);
        this.jsCantidad.setBounds(90, 150, 700, 50);
        this.kgPantalla.add(jsCantidad);
        this.jsCantidad.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                cambiarValorSpinner(jsCantidad, jspCantidadProducto);
            }
        });


        this.jsPuntosUsar = new JSlider(0, 500, 0);
        this.jsPuntosUsar.setOrientation(SwingConstants.HORIZONTAL);
        this.jsPuntosUsar.setMajorTickSpacing((this.jsCantidad.getMaximum() / 10));
        this.jsPuntosUsar.setMinorTickSpacing(10);
        this.jsPuntosUsar.setPaintLabels(true);
        this.jsPuntosUsar.setPaintTicks(true);
        this.jsPuntosUsar.setBounds(90, 200, 700, 50);
        this.kgPantalla.add(jsPuntosUsar);
        this.jsPuntosUsar.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {

                cambiarValorSpinner(jsPuntosUsar, jspPuntosUsar);
                cambio();

            }
        });

    }

    private void spinerConfiguracion() {
        this.jspCantidadProducto = new JSpinner();
        this.jspCantidadProducto.setValue(this.jsCantidad.getValue());
        this.jspCantidadProducto.setBounds(800, 150, 100, 30);
        this.jspCantidadProducto.setBackground(Colores.C_COLOR_GRIS3.getColor());
        this.jspCantidadProducto.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        this.jspCantidadProducto.getEditor().getComponent(0).setEnabled(false);
        this.kgPantalla.add(this.jspCantidadProducto);
        this.jspCantidadProducto.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                cambiarValorSlider(jsCantidad, jspCantidadProducto);
                int valor = (int) jspCantidadProducto.getValue();
                if (valor <= 1) {
                    jspCantidadProducto.setValue(1);
                } else if (valor >= jsCantidad.getMaximum()) {
                    jspCantidadProducto.setValue(jsCantidad.getMaximum());
                }
            }
        });


        this.jspPuntosUsar = new JSpinner();
        this.jspPuntosUsar.setValue(this.jsPuntosUsar.getValue());
        this.jspPuntosUsar.setBounds(800, 200, 100, 30);
        this.jspPuntosUsar.setBackground(Colores.C_COLOR_GRIS3.getColor());
        this.jspPuntosUsar.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        this.jspPuntosUsar.getEditor().getComponent(0).setEnabled(false);
        this.kgPantalla.add(this.jspPuntosUsar);
        this.jspPuntosUsar.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {

                cambiarValorSlider(jsPuntosUsar, jspPuntosUsar);
                int valor = (int) jspPuntosUsar.getValue();
                if (valor <= 0) {
                    jspPuntosUsar.setValue(0);
                } else if (valor >= jsPuntosUsar.getMaximum()) {
                    jspPuntosUsar.setValue(jsPuntosUsar.getMaximum());
                }

            }
        });
    }

    private void cambiarValorSlider(JSlider jsCantidad, JSpinner jspCantidad) {
        jsCantidad.setValue((int) jspCantidad.getValue());
    }

    private void cambiarValorSpinner(JSlider jsCantidad, JSpinner jspCantidad) {
        jspCantidad.setValue(jsCantidad.getValue());
    }

    private void botonesConfiguracion() {
        /*----------------------------------------*/
        this.btnAgregarVenta = new RSButtonRiple();
        this.btnAgregarVenta.setBounds(850, 460, 160, 50);
        this.btnAgregarVenta.setBorderPainted(false);
        this.btnAgregarVenta.setFocusPainted(false);
        this.btnAgregarVenta.setBackground(Colores.C_COLOR_GRIS3.getColor());
        this.btnAgregarVenta.setColorHover(Colores.C_COLOR_GRIS2.getColor());
        this.btnAgregarVenta.setColorText(Colores.C_COLOR_NEGRO.getColor());
        this.btnAgregarVenta.setColorTextHover(Colores.C_COLOR_GRIS4.getColor());
        this.btnAgregarVenta.setText("Agregar Venta");
        this.btnAgregarVenta.setActionCommand("btnClientes");
        this.btnAgregarVenta.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {

                String sNombreClienteIDCliente = rscmCliente.getSelectedItem() == null ? " " : rscmCliente.getSelectedItem().toString();
                ;
                int iComa = sNombreClienteIDCliente.indexOf(", ");
                int iDosPuntosEspacio = sNombreClienteIDCliente.indexOf("ID: ");

                String sNombre = (sNombreClienteIDCliente.contains(", ")) ? (sNombreClienteIDCliente.substring(0, iComa)) : (" ");
                String sId = (sNombreClienteIDCliente.contains("ID: ")) ? (sNombreClienteIDCliente.substring(iDosPuntosEspacio + 4)) : (" ");

                System.out.println("#" + sNombre + "#" + sId + "#" + sNombreClienteIDCliente + "#");

                boolean bValidarCliente = (ValidacionesGenerales.validarNombreID(sNombre, sId, "nombreClienteIDComprovacion"));
                if (bValidarCliente) {
                    String sRFC = lTrabajador.getsRFC();//txtRFC.getText().trim();
                    LocalDate fechaHoy = LocalDate.now();
                    String formatoFecha = fechaHoy.format(DateTimeFormatter.ofPattern("YYYY-MM-dd"));
                    double dTotla = dineroTotalAPagar();
                    String sTotal = String.valueOf(dTotla);
                    String sIdTarjeta = rscmTarjeta.getSelectedItem() == null ? " " : rscmTarjeta.getSelectedItem().toString();
                    ;//txtIDTarjeta.getText().trim();
                    String sPuntosGastados = String.valueOf(jsPuntosUsar.getValue());
                    String sEfectivo = (validarDinero()) ? txtDineroRecibido.getText().trim() : "0";
                    String sCambio = String.valueOf(cambio());
                    if (rsTablaVentas.getRowCount() > 0) {
                        boolean bInsercionCliente = Inserciones.agregarVenta(sId, sRFC, formatoFecha, sTotal, sIdTarjeta, sPuntosGastados, sEfectivo, sCambio);
                        if (bInsercionCliente) {
                            for (int i = 0; i < rsTablaVentas.getRowCount(); i++) {
                                String sIdP = rsTablaVentas.getValueAt(i, 0).toString().trim();
                                String sPrecio = rsTablaVentas.getValueAt(i, 2).toString().trim();
                                String sCantidad = rsTablaVentas.getValueAt(i, 4).toString().trim();
                                Inserciones.agregarFacturaVenta(sIdP, sPrecio, sCantidad);
                            }
                            JOptionPane.showMessageDialog(null, "Venta agregada correctamente");
                            int resp = JOptionPane.showConfirmDialog(null, "Generar Ticket de Venta",
                                    null, JOptionPane.YES_NO_OPTION,
                                    JOptionPane.INFORMATION_MESSAGE);
                            System.out.println(resp);
                            if (resp == 0) {
                                JasperReport archivo;
                                try {
                                    archivo = JasperCompileManager.compileReport("Configuracion/Reportes/TicketVenta.jrxml");
                                    HashMap<String, Object> map = CargarDatos.datosReporteVenta(CargarDatos.ventaMaxReciente());
                                    //JRDataSource data = new JREmptyDataSource();
                                    JasperPrint prin = JasperFillManager.fillReport(archivo, map, Conexion.getConexion());
                                    if (prin != null) {
                                        JasperViewer view = new JasperViewer(prin, false);
                                        view.setVisible(true);
                                    }
                                } catch (JRException ex) {
                                    Logger.getLogger(PantVentVer.class.getName()).log(Level.SEVERE, null, ex);
                                }
                            }
                            rscmProductos.setModel(new DefaultComboBoxModel());
                            rscmTarjeta.setModel(new DefaultComboBoxModel());
                            rscmCliente.setModel(new DefaultComboBoxModel());
                            jsPuntosUsar.setValue(0);
                            jsCantidad.setValue(500);
                            jlIdVenta.setText("ID seleccionado: ");
                            jlTotalCosto.setText("Total a pagar: ");
                            txtDineroRecibido.setText("");
                            rsTablaVentas.setModel(new DefaultTableModel(null, STITULOS_TABLA));
                        }
                    } else {
                        JOptionPane.showMessageDialog(null, "No ha agregado productos");
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Error en los datos proporcionados");
                }
            }
        });
        this.kgPantalla.add(this.btnAgregarVenta);


        /*----------------------------------------*/
        this.btnAgregarProducto = new RSButtonRiple();
        this.btnAgregarProducto.setBounds(850, 300, 160, 50);
        this.btnAgregarProducto.setBorderPainted(false);
        this.btnAgregarProducto.setFocusPainted(false);
        this.btnAgregarProducto.setBackground(Colores.C_COLOR_GRIS3.getColor());
        this.btnAgregarProducto.setColorHover(Colores.C_COLOR_GRIS2.getColor());
        this.btnAgregarProducto.setColorText(Colores.C_COLOR_NEGRO.getColor());
        this.btnAgregarProducto.setColorTextHover(Colores.C_COLOR_GRIS4.getColor());
        this.btnAgregarProducto.setText("Agregar Producto");
        this.btnAgregarProducto.setActionCommand("btnClientes");
        this.btnAgregarProducto.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                String sProductoEmpresa = rscmProductos.getSelectedItem() == null ? " " : rscmProductos.getSelectedItem().toString();
                int iPrimerEspacio = sProductoEmpresa.indexOf(", ");
                int iDosPuntosEspacio = sProductoEmpresa.indexOf(": ");

                String sProducto = (sProductoEmpresa.contains(", ")) ? (sProductoEmpresa.substring(0, iPrimerEspacio)) : ("");
                String sEmpresa = (sProductoEmpresa.contains(": ")) ? (sProductoEmpresa.substring(iDosPuntosEspacio + 2)) : ("");

                System.out.println("#" + sProducto + "#" + sEmpresa + "#" + sProductoEmpresa + "#");
                String sIdProducto = CargarDatos.idProducto(sProducto, sEmpresa);

                boolean bValidarProducto = (ValidacionesGenerales.validarExistencia(sIdProducto, "Producto"));
                if (bValidarProducto) {
                    DefaultTableModel dtmTabla = (DefaultTableModel) rsTablaVentas.getModel();
                    ArrayList<String> sFilaProducto = CargarDatos.obtenerRegistros(sIdProducto, "Producto").get(0);
                    int iCantidad = Integer.parseInt(sFilaProducto.get(4));
                    int iCantidadSelecionada = jsCantidad.getValue();
                    if (iCantidad >= iCantidadSelecionada) {
                        String sPrecio = sFilaProducto.get(2);
                        double dPrecio = Double.parseDouble(sPrecio);
                        double dPrecioCantidad = dPrecio * iCantidadSelecionada;
                        DecimalFormat df = new DecimalFormat("#.00");
                        Object[] oDatosProducto = {sIdProducto, sFilaProducto.get(1), df.format(dPrecioCantidad), sFilaProducto.get(3), iCantidadSelecionada};
                        boolean bAgruparValidar = agruparProductos(oDatosProducto, iCantidad);
                        if (bAgruparValidar) {
                            double dTotla = dineroTotalAPagar();
                            jlTotalCosto.setText("Total a pagar: " + dTotla);
                            cambio();
                        } else {
                            JOptionPane.showMessageDialog(null, "La cantidad es mayor a la del inventario");
                        }
                    } else {
                        JOptionPane.showMessageDialog(null, "La cantidad es mayor a la del inventario");
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Error en los datos proporcionados");
                }
            }
        });
        this.kgPantalla.add(this.btnAgregarProducto);


        this.btnEliminarProducto = new RSButtonRiple();
        this.btnEliminarProducto.setBounds(850, 380, 160, 50);
        this.btnEliminarProducto.setBorderPainted(false);
        this.btnEliminarProducto.setFocusPainted(false);
        this.btnEliminarProducto.setBackground(Colores.C_COLOR_GRIS3.getColor());
        this.btnEliminarProducto.setColorHover(Colores.C_COLOR_GRIS2.getColor());
        this.btnEliminarProducto.setColorText(Colores.C_COLOR_NEGRO.getColor());
        this.btnEliminarProducto.setColorTextHover(Colores.C_COLOR_GRIS4.getColor());
        this.btnEliminarProducto.setText("Eliminar Producto");
        this.btnEliminarProducto.setActionCommand("btnEliminarProducto");
        this.btnEliminarProducto.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (seleccionTrabajador()) {
                    int renglon = rsTablaVentas.getSelectedRow();
                    DefaultTableModel dtm = (DefaultTableModel) rsTablaVentas.getModel();
                    dtm.removeRow(renglon);
                    rsTablaVentas.setModel(dtm);
                    double dTotla = dineroTotalAPagar();
                    jlTotalCosto.setText("Total a pagar: " + dTotla);
                    cambio();
                }
            }
        });
        this.kgPantalla.add(this.btnEliminarProducto);
    }

    private void tablaConfiguracion() {

        DefaultTableModel dtmTabla = new DefaultTableModel(null, this.STITULOS_TABLA);
        this.rsTablaVentas = new RSTableMetro();
        this.rsTablaVentas.setModel(dtmTabla);
        this.rsTablaVentas.setAltoHead(60);
        this.rsTablaVentas.setRowHeight(40);
        this.rsTablaVentas.setGrosorBordeFilas(0);
        this.rsTablaVentas.setColorBackgoundHead(Colores.C_COLOR_GRIS4.getColor());
        this.rsTablaVentas.setColorFilasBackgound1(Colores.C_COLOR_GRIS1.getColor());
        this.rsTablaVentas.setColorFilasBackgound2(Colores.C_COLOR_GRIS2.getColor());
        this.rsTablaVentas.setColorFilasForeground1(Colores.C_COLOR_NEGRO.getColor());
        this.rsTablaVentas.setColorFilasForeground2(Colores.C_COLOR_NEGRO.getColor());
        this.rsTablaVentas.setColorSelBackgound(Colores.C_COLOR_GRIS3.getColor());
        this.rsTablaVentas.setColorBordeFilas(Colores.C_COLOR_GRIS1.getColor());
        this.rsTablaVentas.setFuenteFilas(Fuentes.FUENTE_TEXTOS.getFont());
        this.rsTablaVentas.setFuenteHead(Fuentes.FUENTE_TEXTOS.getFont());
        this.rsTablaVentas.setPreferredScrollableViewportSize(new Dimension(250, 100));
        JScrollPane scrollPane = new JScrollPane(this.rsTablaVentas);
        scrollPane.setBounds(0, 300, 850, 250);
        this.rsTablaVentas.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                seleccionTrabajador();
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }
        });
        this.kgPantalla.add(scrollPane);
    }

    private void comboBoxConfiguracion() {
        this.rscmProductos = new RSComboMetro();
        this.rscmProductos.setColorFondo(Colores.C_COLOR_GRIS4.getColor());
        this.rscmProductos.setColorBorde(Colores.C_COLOR_GRIS3.getColor());
        this.rscmProductos.setColorArrow(Colores.C_COLOR_GRIS3.getColor());
        this.rscmProductos.setForeground(Colores.C_COLOR_NEGRO.getColor());
        this.rscmProductos.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.rscmProductos.setBounds(100, 100, 315, 30);
        this.rscmProductos.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
        this.rscmProductos.setEditable(true);
        CargarDatos.comboBoxPonerDatos(this.rscmProductos, "nombreProductoEmpresa");
        this.rscmProductos.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String sValor = rscmProductos.getSelectedItem().toString();
                sProductoEmpresaCombo = sValor.trim();
            }
        });
        this.kgPantalla.add(this.rscmProductos);

        this.rscmCliente = new RSComboMetro();
        this.rscmCliente.setColorFondo(Colores.C_COLOR_GRIS4.getColor());
        this.rscmCliente.setColorBorde(Colores.C_COLOR_GRIS3.getColor());
        this.rscmCliente.setColorArrow(Colores.C_COLOR_GRIS3.getColor());
        this.rscmCliente.setForeground(Colores.C_COLOR_NEGRO.getColor());
        this.rscmCliente.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.rscmCliente.setBounds(505, 100, 315, 30);
        this.rscmCliente.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
        this.rscmCliente.setEditable(true);
        CargarDatos.comboBoxPonerDatos(this.rscmCliente, "nombreClienteIDCliente");
        this.kgPantalla.add(this.rscmCliente);


        this.rscmTarjeta = new RSComboMetro();
        this.rscmTarjeta.setColorFondo(Colores.C_COLOR_GRIS4.getColor());
        this.rscmTarjeta.setColorBorde(Colores.C_COLOR_GRIS3.getColor());
        this.rscmTarjeta.setColorArrow(Colores.C_COLOR_GRIS3.getColor());
        this.rscmTarjeta.setForeground(Colores.C_COLOR_NEGRO.getColor());
        this.rscmTarjeta.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.rscmTarjeta.setBounds(900, 100, 100, 30);
        this.kgPantalla.add(this.rscmTarjeta);

   /*     this.rscmTarjeta.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String sTarjeta = rscmTarjeta.getSelectedItem().toString();
                String sPuntos = CargarDatos.puntosTajerta(sTarjeta);
                int iPuntos = sPuntos.equals(" ") ? 0 : Integer.parseInt(sPuntos);
                jsPuntosUsar.setMaximum(iPuntos);
            }
        });*/

        this.rscmCliente.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String sValor = rscmCliente.getSelectedItem().toString();
                System.out.println("Id Cliente selecionado:#" + sValor.substring(sValor.lastIndexOf(" ") + 1) + "#");
                DefaultComboBoxModel dcbmNuevo = CargarDatos.cargarComboBox(sValor.substring(sValor.lastIndexOf(" ") + 1), "idTarjeta");
                dcbmNuevo.addElement(" ");
                rscmTarjeta.setModel(dcbmNuevo);
                rscmTarjeta.setSelectedIndex(rscmTarjeta.getItemCount() - 1);
            }
        });

    }

    private boolean seleccionTrabajador() {
        int renglon = this.rsTablaVentas.getSelectedRow();
        System.out.println(renglon);
        if (renglon != -1) {
            String rfc = this.rsTablaVentas.getValueAt(renglon, 0) + "";
            this.jlIdVenta.setText("ID seleccionado: " + rfc);
        } else {
            System.out.println("No selecciono cliente");
            return false;
        }
        return true;
    }

    public KGradientPanel getkgPantalla() {
        return this.kgPantalla;
    }

    private double dineroTotalAPagar() {
        DecimalFormat df = new DecimalFormat("#.00");
        double dDinero = 0.0;
        for (int i = 0; i < this.rsTablaVentas.getRowCount(); i++) {
            String sPrecio = this.rsTablaVentas.getValueAt(i, 2).toString();
            double dPrecio = Double.parseDouble(sPrecio);
            dDinero += (dPrecio);
        }
        dDinero = Double.parseDouble(df.format(dDinero));
        return dDinero;
    }

    private boolean agruparProductos(Object[] oProducto, int iCantidadMaxima) {
        DefaultTableModel dtmTabla = (DefaultTableModel) rsTablaVentas.getModel();
        rsTablaVentas.setModel(dtmTabla);
        for (int i = 0; i < dtmTabla.getRowCount(); i++) {
            String idProducto = dtmTabla.getValueAt(i, 0).toString();
            if (idProducto.equals(oProducto[0].toString())) {
                int iCantidadProductosAnteriores = Integer.parseInt(dtmTabla.getValueAt(i, 4).toString());
                int iCantidadProductosAgregar = Integer.parseInt(oProducto[4].toString());
                int iCaiCantidadProductosTotales = iCantidadProductosAnteriores + iCantidadProductosAgregar;
                if (iCaiCantidadProductosTotales > iCantidadMaxima) {
                    return false;
                }
                oProducto[4] = String.valueOf(iCaiCantidadProductosTotales);
                double dPrecioAnterior = Double.parseDouble(dtmTabla.getValueAt(i, 2).toString());
                double dPrecioAgregar = Double.parseDouble(oProducto[2].toString().toString());
                DecimalFormat df = new DecimalFormat("#.00");
                double dPrecioNuevo = dPrecioAgregar + dPrecioAnterior;
                oProducto[2] = String.valueOf(df.format(dPrecioNuevo));
                dtmTabla.removeRow(i);
                dtmTabla.addRow(oProducto);
                return true;
            }
        }
        dtmTabla.addRow(oProducto);
        return true;
    }

    private boolean validarDinero() {
        String[] sRegex = txtDineroRecibido.getText().split("([0-9])+(\\.)+([0-9])+");
        String[] sRegex2 = txtDineroRecibido.getText().split("[0-9]+");
        return sRegex.length == 0 || sRegex2.length == 0;
    }

    private double cambio() {
        double dPAgo = 0.0d;
        DecimalFormat df = new DecimalFormat("#.00");
        double dTotalAPagaR = this.dineroTotalAPagar();
        if (validarDinero()) {
            dPAgo = jsPuntosUsar.getValue() + Double.parseDouble(txtDineroRecibido.getText());
        } else {
            dPAgo = (jsPuntosUsar.getValue() + 0.00d);
        }
        if (this.rsTablaVentas.getRowCount() > 0) {
            if (dTotalAPagaR <= dPAgo) {
                double dPRecio = Double.parseDouble(df.format(dPAgo - dTotalAPagaR));
                return (dPRecio);
            } else {
                return 0.00d;
            }
        } else {
            return 0.00d;
        }

    }
}
