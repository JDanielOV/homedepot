package Ventanas.Pantallas;

import Recursos.Colores;
import Recursos.Fuentes;
import Recursos.Imagenes;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.MatteBorder;
import javax.swing.table.DefaultTableModel;

import SQL.CargarDatos;
import SQL.Conexion;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.HashMap;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;

import keeptoo.KGradientPanel;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;
import net.sf.jasperreports.view.JasperViewer;
import rojeru_san.RSButtonRiple;
import rojerusan.RSComboMetro;
import rojerusan.RSTableMetro;

/**
 * @author cristina
 * Ochoa Fecha: 08/11/2021
 * Modificacion: cambie el metodo buscarTabla
 */
public class PantPedVer {
    private RSTableMetro rsTablaPedidos;
    private KGradientPanel kgPantalla;
    private JTextField txtNombre;
    private RSComboMetro rscmProveedor;
    private RSButtonRiple btnBuscarPedidos,btnGenerarReporte;
    private String sValor;

    public PantPedVer() {
        this.panelConfiguracion();
        this.labelsConfiguracion();
        this.textosConfiguracion();
        this.tablaConfiguracion();
        this.botonesConfiguracion();
        this.comboBoxConfiguracion();
    }

    private void panelConfiguracion() {
        this.kgPantalla = new KGradientPanel();
        this.kgPantalla = new KGradientPanel();
        this.kgPantalla.setSize(1090, 550);
        this.kgPantalla.setBackground(Colores.C_COLOR_TRANSPARENTE.getColor());
        this.kgPantalla.setkEndColor(Colores.C_COLOR_BLANCO.getColor());
        this.kgPantalla.setkStartColor(Colores.C_COLOR_BLANCO.getColor());
        this.kgPantalla.setkBorderRadius(40);
        this.kgPantalla.setLayout(null);
    }

    private void comboBoxConfiguracion() {
        this.rscmProveedor = new RSComboMetro();
        this.rscmProveedor.setColorFondo(Colores.C_COLOR_GRIS4.getColor());
        this.rscmProveedor.setColorBorde(Colores.C_COLOR_GRIS3.getColor());
        this.rscmProveedor.setColorArrow(Colores.C_COLOR_GRIS3.getColor());
        this.rscmProveedor.setForeground(Colores.C_COLOR_NEGRO.getColor());
        this.rscmProveedor.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.rscmProveedor.setBounds(350, 170, 330, 30);
        this.rscmProveedor.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
        this.rscmProveedor.setEditable(true);
        CargarDatos.comboBoxPonerDatos(this.rscmProveedor, "nombreProveedor");
        this.rscmProveedor.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                sValor = rscmProveedor.getSelectedItem().toString();
                System.out.println("Id Proveedor selecionado:#" + sValor.substring(sValor.lastIndexOf(" ") + 1) + "#");
            }
        });
        this.kgPantalla.add(this.rscmProveedor);
    }
    
    private void textosConfiguracion() {
        this.txtNombre = new JTextField();
        this.txtNombre.setBounds(500, 100, 200, 30);
        this.txtNombre.setBackground(Colores.C_COLOR_GRIS1.getColor());
        this.txtNombre.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
        this.txtNombre.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.txtNombre.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyPressed(KeyEvent e) {
            }

            @Override
            public void keyReleased(KeyEvent e) {
                CargarDatos.buscarTabla(rsTablaPedidos, txtNombre.getText(), "Pedidos", "Pedidos");
                System.out.println("d");
            }
        });
        this.kgPantalla.add(this.txtNombre);
    }

    private void labelsConfiguracion() {
        JLabel labelTitulo = new JLabel("Lista De Pedidos");
        labelTitulo.setFont(Fuentes.FUENTE_TITULOS.getFont());
        labelTitulo.setBounds(400, 20, 300, 30);
        this.kgPantalla.add(labelTitulo);

        JLabel jlImgTitulo = new JLabel(Imagenes.I_IMAGEN_PEDIDO.getImagen());
        jlImgTitulo.setFont(Fuentes.FUENTE_TITULOS.getFont());
        jlImgTitulo.setBounds(320, 10, 64, 64);
        this.kgPantalla.add(jlImgTitulo);

        JLabel jlNombre = new JLabel("Buscar ID: ");
        jlNombre.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlNombre.setBounds(350, 100, 190, 30);
        this.kgPantalla.add(jlNombre);
        
        JLabel jlNombreProv = new JLabel("Buscar Proveedor: ");
        jlNombreProv.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlNombreProv.setBounds(180, 170, 250, 30);
        this.kgPantalla.add(jlNombreProv);
    }

    private void tablaConfiguracion() {

        this.rsTablaPedidos = new RSTableMetro();
        this.rsTablaPedidos.setAltoHead(60);
        this.rsTablaPedidos.setRowHeight(40);
        this.rsTablaPedidos.setGrosorBordeFilas(0);
        this.rsTablaPedidos.setColorBackgoundHead(Colores.C_COLOR_GRIS4.getColor());
        this.rsTablaPedidos.setColorFilasBackgound1(Colores.C_COLOR_GRIS1.getColor());
        this.rsTablaPedidos.setColorFilasBackgound2(Colores.C_COLOR_GRIS2.getColor());
        this.rsTablaPedidos.setColorFilasForeground1(Colores.C_COLOR_NEGRO.getColor());
        this.rsTablaPedidos.setColorFilasForeground2(Colores.C_COLOR_NEGRO.getColor());
        this.rsTablaPedidos.setColorSelBackgound(Colores.C_COLOR_GRIS3.getColor());
        this.rsTablaPedidos.setColorBordeFilas(Colores.C_COLOR_GRIS1.getColor());
        this.rsTablaPedidos.setFuenteFilas(Fuentes.FUENTE_TEXTOS.getFont());
        this.rsTablaPedidos.setFuenteHead(Fuentes.FUENTE_TEXTOS.getFont());
        this.rsTablaPedidos.setPreferredScrollableViewportSize(new Dimension(250, 100));
        JScrollPane scrollPane = new JScrollPane(this.rsTablaPedidos);
        scrollPane.setBounds(10, 250, 1000, 250);
        CargarDatos.rellenarTabla(this.rsTablaPedidos, "Pedidos");
        this.kgPantalla.add(scrollPane);
    }

    private void botonesConfiguracion() {
        
        /*----------------------------------------*/
        this.btnGenerarReporte = new RSButtonRiple();
        this.btnGenerarReporte.setBounds(720, 170, 200, 50);
        this.btnGenerarReporte.setBorderPainted(false);
        this.btnGenerarReporte.setFocusPainted(false);
        this.btnGenerarReporte.setBackground(Colores.C_COLOR_GRIS3.getColor());
        this.btnGenerarReporte.setColorHover(Colores.C_COLOR_GRIS2.getColor());
        this.btnGenerarReporte.setColorText(Colores.C_COLOR_NEGRO.getColor());
        this.btnGenerarReporte.setColorTextHover(Colores.C_COLOR_GRIS4.getColor());
        this.btnGenerarReporte.setText("Generar Reporte");
        this.btnGenerarReporte.setActionCommand("btnEliPedidos");
        this.btnGenerarReporte.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                System.out.println("Reporte");
                JasperReport archivo;
        try {
            HashMap a = new HashMap();
            String sProv = rscmProveedor.getSelectedItem() == null ? " " : rscmProveedor.getSelectedItem().toString();
            ;
            int iDosPuntosEspaciop = sProv.indexOf(": ");
            String sProveedor = (sProv.contains(": ")) ? (sProv.substring(iDosPuntosEspaciop + 2)) : ("");
            a.put("sIDProveedor", sProveedor);
            archivo = JasperCompileManager.compileReport("ReportesPedido.jrxml");
            //archivo = JasperCompileManager.compileReport("Producto.jrxml");
            //Map<String, Object> map = new HashMap<String, Object>();
            Conexion cnn = new Conexion();
            //JRDataSource data = new JREmptyDataSource();
            //JasperPrint prin = JasperFillManager.fillReport(archivo, a, Conexion.getConexion());
            JasperPrint prin = JasperFillManager.fillReport(archivo, a, Conexion.getConexion());
            if (prin != null) {
                JasperViewer view = new JasperViewer(prin, false);
                view.setVisible(true);
                // JasperExportManager.exportReportToHtmlFile(prin, "reporte.html");
                //JasperExportManager.exportReportToPdfFile(prin, "reporte.pdf");
                JasperExportManager.exportReportToHtmlFile(prin, "reporte.html");
                JRPdfExporter exp = new JRPdfExporter();
                exp.setExporterInput(new SimpleExporterInput(prin));
                exp.setExporterOutput(new SimpleOutputStreamExporterOutput("ReporteTrabajadores.pdf"));
                SimplePdfExporterConfiguration conf = new SimplePdfExporterConfiguration();
                exp.setConfiguration(conf);
                exp.exportReport();
            }
        } catch (JRException ex) {
            JOptionPane.showMessageDialog(null,"El documento no tiene paginas");
        }
            }
        });
        this.kgPantalla.add(this.btnGenerarReporte);
    }

    public KGradientPanel getkgPantalla() {
        return this.kgPantalla;
    }
}
