package Ventanas.Pantallas;

import Recursos.Colores;
import Recursos.Fuentes;
import Recursos.Imagenes;
import SQL.CargarDatos;
import keeptoo.KGradientPanel;
import rojeru_san.RSButtonRiple;
import rojerusan.RSTableMetro;

import javax.swing.*;
import javax.swing.border.MatteBorder;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * @author Daniel Ochoa
 * <p>
 * Ochoa Fecha: 08/11/2021
 * Modificacion: cambie el metodo buscarTabla
 */
public class PantVentEliminar {
    private RSTableMetro rsTablaVentas;
    private KGradientPanel kgPantalla;
    private JTextField txtID;
    private JLabel jlIdVenta;
    private RSButtonRiple btnEliminarVenta;

    public PantVentEliminar() {
        this.panelConfiguracion();
        this.labelsConfiguracion();
        this.textosConfiguracion();
        this.tablaConfiguracion();
        this.botonesConfiguracion();
    }

    private void panelConfiguracion() {
        this.kgPantalla = new KGradientPanel();
        this.kgPantalla = new KGradientPanel();
        this.kgPantalla.setSize(1090, 550);
        this.kgPantalla.setBackground(Colores.C_COLOR_TRANSPARENTE.getColor());
        this.kgPantalla.setkEndColor(Colores.C_COLOR_BLANCO.getColor());
        this.kgPantalla.setkStartColor(Colores.C_COLOR_BLANCO.getColor());
        this.kgPantalla.setkBorderRadius(40);
        this.kgPantalla.setLayout(null);
    }

    private void textosConfiguracion() {
        this.txtID = new JTextField();
        this.txtID.setBounds(480, 100, 200, 30);
        this.txtID.setBackground(Colores.C_COLOR_GRIS1.getColor());
        this.txtID.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
        this.txtID.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.txtID.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyPressed(KeyEvent e) {
            }

            @Override
            public void keyReleased(KeyEvent e) {
                CargarDatos.buscarTabla(rsTablaVentas, txtID.getText(), "Venta", "Venta");
                System.out.println("d");
            }
        });
        this.kgPantalla.add(this.txtID);
    }

    private void labelsConfiguracion() {
        JLabel labelTitutlo = new JLabel("Eliminar Venta");
        labelTitutlo.setFont(Fuentes.FUENTE_TITULOS.getFont());
        labelTitutlo.setBounds(350, 20, 350, 30);
        this.kgPantalla.add(labelTitutlo);

        JLabel jlImgTitulo = new JLabel(Imagenes.I_IMAGEN_VENTA.getImagen());
        jlImgTitulo.setFont(Fuentes.FUENTE_TITULOS.getFont());
        jlImgTitulo.setBounds(280, 10, 64, 64);
        this.kgPantalla.add(jlImgTitulo);

        JLabel jlIDCliente = new JLabel("Buscar ID Venta: ");
        jlIDCliente.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlIDCliente.setBounds(280, 100, 200, 30);
        this.kgPantalla.add(jlIDCliente);

        this.jlIdVenta = new JLabel("ID seleccionado: ");
        this.jlIdVenta.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        this.jlIdVenta.setBounds(300, 180, 500, 70);
        this.kgPantalla.add(this.jlIdVenta);
    }

    private void botonesConfiguracion() {
        /*----------------------------------------*/
        this.btnEliminarVenta = new RSButtonRiple();
        this.btnEliminarVenta.setBounds(700, 100, 120, 50);
        this.btnEliminarVenta.setBorderPainted(false);
        this.btnEliminarVenta.setFocusPainted(false);
        this.btnEliminarVenta.setBackground(Colores.C_COLOR_GRIS3.getColor());
        this.btnEliminarVenta.setColorHover(Colores.C_COLOR_GRIS2.getColor());
        this.btnEliminarVenta.setColorText(Colores.C_COLOR_NEGRO.getColor());
        this.btnEliminarVenta.setColorTextHover(Colores.C_COLOR_GRIS4.getColor());
        this.btnEliminarVenta.setText("Eliminar");
        this.btnEliminarVenta.setActionCommand("btnEliminarVenta");
        this.btnEliminarVenta.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (seleccionTrabajador()) {
                    System.out.println("Elimino al cliente");
                }
            }
        });
        this.kgPantalla.add(this.btnEliminarVenta);
    }

    private boolean seleccionTrabajador() {
        int renglon = this.rsTablaVentas.getSelectedRow();
        if (renglon != -1) {
            String rfc = this.rsTablaVentas.getValueAt(renglon, 0) + "";
            this.jlIdVenta.setText("ID seleccionado: " + rfc);
        } else {
            System.out.println("No selecciono venta");
            return false;
        }
        return true;
    }

    private void tablaConfiguracion() {
        this.rsTablaVentas = new RSTableMetro();
        this.rsTablaVentas.setAltoHead(60);
        this.rsTablaVentas.setRowHeight(40);
        this.rsTablaVentas.setGrosorBordeFilas(0);
        this.rsTablaVentas.setColorBackgoundHead(Colores.C_COLOR_GRIS4.getColor());
        this.rsTablaVentas.setColorFilasBackgound1(Colores.C_COLOR_GRIS1.getColor());
        this.rsTablaVentas.setColorFilasBackgound2(Colores.C_COLOR_GRIS2.getColor());
        this.rsTablaVentas.setColorFilasForeground1(Colores.C_COLOR_NEGRO.getColor());
        this.rsTablaVentas.setColorFilasForeground2(Colores.C_COLOR_NEGRO.getColor());
        this.rsTablaVentas.setColorSelBackgound(Colores.C_COLOR_GRIS3.getColor());
        this.rsTablaVentas.setColorBordeFilas(Colores.C_COLOR_GRIS1.getColor());
        this.rsTablaVentas.setFuenteFilas(Fuentes.FUENTE_TEXTOS.getFont());
        this.rsTablaVentas.setFuenteHead(Fuentes.FUENTE_TEXTOS.getFont());
        this.rsTablaVentas.setPreferredScrollableViewportSize(new Dimension(250, 100));
        JScrollPane scrollPane = new JScrollPane(this.rsTablaVentas);
        CargarDatos.rellenarTabla(this.rsTablaVentas, "Venta");
        scrollPane.setBounds(10, 250, 980, 250);
        this.rsTablaVentas.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                seleccionTrabajador();
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }
        });
        this.kgPantalla.add(scrollPane);
    }


    public KGradientPanel getkgPantalla() {
        return this.kgPantalla;
    }
}
