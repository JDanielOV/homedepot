package Ventanas.Pantallas;

import Recursos.Colores;
import Recursos.Fuentes;
import Recursos.Imagenes;
import SQL.CargarDatos;
import keeptoo.KGradientPanel;
import rojerusan.RSTableMetro;

import javax.swing.*;
import javax.swing.border.MatteBorder;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * @author cristina 03-11-2021
 * Ochoa Fecha: 08/11/2021
 * Modificacion: cambie el metodo buscarTabla
 */
public class PantTarjVer {
    private RSTableMetro rsTablaTarjetas;
    private KGradientPanel kgPantalla;
    private JTextField txtNombre;

    public PantTarjVer() {
        this.panelConfiguracion();
        this.labelsConfiguracion();
        this.textosConfiguracion();
        this.tablaConfiguracion();
    }

    private void panelConfiguracion() {
        this.kgPantalla = new KGradientPanel();
        this.kgPantalla = new KGradientPanel();
        this.kgPantalla.setSize(1090, 550);
        this.kgPantalla.setBackground(Colores.C_COLOR_TRANSPARENTE.getColor());
        this.kgPantalla.setkEndColor(Colores.C_COLOR_BLANCO.getColor());
        this.kgPantalla.setkStartColor(Colores.C_COLOR_BLANCO.getColor());
        this.kgPantalla.setkBorderRadius(40);
        this.kgPantalla.setLayout(null);
    }

    private void textosConfiguracion() {
        this.txtNombre = new JTextField();
        this.txtNombre.setBounds(400, 150, 200, 30);
        this.txtNombre.setBackground(Colores.C_COLOR_GRIS1.getColor());
        this.txtNombre.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
        this.txtNombre.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.txtNombre.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyPressed(KeyEvent e) {
            }

            @Override
            public void keyReleased(KeyEvent e) {
                CargarDatos.buscarTabla(rsTablaTarjetas, txtNombre.getText(), "TarjetasCliente", "Tarjetas");
                System.out.println("d");
            }
        });
        this.kgPantalla.add(this.txtNombre);
    }

    private void labelsConfiguracion() {
        JLabel labelTitulo = new JLabel("Lista De Tarjetas");
        labelTitulo.setFont(Fuentes.FUENTE_TITULOS.getFont());
        labelTitulo.setBounds(400, 20, 300, 30);
        this.kgPantalla.add(labelTitulo);

        JLabel jlImgTitulo = new JLabel(Imagenes.I_IMAGEN_TARJETA.getImagen());
        jlImgTitulo.setFont(Fuentes.FUENTE_TITULOS.getFont());
        jlImgTitulo.setBounds(320, 10, 64, 64);
        this.kgPantalla.add(jlImgTitulo);

        JLabel jlNombre = new JLabel("Nombre: ");
        jlNombre.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlNombre.setBounds(300, 150, 100, 30);
        this.kgPantalla.add(jlNombre);
    }

    private void tablaConfiguracion() {

        this.rsTablaTarjetas = new RSTableMetro();
        this.rsTablaTarjetas.setAltoHead(60);
        this.rsTablaTarjetas.setRowHeight(40);
        this.rsTablaTarjetas.setGrosorBordeFilas(0);
        this.rsTablaTarjetas.setColorBackgoundHead(Colores.C_COLOR_GRIS4.getColor());
        this.rsTablaTarjetas.setColorFilasBackgound1(Colores.C_COLOR_GRIS1.getColor());
        this.rsTablaTarjetas.setColorFilasBackgound2(Colores.C_COLOR_GRIS2.getColor());
        this.rsTablaTarjetas.setColorFilasForeground1(Colores.C_COLOR_NEGRO.getColor());
        this.rsTablaTarjetas.setColorFilasForeground2(Colores.C_COLOR_NEGRO.getColor());
        this.rsTablaTarjetas.setColorSelBackgound(Colores.C_COLOR_GRIS3.getColor());
        this.rsTablaTarjetas.setColorBordeFilas(Colores.C_COLOR_GRIS1.getColor());
        this.rsTablaTarjetas.setFuenteFilas(Fuentes.FUENTE_TEXTOS.getFont());
        this.rsTablaTarjetas.setFuenteHead(Fuentes.FUENTE_TEXTOS.getFont());
        this.rsTablaTarjetas.setPreferredScrollableViewportSize(new Dimension(250, 100));
        JScrollPane scrollPane = new JScrollPane(this.rsTablaTarjetas);
        scrollPane.setBounds(50, 250, 910, 250);
        CargarDatos.rellenarTabla(this.rsTablaTarjetas, "Tarjetas");
        this.kgPantalla.add(scrollPane);
    }

    public KGradientPanel getkgPantalla() {
        return this.kgPantalla;
    }
}
