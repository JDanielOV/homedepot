package Ventanas.Pantallas;

import Recursos.Colores;
import Recursos.Fuentes;
import Recursos.Imagenes;
import SQL.Inserciones;
import keeptoo.KGradientPanel;
import rojeru_san.RSButtonRiple;


import javax.swing.*;
import javax.swing.border.MatteBorder;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Juan jose 03-11-2021
 **/
public class PantProvAgregar {
    private KGradientPanel kgPantalla;
    private JTextField txtNombre, txtApePaterno, txtApeMaterno, txtCorreoE, txtTelefono, txtProveedorEmpresa;
    private RSButtonRiple btnCrearUsuario;

    public PantProvAgregar() {
        this.panelConfiguracion();
        this.labelsConfiguracion();
        this.textosConfiguracion();
        this.botonesConfiguracion();
    }



    private void panelConfiguracion() {
        this.kgPantalla = new KGradientPanel();
        this.kgPantalla = new KGradientPanel();
        this.kgPantalla.setSize(1090, 550);
        this.kgPantalla.setBackground(Colores.C_COLOR_TRANSPARENTE.getColor());
        this.kgPantalla.setkEndColor(Colores.C_COLOR_BLANCO.getColor());
        this.kgPantalla.setkStartColor(Colores.C_COLOR_BLANCO.getColor());
        this.kgPantalla.setkBorderRadius(40);
        this.kgPantalla.setLayout(null);
    }

    private void textosConfiguracion() {
        this.txtNombre = new JTextField();
        this.txtNombre.setBounds(100, 150, 200, 30);
        this.txtNombre.setBackground(Colores.C_COLOR_GRIS1.getColor());
        this.txtNombre.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
        this.txtNombre.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.kgPantalla.add(this.txtNombre);


        this.txtApePaterno = new JTextField();
        this.txtApePaterno.setBounds(440, 150, 200, 30);
        this.txtApePaterno.setBackground(Colores.C_COLOR_GRIS1.getColor());
        this.txtApePaterno.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
        this.txtApePaterno.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.kgPantalla.add(this.txtApePaterno);


        this.txtApeMaterno = new JTextField();
        this.txtApeMaterno.setBounds(790, 150, 200, 30);
        this.txtApeMaterno.setBackground(Colores.C_COLOR_GRIS1.getColor());
        this.txtApeMaterno.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
        this.txtApeMaterno.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.kgPantalla.add(this.txtApeMaterno);


        this.txtCorreoE = new JTextField();
        this.txtCorreoE.setBounds(80, 300, 200, 30);
        this.txtCorreoE.setBackground(Colores.C_COLOR_GRIS1.getColor());
        this.txtCorreoE.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
        this.txtCorreoE.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.kgPantalla.add(this.txtCorreoE);
	
        this.txtTelefono = new JTextField();
        this.txtTelefono.setBounds(410, 300, 200, 30);
        this.txtTelefono.setBackground(Colores.C_COLOR_GRIS1.getColor());
        this.txtTelefono.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
        this.txtTelefono.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.kgPantalla.add(this.txtTelefono);
	
	this.txtProveedorEmpresa = new JTextField();
        this.txtProveedorEmpresa.setBounds(810, 300, 180, 30);
        this.txtProveedorEmpresa.setBackground(Colores.C_COLOR_GRIS1.getColor());
        this.txtProveedorEmpresa.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
        this.txtProveedorEmpresa.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.kgPantalla.add(this.txtProveedorEmpresa);
    }

    private void labelsConfiguracion() {
        JLabel labelTitutlo = new JLabel("Agregar Nuevo Proveedor");
        labelTitutlo.setFont(Fuentes.FUENTE_TITULOS.getFont());
        labelTitutlo.setBounds(360, 20, 350, 30);
        this.kgPantalla.add(labelTitutlo);

        JLabel jlImgTitulo = new JLabel(Imagenes.I_IMAGEN_PROVEEDOR.getImagen());
        jlImgTitulo.setFont(Fuentes.FUENTE_TITULOS.getFont());
        jlImgTitulo.setBounds(280, 10, 64, 64);
        this.kgPantalla.add(jlImgTitulo);

        JLabel jlNombreProveedor = new JLabel("Nombre: ");
        jlNombreProveedor.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlNombreProveedor.setBounds(0, 150, 100, 30);
        this.kgPantalla.add(jlNombreProveedor);

        JLabel jApellidoP = new JLabel("Apellido P: ");
        jApellidoP.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jApellidoP.setBounds(330, 150, 130, 30);
        this.kgPantalla.add(jApellidoP);

        JLabel jApellidoM = new JLabel("Apellido M: ");
        jApellidoM.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jApellidoM.setBounds(670, 150, 130, 30);
        this.kgPantalla.add(jApellidoM);

        JLabel jlCorreoE = new JLabel("Email: ");
        jlCorreoE.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlCorreoE.setBounds(0, 300, 130, 30);
        this.kgPantalla.add(jlCorreoE);

        JLabel jlTelefono = new JLabel("Telefono: ");
        jlTelefono.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlTelefono.setBounds(310, 300, 130, 30);
        this.kgPantalla.add(jlTelefono);
	
	JLabel jlProveedorEm = new JLabel("Proveedor Empresa: ");
        jlProveedorEm.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlProveedorEm.setBounds(630, 300, 180, 30);
        this.kgPantalla.add(jlProveedorEm);

        JLabel jlProveedorID = new JLabel("ID Proveedor:          # ");
        jlProveedorID.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlProveedorID.setBounds(0, 450, 200, 30);
        this.kgPantalla.add(jlProveedorID);
    }

  

    private void botonesConfiguracion() {
        /*----------------------------------------*/
        this.btnCrearUsuario = new RSButtonRiple();
        this.btnCrearUsuario.setBounds(310, 450, 120, 50);
        this.btnCrearUsuario.setBorderPainted(false);
        this.btnCrearUsuario.setFocusPainted(false);
        this.btnCrearUsuario.setBackground(Colores.C_COLOR_GRIS3.getColor());
        this.btnCrearUsuario.setColorHover(Colores.C_COLOR_GRIS2.getColor());
        this.btnCrearUsuario.setColorText(Colores.C_COLOR_NEGRO.getColor());
        this.btnCrearUsuario.setColorTextHover(Colores.C_COLOR_GRIS4.getColor());
        this.btnCrearUsuario.setText("Agregar");
        this.btnCrearUsuario.setActionCommand("btnProveedores");
        this.btnCrearUsuario.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                String sNombre = txtNombre.getText().trim();
                String sApellidoP = txtApePaterno.getText().trim();
                String sApellidoM = txtApeMaterno.getText().trim();
                String sCorreo = txtCorreoE.getText().trim();
                String sTelefono = txtTelefono.getText().trim();
                String sProveedorEmpre = txtProveedorEmpresa.getText().trim();
                boolean bInsercion = Inserciones.agregarProveedores(sNombre, sApellidoP, sApellidoM, sCorreo, sTelefono, sProveedorEmpre); 
                if(bInsercion){
                    JOptionPane.showMessageDialog(null,"Proveedor agregado correctamente");
                    txtNombre.setText("");
                    txtApePaterno.setText("");
                    txtApeMaterno.setText("");
                    txtCorreoE.setText("");
                    txtTelefono.setText("");
                    txtProveedorEmpresa.setText("");
                }
               }
        });
        this.kgPantalla.add(this.btnCrearUsuario);
    }

    public KGradientPanel getkgPantalla() {
        return this.kgPantalla;
    }
}

