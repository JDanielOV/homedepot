package Ventanas.Pantallas;

import Recursos.Colores;
import Recursos.Fuentes;
import Recursos.Imagenes;
import SQL.CargarDatos;
import keeptoo.KGradientPanel;
import rojerusan.RSTableMetro;

import javax.swing.*;
import javax.swing.border.MatteBorder;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * Juan Jose Alvarado Lagunes
 * Ochoa Fecha: 08/11/2021
 * Modificacion: cambie el metodo buscarTabla
 **/
public class PantProvVer {
    private RSTableMetro rsTablaProveedores;
    private KGradientPanel kgPantalla;
    private JTextField txtNombre;

    public PantProvVer() {
        this.panelConfiguracion();
        this.labelsConfiguracion();
        this.textosConfiguracion();
        this.tablaConfiguracion();
    }


    private void panelConfiguracion() {
        this.kgPantalla = new KGradientPanel();
        this.kgPantalla = new KGradientPanel();
        this.kgPantalla.setSize(1090, 550);
        this.kgPantalla.setBackground(Colores.C_COLOR_TRANSPARENTE.getColor());
        this.kgPantalla.setkEndColor(Colores.C_COLOR_BLANCO.getColor());
        this.kgPantalla.setkStartColor(Colores.C_COLOR_BLANCO.getColor());
        this.kgPantalla.setkBorderRadius(40);
        this.kgPantalla.setLayout(null);
    }

    private void textosConfiguracion() {
        this.txtNombre = new JTextField();
        this.txtNombre.setBounds(400, 150, 200, 30);
        this.txtNombre.setBackground(Colores.C_COLOR_GRIS1.getColor());
        this.txtNombre.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
        this.txtNombre.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.txtNombre.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyPressed(KeyEvent e) {
            }

            @Override
            public void keyReleased(KeyEvent e) {
                CargarDatos.buscarTabla(rsTablaProveedores, txtNombre.getText(), "ProveedorNombre", "Proveedor");
                System.out.println("d");
            }
        });
        this.kgPantalla.add(this.txtNombre);
    }

    private void labelsConfiguracion() {
        JLabel labelTitulo = new JLabel("Lista De Proveedores");
        labelTitulo.setFont(Fuentes.FUENTE_TITULOS.getFont());
        labelTitulo.setBounds(360, 20, 300, 30);
        this.kgPantalla.add(labelTitulo);

        JLabel jlImgTitulo = new JLabel(Imagenes.I_IMAGEN_PROVEEDOR.getImagen());
        jlImgTitulo.setFont(Fuentes.FUENTE_TITULOS.getFont());
        jlImgTitulo.setBounds(280, 10, 64, 64);
        this.kgPantalla.add(jlImgTitulo);

        JLabel jlNombre = new JLabel("Nombre: ");
        jlNombre.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlNombre.setBounds(300, 150, 100, 30);
        this.kgPantalla.add(jlNombre);
    }

    private void tablaConfiguracion() {

        this.rsTablaProveedores = new RSTableMetro();
        this.rsTablaProveedores.setAltoHead(60);
        this.rsTablaProveedores.setRowHeight(40);
        this.rsTablaProveedores.setGrosorBordeFilas(0);
        this.rsTablaProveedores.setColorBackgoundHead(Colores.C_COLOR_GRIS4.getColor());
        this.rsTablaProveedores.setColorFilasBackgound1(Colores.C_COLOR_GRIS1.getColor());
        this.rsTablaProveedores.setColorFilasBackgound2(Colores.C_COLOR_GRIS2.getColor());
        this.rsTablaProveedores.setColorFilasForeground1(Colores.C_COLOR_NEGRO.getColor());
        this.rsTablaProveedores.setColorFilasForeground2(Colores.C_COLOR_NEGRO.getColor());
        this.rsTablaProveedores.setColorSelBackgound(Colores.C_COLOR_GRIS3.getColor());
        this.rsTablaProveedores.setColorBordeFilas(Colores.C_COLOR_GRIS1.getColor());
        this.rsTablaProveedores.setFuenteFilas(Fuentes.FUENTE_TEXTOS.getFont());
        this.rsTablaProveedores.setFuenteHead(Fuentes.FUENTE_TEXTOS.getFont());
        this.rsTablaProveedores.setPreferredScrollableViewportSize(new Dimension(250, 100));
        JScrollPane scrollPane = new JScrollPane(this.rsTablaProveedores);
        scrollPane.setBounds(50, 250, 910, 250);
        CargarDatos.rellenarTabla(this.rsTablaProveedores, "Proveedor");
        this.kgPantalla.add(scrollPane);
    }

    public KGradientPanel getkgPantalla() {
        return this.kgPantalla;
    }
}