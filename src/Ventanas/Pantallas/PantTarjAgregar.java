package Ventanas.Pantallas;

import Recursos.Colores;
import Recursos.Fuentes;
import Recursos.Imagenes;
import SQL.CargarDatos;
import SQL.Inserciones;
import SQL.Login;
import keeptoo.KGradientPanel;
import rojeru_san.RSButtonRiple;

import javax.swing.*;
import javax.swing.border.MatteBorder;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import rojerusan.RSComboMetro;

/**
 * @author cristina 03-11-2021
 * Ochoa Fecha: 14/11/2021
 * Modificaciones: puse el codigo correspondiente para crear una trjeta y modifque el contructor
 */
public class PantTarjAgregar {
    private KGradientPanel kgPantalla;
    private RSButtonRiple btnCrearTarjeta;
    private JTextField txtClienteId;
    private JLabel txtTrabajadorRfc;
    private RSComboMetro rscmClienteId;
    private LocalDate fechaHoy, expiracion;
    private Login lTrabajador;

    public PantTarjAgregar(Login lUsuario) {
        this.lTrabajador = lUsuario;
        this.panelConfiguracion();
        this.labelsConfiguracion();
        this.textosConfiguracion();
        this.botonesConfiguracion();
	this.comboBoxConfiguracion();
    }

    private void panelConfiguracion() {
        this.kgPantalla = new KGradientPanel();
        this.kgPantalla = new KGradientPanel();
        this.kgPantalla.setSize(1090, 550);
        this.kgPantalla.setBackground(Colores.C_COLOR_TRANSPARENTE.getColor());
        this.kgPantalla.setkEndColor(Colores.C_COLOR_BLANCO.getColor());
        this.kgPantalla.setkStartColor(Colores.C_COLOR_BLANCO.getColor());
        this.kgPantalla.setkBorderRadius(40);
        this.kgPantalla.setLayout(null);
    }

    private void textosConfiguracion() {
        
	this.txtTrabajadorRfc = new JLabel(this.lTrabajador.getsRFC());
        this.txtTrabajadorRfc.setBounds(620, 280, 200, 30);
        this.txtTrabajadorRfc.setBackground(Colores.C_COLOR_GRIS1.getColor());
        this.txtTrabajadorRfc.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
        this.txtTrabajadorRfc.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.kgPantalla.add(this.txtTrabajadorRfc);
    }

    private void labelsConfiguracion() {
        JLabel labelTituto = new JLabel("Agregar Nueva Tarjeta");
        labelTituto.setFont(Fuentes.FUENTE_TITULOS.getFont());
        labelTituto.setBounds(420, 20, 350, 30);
        this.kgPantalla.add(labelTituto);

        JLabel jlImgTitulo = new JLabel(Imagenes.I_IMAGEN_TARJETA.getImagen());
        jlImgTitulo.setFont(Fuentes.FUENTE_TITULOS.getFont());
        jlImgTitulo.setBounds(340, 10, 64, 64);
        this.kgPantalla.add(jlImgTitulo);

        this.fechaHoy = LocalDate.now();
        String formatoFecha = fechaHoy.format(DateTimeFormatter.ofPattern("dd-MM-YYYY"));
        JLabel labelFechaActual = new JLabel("Fecha de alta:  " + formatoFecha);
        labelFechaActual.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        labelFechaActual.setBounds(0, 100, 250, 30);
        this.kgPantalla.add(labelFechaActual);

        this.expiracion = fechaHoy.plusYears(3);
        String formatoExpiracion = expiracion.format(DateTimeFormatter.ofPattern("dd-MM-YYYY"));
        JLabel labelFechaExpiracion = new JLabel("Fecha de expiracion:  " + formatoExpiracion);
        labelFechaExpiracion.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        labelFechaExpiracion.setBounds(400, 100, 290, 30);
        this.kgPantalla.add(labelFechaExpiracion);

        JLabel labelPuntos = new JLabel("Puntos:  " + 0);
        labelPuntos.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        labelPuntos.setBounds(750, 100, 200, 30);
        this.kgPantalla.add(labelPuntos);

        JLabel jlTarjetaNum = new JLabel("Numero de Tarjeta:         # ");
        jlTarjetaNum.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlTarjetaNum.setBounds(0, 450, 240, 30);
        this.kgPantalla.add(jlTarjetaNum);

        JLabel jlClienteId = new JLabel("Cliente: ");
        jlClienteId.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlClienteId.setBounds(0, 280, 160, 30);
        this.kgPantalla.add(jlClienteId);

        JLabel jlTrabajadorRfc = new JLabel("Trabajador RFC: ");
        jlTrabajadorRfc.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlTrabajadorRfc.setBounds(460, 280, 170, 30);
        this.kgPantalla.add(jlTrabajadorRfc);
    }

    private void botonesConfiguracion() {
        this.btnCrearTarjeta = new RSButtonRiple();
        this.btnCrearTarjeta.setBounds(420, 450, 120, 50);
        this.btnCrearTarjeta.setBorderPainted(false);
        this.btnCrearTarjeta.setFocusPainted(false);
        this.btnCrearTarjeta.setBackground(Colores.C_COLOR_GRIS3.getColor());
        this.btnCrearTarjeta.setColorHover(Colores.C_COLOR_GRIS2.getColor());
        this.btnCrearTarjeta.setColorText(Colores.C_COLOR_NEGRO.getColor());
        this.btnCrearTarjeta.setColorTextHover(Colores.C_COLOR_GRIS4.getColor());
        this.btnCrearTarjeta.setText("Agregar");
        this.btnCrearTarjeta.setActionCommand("btnTarjetas");
        this.btnCrearTarjeta.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                String sNombreClienteIDCliente = rscmClienteId.getSelectedItem() == null ? " " : rscmClienteId.getSelectedItem().toString();
                ;
                int iComa = sNombreClienteIDCliente.indexOf(", ");
                int iDosPuntosEspacio = sNombreClienteIDCliente.indexOf("ID: ");

                String sNombrea = (sNombreClienteIDCliente.contains(", ")) ? (sNombreClienteIDCliente.substring(0, iComa)) : (" ");
                String sId = (sNombreClienteIDCliente.contains("ID: ")) ? (sNombreClienteIDCliente.substring(iDosPuntosEspacio + 4)) : (" ");
                //String sId = txtClienteId.getText().trim();
                String sRFC = txtTrabajadorRfc.getText().trim();
                String sFechaHoy = fechaHoy.format(DateTimeFormatter.ofPattern("YYYY-MM-dd"));
                String sFechaExpi = expiracion.format(DateTimeFormatter.ofPattern("YYYY-MM-dd"));
                boolean bControl = Inserciones.insertarTarjeta(sId, sRFC, sFechaHoy, sFechaExpi);
                if (bControl) {
                    JOptionPane.showMessageDialog(null, "Tarjeta creada");
                    rscmClienteId.removeAllItems();
                }
            }
        });
        this.kgPantalla.add(this.btnCrearTarjeta);
    }

    public KGradientPanel getkgPantalla() {
        return this.kgPantalla;
    }


private void comboBoxConfiguracion() {

	this.rscmClienteId = new RSComboMetro();
	this.rscmClienteId.setColorFondo(Colores.C_COLOR_GRIS4.getColor());
	this.rscmClienteId.setColorBorde(Colores.C_COLOR_GRIS3.getColor());
	this.rscmClienteId.setColorArrow(Colores.C_COLOR_GRIS3.getColor());
	this.rscmClienteId.setForeground(Colores.C_COLOR_NEGRO.getColor());
	this.rscmClienteId.setFont(Fuentes.FUENTE_TEXTOS.getFont());
	this.rscmClienteId.setBounds(120, 280, 300, 30);
	this.rscmClienteId.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
	this.rscmClienteId.setEditable(true);
	CargarDatos.comboBoxPonerDatos(this.rscmClienteId, "nombreClienteIdCliente");
	this.kgPantalla.add(this.rscmClienteId);
	
}
}