package Ventanas.Pantallas;

import Recursos.Colores;
import Recursos.Fuentes;
import Recursos.Imagenes;
import SQL.Actualizar;
import SQL.ActualizarCargarDatos;
import SQL.CargarDatos;
import SQL.Inserciones;
import SQL.ValidacionesGenerales;
import static Ventanas.Pantallas.PantVentAgregar.sProductoEmpresaCombo;
import keeptoo.KGradientPanel;
import rojeru_san.RSButtonRiple;
import rojerusan.RSDateChooser;
import rojerusan.RSPopuMenu;
import rojerusan.RSTableMetro;

import javax.swing.*;
import javax.swing.border.MatteBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.*;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import rojerusan.RSComboMetro;

/**
 * @author Ochoa
 * Ochoa Fecha: 12/11/2021
 * Modificacion: hice invisible el datachooser
 */
public class PantPedActualizaciones {
    private KGradientPanel kgPantalla;
    private JTextField txtIDProducto, txtIDPedido;
    private RSButtonRiple btnAgregarPedido, btnAgregarProducto, btnEliminarProducto;
    private JSlider jsCantidad;
    private JSpinner jspCantidad;
    private RSDateChooser rdFechas;
    private RSTableMetro rsTablaVentas;
    private RSComboMetro rscmProductos;
    private JLabel jlIdVenta, jlTotalCosto,txtRFC,txtIDProveedor;
    private JRadioButton rbEntrega;
    private boolean correctoP,correctoPedido;
    public static String sProveedorCombo,cadena,idProveedor,idProducto;
    
    private double dDinero = 0.0;
    private final String[] STITULOS_TABLA = {"ID Pedido", "ID Producto", "Nombre", "Precio_Total", "Departamento", "Cantidad"};
    private Object[][] oContenidoTabla = {};

    public PantPedActualizaciones() {
        this.panelConfiguracion();
        this.labelsConfiguracion();
        this.textosConfiguracion();
        this.botonesConfiguracion();
        this.sliderConfiguracion();
        this.spinnerConfiguracion();
        this.tablaConfiguracion();
        this.dateChooserConfiguracion();
        this.comboBoxConfiguracion();
        this.radioButtonConfiguracion();
    }

    private void panelConfiguracion() {
        this.kgPantalla = new KGradientPanel();
        this.kgPantalla = new KGradientPanel();
        this.kgPantalla.setSize(1090, 550);
        this.kgPantalla.setBackground(Colores.C_COLOR_TRANSPARENTE.getColor());
        this.kgPantalla.setkEndColor(Colores.C_COLOR_BLANCO.getColor());
        this.kgPantalla.setkStartColor(Colores.C_COLOR_BLANCO.getColor());
        this.kgPantalla.setkBorderRadius(40);
        this.kgPantalla.setLayout(null);
    }
    
    private void radioButtonConfiguracion(){
        this.rbEntrega = new JRadioButton();
        this.rbEntrega.setText("Ya ha sido entregado");
        this.rbEntrega.setBackground(Colores.C_COLOR_BLANCO.getColor());
        this.rbEntrega.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.rbEntrega.setBounds(800, 90, 250, 30);
        this.kgPantalla.add(this.rbEntrega);
    }

    private void dateChooserConfiguracion() {
        this.rdFechas = new RSDateChooser();
        this.rdFechas.setColorBackground(Colores.C_COLOR_GRIS3.getColor());
        this.rdFechas.setColorButtonHover(Colores.C_COLOR_GRIS3.getColor());
        this.rdFechas.setColorTextDiaActual(Colores.C_COLOR_NEGRO.getColor());
        this.rdFechas.setColorForeground(Colores.C_COLOR_NEGRO.getColor());
        this.rdFechas.setFormatoFecha("YYYY-MM-dd");
        this.rdFechas.setFuente(Fuentes.FUENTE_TEXTOS.getFont());
        this.rdFechas.setBounds(660, 240, 180, 30);
        this.rdFechas.setVisible(false);
        this.kgPantalla.add(rdFechas);
    }
    
    private void comboBoxConfiguracion() {
        /*this.rscmProveedor = new RSComboMetro();
        this.rscmProveedor.setColorFondo(Colores.C_COLOR_GRIS4.getColor());
        this.rscmProveedor.setColorBorde(Colores.C_COLOR_GRIS3.getColor());
        this.rscmProveedor.setColorArrow(Colores.C_COLOR_GRIS3.getColor());
        this.rscmProveedor.setForeground(Colores.C_COLOR_NEGRO.getColor());
        this.rscmProveedor.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.rscmProveedor.setBounds(700, 160, 320, 30);
        this.rscmProveedor.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
        this.rscmProveedor.setEditable(true);
        CargarDatos.comboBoxPonerDatos(this.rscmProveedor, "nombreProveedor");
        this.rscmProveedor.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String sValor = rscmProveedor.getSelectedItem().toString();
                System.out.println("Id Proveedor selecionado:#" + sValor.substring(sValor.lastIndexOf(" ") + 1) + "#");
                DefaultComboBoxModel dcbmNuevo = CargarDatos.cargarComboBox(sValor.substring(sValor.lastIndexOf(" ") + 1), "nombreProductoIdProveedorV");
                dcbmNuevo.addElement(" ");
                rscmProductos.setModel(dcbmNuevo);
                rscmProductos.setSelectedIndex(rscmProductos.getItemCount() - 1);
                
            }
        });
        this.kgPantalla.add(this.rscmProveedor);*/
        
        this.rscmProductos = new RSComboMetro();
        this.rscmProductos.setColorFondo(Colores.C_COLOR_GRIS4.getColor());
        this.rscmProductos.setColorBorde(Colores.C_COLOR_GRIS3.getColor());
        this.rscmProductos.setColorArrow(Colores.C_COLOR_GRIS3.getColor());
        this.rscmProductos.setForeground(Colores.C_COLOR_NEGRO.getColor());
        this.rscmProductos.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.rscmProductos.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
        this.rscmProductos.setBounds(345, 90, 300, 30);
        this.kgPantalla.add(this.rscmProductos);
    }

    private void textosConfiguracion() {
        this.txtIDPedido = new JTextField();
        this.txtIDPedido.setBounds(100, 90, 145, 30);
        this.txtIDPedido.setBackground(Colores.C_COLOR_GRIS1.getColor());
        this.txtIDPedido.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
        this.txtIDPedido.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.txtIDPedido.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent keyEvent) {

            }

            @Override
            public void keyPressed(KeyEvent keyEvent) {

            }

            @Override
            public void keyReleased(KeyEvent keyEvent) {
                String sId = txtIDPedido.getText();
                txtRFC.setText("");
                txtIDProveedor.setText("");
                boolean bValidarPedido = ValidacionesGenerales.validarExistencia(sId, "Pedidos");
                System.out.println("estado"+bValidarPedido);
                if(bValidarPedido){
                    rsTablaVentas.setVisible(true);
                dDinero=0.0;
                JComponent[] jtMisEntradas = {txtRFC,txtIDProveedor,rdFechas};
                String sIdCliente = txtIDPedido.getText();
                int[] iCampos = {2, 3,5};
                ActualizarCargarDatos.cargarDatos(sIdCliente, 4, jtMisEntradas, iCampos);
                CargarDatos.buscarTabla(rsTablaVentas, sIdCliente, "PedidoProductos", "PedidoProductosId");
                jspCantidad.getEditor().getComponent(0).setEnabled(false);
                //txtRFC.setEnabled(false);
                //txtIDProveedor.setEnabled(false);
                dDinero = dineroTotalAPagar();
                DecimalFormatSymbols separadoresPersonalizados = new DecimalFormatSymbols();
                separadoresPersonalizados.setDecimalSeparator('.');
                DecimalFormat formato1 = new DecimalFormat("#.00",separadoresPersonalizados);
                jlTotalCosto.setText("Total a pagar: " + formato1.format(dDinero));
                String idPro=txtIDProveedor.getText().trim();
                DefaultComboBoxModel dcbmNuevo = CargarDatos.cargarComboBox(idPro, "nombreProductoIdProveedorV");
                dcbmNuevo.addElement(" ");
                rscmProductos.setModel(dcbmNuevo);
                rscmProductos.setSelectedIndex(rscmProductos.getItemCount() - 1);
                }
                if(sId.isBlank()){
                    rsTablaVentas.setVisible(false);
                    txtRFC.setText("");
                    txtIDProveedor.setText("");
                }if(!bValidarPedido){
                    rsTablaVentas.setVisible(false);
                    txtRFC.setText("");
                    txtIDProveedor.setText("");
                }
            }
        });
        this.kgPantalla.add(this.txtIDPedido);


        this.txtIDProducto = new JTextField();
        this.txtIDProducto.setBounds(430, 90, 200, 30);
        this.txtIDProducto.setBackground(Colores.C_COLOR_GRIS1.getColor());
        this.txtIDProducto.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
        this.txtIDProducto.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        //this.kgPantalla.add(this.txtIDProducto);


        this.txtIDProveedor = new JLabel();
        this.txtIDProveedor.setBounds(700, 160, 200, 30);
        this.txtIDProveedor.setBackground(Colores.C_COLOR_GRIS1.getColor());
        this.txtIDProveedor.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
        this.txtIDProveedor.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        this.kgPantalla.add(this.txtIDProveedor);


        this.txtRFC = new JLabel();
        this.txtRFC.setBounds(900, 0, 200, 30);
        this.txtRFC.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        this.kgPantalla.add(this.txtRFC);

    }

    private void labelsConfiguracion() {
        JLabel labelTitutlo = new JLabel("Actualizar Pedido");
        labelTitutlo.setFont(Fuentes.FUENTE_TITULOS.getFont());
        labelTitutlo.setBounds(400, 20, 350, 30);
        this.kgPantalla.add(labelTitutlo);

        JLabel jlImgTitulo = new JLabel(Imagenes.I_IMAGEN_PEDIDO.getImagen());
        jlImgTitulo.setFont(Fuentes.FUENTE_TITULOS.getFont());
        jlImgTitulo.setBounds(320, 10, 64, 64);
        this.kgPantalla.add(jlImgTitulo);

        JLabel jlIDPedido = new JLabel("ID Pedido: ");
        jlIDPedido.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlIDPedido.setBounds(0, 90, 100, 30);
        this.kgPantalla.add(jlIDPedido);


        JLabel jlIDProducto = new JLabel("Producto: ");
        jlIDProducto.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlIDProducto.setBounds(255, 90, 150, 30);
        this.kgPantalla.add(jlIDProducto);

        JLabel jlIDProveedor = new JLabel("Proveedor: ");
        jlIDProveedor.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlIDProveedor.setBounds(595, 160, 130, 30);
        this.kgPantalla.add(jlIDProveedor);

        JLabel jlRFC = new JLabel("RFC Trabajador: ");
        jlRFC.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlRFC.setBounds(750, 0, 250, 30);
        this.kgPantalla.add(jlRFC);

        JLabel jlCantidad = new JLabel("Cantidad: ");
        jlCantidad.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlCantidad.setBounds(0, 160, 180, 30);
        this.kgPantalla.add(jlCantidad);

        JLabel labelFechaExpiracion = new JLabel("Fecha de entrega:  ");
        labelFechaExpiracion.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        labelFechaExpiracion.setBounds(480, 240, 250, 30);
        this.kgPantalla.add(labelFechaExpiracion);

        this.jlIdVenta = new JLabel("ID seleccionado: ");
        this.jlIdVenta.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        this.jlIdVenta.setBounds(0, 250, 500, 30);
        this.kgPantalla.add(this.jlIdVenta);
        
        JLabel entrega= new JLabel("Entregado: ");
        entrega.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        entrega.setBounds(700, 90, 240, 30);
        this.kgPantalla.add(entrega);
        
        DecimalFormatSymbols separadoresPersonalizados = new DecimalFormatSymbols();
        separadoresPersonalizados.setDecimalSeparator('.');
        DecimalFormat formato1 = new DecimalFormat("#.00",separadoresPersonalizados);
        this.jlTotalCosto = new JLabel("Total a pagar: "+ formato1.format(dDinero));
        this.jlTotalCosto.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        this.jlTotalCosto.setBounds(250, 250, 230, 30);
        this.kgPantalla.add(this.jlTotalCosto);
    }

    private void sliderConfiguracion() {
        this.jsCantidad = new JSlider(1, 1000, 500);
        this.jsCantidad.setOrientation(SwingConstants.HORIZONTAL);
        this.jsCantidad.setMajorTickSpacing(100);
        this.jsCantidad.setMinorTickSpacing(10);
        this.jsCantidad.setPaintLabels(true);
        this.jsCantidad.setPaintTicks(true);
        this.jsCantidad.setBounds(90, 170, 500, 50);
        this.kgPantalla.add(jsCantidad);
        this.jsCantidad.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                cambiarValorSpinner();
            }
        });
    }

    private void spinnerConfiguracion() {
        this.jspCantidad = new JSpinner();
        this.jspCantidad.setBounds(120, 220, 80, 30);
        this.jspCantidad.setValue(this.jsCantidad.getValue());
        this.jspCantidad.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        this.jspCantidad.getEditor().getComponent(0).setEnabled(false);
        this.jspCantidad.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                cambiarValorSlider();
                int valor = (int) jspCantidad.getValue();
                if (valor <= 0) {
                    jspCantidad.setValue(0);
                } else if (valor >= 1000) {
                    jspCantidad.setValue(1000);
                }
            }

        });
        this.kgPantalla.add(this.jspCantidad);
    }

    private void botonesConfiguracion() {
        /*----------------------------------------*/
        this.btnAgregarPedido = new RSButtonRiple();
        this.btnAgregarPedido.setBounds(850, 460, 160, 50);
        this.btnAgregarPedido.setBorderPainted(false);
        this.btnAgregarPedido.setFocusPainted(false);
        this.btnAgregarPedido.setBackground(Colores.C_COLOR_GRIS3.getColor());
        this.btnAgregarPedido.setColorHover(Colores.C_COLOR_GRIS2.getColor());
        this.btnAgregarPedido.setColorText(Colores.C_COLOR_NEGRO.getColor());
        this.btnAgregarPedido.setColorTextHover(Colores.C_COLOR_GRIS4.getColor());
        this.btnAgregarPedido.setText("Actualizar Pedido");
        this.btnAgregarPedido.setActionCommand("btnPedidos");
        this.btnAgregarPedido.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                String sProveedor = txtIDProveedor.getText().trim();
                LocalDate fechaHoy = LocalDate.now();
                String formatoFecha = fechaHoy.format(DateTimeFormatter.ofPattern("YYYY-MM-dd"));
                String fechaEntrega = rdFechas.getFechaSeleccionada().trim();
                double dTotla = dineroTotalAPagar();
                String sTotal = String.valueOf(dTotla);
                
                            if(validarProducto()){
                                System.out.println("Estan correctos los productos");
                            } else if (rsTablaVentas.getRowCount()==0){
                                JOptionPane.showMessageDialog(null, "No ha agregado productos");
                                }else {
                                 JOptionPane.showMessageDialog(null, "Error en los productos, todos los productos deben pertenecer al proveedor= "+sProveedor);
                                }
                if (validarProducto()) {
                    if(!txtRFC.equals("")){
                        dDinero=0.0;
                            dDinero = dineroTotalAPagar();
                            String dinero = String.valueOf(dDinero);
                            String entrega;
                            if(rbEntrega.isSelected()){
                                    entrega="1";
                                }else{
                                    entrega="0";
                                }
                        String[] sParametros = {
                            rdFechas.getFechaSeleccionada().trim(),
                            dinero,
                            txtIDPedido.getText().trim(),
                            entrega
                    };
                        boolean bActualizarCliente = Actualizar.actualizarPedido(sParametros);
                    if (bActualizarCliente) {
                    for (int i = 0; i < rsTablaVentas.getRowCount(); i++) {
                            String sId = rsTablaVentas.getValueAt(i, 1).toString().trim();
                            String sPrecio = rsTablaVentas.getValueAt(i, 3).toString().trim();
                            String sCantidad = rsTablaVentas.getValueAt(i, 5).toString().trim();
                            System.out.println(sId);
                            if(validarProducto()){
                                String pedido = txtIDPedido.getText().trim();
                                boolean bValidarProducto = ValidacionesGenerales.validarExistenciaPedidoPro(pedido,sId,"PedidoProductosId",2);
                                if(bValidarProducto){
                                    System.out.println("Ya esta en factura"+bValidarProducto);
                                String[] sParametrosP = {
                                sId,
                                sPrecio,
                                sCantidad,
                                txtIDPedido.getText().trim(),
                                entrega
                                };
                                correctoP=Actualizar.actualizarFacturaPedido(sParametrosP);
                                }else if(!bValidarProducto){
                                    System.out.println("No esta en factura");
                                    correctoP=Inserciones.agregarFacturaPedidoActualizar(pedido,sId, sPrecio, sCantidad);
                                }
                            } else {
                             JOptionPane.showMessageDialog(null, "Error en los productos, todos los productos deben pertenecer al proveedor= "+sProveedor);
                            }
                        }
                    if (bActualizarCliente && correctoP) {
                    JOptionPane.showMessageDialog(null, "Pedido actualizado correctamente");
                    dDinero=0.0;
                    dDinero = dineroTotalAPagar();
                    String dineroT = String.valueOf(dDinero);
                    String[] sParametros1 = {
                        dineroT,
                        txtIDPedido.getText().trim()
                    };
                     Actualizar.actualizarPedidoTotal(sParametros1);
                    rscmProductos.removeAllItems();
                    txtIDPedido.setText("");
                    txtRFC.setText("");
                    rdFechas.setTextoFecha(formatoFecha);
                    rsTablaVentas.setVisible(false);
                    dDinero=0.0;
                    }
                    }
                    } else {
                    JOptionPane.showMessageDialog(null, "No a selecionado un pedido o no existe");
                        }
                    DecimalFormatSymbols separadoresPersonalizados = new DecimalFormatSymbols();
                    separadoresPersonalizados.setDecimalSeparator('.');
                    DecimalFormat formato1 = new DecimalFormat("#.00",separadoresPersonalizados);
                    jlTotalCosto.setText("Total a pagar: " + formato1.format(dDinero));
                } 
            }
        });
        this.kgPantalla.add(this.btnAgregarPedido);


        /*----------------------------------------*/
        this.btnAgregarProducto = new RSButtonRiple();
        this.btnAgregarProducto.setBounds(850, 300, 160, 50);
        this.btnAgregarProducto.setBorderPainted(false);
        this.btnAgregarProducto.setFocusPainted(false);
        this.btnAgregarProducto.setBackground(Colores.C_COLOR_GRIS3.getColor());
        this.btnAgregarProducto.setColorHover(Colores.C_COLOR_GRIS2.getColor());
        this.btnAgregarProducto.setColorText(Colores.C_COLOR_NEGRO.getColor());
        this.btnAgregarProducto.setColorTextHover(Colores.C_COLOR_GRIS4.getColor());
        this.btnAgregarProducto.setText("Agregar Producto");
        this.btnAgregarProducto.setActionCommand("btnClientes");
        this.btnAgregarProducto.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                DecimalFormatSymbols separadoresPersonalizados = new DecimalFormatSymbols();
                separadoresPersonalizados.setDecimalSeparator('.');
                DecimalFormat formato1 = new DecimalFormat("#.00",separadoresPersonalizados);
                String sProductoEmpresa = rscmProductos.getSelectedItem() == null ? " " : rscmProductos.getSelectedItem().toString();
                int iPrimerEspacio = sProductoEmpresa.indexOf(", ");
                int iDosPuntosEspacio = sProductoEmpresa.indexOf(": ");

                String sProducto = (sProductoEmpresa.contains(", ")) ? (sProductoEmpresa.substring(0, iPrimerEspacio)) : ("");
                String sEmpresa = (sProductoEmpresa.contains(": ")) ? (sProductoEmpresa.substring(iDosPuntosEspacio + 2)) : ("");

                System.out.println("#" + sProducto + "#" + sEmpresa + "#" + sProductoEmpresa + "#");
                String sIdProducto = sEmpresa;
                
                String sProveedor = txtIDProveedor.getText().trim();
                System.out.println(sProveedor);
                String sIdPedido = txtIDPedido.getText().trim();
                String sCantidadPiezas = String.valueOf(jsCantidad.getValue()); 
                boolean bValidarProducto = (ValidacionesGenerales.validarNumero(sIdProducto)) &&
                        ValidacionesGenerales.validarExistencia(sIdProducto, "Producto") && 
                        ValidacionesGenerales.validarProveedorPedido(sProveedor, sIdProducto, "Producto") &&
                        ValidacionesGenerales.validarCantidadPedido(sIdProducto,sCantidadPiezas);
                if (bValidarProducto) {
                    DefaultTableModel dtmTabla = (DefaultTableModel) rsTablaVentas.getModel();
                    ArrayList<String> sFilaProducto = CargarDatos.obtenerRegistros(sIdProducto, "Producto").get(0);
                    int iCantidadSelecionada = jsCantidad.getValue();    
                    String sPrecio = sFilaProducto.get(2);
                        double dPrecio = Double.parseDouble(sPrecio);
                        double preciototal = dPrecio * iCantidadSelecionada;
                        Object[] oDatosProducto = {sIdPedido,sIdProducto, sFilaProducto.get(1), formato1.format(preciototal), sFilaProducto.get(3), iCantidadSelecionada};
                        boolean bAgruparValidar = agruparProductos(oDatosProducto);
                        rsTablaVentas.setModel(dtmTabla);
                        if (bAgruparValidar) {
                            dDinero=0.0;
                            dDinero = dineroTotalAPagar();
                            jlTotalCosto.setText("Total a pagar: " + formato1.format(dDinero));
                        }else {
                             JOptionPane.showMessageDialog(null, "Error con la cantidad");
                         }
                } else {
                    JOptionPane.showMessageDialog(null, "Error en los datos proporcionados");
                }
            }
        });
        this.kgPantalla.add(this.btnAgregarProducto);

        this.btnEliminarProducto = new RSButtonRiple();
        this.btnEliminarProducto.setBounds(850, 380, 160, 50);
        this.btnEliminarProducto.setBorderPainted(false);
        this.btnEliminarProducto.setFocusPainted(false);
        this.btnEliminarProducto.setBackground(Colores.C_COLOR_GRIS3.getColor());
        this.btnEliminarProducto.setColorHover(Colores.C_COLOR_GRIS2.getColor());
        this.btnEliminarProducto.setColorText(Colores.C_COLOR_NEGRO.getColor());
        this.btnEliminarProducto.setColorTextHover(Colores.C_COLOR_GRIS4.getColor());
        this.btnEliminarProducto.setText("Eliminar producto");
        this.btnEliminarProducto.setActionCommand("btnEliminarProducto");
        this.btnEliminarProducto.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                    if (seleccionTrabajador()) {
                    String pedido = txtIDPedido.getText().trim();
                    int renglon = rsTablaVentas.getSelectedRow();
                    String sId = rsTablaVentas.getValueAt(renglon, 1).toString().trim();
                    boolean bEliminar=Actualizar.eliminarFacturaPedido(pedido,sId);
                    if(bEliminar){
                    JOptionPane.showMessageDialog(null, "Producto eliminado correctamente");
                    }else{
                    JOptionPane.showMessageDialog(null, "No se elimino correctamente");
                    }
                    DefaultTableModel dtm = (DefaultTableModel) rsTablaVentas.getModel();
                    dtm.removeRow(renglon);
                    rsTablaVentas.setModel(dtm);
                    dDinero=0.0;
                    dDinero = dineroTotalAPagar();
                    DecimalFormatSymbols separadoresPersonalizados = new DecimalFormatSymbols();
                    separadoresPersonalizados.setDecimalSeparator('.');
                    DecimalFormat formato1 = new DecimalFormat("#.00",separadoresPersonalizados);
                    jlTotalCosto.setText("Total a pagar: " + formato1.format(dDinero));
                }
            }
        });
        this.kgPantalla.add(this.btnEliminarProducto);
    }

    private void tablaConfiguracion() {

        DefaultTableModel dtmTabla = new DefaultTableModel(this.oContenidoTabla, this.STITULOS_TABLA);
        this.rsTablaVentas = new RSTableMetro();
        this.rsTablaVentas.setModel(dtmTabla);
        this.rsTablaVentas.setAltoHead(60);
        this.rsTablaVentas.setRowHeight(40);
        this.rsTablaVentas.setGrosorBordeFilas(0);
        this.rsTablaVentas.setColorBackgoundHead(Colores.C_COLOR_GRIS4.getColor());
        this.rsTablaVentas.setColorFilasBackgound1(Colores.C_COLOR_GRIS1.getColor());
        this.rsTablaVentas.setColorFilasBackgound2(Colores.C_COLOR_GRIS2.getColor());
        this.rsTablaVentas.setColorFilasForeground1(Colores.C_COLOR_NEGRO.getColor());
        this.rsTablaVentas.setColorFilasForeground2(Colores.C_COLOR_NEGRO.getColor());
        this.rsTablaVentas.setColorSelBackgound(Colores.C_COLOR_GRIS3.getColor());
        this.rsTablaVentas.setColorBordeFilas(Colores.C_COLOR_GRIS1.getColor());
        this.rsTablaVentas.setFuenteFilas(Fuentes.FUENTE_TEXTOS.getFont());
        this.rsTablaVentas.setFuenteHead(Fuentes.FUENTE_TEXTOS.getFont());
        this.rsTablaVentas.setPreferredScrollableViewportSize(new Dimension(250, 100));
        JScrollPane scrollPane = new JScrollPane(this.rsTablaVentas);
        scrollPane.setBounds(0, 280, 850, 250);
        this.rsTablaVentas.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                seleccionTrabajador();
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }
        });
        this.kgPantalla.add(scrollPane);
    }

    private boolean seleccionTrabajador() {
        int renglon = this.rsTablaVentas.getSelectedRow();
        if (renglon != -1) {
            String rfc = this.rsTablaVentas.getValueAt(renglon, 0) + "";
            this.jlIdVenta.setText("ID seleccionado: " + rfc);
        } else {
            System.out.println("No selecciono cliente");
            return false;
        }
        return true;
    }

    private void cambiarValorSpinner() {
        this.jspCantidad.setValue(this.jsCantidad.getValue());
    }

    private void cambiarValorSlider() {
        this.jsCantidad.setValue((int) this.jspCantidad.getValue());
    }

    public KGradientPanel getkgPantalla() {
        return this.kgPantalla;
    }
    
    private double dineroTotalAPagar() {
        for (int i = 0; i < this.rsTablaVentas.getRowCount(); i++) {
            String sPrecio = this.rsTablaVentas.getValueAt(i, 3).toString();
            double dPrecio = Double.parseDouble(sPrecio);
            dDinero += (dPrecio);
        }
        return dDinero;
    }
    
    private boolean agruparProductos(Object[] oProducto) {
        DefaultTableModel dtmTabla = (DefaultTableModel) rsTablaVentas.getModel();
        rsTablaVentas.setModel(dtmTabla);
        for (int i = 0; i < dtmTabla.getRowCount(); i++) {
            String idProducto = dtmTabla.getValueAt(i, 1).toString();
            if (idProducto.equals(oProducto[1].toString())) {
                int iCantidadProductosAnteriores = Integer.parseInt(dtmTabla.getValueAt(i, 5).toString());
                int iCantidadProductosAgregar = Integer.parseInt(oProducto[5].toString());
                int iCaiCantidadProductosTotales = iCantidadProductosAnteriores + iCantidadProductosAgregar;
                boolean bValidarProducto =  ValidacionesGenerales.validarCantidadPedido(idProducto,String.valueOf(iCaiCantidadProductosTotales));
                if(!bValidarProducto){
                    return false;
                }
                oProducto[5] = String.valueOf(iCaiCantidadProductosTotales);
                double dPrecioAnterior = Double.parseDouble(dtmTabla.getValueAt(i, 3).toString());
                double dPrecioAgregar = Double.parseDouble(oProducto[3].toString().toString());
                DecimalFormat df = new DecimalFormat("#.00");
                double dPrecioNuevo = dPrecioAgregar + dPrecioAnterior;
                oProducto[3] = String.valueOf(df.format(dPrecioNuevo));
                dtmTabla.removeRow(i);
                dtmTabla.addRow(oProducto);
                return true;
            }
        }
        dtmTabla.addRow(oProducto);
        return true;
    }
    
    private boolean validarProducto(){
        dDinero=0.0;
        String sProveedor = txtIDProveedor.getText().trim();
            for (int i = 0; i < rsTablaVentas.getRowCount(); i++) {
                            String sId = rsTablaVentas.getValueAt(i, 1).toString().trim();
                            boolean bValidarProducto = (ValidacionesGenerales.validarNumero(sId)) &&
                            ValidacionesGenerales.validarExistencia(sId, "Producto") && 
                            ValidacionesGenerales.validarProveedorPedido(sProveedor, sId, "Producto");
                            if(bValidarProducto){
                                String sPrecio1 = rsTablaVentas.getValueAt(i, 3).toString();
                                double dPrecio = Double.parseDouble(sPrecio1);
                                dDinero += (dPrecio);
                                return true;
                            } else {
                                return false;
                            }
            }
            return false;
    }
}
