package Ventanas.Pantallas;

import Recursos.Colores;
import Recursos.Fuentes;
import Recursos.Imagenes;
import SQL.Inserciones;
import keeptoo.KGradientPanel;
import rojeru_san.RSButtonRiple;
import rojerusan.RSDateChooser;

import javax.swing.*;
import javax.swing.border.MatteBorder;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * Ochoa Fecha: 02/11/2021
 * Modificaciones: Correcion ortografica en cliente id
 **/
public class PantCliAgregar {
    private KGradientPanel kgPantalla;
    private JTextField txtNombre, txtApePaterno, txtApeMaterno, txtCorreoE, txtTelefono;
    private RSDateChooser rdFechas;
    private RSButtonRiple btnCrearUsuario;

    public PantCliAgregar() {
        this.panelConfiguracion();
        this.labelsConfiguracion();
        this.textosConfiguracion();
        this.dateChooserConfiguracion();
        this.botonesConfiguracion();
    }

    private void panelConfiguracion() {
        this.kgPantalla = new KGradientPanel();
        this.kgPantalla = new KGradientPanel();
        this.kgPantalla.setSize(1090, 550);
        this.kgPantalla.setBackground(Colores.C_COLOR_TRANSPARENTE.getColor());
        this.kgPantalla.setkEndColor(Colores.C_COLOR_BLANCO.getColor());
        this.kgPantalla.setkStartColor(Colores.C_COLOR_BLANCO.getColor());
        this.kgPantalla.setkBorderRadius(40);
        this.kgPantalla.setLayout(null);
    }

    private void textosConfiguracion() {
        this.txtNombre = new JTextField();
        this.txtNombre.setBounds(100, 150, 200, 30);
        this.txtNombre.setBackground(Colores.C_COLOR_GRIS1.getColor());
        this.txtNombre.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
        this.txtNombre.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.kgPantalla.add(this.txtNombre);


        this.txtApePaterno = new JTextField();
        this.txtApePaterno.setBounds(440, 150, 200, 30);
        this.txtApePaterno.setBackground(Colores.C_COLOR_GRIS1.getColor());
        this.txtApePaterno.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
        this.txtApePaterno.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.kgPantalla.add(this.txtApePaterno);


        this.txtApeMaterno = new JTextField();
        this.txtApeMaterno.setBounds(790, 150, 200, 30);
        this.txtApeMaterno.setBackground(Colores.C_COLOR_GRIS1.getColor());
        this.txtApeMaterno.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
        this.txtApeMaterno.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.kgPantalla.add(this.txtApeMaterno);


        this.txtCorreoE = new JTextField();
        this.txtCorreoE.setBounds(80, 300, 200, 30);
        this.txtCorreoE.setBackground(Colores.C_COLOR_GRIS1.getColor());
        this.txtCorreoE.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
        this.txtCorreoE.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.kgPantalla.add(this.txtCorreoE);


        this.txtTelefono = new JTextField();
        this.txtTelefono.setBounds(410, 300, 200, 30);
        this.txtTelefono.setBackground(Colores.C_COLOR_GRIS1.getColor());
        this.txtTelefono.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
        this.txtTelefono.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.kgPantalla.add(this.txtTelefono);
    }

    private void labelsConfiguracion() {
        JLabel labelTitutlo = new JLabel("Agregar Nuevo Cliente");
        labelTitutlo.setFont(Fuentes.FUENTE_TITULOS.getFont());
        labelTitutlo.setBounds(350, 20, 350, 30);
        this.kgPantalla.add(labelTitutlo);

        JLabel jlImgTitulo = new JLabel(Imagenes.I_IMAGEN_CLIENTE.getImagen());
        jlImgTitulo.setFont(Fuentes.FUENTE_TITULOS.getFont());
        jlImgTitulo.setBounds(280, 10, 64, 64);
        this.kgPantalla.add(jlImgTitulo);

        JLabel jlNombreCliente = new JLabel("Nombre: ");
        jlNombreCliente.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlNombreCliente.setBounds(0, 150, 100, 30);
        this.kgPantalla.add(jlNombreCliente);

        JLabel jApellidoP = new JLabel("Apellido P: ");
        jApellidoP.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jApellidoP.setBounds(330, 150, 130, 30);
        this.kgPantalla.add(jApellidoP);

        JLabel jApellidoM = new JLabel("Apellido M: ");
        jApellidoM.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jApellidoM.setBounds(670, 150, 130, 30);
        this.kgPantalla.add(jApellidoM);

        JLabel jlCorreoE = new JLabel("Email: ");
        jlCorreoE.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlCorreoE.setBounds(0, 300, 130, 30);
        this.kgPantalla.add(jlCorreoE);

        JLabel jlTelefono = new JLabel("Telefono: ");
        jlTelefono.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlTelefono.setBounds(310, 300, 130, 30);
        this.kgPantalla.add(jlTelefono);

        JLabel jlFechaN = new JLabel("Fecha Nacimiento: ");
        jlFechaN.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlFechaN.setBounds(630, 300, 180, 30);
        this.kgPantalla.add(jlFechaN);

        JLabel jlClienteID = new JLabel("ID Cliente:          # ");
        jlClienteID.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlClienteID.setBounds(0, 450, 200, 30);
        this.kgPantalla.add(jlClienteID);
    }

    private void dateChooserConfiguracion() {
        this.rdFechas = new RSDateChooser();
        this.rdFechas.setColorBackground(Colores.C_COLOR_GRIS4.getColor());
        this.rdFechas.setColorButtonHover(Colores.C_COLOR_GRIS4.getColor());
        this.rdFechas.setColorTextDiaActual(Colores.C_COLOR_NEGRO.getColor());
        this.rdFechas.setColorForeground(Colores.C_COLOR_NEGRO.getColor());
        this.rdFechas.setFormatoFecha("YYYY-MM-dd");
        this.rdFechas.setFuente(Fuentes.FUENTE_TEXTOS.getFont());
        this.rdFechas.setBounds(810, 300, 180, 30);
        this.kgPantalla.add(this.rdFechas);
    }

    private void botonesConfiguracion() {
        /*----------------------------------------*/
        this.btnCrearUsuario = new RSButtonRiple();
        this.btnCrearUsuario.setBounds(310, 450, 120, 50);
        this.btnCrearUsuario.setBorderPainted(false);
        this.btnCrearUsuario.setFocusPainted(false);
        this.btnCrearUsuario.setBackground(Colores.C_COLOR_GRIS3.getColor());
        this.btnCrearUsuario.setColorHover(Colores.C_COLOR_GRIS2.getColor());
        this.btnCrearUsuario.setColorText(Colores.C_COLOR_NEGRO.getColor());
        this.btnCrearUsuario.setColorTextHover(Colores.C_COLOR_GRIS4.getColor());
        this.btnCrearUsuario.setText("Agregar");
        this.btnCrearUsuario.setActionCommand("btnClientes");
        this.btnCrearUsuario.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                LocalDate fechaHoy = LocalDate.now();
                String formatoFecha = fechaHoy.format(DateTimeFormatter.ofPattern("YYYY-MM-dd"));
                String sNombre = txtNombre.getText().trim();
                String sApellidoP = txtApePaterno.getText().trim();
                String sApellidoM = txtApeMaterno.getText().trim();
                String sCorreo = txtCorreoE.getText().trim();
                String sTelefono = txtTelefono.getText().trim();
                String sFecha = rdFechas.getFechaSeleccionada().trim();
                boolean bInsercionCliente=Inserciones.insertarCliente(sNombre, sApellidoP, sApellidoM, sCorreo, sTelefono, sFecha);
                if(bInsercionCliente){
                    JOptionPane.showMessageDialog(null,"Cliente agregado correctamente");
                    txtNombre.setText("");
                    txtApePaterno.setText("");
                    txtApeMaterno.setText("");
                    txtCorreoE.setText("");
                    txtTelefono.setText("");
                    rdFechas.setTextoFecha(formatoFecha);
                }
            }
        });
        this.kgPantalla.add(this.btnCrearUsuario);
    }

    public KGradientPanel getkgPantalla() {
        return this.kgPantalla;
    }
}
