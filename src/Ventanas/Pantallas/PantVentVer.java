package Ventanas.Pantallas;

import Recursos.Colores;
import Recursos.Fuentes;
import Recursos.Imagenes;
import SQL.CargarDatos;
import SQL.Conexion;
import SQL.Eliminaciones;
import keeptoo.KGradientPanel;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.view.JasperViewer;
import rojeru_san.RSButtonRiple;
import rojeru_san.rsdate.RSDateChooser;
import rojerusan.RSTableMetro;

import javax.swing.*;
import javax.swing.border.MatteBorder;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Daniel Ochoa
 * Ochoa Fecha: 08/11/2021
 * Modificacion: cambie el metodo buscarTabla
 */
public class PantVentVer {
    private RSTableMetro rsTablaVentas;
    private KGradientPanel kgPantalla;
    private JTextField txtID;
    private RSDateChooser rdcFechaInicio, rdcFechaFinal;
    private RSButtonRiple btnGenerarReporte, btnGenerarReporteFecha;

    public PantVentVer() {
        this.panelConfiguracion();
        this.labelsConfiguracion();
        this.textosConfiguracion();
        this.tablaConfiguracion();
        this.botonesConfiguracion();
        this.dateChooserConfiguracion();
    }

    private void panelConfiguracion() {
        this.kgPantalla = new KGradientPanel();
        this.kgPantalla = new KGradientPanel();
        this.kgPantalla.setSize(1090, 550);
        this.kgPantalla.setBackground(Colores.C_COLOR_TRANSPARENTE.getColor());
        this.kgPantalla.setkEndColor(Colores.C_COLOR_BLANCO.getColor());
        this.kgPantalla.setkStartColor(Colores.C_COLOR_BLANCO.getColor());
        this.kgPantalla.setkBorderRadius(40);
        this.kgPantalla.setLayout(null);
    }

    private void textosConfiguracion() {
        this.txtID = new JTextField();
        this.txtID.setBounds(480, 100, 200, 30);
        this.txtID.setBackground(Colores.C_COLOR_GRIS1.getColor());
        this.txtID.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
        this.txtID.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.txtID.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyPressed(KeyEvent e) {
            }

            @Override
            public void keyReleased(KeyEvent e) {
                CargarDatos.buscarTabla(rsTablaVentas, txtID.getText(), "Venta", "Venta");
            }
        });
        this.kgPantalla.add(this.txtID);
    }

    private void labelsConfiguracion() {
        JLabel labelTitutlo = new JLabel("Ver Ventas");
        labelTitutlo.setFont(Fuentes.FUENTE_TITULOS.getFont());
        labelTitutlo.setBounds(350, 20, 350, 30);
        this.kgPantalla.add(labelTitutlo);

        JLabel jlImgTitulo = new JLabel(Imagenes.I_IMAGEN_VENTA.getImagen());
        jlImgTitulo.setFont(Fuentes.FUENTE_TITULOS.getFont());
        jlImgTitulo.setBounds(280, 10, 64, 64);
        this.kgPantalla.add(jlImgTitulo);

        JLabel jlIDCliente = new JLabel("Buscar ID Venta: ");
        jlIDCliente.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlIDCliente.setBounds(280, 100, 200, 30);
        this.kgPantalla.add(jlIDCliente);

        JLabel jlFechaInicio = new JLabel("Fecha Inicio: ");
        jlFechaInicio.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlFechaInicio.setBounds(0, 70, 200, 30);
        this.kgPantalla.add(jlFechaInicio);

        JLabel jlFechaFinal = new JLabel("Fecha Final: ");
        jlFechaFinal.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlFechaFinal.setBounds(0, 130, 200, 30);
        this.kgPantalla.add(jlFechaFinal);
    }

    private void botonesConfiguracion() {
        /*----------------------------------------*/
        this.btnGenerarReporte = new RSButtonRiple();
        this.btnGenerarReporte.setBounds(700, 100, 150, 50);
        this.btnGenerarReporte.setBorderPainted(false);
        this.btnGenerarReporte.setFocusPainted(false);
        this.btnGenerarReporte.setBackground(Colores.C_COLOR_GRIS3.getColor());
        this.btnGenerarReporte.setColorHover(Colores.C_COLOR_GRIS2.getColor());
        this.btnGenerarReporte.setColorText(Colores.C_COLOR_NEGRO.getColor());
        this.btnGenerarReporte.setColorTextHover(Colores.C_COLOR_GRIS4.getColor());
        this.btnGenerarReporte.setText("Generar Ticket");
        this.btnGenerarReporte.setActionCommand("btnGenerarReporte");
        this.btnGenerarReporte.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                String sValor = seleccionTrabajador();
                if (!sValor.isBlank()) {
                    JasperReport archivo;
                    try {
                        archivo = JasperCompileManager.compileReport("Configuracion/Reportes/TicketVenta.jrxml");
                        HashMap<String, Object> map = CargarDatos.datosReporteVenta(sValor);
                        //JRDataSource data = new JREmptyDataSource();
                        JasperPrint prin = JasperFillManager.fillReport(archivo, map, Conexion.getConexion());
                        if (prin != null) {
                            JasperViewer view = new JasperViewer(prin, false);
                            view.setVisible(true);
                        }
                    } catch (JRException ex) {
                        Logger.getLogger(PantVentVer.class.getName()).log(Level.SEVERE, null, ex);
                    }

                } else {
                    JOptionPane.showMessageDialog(null, "No ha selecionado Venta");
                }
            }
        });
        this.kgPantalla.add(this.btnGenerarReporte);


        /*----------------------------------------*/
        this.btnGenerarReporteFecha = new RSButtonRiple();
        this.btnGenerarReporteFecha.setBounds(0, 195, 150, 50);
        this.btnGenerarReporteFecha.setBorderPainted(false);
        this.btnGenerarReporteFecha.setFocusPainted(false);
        this.btnGenerarReporteFecha.setBackground(Colores.C_COLOR_GRIS3.getColor());
        this.btnGenerarReporteFecha.setColorHover(Colores.C_COLOR_GRIS2.getColor());
        this.btnGenerarReporteFecha.setColorText(Colores.C_COLOR_NEGRO.getColor());
        this.btnGenerarReporteFecha.setColorTextHover(Colores.C_COLOR_GRIS4.getColor());
        this.btnGenerarReporteFecha.setText("Generar Reporte");
        this.btnGenerarReporteFecha.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                Date sFechaInicio = rdcFechaInicio.getDatoFecha();
                Date sFechaFinal = rdcFechaFinal.getDatoFecha();
                System.out.println(sFechaInicio + " " + sFechaFinal + " " + (sFechaFinal.after(sFechaInicio)) + " " + sFechaFinal.compareTo(sFechaInicio));
                if (sFechaFinal.compareTo(sFechaInicio) >= 0) {
                    String sFechaI = fecha((sFechaInicio.getYear() + 1900), (sFechaInicio.getMonth() + 1), sFechaInicio.getDate());
                    String sFechaF = fecha((sFechaFinal.getYear() + 1900), (sFechaFinal.getMonth() + 1), sFechaFinal.getDate());
                    JasperReport archivo;
                    try {
                        archivo = JasperCompileManager.compileReport("Configuracion/Reportes/Ventas.jrxml");
                        HashMap<String, Object> map = new HashMap<>();
                        map.put("sFechaInicio", sFechaI);
                        map.put("sFechaFinal", sFechaF);
                        //JRDataSource data = new JREmptyDataSource();
                        JasperPrint prin = JasperFillManager.fillReport(archivo, map, Conexion.getConexion());
                        if (prin != null) {
                            JasperViewer view = new JasperViewer(prin, false);
                            view.setVisible(true);
                        }
                    } catch (JRException ex) {
                        Logger.getLogger(PantVentVer.class.getName()).log(Level.SEVERE, null, ex);
                    }

                } else {
                    JOptionPane.showMessageDialog(null, "La fecha de inicio debe ser menor o igual a la fecha final");
                }
            }
        });
        this.kgPantalla.add(this.btnGenerarReporteFecha);
    }

    private void tablaConfiguracion() {
        this.rsTablaVentas = new RSTableMetro();
        this.rsTablaVentas.setAltoHead(60);
        this.rsTablaVentas.setRowHeight(40);
        this.rsTablaVentas.setGrosorBordeFilas(0);
        this.rsTablaVentas.setColorBackgoundHead(Colores.C_COLOR_GRIS4.getColor());
        this.rsTablaVentas.setColorFilasBackgound1(Colores.C_COLOR_GRIS1.getColor());
        this.rsTablaVentas.setColorFilasBackgound2(Colores.C_COLOR_GRIS2.getColor());
        this.rsTablaVentas.setColorFilasForeground1(Colores.C_COLOR_NEGRO.getColor());
        this.rsTablaVentas.setColorFilasForeground2(Colores.C_COLOR_NEGRO.getColor());
        this.rsTablaVentas.setColorSelBackgound(Colores.C_COLOR_GRIS3.getColor());
        this.rsTablaVentas.setColorBordeFilas(Colores.C_COLOR_GRIS1.getColor());
        this.rsTablaVentas.setFuenteFilas(Fuentes.FUENTE_TEXTOS.getFont());
        this.rsTablaVentas.setFuenteHead(Fuentes.FUENTE_TEXTOS.getFont());
        this.rsTablaVentas.setPreferredScrollableViewportSize(new Dimension(250, 100));
        JScrollPane scrollPane = new JScrollPane(this.rsTablaVentas);
        scrollPane.setBounds(10, 250, 980, 250);
        CargarDatos.rellenarTabla(this.rsTablaVentas, "Venta");
        this.kgPantalla.add(scrollPane);
        this.rsTablaVentas.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                seleccionTrabajador();
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }
        });
    }

    private void dateChooserConfiguracion() {
        SimpleDateFormat objSDF = new SimpleDateFormat("yyyy-MM-dd");
        Date objDate = null;
        try {
            LocalDate ldFecha = LocalDate.now();
            String formato = ldFecha.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
            objDate = objSDF.parse(formato);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        this.rdcFechaInicio = new RSDateChooser();
        this.rdcFechaInicio.setColorBackground(Colores.C_COLOR_GRIS3.getColor());
        this.rdcFechaInicio.setColorButtonHover(Colores.C_COLOR_GRIS3.getColor());
        this.rdcFechaInicio.setColorTextDiaActual(Colores.C_COLOR_NEGRO.getColor());
        this.rdcFechaInicio.setColorForeground(Colores.C_COLOR_NEGRO.getColor());
        this.rdcFechaInicio.setFormatoFecha("yyyy-MM-dd");
        this.rdcFechaInicio.setDatoFecha(objDate);
        this.rdcFechaInicio.setFuente(Fuentes.FUENTE_TEXTOS.getFont());
        this.rdcFechaInicio.setBounds(0, 100, 180, 30);
        this.kgPantalla.add(this.rdcFechaInicio);

        this.rdcFechaFinal = new RSDateChooser();
        this.rdcFechaFinal.setColorBackground(Colores.C_COLOR_GRIS3.getColor());
        this.rdcFechaFinal.setColorButtonHover(Colores.C_COLOR_GRIS3.getColor());
        this.rdcFechaFinal.setColorTextDiaActual(Colores.C_COLOR_NEGRO.getColor());
        this.rdcFechaFinal.setColorForeground(Colores.C_COLOR_NEGRO.getColor());
        this.rdcFechaFinal.setFormatoFecha("yyyy-MM-dd");
        this.rdcFechaFinal.setDatoFecha(objDate);
        this.rdcFechaFinal.setFuente(Fuentes.FUENTE_TEXTOS.getFont());
        this.rdcFechaFinal.setBounds(0, 160, 180, 30);
        this.kgPantalla.add(this.rdcFechaFinal);


    }


    public KGradientPanel getkgPantalla() {
        return this.kgPantalla;
    }

    public String fecha(int iAno, int iMes, int iDia) {
        String sFecha = String.valueOf(iAno).concat("-");
        sFecha = (iMes < 10) ? sFecha.concat("0" + iMes + "-") : sFecha.concat(iMes + "-");
        sFecha = (iDia < 10) ? sFecha.concat("0" + iDia) : sFecha.concat(String.valueOf(iDia));
        return sFecha;
    }

    private String seleccionTrabajador() {
        String sValor = "";
        int renglon = this.rsTablaVentas.getSelectedRow();
        if (renglon != -1) {
            sValor = this.rsTablaVentas.getValueAt(renglon, 0) + "";
        }
        return sValor;
    }
}
