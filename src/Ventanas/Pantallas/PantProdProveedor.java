package Ventanas.Pantallas;

import Recursos.Colores;
import Recursos.Fuentes;
import Recursos.Imagenes;
import SQL.CargarDatos;
import keeptoo.KGradientPanel;
import rojerusan.RSTableMetro;

import javax.swing.*;
import javax.swing.border.MatteBorder;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 *
 * @author cristina 12-11-2021
 */
public class PantProdProveedor {
    private RSTableMetro rsTablaClientes;
    private KGradientPanel kgPantalla;
    private JTextField txtNombre;

    public PantProdProveedor() {
        this.panelConfiguracion();
        this.labelsConfiguracion();
        this.textosConfiguracion();
        this.tablaConfiguracion();
    }

    private void panelConfiguracion() {
        this.kgPantalla = new KGradientPanel();
        this.kgPantalla = new KGradientPanel();
        this.kgPantalla.setSize(1090, 550);
        this.kgPantalla.setBackground(Colores.C_COLOR_TRANSPARENTE.getColor());
        this.kgPantalla.setkEndColor(Colores.C_COLOR_BLANCO.getColor());
        this.kgPantalla.setkStartColor(Colores.C_COLOR_BLANCO.getColor());
        this.kgPantalla.setkBorderRadius(40);
        this.kgPantalla.setLayout(null);
    }

    private void textosConfiguracion() {
        this.txtNombre = new JTextField();
        this.txtNombre.setBounds(400, 150, 200, 30);
        this.txtNombre.setBackground(Colores.C_COLOR_GRIS1.getColor());
        this.txtNombre.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
        this.txtNombre.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.txtNombre.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyPressed(KeyEvent e) {
            }

            @Override
            public void keyReleased(KeyEvent e) {
                CargarDatos.buscarTabla(rsTablaClientes, txtNombre.getText(), "ProveedorProducto", "ProvProd");
                System.out.println("d");
            }
        });
        this.kgPantalla.add(this.txtNombre);
    }

    private void labelsConfiguracion() {
        JLabel labelTitulo = new JLabel("Lista De Proveedores con Productos");
        labelTitulo.setFont(Fuentes.FUENTE_TITULOS.getFont());
        labelTitulo.setBounds(350, 20, 490, 30);
        this.kgPantalla.add(labelTitulo);

        JLabel jlImgTitulo = new JLabel(Imagenes.I_IMAGEN_PRODUCTO.getImagen());
        jlImgTitulo.setFont(Fuentes.FUENTE_TITULOS.getFont());
        jlImgTitulo.setBounds(280, 10, 64, 64);
        this.kgPantalla.add(jlImgTitulo);

        JLabel jlNombre = new JLabel("ID Proveedor: ");
        jlNombre.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlNombre.setBounds(250, 150, 150, 30);
        this.kgPantalla.add(jlNombre);
    }

    private void tablaConfiguracion() {

        this.rsTablaClientes = new RSTableMetro();
        this.rsTablaClientes.setAltoHead(60);
        this.rsTablaClientes.setRowHeight(40);
        this.rsTablaClientes.setGrosorBordeFilas(0);
        this.rsTablaClientes.setColorBackgoundHead(Colores.C_COLOR_GRIS4.getColor());
        this.rsTablaClientes.setColorFilasBackgound1(Colores.C_COLOR_GRIS1.getColor());
        this.rsTablaClientes.setColorFilasBackgound2(Colores.C_COLOR_GRIS2.getColor());
        this.rsTablaClientes.setColorFilasForeground1(Colores.C_COLOR_NEGRO.getColor());
        this.rsTablaClientes.setColorFilasForeground2(Colores.C_COLOR_NEGRO.getColor());
        this.rsTablaClientes.setColorSelBackgound(Colores.C_COLOR_GRIS3.getColor());
        this.rsTablaClientes.setColorBordeFilas(Colores.C_COLOR_GRIS1.getColor());
        this.rsTablaClientes.setFuenteFilas(Fuentes.FUENTE_TEXTOS.getFont());
        this.rsTablaClientes.setFuenteHead(Fuentes.FUENTE_TEXTOS.getFont());
        this.rsTablaClientes.setPreferredScrollableViewportSize(new Dimension(250, 100));
        JScrollPane scrollPane = new JScrollPane(this.rsTablaClientes);
        scrollPane.setBounds(50, 250, 910, 250);
        CargarDatos.rellenarTabla(this.rsTablaClientes, "ProvProd");
        this.kgPantalla.add(scrollPane);
    }

    public KGradientPanel getkgPantalla() {
        return this.kgPantalla;
    }
}
