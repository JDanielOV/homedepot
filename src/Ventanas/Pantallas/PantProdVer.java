package Ventanas.Pantallas;

import Recursos.Colores;
import Recursos.Fuentes;
import Recursos.Imagenes;
import SQL.CargarDatos;
import SQL.Conexion;
import keeptoo.KGradientPanel;
import rojerusan.RSTableMetro;

import javax.swing.*;
import javax.swing.border.MatteBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;
import net.sf.jasperreports.view.JasperViewer;
import rojeru_san.RSButtonRiple;

/**
 * @author Daniel Ochoa
 * Ochoa Fecha: 08/11/2021
 * Modificacion: cambie el metodo buscarTabla
 */
public class PantProdVer {
    private RSTableMetro rsTablaClientes;
    private KGradientPanel kgPantalla;
    private RSButtonRiple btnGenerarReporte;
    private JTextField txtNombre;

    public PantProdVer() {
        this.panelConfiguracion();
        this.labelsConfiguracion();
        this.textosConfiguracion();
        this.tablaConfiguracion();
        this.botonesConfiguracion();
    }

    private void panelConfiguracion() {
        this.kgPantalla = new KGradientPanel();
        this.kgPantalla = new KGradientPanel();
        this.kgPantalla.setSize(1090, 550);
        this.kgPantalla.setBackground(Colores.C_COLOR_TRANSPARENTE.getColor());
        this.kgPantalla.setkEndColor(Colores.C_COLOR_BLANCO.getColor());
        this.kgPantalla.setkStartColor(Colores.C_COLOR_BLANCO.getColor());
        this.kgPantalla.setkBorderRadius(40);
        this.kgPantalla.setLayout(null);
    }

    private void textosConfiguracion() {
        this.txtNombre = new JTextField();
        this.txtNombre.setBounds(400, 150, 200, 30);
        this.txtNombre.setBackground(Colores.C_COLOR_GRIS1.getColor());
        this.txtNombre.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
        this.txtNombre.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.txtNombre.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyPressed(KeyEvent e) {
            }

            @Override
            public void keyReleased(KeyEvent e) {
                CargarDatos.buscarTabla(rsTablaClientes, txtNombre.getText(), "ProductoNombre", "Producto");
                System.out.println("d");
            }
        });
        this.kgPantalla.add(this.txtNombre);
    }

    private void labelsConfiguracion() {
        JLabel labelTitulo = new JLabel("Lista De Productos");
        labelTitulo.setFont(Fuentes.FUENTE_TITULOS.getFont());
        labelTitulo.setBounds(350, 20, 300, 30);
        this.kgPantalla.add(labelTitulo);

        JLabel jlImgTitulo = new JLabel(Imagenes.I_IMAGEN_PRODUCTO.getImagen());
        jlImgTitulo.setFont(Fuentes.FUENTE_TITULOS.getFont());
        jlImgTitulo.setBounds(280, 10, 64, 64);
        this.kgPantalla.add(jlImgTitulo);

        JLabel jlNombre = new JLabel("Nombre Producto: ");
        jlNombre.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlNombre.setBounds(245, 150, 230, 30);
        this.kgPantalla.add(jlNombre);
    }
    
    private void botonesConfiguracion() {
        /*----------------------------------------*/
        this.btnGenerarReporte = new RSButtonRiple();
        this.btnGenerarReporte.setBounds(750, 140, 200, 50);
        this.btnGenerarReporte.setBorderPainted(false);
        this.btnGenerarReporte.setFocusPainted(false);
        this.btnGenerarReporte.setBackground(Colores.C_COLOR_GRIS3.getColor());
        this.btnGenerarReporte.setColorHover(Colores.C_COLOR_GRIS2.getColor());
        this.btnGenerarReporte.setColorText(Colores.C_COLOR_NEGRO.getColor());
        this.btnGenerarReporte.setColorTextHover(Colores.C_COLOR_GRIS4.getColor());
        this.btnGenerarReporte.setText("Generar Reporte");
        this.btnGenerarReporte.setActionCommand("btnEliPedidos");
        this.btnGenerarReporte.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                System.out.println("Reporte");
                JasperReport archivo;
        try {
            /*HashMap a = new HashMap();
            a.put("sIDProveedor", 1);*/
            //archivo = JasperCompileManager.compileReport("ReportesPedido.jrxml");
            archivo = JasperCompileManager.compileReport("Producto.jrxml");
            //Map<String, Object> map = new HashMap<String, Object>();
            Conexion cnn = new Conexion();
            //JRDataSource data = new JREmptyDataSource();
            //JasperPrint prin = JasperFillManager.fillReport(archivo, a, Conexion.getConexion());
            JasperPrint prin = JasperFillManager.fillReport(archivo, null, Conexion.getConexion());
            if (prin != null) {
                JasperViewer view = new JasperViewer(prin, false);
                view.setVisible(true);
                // JasperExportManager.exportReportToHtmlFile(prin, "reporte.html");
                //JasperExportManager.exportReportToPdfFile(prin, "reporte.pdf");
                JasperExportManager.exportReportToHtmlFile(prin, "reporte.html");
                JRPdfExporter exp = new JRPdfExporter();
                exp.setExporterInput(new SimpleExporterInput(prin));
                exp.setExporterOutput(new SimpleOutputStreamExporterOutput("ReporteTrabajadores.pdf"));
                SimplePdfExporterConfiguration conf = new SimplePdfExporterConfiguration();
                exp.setConfiguration(conf);
                exp.exportReport();
            }
        } catch (JRException ex) {
            JOptionPane.showMessageDialog(null,"El documento no tiene paginas");
        }
            }
        });
        this.kgPantalla.add(this.btnGenerarReporte);
    }

    private void tablaConfiguracion() {

        this.rsTablaClientes = new RSTableMetro();
        this.rsTablaClientes.setAltoHead(60);
        this.rsTablaClientes.setRowHeight(40);
        this.rsTablaClientes.setGrosorBordeFilas(0);
        this.rsTablaClientes.setColorBackgoundHead(Colores.C_COLOR_GRIS4.getColor());
        this.rsTablaClientes.setColorFilasBackgound1(Colores.C_COLOR_GRIS1.getColor());
        this.rsTablaClientes.setColorFilasBackgound2(Colores.C_COLOR_GRIS2.getColor());
        this.rsTablaClientes.setColorFilasForeground1(Colores.C_COLOR_NEGRO.getColor());
        this.rsTablaClientes.setColorFilasForeground2(Colores.C_COLOR_NEGRO.getColor());
        this.rsTablaClientes.setColorSelBackgound(Colores.C_COLOR_GRIS3.getColor());
        this.rsTablaClientes.setColorBordeFilas(Colores.C_COLOR_GRIS1.getColor());
        this.rsTablaClientes.setFuenteFilas(Fuentes.FUENTE_TEXTOS.getFont());
        this.rsTablaClientes.setFuenteHead(Fuentes.FUENTE_TEXTOS.getFont());
        this.rsTablaClientes.setPreferredScrollableViewportSize(new Dimension(250, 100));
        JScrollPane scrollPane = new JScrollPane(this.rsTablaClientes);
        scrollPane.setBounds(50, 250, 910, 250);
        CargarDatos.rellenarTabla(this.rsTablaClientes, "Producto");
        this.kgPantalla.add(scrollPane);
    }

    public KGradientPanel getkgPantalla() {
        return this.kgPantalla;
    }
}
