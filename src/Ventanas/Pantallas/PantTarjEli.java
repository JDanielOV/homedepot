package Ventanas.Pantallas;

import Recursos.Colores;
import Recursos.Fuentes;
import Recursos.Imagenes;
import SQL.CargarDatos;
import SQL.Eliminaciones;
import keeptoo.KGradientPanel;
import rojerusan.RSTableMetro;

import javax.swing.*;
import javax.swing.border.MatteBorder;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import rojeru_san.RSButtonRiple;

/**
 * @author cristina 04-11-2021
 * Ochoa Fecha: 08/11/2021
 * Modificacion: cambie el metodo buscarTabla
 */
public class PantTarjEli {
    private RSTableMetro rsTablaTarjetas;
    private KGradientPanel kgPantalla;
    private JTextField txtID;
    private JLabel txtIDSelect;
    private RSButtonRiple btnEliminarTarjetas;
    private String sID;

    public PantTarjEli() {
        this.panelConfiguracion();
        this.labelsConfiguracion();
        this.textosConfiguracion();
        this.tablaConfiguracion();
        this.botonesConfiguracion();
    }

    private void panelConfiguracion() {
        this.kgPantalla = new KGradientPanel();
        this.kgPantalla = new KGradientPanel();
        this.kgPantalla.setSize(1090, 550);
        this.kgPantalla.setBackground(Colores.C_COLOR_TRANSPARENTE.getColor());
        this.kgPantalla.setkEndColor(Colores.C_COLOR_BLANCO.getColor());
        this.kgPantalla.setkStartColor(Colores.C_COLOR_BLANCO.getColor());
        this.kgPantalla.setkBorderRadius(40);
        this.kgPantalla.setLayout(null);
    }

    private void textosConfiguracion() {
        this.txtID = new JTextField();
        this.txtID.setBounds(410, 100, 200, 30);
        this.txtID.setBackground(Colores.C_COLOR_GRIS1.getColor());
        this.txtID.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
        this.txtID.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.txtID.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyPressed(KeyEvent e) {
            }

            @Override
            public void keyReleased(KeyEvent e) {
                CargarDatos.buscarTabla(rsTablaTarjetas, txtID.getText(), "Tarjetas", "Tarjetas");
                txtIDSelect.setText("Num de Tarjeta seleccionado: ");
                sID = "";
            }
        });
        this.kgPantalla.add(this.txtID);
    }

    private void labelsConfiguracion() {
        JLabel labelTitutlo = new JLabel("Eliminar Tarjeta");
        labelTitutlo.setFont(Fuentes.FUENTE_TITULOS.getFont());
        labelTitutlo.setBounds(420, 20, 400, 30);
        this.kgPantalla.add(labelTitutlo);

        JLabel jlImgTitulo = new JLabel(Imagenes.I_IMAGEN_TARJETA.getImagen());
        jlImgTitulo.setFont(Fuentes.FUENTE_TITULOS.getFont());
        jlImgTitulo.setBounds(340, 10, 64, 64);
        this.kgPantalla.add(jlImgTitulo);

        JLabel jlIDProveedor = new JLabel("Buscar Num: ");
        jlIDProveedor.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlIDProveedor.setBounds(300, 100, 130, 30);
        this.kgPantalla.add(jlIDProveedor);

        this.txtIDSelect = new JLabel("Num de Tarjeta seleccionado: ");
        this.txtIDSelect.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        this.txtIDSelect.setBounds(300, 180, 500, 70);
        this.kgPantalla.add(this.txtIDSelect);
    }

    private void botonesConfiguracion() {
        /*----------------------------------------*/
        this.btnEliminarTarjetas = new RSButtonRiple();
        this.btnEliminarTarjetas.setBounds(700, 100, 120, 50);
        this.btnEliminarTarjetas.setBorderPainted(false);
        this.btnEliminarTarjetas.setFocusPainted(false);
        this.btnEliminarTarjetas.setBackground(Colores.C_COLOR_GRIS3.getColor());
        this.btnEliminarTarjetas.setColorHover(Colores.C_COLOR_GRIS2.getColor());
        this.btnEliminarTarjetas.setColorText(Colores.C_COLOR_NEGRO.getColor());
        this.btnEliminarTarjetas.setColorTextHover(Colores.C_COLOR_GRIS4.getColor());
        this.btnEliminarTarjetas.setText("Eliminar");
        this.btnEliminarTarjetas.setActionCommand("btnEliTarjetas");
        this.btnEliminarTarjetas.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (seleccionProveedor() == true) {
                    boolean bEstado = Eliminaciones.eliminarTarjeta(sID.trim());
                    if (bEstado) {
                        JOptionPane.showMessageDialog(null, "Tarjeta eliminado");
                        txtID.setText("");
                        CargarDatos.buscarTabla(rsTablaTarjetas, txtID.getText(), "Tarjetas", "Tarjetas");
                        txtIDSelect.setText("Num de Tarjeta seleccionado: ");
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "No ha selecionado tarjeta");
                }
            }
        });
        this.kgPantalla.add(this.btnEliminarTarjetas);
    }

    private boolean seleccionProveedor() {
        int renglon = this.rsTablaTarjetas.getSelectedRow();
        if (renglon != -1) {
            String ID = this.rsTablaTarjetas.getValueAt(renglon, 0) + "";
            this.sID = ID;
            this.txtIDSelect.setText("Num de Tarjeta seleccionado: " + ID);
        } else {
            System.out.println("No selecciono proveedor");
            this.sID = "";
            return false;
        }
        return true;
    }

    private void tablaConfiguracion() {
        this.rsTablaTarjetas = new RSTableMetro();
        this.rsTablaTarjetas.setAltoHead(60);
        this.rsTablaTarjetas.setRowHeight(40);
        this.rsTablaTarjetas.setGrosorBordeFilas(0);
        this.rsTablaTarjetas.setColorBackgoundHead(Colores.C_COLOR_GRIS4.getColor());
        this.rsTablaTarjetas.setColorFilasBackgound1(Colores.C_COLOR_GRIS1.getColor());
        this.rsTablaTarjetas.setColorFilasBackgound2(Colores.C_COLOR_GRIS2.getColor());
        this.rsTablaTarjetas.setColorFilasForeground1(Colores.C_COLOR_NEGRO.getColor());
        this.rsTablaTarjetas.setColorFilasForeground2(Colores.C_COLOR_NEGRO.getColor());
        this.rsTablaTarjetas.setColorSelBackgound(Colores.C_COLOR_GRIS3.getColor());
        this.rsTablaTarjetas.setColorBordeFilas(Colores.C_COLOR_GRIS1.getColor());
        this.rsTablaTarjetas.setFuenteFilas(Fuentes.FUENTE_TEXTOS.getFont());
        this.rsTablaTarjetas.setFuenteHead(Fuentes.FUENTE_TEXTOS.getFont());
        this.rsTablaTarjetas.setPreferredScrollableViewportSize(new Dimension(250, 100));
        JScrollPane scrollPane = new JScrollPane(this.rsTablaTarjetas);
        scrollPane.setBounds(10, 250, 980, 250);
        CargarDatos.rellenarTabla(this.rsTablaTarjetas, "Tarjetas");
        this.rsTablaTarjetas.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                seleccionProveedor();
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }


        });
        this.kgPantalla.add(scrollPane);
    }


    public KGradientPanel getkgPantalla() {
        return this.kgPantalla;
    }
}
