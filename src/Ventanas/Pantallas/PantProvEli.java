package Ventanas.Pantallas;

/**
 * @author Juan Jose
 * Ochoa Fecha: 08/11/2021
 * Modificacion: cambie el metodo buscarTabla
 */

import Recursos.Colores;
import Recursos.Fuentes;
import Recursos.Imagenes;
import SQL.CargarDatos;
import SQL.Eliminaciones;
import keeptoo.KGradientPanel;
import rojerusan.RSTableMetro;

import javax.swing.*;
import javax.swing.border.MatteBorder;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import rojeru_san.RSButtonRiple;

public class PantProvEli {
    private RSTableMetro rsTablaProveedores;
    private KGradientPanel kgPantalla;
    private JTextField txtID;
    private JLabel txtIDSelect;
    private RSButtonRiple btnEliminarProveedor;
    private String sID;

    public PantProvEli() {
        this.panelConfiguracion();
        this.labelsConfiguracion();
        this.textosConfiguracion();
        this.tablaConfiguracion();
        this.botonesConfiguracion();
    }

    private void panelConfiguracion() {
        this.kgPantalla = new KGradientPanel();
        this.kgPantalla = new KGradientPanel();
        this.kgPantalla.setSize(1090, 550);
        this.kgPantalla.setBackground(Colores.C_COLOR_TRANSPARENTE.getColor());
        this.kgPantalla.setkEndColor(Colores.C_COLOR_BLANCO.getColor());
        this.kgPantalla.setkStartColor(Colores.C_COLOR_BLANCO.getColor());
        this.kgPantalla.setkBorderRadius(40);
        this.kgPantalla.setLayout(null);
    }

    private void textosConfiguracion() {
        this.txtID = new JTextField();
        this.txtID.setBounds(410, 100, 200, 30);
        this.txtID.setBackground(Colores.C_COLOR_GRIS1.getColor());
        this.txtID.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
        this.txtID.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.txtID.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyPressed(KeyEvent e) {
            }

            @Override
            public void keyReleased(KeyEvent e) {
                CargarDatos.buscarTabla(rsTablaProveedores, txtID.getText(), "Proveedor", "Proveedor");
                txtIDSelect.setText("ID Proveedor seleccionado: ");
                sID = "";
            }
        });
        this.kgPantalla.add(this.txtID);
    }

    private void labelsConfiguracion() {
        JLabel labelTitutlo = new JLabel("Eliminar Proveedor");
        labelTitutlo.setFont(Fuentes.FUENTE_TITULOS.getFont());
        labelTitutlo.setBounds(400, 20, 400, 30);
        this.kgPantalla.add(labelTitutlo);

        JLabel jlImgTitulo = new JLabel(Imagenes.I_IMAGEN_PROVEEDOR.getImagen());
        jlImgTitulo.setFont(Fuentes.FUENTE_TITULOS.getFont());
        jlImgTitulo.setBounds(320, 10, 64, 64);
        this.kgPantalla.add(jlImgTitulo);

        JLabel jlIDProveedor = new JLabel("Buscar ID: ");
        jlIDProveedor.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlIDProveedor.setBounds(300, 100, 100, 30);
        this.kgPantalla.add(jlIDProveedor);

        this.txtIDSelect = new JLabel("ID Proveedor seleccionado: ");
        this.txtIDSelect.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        this.txtIDSelect.setBounds(300, 180, 500, 70);
        this.kgPantalla.add(this.txtIDSelect);
    }

    private void botonesConfiguracion() {
        /*----------------------------------------*/
        this.btnEliminarProveedor = new RSButtonRiple();
        this.btnEliminarProveedor.setBounds(700, 100, 120, 50);
        this.btnEliminarProveedor.setBorderPainted(false);
        this.btnEliminarProveedor.setFocusPainted(false);
        this.btnEliminarProveedor.setBackground(Colores.C_COLOR_GRIS3.getColor());
        this.btnEliminarProveedor.setColorHover(Colores.C_COLOR_GRIS2.getColor());
        this.btnEliminarProveedor.setColorText(Colores.C_COLOR_NEGRO.getColor());
        this.btnEliminarProveedor.setColorTextHover(Colores.C_COLOR_GRIS4.getColor());
        this.btnEliminarProveedor.setText("Eliminar");
        this.btnEliminarProveedor.setActionCommand("btnEliProveedores");
        this.btnEliminarProveedor.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (seleccionProveedor() == true) {
                   boolean bEstado = Eliminaciones.eliminarProveedor(sID.trim());
                    if (bEstado) {
                        JOptionPane.showMessageDialog(null, "Proveedor eliminado");
                        txtID.setText("");
                        CargarDatos.buscarTabla(rsTablaProveedores, txtID.getText(), "Proveedor", "Proveedor");
                        txtIDSelect.setText("ID Proveedor seleccionado: ");
                    }
                   } else {
                    JOptionPane.showMessageDialog(null, "No ha selecionado proveedor"); 
                }
            }
        });
        this.kgPantalla.add(this.btnEliminarProveedor);
    }

    private boolean seleccionProveedor() {
        int renglon = this.rsTablaProveedores.getSelectedRow();
        if (renglon != -1) {
            String ID = this.rsTablaProveedores.getValueAt(renglon, 0) + "";
            this.sID = ID;
            this.txtIDSelect.setText("ID Proveedor seleccionado: " + ID);
        } else {
            System.out.println("No selecciono proveedor");
            this.sID = "";
            return false;
        }
        return true;
    }

    private void tablaConfiguracion() {
        this.rsTablaProveedores = new RSTableMetro();
        this.rsTablaProveedores.setAltoHead(60);
        this.rsTablaProveedores.setRowHeight(40);
        this.rsTablaProveedores.setGrosorBordeFilas(0);
        this.rsTablaProveedores.setColorBackgoundHead(Colores.C_COLOR_GRIS4.getColor());
        this.rsTablaProveedores.setColorFilasBackgound1(Colores.C_COLOR_GRIS1.getColor());
        this.rsTablaProveedores.setColorFilasBackgound2(Colores.C_COLOR_GRIS2.getColor());
        this.rsTablaProveedores.setColorFilasForeground1(Colores.C_COLOR_NEGRO.getColor());
        this.rsTablaProveedores.setColorFilasForeground2(Colores.C_COLOR_NEGRO.getColor());
        this.rsTablaProveedores.setColorSelBackgound(Colores.C_COLOR_GRIS3.getColor());
        this.rsTablaProveedores.setColorBordeFilas(Colores.C_COLOR_GRIS1.getColor());
        this.rsTablaProveedores.setFuenteFilas(Fuentes.FUENTE_TEXTOS.getFont());
        this.rsTablaProveedores.setFuenteHead(Fuentes.FUENTE_TEXTOS.getFont());
        this.rsTablaProveedores.setPreferredScrollableViewportSize(new Dimension(250, 100));
        JScrollPane scrollPane = new JScrollPane(this.rsTablaProveedores);
        scrollPane.setBounds(10, 250, 980, 250);
        CargarDatos.rellenarTabla(this.rsTablaProveedores, "Proveedor");
        this.rsTablaProveedores.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                seleccionProveedor();
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }


        });
        this.kgPantalla.add(scrollPane);
    }


    public KGradientPanel getkgPantalla() {
        return this.kgPantalla;
    }
}

