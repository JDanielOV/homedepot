package Ventanas.Pantallas;

import Recursos.Colores;
import Recursos.Fuentes;
import Recursos.Imagenes;
import SQL.Inserciones;
import java.awt.Color;
import keeptoo.KGradientPanel;
import rojeru_san.RSButtonRiple;
import rojerusan.RSComboMetro;

import javax.swing.*;
import javax.swing.border.MatteBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * @author Daniel Ochoa
 * Ochoa Fecha: 19/11/2021
 * Modificaciones: agrege para el provedor y cambie el metodo agregarproductp

 */
public class PantProdAgregar {
    private KGradientPanel kgPantalla;
    private JTextField txtNombre, txtIDProveedor;
    private RSButtonRiple btnCrearUsuario;
    private JSlider slPrecio, jsCantidad;
    private RSComboMetro cbCargos;
    private JSpinner jsCambiarValor, jsCantidadProdcutos;

    public PantProdAgregar() {
        this.panelConfiguracion();
        this.labelsConfiguracion();
        this.textosConfiguracion();
        this.botonesConfiguracion();
        this.comboBoxConfiguracion();
        this.sliderConfiguracion();
        this.spinerConfiguracion();
    }

    private void panelConfiguracion() {
        this.kgPantalla = new KGradientPanel();
        this.kgPantalla = new KGradientPanel();
        this.kgPantalla.setSize(1090, 550);
        this.kgPantalla.setBackground(Colores.C_COLOR_TRANSPARENTE.getColor());
        this.kgPantalla.setkEndColor(Colores.C_COLOR_BLANCO.getColor());
        this.kgPantalla.setkStartColor(Colores.C_COLOR_BLANCO.getColor());
        this.kgPantalla.setkBorderRadius(40);
        this.kgPantalla.setLayout(null);
    }

    private void textosConfiguracion() {
        this.txtNombre = new JTextField();
        this.txtNombre.setBounds(100, 150, 200, 30);
        this.txtNombre.setBackground(Colores.C_COLOR_GRIS1.getColor());
        this.txtNombre.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
        this.txtNombre.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.kgPantalla.add(this.txtNombre);


        this.txtIDProveedor = new JTextField();
        this.txtIDProveedor.setBounds(840, 150, 180, 30);
        this.txtIDProveedor.setBackground(Colores.C_COLOR_GRIS1.getColor());
        this.txtIDProveedor.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
        this.txtIDProveedor.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.kgPantalla.add(this.txtIDProveedor);

    }

    private void labelsConfiguracion() {
        JLabel labelTitutlo = new JLabel("Agregar Nuevo Producto");
        labelTitutlo.setFont(Fuentes.FUENTE_TITULOS.getFont());
        labelTitutlo.setBounds(350, 20, 350, 30);
        this.kgPantalla.add(labelTitutlo);

        JLabel jlImgTitulo = new JLabel(Imagenes.I_IMAGEN_PRODUCTO.getImagen());
        jlImgTitulo.setFont(Fuentes.FUENTE_TITULOS.getFont());
        jlImgTitulo.setBounds(280, 10, 64, 64);
        this.kgPantalla.add(jlImgTitulo);

        JLabel jlNombreCliente = new JLabel("Nombre: ");
        jlNombreCliente.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlNombreCliente.setBounds(0, 150, 100, 30);
        this.kgPantalla.add(jlNombreCliente);

        JLabel jlDepartamento = new JLabel("Departamento: ");
        jlDepartamento.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlDepartamento.setBounds(320, 150, 160, 30);
        this.kgPantalla.add(jlDepartamento);


        JLabel jlProveedor = new JLabel("ID Proveedor: ");
        jlProveedor.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlProveedor.setBounds(710, 150, 160, 30);
        this.kgPantalla.add(jlProveedor);


        JLabel jlPrecio = new JLabel("Precio: ");
        jlPrecio.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlPrecio.setBounds(0, 250, 130, 30);
        this.kgPantalla.add(jlPrecio);


        JLabel jlCantidad = new JLabel("Cantidad:  ");
        jlCantidad.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlCantidad.setBounds(0, 400, 200, 30);
        this.kgPantalla.add(jlCantidad);
    }

    private void botonesConfiguracion() {
        /*----------------------------------------*/
        this.btnCrearUsuario = new RSButtonRiple();
        this.btnCrearUsuario.setBounds(800, 400, 120, 50);
        this.btnCrearUsuario.setBorderPainted(false);
        this.btnCrearUsuario.setFocusPainted(false);
        this.btnCrearUsuario.setBackground(Colores.C_COLOR_GRIS3.getColor());
        this.btnCrearUsuario.setColorHover(Colores.C_COLOR_GRIS2.getColor());
        this.btnCrearUsuario.setColorText(Colores.C_COLOR_NEGRO.getColor());
        this.btnCrearUsuario.setColorTextHover(Colores.C_COLOR_GRIS4.getColor());
        this.btnCrearUsuario.setText("Agregar");
        this.btnCrearUsuario.setActionCommand("btnClientes");
        this.btnCrearUsuario.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                String sNombre = txtNombre.getText().trim();
                String sPrecio = jsCambiarValor.getValue().toString();
                String sDepartamento = cbCargos.getSelectedItem().toString();
                String sCantidad = jsCantidadProdcutos.getValue().toString();
                String sProveedor = txtIDProveedor.getText().trim();
                boolean bInsercionProducto = Inserciones.insertarProducto(sNombre, sPrecio, sDepartamento, sCantidad, sProveedor);
                if (bInsercionProducto) {
                    JOptionPane.showMessageDialog(null, "Producto agregado correctamente");
                    txtNombre.setText("");
                    txtIDProveedor.setText("");
                }
            }
        });
        this.kgPantalla.add(this.btnCrearUsuario);
    }

    private void comboBoxConfiguracion() {
        this.cbCargos = new RSComboMetro();
        this.cbCargos.addItem("Baños");
        this.cbCargos.addItem("Decoracion");
        this.cbCargos.addItem("Electrico");
        this.cbCargos.addItem("Ferreteria");
        this.cbCargos.addItem("Herramientas");
        this.cbCargos.addItem("Iluminacion");
        this.cbCargos.addItem("Jardin");
        this.cbCargos.addItem("Limpieza");
        this.cbCargos.addItem("Materiales de construcion");
        this.cbCargos.addItem("Pinturas");
        this.cbCargos.addItem("Pisos");
        this.cbCargos.addItem("Plomeria");
        this.cbCargos.addItem("Seguridad");
        this.cbCargos.addItem("Puertas y ventanas");
        this.cbCargos.addItem("Linea blanca y cocinas");
        this.cbCargos.addItem("Ventilacion y calefacion");
        this.cbCargos.addItem("Organizadores y closets");
        this.cbCargos.setSelectedIndex(2);
        this.cbCargos.setColorFondo(Colores.C_COLOR_GRIS2.getColor());
        this.cbCargos.setColorBorde(Colores.C_COLOR_GRIS3.getColor());
        this.cbCargos.setColorArrow(Colores.C_COLOR_GRIS3.getColor());
        this.cbCargos.setForeground(Colores.C_COLOR_NEGRO.getColor());
        this.cbCargos.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.cbCargos.setBounds(480, 150, 220, 30);
        this.kgPantalla.add(cbCargos);
    }

    private void sliderConfiguracion() {
        this.slPrecio = new JSlider(0, 15000, 7500);
        this.slPrecio.setOrientation(SwingConstants.HORIZONTAL);
        this.slPrecio.setMajorTickSpacing(1000);
        this.slPrecio.setMinorTickSpacing(2);
        this.slPrecio.setPaintLabels(true);
        this.slPrecio.setPaintTicks(true);
        this.slPrecio.setBounds(60, 250, 900, 50);
        this.kgPantalla.add(slPrecio);
        this.slPrecio.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                cambiarValorSpinner1();
            }
        });


        this.jsCantidad = new JSlider(0, 1000, 500);
        this.jsCantidad.setOrientation(SwingConstants.HORIZONTAL);
        this.jsCantidad.setMajorTickSpacing(100);
        this.jsCantidad.setMinorTickSpacing(2);
        this.jsCantidad.setPaintLabels(true);
        this.jsCantidad.setPaintTicks(true);
        this.jsCantidad.setBounds(90, 400, 700, 50);
        this.kgPantalla.add(jsCantidad);
        this.jsCantidad.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                cambiarValorSpinner2();
            }
        });
    }

    private void spinerConfiguracion() {
        this.jsCambiarValor = new JSpinner();
        this.jsCambiarValor.setValue(this.slPrecio.getValue());
        this.jsCambiarValor.setBounds(0, 300, 100, 30);
        this.jsCambiarValor.setBackground(Colores.C_COLOR_GRIS3.getColor());
        this.jsCambiarValor.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        this.jsCambiarValor.getEditor().getComponent(0).setEnabled(false);
        this.kgPantalla.add(this.jsCambiarValor);
        this.jsCambiarValor.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                cambiarValorSlider1();
                int valor = (int) jsCambiarValor.getValue();
                if(valor<=0){
                    jsCambiarValor.setValue(0);
                } else if(valor>=15000){
                    jsCambiarValor.setValue(15000);
                }
            }
        });

        this.jsCantidadProdcutos = new JSpinner();
        this.jsCantidadProdcutos.setValue(this.jsCantidad.getValue());
        this.jsCantidadProdcutos.setBounds(0, 450, 100, 30);
        this.jsCantidadProdcutos.setBackground(Colores.C_COLOR_GRIS3.getColor());
        this.jsCantidadProdcutos.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        this.jsCantidadProdcutos.getEditor().getComponent(0).setEnabled(false);
        this.kgPantalla.add(this.jsCantidadProdcutos);
        this.jsCantidadProdcutos.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                cambiarValorSlider2();
                int valor = (int) jsCantidadProdcutos.getValue();
                if(valor<=0){
                    jsCantidadProdcutos.setValue(0);
                } else if(valor>=1000){
                    jsCantidadProdcutos.setValue(1000);
                }
            }
        });
    }
    
    private void cambiarValorSpinner1(){
        this.jsCambiarValor.setValue(this.slPrecio.getValue());
    }
    
    private void cambiarValorSlider1(){
        this.slPrecio.setValue((int) this.jsCambiarValor.getValue());
    }
    
    private void cambiarValorSpinner2(){
        this.jsCantidadProdcutos.setValue(this.jsCantidad.getValue());
    }
    
    private void cambiarValorSlider2(){
        this.jsCantidad.setValue((int) this.jsCantidadProdcutos.getValue());
    }

    //Evento del slider para tomar valor
    /*private void barraValoresStateChanged(javax.swing.event.ChangeEvent evt) {
        this.jsCambiarValor.setValue(this.slPrecio.getValue());
    }

    private void barraCantidadStateChanged(javax.swing.event.ChangeEvent evt) {
        this.jsCantidadProdcutos.setValue(this.jsCantidad.getValue());
    }*/

    public KGradientPanel getkgPantalla() {
        return this.kgPantalla;
    }
}
