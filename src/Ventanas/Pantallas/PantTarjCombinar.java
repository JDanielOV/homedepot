package Ventanas.Pantallas;

import Recursos.Colores;
import Recursos.Fuentes;
import Recursos.Imagenes;
import SQL.Actualizar;
import SQL.CargarDatos;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.border.MatteBorder;
import keeptoo.KGradientPanel;
import rojeru_san.RSButtonRiple;
import rojerusan.RSComboMetro;

/**
 *
 * @author cristina 04-11-2021 hice esta clase para combinar los puntos de las
 * tarjetas de los clientes
 */
public class PantTarjCombinar {

    private KGradientPanel kgPantalla;
    private RSButtonRiple btnCombinarTarjeta;
    private JTextField txtClienteId, txtNumerodeTarjetaOrigen, txtNumerodeTarjetaDestino;
    private RSComboMetro rscmCliente, rscmNumerodeTarjetaOrigen, rscmNumerodeTarjetaDestino;
    private String sClienteId, sNumerodeTarjetaOrigen, sNumerodeTarjetaDestino;
    private int iPuntos,iPuntos2, iTotal;
    private JLabel labelPuntos;

    public PantTarjCombinar() {
	this.panelConfiguracion();
	this.labelsConfiguracion();
	this.textosConfiguracion();
	this.botonesConfiguracion();
	this.comboBoxConfiguracion();

    }

    private void panelConfiguracion() {
	this.kgPantalla = new KGradientPanel();
	this.kgPantalla = new KGradientPanel();
	this.kgPantalla.setSize(1090, 550);
	this.kgPantalla.setBackground(Colores.C_COLOR_TRANSPARENTE.getColor());
	this.kgPantalla.setkEndColor(Colores.C_COLOR_BLANCO.getColor());
	this.kgPantalla.setkStartColor(Colores.C_COLOR_BLANCO.getColor());
	this.kgPantalla.setkBorderRadius(40);
	this.kgPantalla.setLayout(null);
    }

    private void textosConfiguracion() {

    }

    private void labelsConfiguracion() {
	JLabel labelTituto = new JLabel("Combinar Tarjetas");
	labelTituto.setFont(Fuentes.FUENTE_TITULOS.getFont());
	labelTituto.setBounds(420, 20, 350, 30);
	this.kgPantalla.add(labelTituto);

	JLabel jlImgTitulo = new JLabel(Imagenes.I_IMAGEN_TARJETA.getImagen());
	jlImgTitulo.setFont(Fuentes.FUENTE_TITULOS.getFont());
	jlImgTitulo.setBounds(340, 10, 64, 64);
	this.kgPantalla.add(jlImgTitulo);

	JLabel jlIDCliente = new JLabel("Cliente: ");
	jlIDCliente.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
	jlIDCliente.setBounds(0, 100, 160, 30);
	this.kgPantalla.add(jlIDCliente);

        labelPuntos = new JLabel("Puntos:     #");
	labelPuntos.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
	labelPuntos.setBounds(0, 300, 200, 30);
	this.kgPantalla.add(labelPuntos);

	JLabel jlTarjetaNum = new JLabel("Numero de Tarjeta Origen:       #");
	jlTarjetaNum.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
	jlTarjetaNum.setBounds(0, 200, 320, 30);
	this.kgPantalla.add(jlTarjetaNum);

	JLabel jlTarjetaOtra = new JLabel("Numero de Tarjeta Destino:       #");
	jlTarjetaOtra.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
	jlTarjetaOtra.setBounds(420, 100, 320, 30);
	this.kgPantalla.add(jlTarjetaOtra);

    }

    private void botonesConfiguracion() {
	this.btnCombinarTarjeta = new RSButtonRiple();
	this.btnCombinarTarjeta.setBounds(420, 450, 160, 50);
	this.btnCombinarTarjeta.setBorderPainted(false);
	this.btnCombinarTarjeta.setFocusPainted(false);
	this.btnCombinarTarjeta.setBackground(Colores.C_COLOR_GRIS3.getColor());
	this.btnCombinarTarjeta.setColorHover(Colores.C_COLOR_GRIS2.getColor());
	this.btnCombinarTarjeta.setColorText(Colores.C_COLOR_NEGRO.getColor());
	this.btnCombinarTarjeta.setColorTextHover(Colores.C_COLOR_GRIS4.getColor());
	this.btnCombinarTarjeta.setText("Combinar");
	this.btnCombinarTarjeta.setActionCommand("btnTarjetas");
	this.btnCombinarTarjeta.addActionListener(new ActionListener() {
	    @Override
	    public void actionPerformed(ActionEvent actionEvent) {
		String sNombreClienteIDCliente = rscmCliente.getSelectedItem() == null ? " " : rscmCliente.getSelectedItem().toString();
		;
		int iComa = sNombreClienteIDCliente.indexOf(", ");
		int iDosPuntosEspacio = sNombreClienteIDCliente.indexOf("ID: ");

		String sClienteId = (sNombreClienteIDCliente.contains(", ")) ? (sNombreClienteIDCliente.substring(0, iComa)) : (" ");
		String sNumerodeTarjetaOrigen = (sNombreClienteIDCliente.contains("ID: ")) ? (sNombreClienteIDCliente.substring(iDosPuntosEspacio + 4)) : (" ");
		String sNumerodeTarjetaDestino = (sNombreClienteIDCliente.contains("ID: ")) ? (sNombreClienteIDCliente.substring(iDosPuntosEspacio + 4)) : (" ");
		System.out.println("#" + sClienteId + "#" + sNumerodeTarjetaOrigen + "#" + sNumerodeTarjetaDestino + "#");
		String[] rscmParametros = {
		    rscmNumerodeTarjetaOrigen.getSelectedItem() == null ? " " : rscmNumerodeTarjetaOrigen.getSelectedItem().toString(),
		    rscmNumerodeTarjetaDestino.getSelectedItem() == null ? " " : rscmNumerodeTarjetaDestino.getSelectedItem().toString()
		    
		};
		    boolean bValidarCliente = Actualizar.actualizarTarjeta(rscmParametros);
		if (bValidarCliente) {
		    JOptionPane.showMessageDialog(null, "Tarjeta Combinada correctamente");
                    labelPuntos.setText ("Puntos:     #");
		    rscmCliente.removeAllItems();
		    rscmNumerodeTarjetaOrigen.removeAllItems();
		    rscmNumerodeTarjetaDestino.removeAllItems();
		    iPuntos=0;
                    iPuntos2=0;
                    iTotal=0;
		}else {
		    JOptionPane.showMessageDialog(null, "No a selecionado una tarjeta correctamente");
	         }

	}});
	this.kgPantalla.add(this.btnCombinarTarjeta);
    }

    public KGradientPanel getkgPantalla() {
	return this.kgPantalla;
    }

    private void comboBoxConfiguracion() {

	this.rscmCliente = new RSComboMetro();
	this.rscmCliente.setColorFondo(Colores.C_COLOR_GRIS4.getColor());
	this.rscmCliente.setColorBorde(Colores.C_COLOR_GRIS3.getColor());
	this.rscmCliente.setColorArrow(Colores.C_COLOR_GRIS3.getColor());
	this.rscmCliente.setForeground(Colores.C_COLOR_NEGRO.getColor());
	this.rscmCliente.setFont(Fuentes.FUENTE_TEXTOS.getFont());
	this.rscmCliente.setBounds(100, 100, 315, 30);
	this.rscmCliente.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
	this.rscmCliente.setEditable(true);
	CargarDatos.comboBoxPonerDatos(this.rscmCliente, "nombreClienteIDCliente");
	this.kgPantalla.add(this.rscmCliente);

	this.rscmNumerodeTarjetaOrigen = new RSComboMetro();
	this.rscmNumerodeTarjetaOrigen.setColorFondo(Colores.C_COLOR_GRIS4.getColor());
	this.rscmNumerodeTarjetaOrigen.setColorBorde(Colores.C_COLOR_GRIS3.getColor());
	this.rscmNumerodeTarjetaOrigen.setColorArrow(Colores.C_COLOR_GRIS3.getColor());
	this.rscmNumerodeTarjetaOrigen.setForeground(Colores.C_COLOR_NEGRO.getColor());
	this.rscmNumerodeTarjetaOrigen.setFont(Fuentes.FUENTE_TEXTOS.getFont());
	this.rscmNumerodeTarjetaOrigen.setBounds(350, 200, 200, 30);
	this.kgPantalla.add(this.rscmNumerodeTarjetaOrigen);
        this.rscmNumerodeTarjetaOrigen.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String sTarjeta = rscmNumerodeTarjetaOrigen.getSelectedItem() == null ? " " : rscmNumerodeTarjetaOrigen.getSelectedItem().toString();
                if(!sTarjeta.equals(" ")){
                String sPuntos = CargarDatos.puntosTajerta(sTarjeta);
                iPuntos=0;
		iPuntos = sPuntos.equals(" ") ? 0 : Integer.parseInt(sPuntos);
                iTotal=0;
		iTotal= iPuntos2+iPuntos;
		labelPuntos.setText ("Puntos: "+iTotal);
                }
            }
        });

	this.rscmNumerodeTarjetaDestino = new RSComboMetro();
	this.rscmNumerodeTarjetaDestino.setColorFondo(Colores.C_COLOR_GRIS4.getColor());
	this.rscmNumerodeTarjetaDestino.setColorBorde(Colores.C_COLOR_GRIS3.getColor());
	this.rscmNumerodeTarjetaDestino.setColorArrow(Colores.C_COLOR_GRIS3.getColor());
	this.rscmNumerodeTarjetaDestino.setForeground(Colores.C_COLOR_NEGRO.getColor());
	this.rscmNumerodeTarjetaDestino.setFont(Fuentes.FUENTE_TEXTOS.getFont());
	this.rscmNumerodeTarjetaDestino.setBounds(800, 100, 200, 30);
	this.kgPantalla.add(this.rscmNumerodeTarjetaDestino);
        this.rscmNumerodeTarjetaDestino.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String sTarjeta = rscmNumerodeTarjetaDestino.getSelectedItem() == null ? " " : rscmNumerodeTarjetaDestino.getSelectedItem().toString();
                if(!sTarjeta.equals(" ")){
                String sPuntos = CargarDatos.puntosTajerta(sTarjeta);
                iPuntos2=0;
		iPuntos2 = sPuntos.equals(" ") ? 0 : Integer.parseInt(sPuntos);
                iTotal=0;
		iTotal= iPuntos+iPuntos2;
		labelPuntos.setText ("Puntos: "+iTotal);
                }
	    }
        });

	/*     this.rscmTarjeta.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String sTarjeta = rscmTarjeta.getSelectedItem().toString();
                String sPuntos = CargarDatos.puntosTajerta(sTarjeta);
                int iPuntos = sPuntos.equals(" ") ? 0 : Integer.parseInt(sPuntos);
                jsPuntosUsar.setMaximum(iPuntos);
            }
        });*/
	this.rscmCliente.addActionListener(new ActionListener() {
	    @Override
	    public void actionPerformed(ActionEvent e) {
                String sNombreClienteIDCliente = rscmCliente.getSelectedItem() == null ? " " : rscmCliente.getSelectedItem().toString();
                ;
                int iComa = sNombreClienteIDCliente.indexOf(", ");
                int iDosPuntosEspacio = sNombreClienteIDCliente.indexOf("ID: ");

                String sNombrea = (sNombreClienteIDCliente.contains(", ")) ? (sNombreClienteIDCliente.substring(0, iComa)) : (" ");
                String sClienteId = (sNombreClienteIDCliente.contains("ID: ")) ? (sNombreClienteIDCliente.substring(iDosPuntosEspacio + 4)) : (" ");

		System.out.println("Id Cliente selecionado:#" + sClienteId + "#");
		DefaultComboBoxModel dcbmNuevo = CargarDatos.cargarComboBox(sClienteId, "idTarjeta");
		dcbmNuevo.addElement(" ");
		rscmNumerodeTarjetaOrigen.setModel(dcbmNuevo);
                rscmNumerodeTarjetaOrigen.setSelectedIndex(rscmNumerodeTarjetaOrigen.getItemCount() - 1);
                DefaultComboBoxModel dcbmNuevo2 = CargarDatos.cargarComboBox(sClienteId, "idTarjeta");
		dcbmNuevo2.addElement(" ");
		rscmNumerodeTarjetaDestino.setModel(dcbmNuevo2);
                rscmNumerodeTarjetaDestino.setSelectedIndex(rscmNumerodeTarjetaDestino.getItemCount() - 1);
                labelPuntos.setText ("Puntos:    #");
                iPuntos=0;
                iPuntos2=0;
                iTotal=0;
		//rscmNumerodeTarjetaDestino.setSelectedIndex(rscmNumerodeTarjetaDestino.getItemCount() - 1);
	    }
	});

    }
}