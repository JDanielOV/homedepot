package Ventanas.Pantallas;

/**
 * @author cristina
 * fecha 01-11-2021
 * fecha: 02-11-2021 Cambie el tamaño que habia entre cada etiqueta
 * Agregue el campo contraseña
 * cristina 14-11-2021 agregue para insertar datos
 * cristina 14-11-2021
 * modificaciones:cambie el slider y spinner para poner limites
 */

import Recursos.Colores;
import Recursos.Fuentes;
import Recursos.Imagenes;
import SQL.Inserciones;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;

import keeptoo.KGradientPanel;

import javax.swing.*;
import javax.swing.border.MatteBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import rojeru_san.RSButtonRiple;
import rojerusan.RSDateChooser;
import rojerusan.RSComboMetro;
import rojerusan.RSPasswordTextPlaceHolder;


public class PantTraAgregar {
    private KGradientPanel kgPantalla;
    private JTextField txtRfc, txtNombre, txtApePaterno, txtApeMaterno, txtEmail, txtTelefono, txtDireccion;
    private RSDateChooser rdFechas;
    private RSComboMetro cbCargos;
    private JSpinner Sueldo;
    private JSlider slSueldo;
    private RSButtonRiple btnCrearTrabajador;
    private RSPasswordTextPlaceHolder rsContrasena;

    public PantTraAgregar() {
        this.panelConfiguracion();
        this.labelsConfiguracion();
        this.textosConfiguracion();
        this.dateChooserConfiguracion();
        this.comboBoxConfiguracion();
        this.sliderConfiguracion();
        this.botonesConfiguracion();
        this.spinnerConfiguracion();
    }

    private void dateChooserConfiguracion() {
        this.rdFechas = new RSDateChooser();
        this.rdFechas.setColorBackground(Colores.C_COLOR_GRIS3.getColor());
        this.rdFechas.setColorButtonHover(Colores.C_COLOR_GRIS3.getColor());
        this.rdFechas.setColorTextDiaActual(Colores.C_COLOR_NEGRO.getColor());
        this.rdFechas.setColorForeground(Colores.C_COLOR_NEGRO.getColor());
        this.rdFechas.setFormatoFecha("YYYY-MM-dd");
        this.rdFechas.setFuente(Fuentes.FUENTE_TEXTOS.getFont());
        this.rdFechas.setBounds(190, 300, 180, 30);
        this.kgPantalla.add(rdFechas);
    }

    private void comboBoxConfiguracion() {
        this.cbCargos = new RSComboMetro();
        this.cbCargos.addItem("Cajero");
        this.cbCargos.addItem("Almacenista");
        this.cbCargos.addItem("Vendedor");
        this.cbCargos.addItem("Gerente");
        this.cbCargos.addItem("Oficinista");
        this.cbCargos.setSelectedIndex(2);
        this.cbCargos.setColorFondo(Colores.C_COLOR_GRIS2.getColor());
        this.cbCargos.setColorBorde(Colores.C_COLOR_GRIS3.getColor());
        this.cbCargos.setColorArrow(Colores.C_COLOR_GRIS3.getColor());
        this.cbCargos.setForeground(Colores.C_COLOR_NEGRO.getColor());
        this.cbCargos.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.cbCargos.setBounds(780, 300, 190, 30);
        this.kgPantalla.add(cbCargos);
    }

    private void spinnerConfiguracion() {
        this.Sueldo = new JSpinner();
        this.Sueldo.setBounds(120, 440, 80, 25);
        this.Sueldo.setValue(this.slSueldo.getValue());
        this.Sueldo.getEditor().getComponent(0).setEnabled(false);
        this.Sueldo.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        this.Sueldo.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                cambiarValorSlider();
                int valor = (int) Sueldo.getValue();
                if (valor <= 2000) {
                    Sueldo.setValue(2000);
                } else if (valor >= 30000) {
                    Sueldo.setValue(30000);
                }
            }

        });
        this.kgPantalla.add(this.Sueldo);
    }

    private void sliderConfiguracion() {
        this.slSueldo = new JSlider(2000, 30000, 15000);
        this.slSueldo.setOrientation(SwingConstants.HORIZONTAL);
        this.slSueldo.setMajorTickSpacing(2000);
        this.slSueldo.setMinorTickSpacing(2);
        this.slSueldo.setPaintLabels(true);
        this.slSueldo.setPaintTicks(true);
        this.slSueldo.setBounds(90, 390, 700, 50);
        this.kgPantalla.add(this.slSueldo);
        this.slSueldo.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                cambiarValorSpinner();
            }
        });
        ;
    }

    private void cambiarValorSpinner() {
        this.Sueldo.setValue(this.slSueldo.getValue());
    }

    private void cambiarValorSlider() {
        this.slSueldo.setValue((int) this.Sueldo.getValue());
    }

    private void panelConfiguracion() {
        this.kgPantalla = new KGradientPanel();
        this.kgPantalla = new KGradientPanel();
        this.kgPantalla.setSize(1090, 550);
        this.kgPantalla.setBackground(Colores.C_COLOR_TRANSPARENTE.getColor());
        this.kgPantalla.setkEndColor(Colores.C_COLOR_BLANCO.getColor());
        this.kgPantalla.setkStartColor(Colores.C_COLOR_BLANCO.getColor());
        this.kgPantalla.setkBorderRadius(40);
        this.kgPantalla.setLayout(null);
    }

    private void textosConfiguracion() {
        this.txtRfc = new JTextField();
        this.txtRfc.setBounds(60, 100, 200, 30);
        this.txtRfc.setBackground(Colores.C_COLOR_GRIS1.getColor());
        this.txtRfc.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
        this.txtRfc.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.kgPantalla.add(this.txtRfc);

        this.txtNombre = new JTextField();
        this.txtNombre.setBounds(390, 100, 200, 30);
        this.txtNombre.setBackground(Colores.C_COLOR_GRIS1.getColor());
        this.txtNombre.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
        this.txtNombre.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.kgPantalla.add(this.txtNombre);

        this.txtApePaterno = new JTextField();
        this.txtApePaterno.setBounds(770, 100, 200, 30);
        this.txtApePaterno.setBackground(Colores.C_COLOR_GRIS1.getColor());
        this.txtApePaterno.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
        this.txtApePaterno.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.kgPantalla.add(this.txtApePaterno);

        this.txtApeMaterno = new JTextField();
        this.txtApeMaterno.setBounds(770, 200, 200, 30);
        this.txtApeMaterno.setBackground(Colores.C_COLOR_GRIS1.getColor());
        this.txtApeMaterno.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
        this.txtApeMaterno.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.kgPantalla.add(this.txtApeMaterno);

        this.txtEmail = new JTextField();
        this.txtEmail.setBounds(70, 200, 200, 30);
        this.txtEmail.setBackground(Colores.C_COLOR_GRIS1.getColor());
        this.txtEmail.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
        this.txtEmail.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.kgPantalla.add(this.txtEmail);

        this.txtTelefono = new JTextField();
        this.txtTelefono.setBounds(390, 200, 200, 30);
        this.txtTelefono.setBackground(Colores.C_COLOR_GRIS1.getColor());
        this.txtTelefono.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
        this.txtTelefono.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.kgPantalla.add(this.txtTelefono);

        this.txtDireccion = new JTextField();
        this.txtDireccion.setBounds(500, 300, 200, 30);
        this.txtDireccion.setBackground(Colores.C_COLOR_GRIS1.getColor());
        this.txtDireccion.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
        this.txtDireccion.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.kgPantalla.add(this.txtDireccion);

        this.rsContrasena = new RSPasswordTextPlaceHolder();
        this.rsContrasena.setBounds(450, 480, 200, 30);
        this.rsContrasena.setBackground(Colores.C_COLOR_GRIS1.getColor());
        this.rsContrasena.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
        this.rsContrasena.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.rsContrasena.setForeground(Colores.C_COLOR_NEGRO.getColor());
        this.kgPantalla.add(this.rsContrasena);
    }

    private void labelsConfiguracion() {
        JLabel labelTitutlo = new JLabel("Agregar Nuevo Trabajador");
        labelTitutlo.setFont(Fuentes.FUENTE_TITULOS.getFont());
        labelTitutlo.setBounds(400, 20, 350, 30);
        this.kgPantalla.add(labelTitutlo);

        JLabel jlImgTitulo = new JLabel(Imagenes.I_IMAGEN_TRABAJADOR.getImagen());
        jlImgTitulo.setFont(Fuentes.FUENTE_TITULOS.getFont());
        jlImgTitulo.setBounds(320, 10, 64, 64);
        this.kgPantalla.add(jlImgTitulo);

        JLabel jlRfcTrabajador = new JLabel("Rfc: ");
        jlRfcTrabajador.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlRfcTrabajador.setBounds(0, 100, 100, 30);
        this.kgPantalla.add(jlRfcTrabajador);

        JLabel jlNombreTrabajador = new JLabel("Nombre: ");
        jlNombreTrabajador.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlNombreTrabajador.setBounds(300, 100, 100, 30);
        this.kgPantalla.add(jlNombreTrabajador);

        JLabel jlApePTrabajador = new JLabel("Apellido P: ");
        jlApePTrabajador.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlApePTrabajador.setBounds(620, 100, 130, 30);
        this.kgPantalla.add(jlApePTrabajador);

        JLabel jlApeMTrabajador = new JLabel("Apellido M: ");
        jlApeMTrabajador.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlApeMTrabajador.setBounds(620, 200, 130, 30);
        this.kgPantalla.add(jlApeMTrabajador);

        JLabel jlEmailTrabajador = new JLabel("Email: ");
        jlEmailTrabajador.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlEmailTrabajador.setBounds(0, 200, 100, 30);
        this.kgPantalla.add(jlEmailTrabajador);

        JLabel jlTelTrabajador = new JLabel("Telefono: ");
        jlTelTrabajador.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlTelTrabajador.setBounds(300, 200, 100, 30);
        this.kgPantalla.add(jlTelTrabajador);

        JLabel jlFechaNacTrabajador = new JLabel("Fecha nacimiento: ");
        jlFechaNacTrabajador.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlFechaNacTrabajador.setBounds(0, 300, 190, 30);
        this.kgPantalla.add(jlFechaNacTrabajador);

        JLabel jlDireccionTrabajador = new JLabel("Direccion: ");
        jlDireccionTrabajador.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlDireccionTrabajador.setBounds(400, 300, 190, 30);
        this.kgPantalla.add(jlDireccionTrabajador);

        JLabel jlCargoTrabajador = new JLabel("Cargo: ");
        jlCargoTrabajador.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlCargoTrabajador.setBounds(710, 300, 190, 30);
        this.kgPantalla.add(jlCargoTrabajador);

        JLabel jlSueldoTrabajador = new JLabel("Sueldo: ");
        jlSueldoTrabajador.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlSueldoTrabajador.setBounds(0, 390, 100, 30);
        this.kgPantalla.add(jlSueldoTrabajador);

        JLabel jlContraseñaTrabajador = new JLabel("Contraseña: ");
        jlContraseñaTrabajador.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlContraseñaTrabajador.setBounds(330, 480, 150, 30);
        this.kgPantalla.add(jlContraseñaTrabajador);
    }

    private void botonesConfiguracion() {
        /*----------------------------------------*/
        this.btnCrearTrabajador = new RSButtonRiple();
        this.btnCrearTrabajador.setBounds(860, 450, 120, 50);
        this.btnCrearTrabajador.setBorderPainted(false);
        this.btnCrearTrabajador.setFocusPainted(false);
        this.btnCrearTrabajador.setBackground(Colores.C_COLOR_GRIS3.getColor());
        this.btnCrearTrabajador.setColorHover(Colores.C_COLOR_GRIS2.getColor());
        this.btnCrearTrabajador.setColorText(Colores.C_COLOR_NEGRO.getColor());
        this.btnCrearTrabajador.setColorTextHover(Colores.C_COLOR_GRIS4.getColor());
        this.btnCrearTrabajador.setText("Agregar");
        this.btnCrearTrabajador.setActionCommand("btnTrabajadores");
        this.btnCrearTrabajador.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                String sNombre = txtNombre.getText().trim();
                String sApellidoP = txtApePaterno.getText().trim();
                String sApellidoM = txtApeMaterno.getText().trim();
                String sCorreo = txtEmail.getText().trim();
                String sTelefono = txtTelefono.getText().trim();
                String sRfc = txtRfc.getText().trim();
                String sFecha = rdFechas.getFechaSeleccionada().trim();
                String sDireccion = txtDireccion.getText().trim();
                String sCargo = cbCargos.getSelectedItem().toString();
                String sSueldo = Sueldo.getValue().toString();
                String sContraseña = rsContrasena.getText().trim();
                boolean bInsercionTrabajador = Inserciones.insertarTrabajador(sNombre, sApellidoP, sApellidoM, sCorreo, sTelefono, sRfc, sFecha, sDireccion, sCargo, sSueldo, sContraseña);
                if (bInsercionTrabajador) {
                    JOptionPane.showMessageDialog(null, "Trabajador agregado correctamente");
                    txtNombre.setText("");
                    txtApePaterno.setText("");
                    txtApeMaterno.setText("");
                    txtEmail.setText("");
                    txtTelefono.setText("");
                    txtRfc.setText("");
                    txtDireccion.setText("");
                    rsContrasena.setText("");
                    slSueldo.setValue(slSueldo.getMajorTickSpacing());
                    rdFechas.setTextoFecha(LocalDate.now().toString());
                    rdFechas.setFormatoFecha("YYYY-MM-dd");
                }
            }
        });
        this.kgPantalla.add(this.btnCrearTrabajador);


    }

    public KGradientPanel getkgPantalla() {
        return this.kgPantalla;
    }
}