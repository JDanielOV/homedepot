package Ventanas.Pantallas;

/**
 * @author cristina
 * Ochoa Fecha: 08/11/2021
 * Modificacion: cambie el metodo buscarTabla
 */

import Recursos.Colores;
import Recursos.Fuentes;
import Recursos.Imagenes;
import SQL.CargarDatos;
import SQL.Conexion;
import keeptoo.KGradientPanel;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;
import net.sf.jasperreports.view.JasperViewer;
import rojeru_san.RSButtonRiple;
import rojerusan.RSTableMetro;

import javax.swing.*;
import javax.swing.border.MatteBorder;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class PantTraVer {
    private RSTableMetro rsTablaTrabajadores;
    private KGradientPanel kgPantalla;
    private JTextField txtNombre;
    private RSButtonRiple btnGenerarReporte;

    public PantTraVer() {
        this.panelConfiguracion();
        this.labelsConfiguracion();
        this.textosConfiguracion();
        this.tablaConfiguracion();
        this.botonesConfiguracion();
    }

    private void panelConfiguracion() {
        this.kgPantalla = new KGradientPanel();
        this.kgPantalla = new KGradientPanel();
        this.kgPantalla.setSize(1090, 550);
        this.kgPantalla.setBackground(Colores.C_COLOR_TRANSPARENTE.getColor());
        this.kgPantalla.setkEndColor(Colores.C_COLOR_BLANCO.getColor());
        this.kgPantalla.setkStartColor(Colores.C_COLOR_BLANCO.getColor());
        this.kgPantalla.setkBorderRadius(40);
        this.kgPantalla.setLayout(null);
    }

    private void textosConfiguracion() {
        this.txtNombre = new JTextField();
        this.txtNombre.setBounds(410, 100, 200, 30);
        this.txtNombre.setBackground(Colores.C_COLOR_GRIS1.getColor());
        this.txtNombre.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
        this.txtNombre.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.txtNombre.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyPressed(KeyEvent e) {
            }

            @Override
            public void keyReleased(KeyEvent e) {
                CargarDatos.buscarTabla(rsTablaTrabajadores, txtNombre.getText(), "TrabajadorNombre", "Trabajador");
                System.out.println("d");
            }
        });
        this.kgPantalla.add(this.txtNombre);
    }

    private void labelsConfiguracion() {
        JLabel labelTitutlo = new JLabel("Lista De Trabajadores");
        labelTitutlo.setFont(Fuentes.FUENTE_TITULOS.getFont());
        labelTitutlo.setBounds(400, 20, 400, 30);
        this.kgPantalla.add(labelTitutlo);

        JLabel jlImgTitulo = new JLabel(Imagenes.I_IMAGEN_TRABAJADOR.getImagen());
        jlImgTitulo.setFont(Fuentes.FUENTE_TITULOS.getFont());
        jlImgTitulo.setBounds(320, 10, 64, 64);
        this.kgPantalla.add(jlImgTitulo);

        JLabel jlNombre = new JLabel("Nombre: ");
        jlNombre.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlNombre.setBounds(320, 100, 100, 30);
        this.kgPantalla.add(jlNombre);
    }

    private void tablaConfiguracion() {
        this.rsTablaTrabajadores = new RSTableMetro();
        this.rsTablaTrabajadores.setAltoHead(60);
        this.rsTablaTrabajadores.setRowHeight(40);
        this.rsTablaTrabajadores.setGrosorBordeFilas(0);
        this.rsTablaTrabajadores.setColorBackgoundHead(Colores.C_COLOR_GRIS4.getColor());
        this.rsTablaTrabajadores.setColorFilasBackgound1(Colores.C_COLOR_GRIS1.getColor());
        this.rsTablaTrabajadores.setColorFilasBackgound2(Colores.C_COLOR_GRIS2.getColor());
        this.rsTablaTrabajadores.setColorFilasForeground1(Colores.C_COLOR_NEGRO.getColor());
        this.rsTablaTrabajadores.setColorFilasForeground2(Colores.C_COLOR_NEGRO.getColor());
        this.rsTablaTrabajadores.setColorSelBackgound(Colores.C_COLOR_GRIS3.getColor());
        this.rsTablaTrabajadores.setColorBordeFilas(Colores.C_COLOR_GRIS1.getColor());
        this.rsTablaTrabajadores.setFuenteFilas(Fuentes.FUENTE_TEXTOS.getFont());
        this.rsTablaTrabajadores.setFuenteHead(Fuentes.FUENTE_TEXTOS.getFont());
        this.rsTablaTrabajadores.setPreferredScrollableViewportSize(new Dimension(250, 100));
        JScrollPane scrollPane = new JScrollPane(this.rsTablaTrabajadores);
        scrollPane.setBounds(10, 250, 980, 250);
        CargarDatos.rellenarTabla(this.rsTablaTrabajadores, "Trabajador");
        this.kgPantalla.add(scrollPane);
    }

    private void botonesConfiguracion() {
        /*----------------------------------------*/
        this.btnGenerarReporte = new RSButtonRiple();
        this.btnGenerarReporte.setBounds(750, 140, 200, 50);
        this.btnGenerarReporte.setBorderPainted(false);
        this.btnGenerarReporte.setFocusPainted(false);
        this.btnGenerarReporte.setBackground(Colores.C_COLOR_GRIS3.getColor());
        this.btnGenerarReporte.setColorHover(Colores.C_COLOR_GRIS2.getColor());
        this.btnGenerarReporte.setColorText(Colores.C_COLOR_NEGRO.getColor());
        this.btnGenerarReporte.setColorTextHover(Colores.C_COLOR_GRIS4.getColor());
        this.btnGenerarReporte.setText("Generar Reporte");
        this.btnGenerarReporte.setActionCommand("btnEliPedidos");
        this.btnGenerarReporte.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                System.out.println("Reporte");
                JasperReport archivo;
                try {
                    archivo = JasperCompileManager.compileReport("Configuracion/Reportes/Trabajadores.jrxml");
                    JasperPrint prin = JasperFillManager.fillReport(archivo, null, Conexion.getConexion());
                    if (prin != null) {
                        JasperViewer view = new JasperViewer(prin, false);
                        view.setVisible(true);
                    }
                } catch (JRException ex) {
                    JOptionPane.showMessageDialog(null, "El documento no tiene paginas");
                }
            }
        });
        this.kgPantalla.add(this.btnGenerarReporte);
    }

    public KGradientPanel getkgPantalla() {
        return this.kgPantalla;
    }
}
