package Ventanas.Pantallas;

import Recursos.Colores;
import Recursos.Fuentes;
import Recursos.Imagenes;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.MatteBorder;
import javax.swing.table.DefaultTableModel;

import SQL.CargarDatos;
import SQL.Eliminaciones;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JOptionPane;

import keeptoo.KGradientPanel;
import rojeru_san.RSButtonRiple;
import rojerusan.RSTableMetro;

/**
 * @author Ochoa
 * Ochoa Fecha: 08/11/2021
 * Modificacion: cambie el metodo buscarTabla
 */
public class PantPedEliminar {
    private RSTableMetro rsTablaPedidos;
    private KGradientPanel kgPantalla;
    private JTextField txtID;
    private JLabel txtIDSelect;
    private RSButtonRiple btnEliminarPedidos;
    private String sID;

    public PantPedEliminar() {
        this.panelConfiguracion();
        this.labelsConfiguracion();
        this.textosConfiguracion();
        this.tablaConfiguracion();
        this.botonesConfiguracion();
    }

    private void panelConfiguracion() {
        this.kgPantalla = new KGradientPanel();
        this.kgPantalla = new KGradientPanel();
        this.kgPantalla.setSize(1090, 550);
        this.kgPantalla.setBackground(Colores.C_COLOR_TRANSPARENTE.getColor());
        this.kgPantalla.setkEndColor(Colores.C_COLOR_BLANCO.getColor());
        this.kgPantalla.setkStartColor(Colores.C_COLOR_BLANCO.getColor());
        this.kgPantalla.setkBorderRadius(40);
        this.kgPantalla.setLayout(null);
    }

    private void textosConfiguracion() {
        this.txtID = new JTextField();
        this.txtID.setBounds(410, 100, 200, 30);
        this.txtID.setBackground(Colores.C_COLOR_GRIS1.getColor());
        this.txtID.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
        this.txtID.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.txtID.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyPressed(KeyEvent e) {
            }

            @Override
            public void keyReleased(KeyEvent e) {
                CargarDatos.buscarTabla(rsTablaPedidos, txtID.getText(), "Pedidos", "Pedidos");
                txtIDSelect.setText("ID Pedido seleccionado: ");
                sID = "";
            }
        });
        this.kgPantalla.add(this.txtID);
    }

    private void labelsConfiguracion() {
        JLabel labelTitutlo = new JLabel("Eliminar Pedido");
        labelTitutlo.setFont(Fuentes.FUENTE_TITULOS.getFont());
        labelTitutlo.setBounds(400, 20, 400, 30);
        this.kgPantalla.add(labelTitutlo);

        JLabel jlImgTitulo = new JLabel(Imagenes.I_IMAGEN_PEDIDO.getImagen());
        jlImgTitulo.setFont(Fuentes.FUENTE_TITULOS.getFont());
        jlImgTitulo.setBounds(320, 10, 64, 64);
        this.kgPantalla.add(jlImgTitulo);

        JLabel jlIDProveedor = new JLabel("Buscar ID: ");
        jlIDProveedor.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlIDProveedor.setBounds(300, 100, 130, 30);
        this.kgPantalla.add(jlIDProveedor);

        this.txtIDSelect = new JLabel("ID Pedido seleccionado: ");
        this.txtIDSelect.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        this.txtIDSelect.setBounds(300, 180, 500, 70);
        this.kgPantalla.add(this.txtIDSelect);
    }

    private void botonesConfiguracion() {
        /*----------------------------------------*/
        this.btnEliminarPedidos = new RSButtonRiple();
        this.btnEliminarPedidos.setBounds(700, 100, 120, 50);
        this.btnEliminarPedidos.setBorderPainted(false);
        this.btnEliminarPedidos.setFocusPainted(false);
        this.btnEliminarPedidos.setBackground(Colores.C_COLOR_GRIS3.getColor());
        this.btnEliminarPedidos.setColorHover(Colores.C_COLOR_GRIS2.getColor());
        this.btnEliminarPedidos.setColorText(Colores.C_COLOR_NEGRO.getColor());
        this.btnEliminarPedidos.setColorTextHover(Colores.C_COLOR_GRIS4.getColor());
        this.btnEliminarPedidos.setText("Eliminar");
        this.btnEliminarPedidos.setActionCommand("btnEliPedidos");
        this.btnEliminarPedidos.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (seleccionProveedor() == true) {
                    boolean bEstado = Eliminaciones.eliminarPedido(sID.trim());
                    if (bEstado) {
                        JOptionPane.showMessageDialog(null, "Pedido eliminado");
                        txtID.setText("");
                        CargarDatos.buscarTabla(rsTablaPedidos, txtID.getText(), "Pedidos", "Pedidos");
                        txtIDSelect.setText("ID Pedido seleccionado: ");
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "No ha selecionado pedido");
                }
            }
        });
        this.kgPantalla.add(this.btnEliminarPedidos);
    }

    private boolean seleccionProveedor() {
        int renglon = this.rsTablaPedidos.getSelectedRow();
        if (renglon != -1) {
            String ID = this.rsTablaPedidos.getValueAt(renglon, 0) + "";
            this.sID = ID;
            this.txtIDSelect.setText("ID Pedido seleccionado: " + ID);
        } else {
            System.out.println("No selecciono pedido");
            this.sID = "";
            return false;
        }
        return true;
    }

    private void tablaConfiguracion() {
        this.rsTablaPedidos = new RSTableMetro();
        this.rsTablaPedidos.setAltoHead(60);
        this.rsTablaPedidos.setRowHeight(40);
        this.rsTablaPedidos.setGrosorBordeFilas(0);
        this.rsTablaPedidos.setColorBackgoundHead(Colores.C_COLOR_GRIS4.getColor());
        this.rsTablaPedidos.setColorFilasBackgound1(Colores.C_COLOR_GRIS1.getColor());
        this.rsTablaPedidos.setColorFilasBackgound2(Colores.C_COLOR_GRIS2.getColor());
        this.rsTablaPedidos.setColorFilasForeground1(Colores.C_COLOR_NEGRO.getColor());
        this.rsTablaPedidos.setColorFilasForeground2(Colores.C_COLOR_NEGRO.getColor());
        this.rsTablaPedidos.setColorSelBackgound(Colores.C_COLOR_GRIS3.getColor());
        this.rsTablaPedidos.setColorBordeFilas(Colores.C_COLOR_GRIS1.getColor());
        this.rsTablaPedidos.setFuenteFilas(Fuentes.FUENTE_TEXTOS.getFont());
        this.rsTablaPedidos.setFuenteHead(Fuentes.FUENTE_TEXTOS.getFont());
        this.rsTablaPedidos.setPreferredScrollableViewportSize(new Dimension(250, 100));
        JScrollPane scrollPane = new JScrollPane(this.rsTablaPedidos);
        scrollPane.setBounds(10, 250, 1000, 250);
        CargarDatos.rellenarTabla(this.rsTablaPedidos, "Pedidos");
        this.rsTablaPedidos.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                seleccionProveedor();
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }

        });
        this.kgPantalla.add(scrollPane);
    }


    public KGradientPanel getkgPantalla() {
        return this.kgPantalla;
    }
}
