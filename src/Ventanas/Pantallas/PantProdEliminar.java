package Ventanas.Pantallas;

/**
* @author Juan Jose
 * Fecha: 24/11/2021
 
 */



import Recursos.Colores;
import Recursos.Fuentes;
import Recursos.Imagenes;
import SQL.CargarDatos;
import keeptoo.KGradientPanel;
import rojeru_san.RSButtonRiple;
import rojerusan.RSTableMetro;
import SQL.Eliminaciones;
import javax.swing.*;
import javax.swing.border.MatteBorder;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;


public class PantProdEliminar {
    private RSTableMetro rsTablaProductos;
    private KGradientPanel kgPantalla;
    private JTextField txtID;
    private JLabel txtIDSelect;
    private RSButtonRiple btnEliminarProducto;
    private String sID;
    
    public PantProdEliminar() {
        this.panelConfiguracion();
        this.labelsConfiguracion();
        this.textosConfiguracion();
        this.tablaConfiguracion();
        this.botonesConfiguracion();
    }

    private void panelConfiguracion() {
        this.kgPantalla = new KGradientPanel();
        this.kgPantalla = new KGradientPanel();
        this.kgPantalla.setSize(1090, 550);
        this.kgPantalla.setBackground(Colores.C_COLOR_TRANSPARENTE.getColor());
        this.kgPantalla.setkEndColor(Colores.C_COLOR_BLANCO.getColor());
        this.kgPantalla.setkStartColor(Colores.C_COLOR_BLANCO.getColor());
        this.kgPantalla.setkBorderRadius(40);
        this.kgPantalla.setLayout(null);
    }

    private void textosConfiguracion() {
        this.txtID = new JTextField();
        this.txtID.setBounds(480, 100, 200, 30);
        this.txtID.setBackground(Colores.C_COLOR_GRIS1.getColor());
        this.txtID.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
        this.txtID.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.txtID.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyPressed(KeyEvent e) {
            }

            @Override
            public void keyReleased(KeyEvent e) {
                CargarDatos.buscarTabla(rsTablaProductos,txtID.getText(),"Producto","Producto");
                txtIDSelect.setText("ID Producto seleccionado: ");
                sID = "";
            }
        });
        this.kgPantalla.add(this.txtID);
    }

    private void labelsConfiguracion() {
        JLabel labelTitutlo = new JLabel("Eliminar Producto");
        labelTitutlo.setFont(Fuentes.FUENTE_TITULOS.getFont());
        labelTitutlo.setBounds(350, 20, 350, 30);
        this.kgPantalla.add(labelTitutlo);

        JLabel jlImgTitulo = new JLabel(Imagenes.I_IMAGEN_PRODUCTO.getImagen());
        jlImgTitulo.setFont(Fuentes.FUENTE_TITULOS.getFont());
        jlImgTitulo.setBounds(280, 10, 64, 64);
        this.kgPantalla.add(jlImgTitulo);

        JLabel jlIDCliente = new JLabel("Buscar ID Producto: ");
        jlIDCliente.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlIDCliente.setBounds(280, 100, 200, 30);
        this.kgPantalla.add(jlIDCliente);

        this.txtIDSelect = new JLabel("ID Producto seleccionado: ");
        this.txtIDSelect.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        this.txtIDSelect.setBounds(300, 180, 500, 70);
        this.kgPantalla.add(this.txtIDSelect);
    }

    private void botonesConfiguracion() {
        this.btnEliminarProducto = new RSButtonRiple();
        this.btnEliminarProducto.setBounds(700, 100, 120, 50);
        this.btnEliminarProducto.setBorderPainted(false);
        this.btnEliminarProducto.setFocusPainted(false);
        this.btnEliminarProducto.setBackground(Colores.C_COLOR_GRIS3.getColor());
        this.btnEliminarProducto.setColorHover(Colores.C_COLOR_GRIS2.getColor());
        this.btnEliminarProducto.setColorText(Colores.C_COLOR_NEGRO.getColor());
        this.btnEliminarProducto.setColorTextHover(Colores.C_COLOR_GRIS4.getColor());
        this.btnEliminarProducto.setText("Eliminar");
        this.btnEliminarProducto.setActionCommand("btnEliProducto");
        this.btnEliminarProducto.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (seleccionProducto() == true) {
                  boolean bEstado = Eliminaciones.eliminarProducto(sID.trim());
                    if (bEstado) {
                        JOptionPane.showMessageDialog(null, "Producto eliminado");
                        txtID.setText("");
                        CargarDatos.buscarTabla(rsTablaProductos, txtID.getText(), "Producto", "Producto");
                        txtIDSelect.setText("ID Producto seleccionado: ");
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "No ha selecionado producto");
                }
            }

        });
        this.kgPantalla.add(this.btnEliminarProducto);
    }

    private boolean seleccionProducto() {
        int renglon = this.rsTablaProductos.getSelectedRow();
        if (renglon != -1) {
            String ID = this.rsTablaProductos.getValueAt(renglon, 0) + "";
	    this.sID = ID;
            this.txtIDSelect.setText("ID Producto seleccionado: " + ID);
        } else {
            System.out.println("No selecciono producto");
	    this.sID = "";
            return false;
        }
        return true;
    }

    private void tablaConfiguracion() {
        this.rsTablaProductos = new RSTableMetro();
        this.rsTablaProductos.setAltoHead(60);
        this.rsTablaProductos.setRowHeight(40);
        this.rsTablaProductos.setGrosorBordeFilas(0);
        this.rsTablaProductos.setColorBackgoundHead(Colores.C_COLOR_GRIS4.getColor());
        this.rsTablaProductos.setColorFilasBackgound1(Colores.C_COLOR_GRIS1.getColor());
        this.rsTablaProductos.setColorFilasBackgound2(Colores.C_COLOR_GRIS2.getColor());
        this.rsTablaProductos.setColorFilasForeground1(Colores.C_COLOR_NEGRO.getColor());
        this.rsTablaProductos.setColorFilasForeground2(Colores.C_COLOR_NEGRO.getColor());
        this.rsTablaProductos.setColorSelBackgound(Colores.C_COLOR_GRIS3.getColor());
        this.rsTablaProductos.setColorBordeFilas(Colores.C_COLOR_GRIS1.getColor());
        this.rsTablaProductos.setFuenteFilas(Fuentes.FUENTE_TEXTOS.getFont());
        this.rsTablaProductos.setFuenteHead(Fuentes.FUENTE_TEXTOS.getFont());
        this.rsTablaProductos.setPreferredScrollableViewportSize(new Dimension(250, 100));
        JScrollPane scrollPane = new JScrollPane(this.rsTablaProductos);
        scrollPane.setBounds(10, 250, 980, 250);
        CargarDatos.rellenarTabla(this.rsTablaProductos,"Producto");
        this.rsTablaProductos.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                seleccionProducto();
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }
        });
        this.kgPantalla.add(scrollPane);
    }


    public KGradientPanel getkgPantalla() {
        return this.kgPantalla;
    }
}