package Ventanas.Pantallas;

import Recursos.Colores;
import Recursos.Fuentes;
import Recursos.Imagenes;
import SQL.CargarDatos;
import keeptoo.KGradientPanel;
import rojeru_san.RSButtonRiple;
import rojerusan.RSTableMetro;

import javax.swing.*;
import javax.swing.border.MatteBorder;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * @author Daniel Ochoa
 * Ochoa Fecha: 08/11/2021
 * Modificacion: cambie el metodo buscarTabla
 */
public class PantCliEliminar {
    private RSTableMetro rsTablaClientes;
    private KGradientPanel kgPantalla;
    private JTextField txtRfc;
    private JLabel jlIdCliente;
    private RSButtonRiple btnEliminarCliente;

    public PantCliEliminar() {
        this.panelConfiguracion();
        this.labelsConfiguracion();
        this.textosConfiguracion();
        this.tablaConfiguracion();
        this.botonesConfiguracion();
    }

    private void panelConfiguracion() {
        this.kgPantalla = new KGradientPanel();
        this.kgPantalla = new KGradientPanel();
        this.kgPantalla.setSize(1090, 550);
        this.kgPantalla.setBackground(Colores.C_COLOR_TRANSPARENTE.getColor());
        this.kgPantalla.setkEndColor(Colores.C_COLOR_BLANCO.getColor());
        this.kgPantalla.setkStartColor(Colores.C_COLOR_BLANCO.getColor());
        this.kgPantalla.setkBorderRadius(40);
        this.kgPantalla.setLayout(null);
    }

    private void textosConfiguracion() {
        this.txtRfc = new JTextField();
        this.txtRfc.setBounds(480, 100, 200, 30);
        this.txtRfc.setBackground(Colores.C_COLOR_GRIS1.getColor());
        this.txtRfc.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
        this.txtRfc.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.txtRfc.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyPressed(KeyEvent e) {
            }

            @Override
            public void keyReleased(KeyEvent e) {
                CargarDatos.buscarTabla(rsTablaClientes, txtRfc.getText(), "Cliente", "Cliente");
                System.out.println("d");
            }
        });
        this.kgPantalla.add(this.txtRfc);
    }

    private void labelsConfiguracion() {
        JLabel labelTitutlo = new JLabel("Eliminar Cliente");
        labelTitutlo.setFont(Fuentes.FUENTE_TITULOS.getFont());
        labelTitutlo.setBounds(350, 20, 350, 30);
        this.kgPantalla.add(labelTitutlo);

        JLabel jlImgTitulo = new JLabel(Imagenes.I_IMAGEN_CLIENTE.getImagen());
        jlImgTitulo.setFont(Fuentes.FUENTE_TITULOS.getFont());
        jlImgTitulo.setBounds(280, 10, 64, 64);
        this.kgPantalla.add(jlImgTitulo);

        JLabel jlIDCliente = new JLabel("Buscar ID Cliente: ");
        jlIDCliente.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlIDCliente.setBounds(300, 100, 180, 30);
        this.kgPantalla.add(jlIDCliente);

        this.jlIdCliente = new JLabel("ID seleccionado: ");
        this.jlIdCliente.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        this.jlIdCliente.setBounds(300, 180, 500, 70);
        this.kgPantalla.add(this.jlIdCliente);
    }

    private void botonesConfiguracion() {
        /*----------------------------------------*/
        this.btnEliminarCliente = new RSButtonRiple();
        this.btnEliminarCliente.setBounds(700, 100, 120, 50);
        this.btnEliminarCliente.setBorderPainted(false);
        this.btnEliminarCliente.setFocusPainted(false);
        this.btnEliminarCliente.setBackground(Colores.C_COLOR_GRIS3.getColor());
        this.btnEliminarCliente.setColorHover(Colores.C_COLOR_GRIS2.getColor());
        this.btnEliminarCliente.setColorText(Colores.C_COLOR_NEGRO.getColor());
        this.btnEliminarCliente.setColorTextHover(Colores.C_COLOR_GRIS4.getColor());
        this.btnEliminarCliente.setText("Eliminar");
        this.btnEliminarCliente.setActionCommand("btnEliTrabajadores");
        this.btnEliminarCliente.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (seleccionTrabajador()) {
                    System.out.println("Elimino al cliente");
                }
            }
        });
        this.kgPantalla.add(this.btnEliminarCliente);
    }

    private boolean seleccionTrabajador() {
        int renglon = this.rsTablaClientes.getSelectedRow();
        if (renglon != -1) {
            String rfc = this.rsTablaClientes.getValueAt(renglon, 0) + "";
            this.jlIdCliente.setText("ID seleccionado: " + rfc);
        } else {
            System.out.println("No selecciono cliente");
            return false;
        }
        return true;
    }

    private void tablaConfiguracion() {
        this.rsTablaClientes = new RSTableMetro();
        this.rsTablaClientes.setAltoHead(60);
        this.rsTablaClientes.setRowHeight(40);
        this.rsTablaClientes.setGrosorBordeFilas(0);
        this.rsTablaClientes.setColorBackgoundHead(Colores.C_COLOR_GRIS4.getColor());
        this.rsTablaClientes.setColorFilasBackgound1(Colores.C_COLOR_GRIS1.getColor());
        this.rsTablaClientes.setColorFilasBackgound2(Colores.C_COLOR_GRIS2.getColor());
        this.rsTablaClientes.setColorFilasForeground1(Colores.C_COLOR_NEGRO.getColor());
        this.rsTablaClientes.setColorFilasForeground2(Colores.C_COLOR_NEGRO.getColor());
        this.rsTablaClientes.setColorSelBackgound(Colores.C_COLOR_GRIS3.getColor());
        this.rsTablaClientes.setColorBordeFilas(Colores.C_COLOR_GRIS1.getColor());
        this.rsTablaClientes.setFuenteFilas(Fuentes.FUENTE_TEXTOS.getFont());
        this.rsTablaClientes.setFuenteHead(Fuentes.FUENTE_TEXTOS.getFont());
        this.rsTablaClientes.setPreferredScrollableViewportSize(new Dimension(250, 100));
        JScrollPane scrollPane = new JScrollPane(this.rsTablaClientes);
        scrollPane.setBounds(10, 250, 980, 250);
        CargarDatos.rellenarTabla(this.rsTablaClientes, "Cliente");
        this.rsTablaClientes.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                seleccionTrabajador();
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }
        });
        this.kgPantalla.add(scrollPane);
    }


    public KGradientPanel getkgPantalla() {
        return this.kgPantalla;
    }
}
