package Ventanas.Pantallas;

import Recursos.Colores;
import Recursos.Fuentes;
import Recursos.Imagenes;
import SQL.Actualizar;
import SQL.ActualizarCargarDatos;
import SQL.CargarDatos;
import keeptoo.KGradientPanel;
import rojeru_san.RSButtonRiple;
import rojerusan.RSComboMetro;

import javax.swing.*;
import javax.swing.border.MatteBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * @author Daniel Ochoa
 * Ochoa Fecha: 07/11/2021
 * Modificaciones: Implmente el codigo para que con el evento KeyRelesead se aplicara
 * la busqueda
 */
public class PantProdActualizaciones {
    private KGradientPanel kgPantalla;
    private JTextField txtNombre, txtAuxiliar;
    private RSButtonRiple btnCrearUsuario;
    private JSlider slPrecio;
    private RSComboMetro cbCargos, rscmProducto, rscmProveedor;
    private JSpinner jsCambiarValor;
    private String sIdProveedorAnterior;
    private String sIdProductoAtributo;

    public PantProdActualizaciones() {
        this.panelConfiguracion();
        this.labelsConfiguracion();
        this.textosConfiguracion();
        this.botonesConfiguracion();
        this.comboBoxConfiguracion();
        this.sliderConfiguracion();
        this.spinerConfiguracion();
    }

    private void panelConfiguracion() {
        this.kgPantalla = new KGradientPanel();
        this.kgPantalla = new KGradientPanel();
        this.kgPantalla.setSize(1090, 550);
        this.kgPantalla.setBackground(Colores.C_COLOR_TRANSPARENTE.getColor());
        this.kgPantalla.setkEndColor(Colores.C_COLOR_BLANCO.getColor());
        this.kgPantalla.setkStartColor(Colores.C_COLOR_BLANCO.getColor());
        this.kgPantalla.setkBorderRadius(40);
        this.kgPantalla.setLayout(null);
    }

    private void textosConfiguracion() {
/*
        this.txtIdProducto = new JTextField();
        this.txtIdProducto.setBounds(120, 150, 200, 30);
        this.txtIdProducto.setBackground(Colores.C_COLOR_GRIS1.getColor());
        this.txtIdProducto.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
        this.txtIdProducto.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.txtIdProducto.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent keyEvent) {

            }

            @Override
            public void keyPressed(KeyEvent keyEvent) {

            }

            @Override
            public void keyReleased(KeyEvent keyEvent) {
                JComponent[] jtMisEntradas = {txtNombre, cbCargos, slPrecio, jsCantidad, jsCambiarValor, jsCantidadProdcutos, txtProveedor};
                String sIdCliente = txtIdProducto.getText();
                int[] iCampos = {2, 4, 3, 5, 3, 5, 6};
                ActualizarCargarDatos.cargarDatos(sIdCliente, 3, jtMisEntradas, iCampos);
                sIdProveedorAnterior = txtProveedor.getText();
                jsCambiarValor.getEditor().getComponent(0).setEnabled(false);
                jsCantidadProdcutos.getEditor().getComponent(0).setEnabled(false);
            }
        });
        this.kgPantalla.add(this.txtIdProducto);
*/

        this.txtNombre = new JTextField();
        this.txtNombre.setBounds(420, 150, 200, 30);
        this.txtNombre.setBackground(Colores.C_COLOR_GRIS1.getColor());
        this.txtNombre.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
        this.txtNombre.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.txtNombre.setEnabled(false);
        this.kgPantalla.add(this.txtNombre);

        this.txtAuxiliar = new JTextField();
        this.txtAuxiliar.setBounds(0, -200, 100, 30);
        this.txtAuxiliar.setVisible(false);
        kgPantalla.add(this.txtAuxiliar);

/*        this.txtProveedor = new JTextField();
        this.txtProveedor.setBounds(130, 230, 200, 30);
        this.txtProveedor.setBackground(Colores.C_COLOR_GRIS1.getColor());
        this.txtProveedor.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
        this.txtProveedor.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.txtProveedor.setEnabled(false);
        this.kgPantalla.add(this.txtProveedor);*/


    }

    private void labelsConfiguracion() {
        JLabel labelTitutlo = new JLabel("Actualizar Producto");
        labelTitutlo.setFont(Fuentes.FUENTE_TITULOS.getFont());
        labelTitutlo.setBounds(350, 20, 350, 30);
        this.kgPantalla.add(labelTitutlo);

        JLabel jlImgTitulo = new JLabel(Imagenes.I_IMAGEN_PRODUCTO.getImagen());
        jlImgTitulo.setFont(Fuentes.FUENTE_TITULOS.getFont());
        jlImgTitulo.setBounds(280, 10, 64, 64);
        this.kgPantalla.add(jlImgTitulo);

        JLabel jlIDProducto = new JLabel("Producto: ");
        jlIDProducto.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlIDProducto.setBounds(0, 100, 150, 30);
        this.kgPantalla.add(jlIDProducto);

        JLabel jlNombreCliente = new JLabel("Nombre: ");
        jlNombreCliente.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlNombreCliente.setBounds(330, 150, 100, 30);
        this.kgPantalla.add(jlNombreCliente);

        JLabel jlDepartamento = new JLabel("Departamento: ");
        jlDepartamento.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlDepartamento.setBounds(650, 150, 160, 30);
        this.kgPantalla.add(jlDepartamento);


        JLabel jlPrecio = new JLabel("Precio: ");
        jlPrecio.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlPrecio.setBounds(0, 300, 130, 30);
        this.kgPantalla.add(jlPrecio);

        JLabel jlIdProveedor = new JLabel("Proveedor: ");
        jlIdProveedor.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        jlIdProveedor.setBounds(0, 230, 130, 30);
        this.kgPantalla.add(jlIdProveedor);

    }

    private void botonesConfiguracion() {
        /*----------------------------------------*/
        this.btnCrearUsuario = new RSButtonRiple();
        this.btnCrearUsuario.setBounds(800, 400, 120, 50);
        this.btnCrearUsuario.setBorderPainted(false);
        this.btnCrearUsuario.setFocusPainted(false);
        this.btnCrearUsuario.setBackground(Colores.C_COLOR_GRIS3.getColor());
        this.btnCrearUsuario.setColorHover(Colores.C_COLOR_GRIS2.getColor());
        this.btnCrearUsuario.setColorText(Colores.C_COLOR_NEGRO.getColor());
        this.btnCrearUsuario.setColorTextHover(Colores.C_COLOR_GRIS4.getColor());
        this.btnCrearUsuario.setText("Agregar");
        this.btnCrearUsuario.setActionCommand("btnClientes");
        this.btnCrearUsuario.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (txtNombre.isEnabled()) {

                    String sIdProveedorMarcaNombre = rscmProveedor.getSelectedItem() == null ? "0" : rscmProveedor.getSelectedItem().toString();
                    int iPatronId = sIdProveedorMarcaNombre.indexOf("ID: ");

                    String sId = (sIdProveedorMarcaNombre.contains("ID: ")) ? (sIdProveedorMarcaNombre.substring(iPatronId + 4)) : ("0");


                    String[] sParametros = {
                            txtNombre.getText().trim(),
                            cbCargos.getSelectedItem().toString(),
                            sId,
                            jsCambiarValor.getValue().toString(),
                            sIdProductoAtributo,
                            sIdProveedorAnterior
                    };
                    boolean bInsercionCliente = Actualizar.actualizarProducto(sParametros);
                    if (bInsercionCliente) {
                        JOptionPane.showMessageDialog(null, "Producto modificado correctamente");
                        rscmProducto.setModel(new DefaultComboBoxModel());
                        txtNombre.setText("");
                        cbCargos.setEnabled(false);
                        txtNombre.setEnabled(false);
                        rscmProveedor.setEnabled(false);
                        jsCambiarValor.setEnabled(false);
                        slPrecio.setEnabled(false);
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "No a selecionado un producto o no existe");
                }
            }
        });
        this.kgPantalla.add(this.btnCrearUsuario);
    }

    private void comboBoxConfiguracion() {
        this.cbCargos = new RSComboMetro();
        this.cbCargos.addItem("Baños");
        this.cbCargos.addItem("Decoracion");
        this.cbCargos.addItem("Electrico");
        this.cbCargos.addItem("Ferreteria");
        this.cbCargos.addItem("Herramientas");
        this.cbCargos.addItem("Iluminacion");
        this.cbCargos.addItem("Jardin");
        this.cbCargos.addItem("Limpieza");
        this.cbCargos.addItem("Materiales de construcion");
        this.cbCargos.addItem("Pinturas");
        this.cbCargos.addItem("Pisos");
        this.cbCargos.addItem("Plomeria");
        this.cbCargos.addItem("Seguridad");
        this.cbCargos.addItem("Puertas y ventanas");
        this.cbCargos.addItem("Linea blanca y cocinas");
        this.cbCargos.addItem("Ventilacion y calefacion");
        this.cbCargos.addItem("Organizadores y closets");
        this.cbCargos.setSelectedIndex(2);
        this.cbCargos.setColorFondo(Colores.C_COLOR_GRIS2.getColor());
        this.cbCargos.setColorBorde(Colores.C_COLOR_GRIS3.getColor());
        this.cbCargos.setColorArrow(Colores.C_COLOR_GRIS3.getColor());
        this.cbCargos.setForeground(Colores.C_COLOR_NEGRO.getColor());
        this.cbCargos.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.cbCargos.setBounds(800, 150, 220, 30);
        this.cbCargos.setEnabled(false);
        this.kgPantalla.add(cbCargos);


        this.rscmProveedor = new RSComboMetro();
        this.rscmProveedor.setColorFondo(Colores.C_COLOR_GRIS4.getColor());
        this.rscmProveedor.setColorBorde(Colores.C_COLOR_GRIS3.getColor());
        this.rscmProveedor.setColorArrow(Colores.C_COLOR_GRIS3.getColor());
        this.rscmProveedor.setForeground(Colores.C_COLOR_NEGRO.getColor());
        this.rscmProveedor.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.rscmProveedor.setBounds(110, 230, 350, 30);
        this.rscmProveedor.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
        this.rscmProveedor.setEditable(true);
        this.rscmProveedor.setEnabled(false);
        CargarDatos.comboBoxPonerDatos(this.rscmProveedor, "nombreProveedor");
        this.kgPantalla.add(this.rscmProveedor);


        this.rscmProducto = new RSComboMetro();
        this.rscmProducto.setColorFondo(Colores.C_COLOR_GRIS4.getColor());
        this.rscmProducto.setColorBorde(Colores.C_COLOR_GRIS3.getColor());
        this.rscmProducto.setColorArrow(Colores.C_COLOR_GRIS3.getColor());
        this.rscmProducto.setForeground(Colores.C_COLOR_NEGRO.getColor());
        this.rscmProducto.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.rscmProducto.setBounds(0, 150, 300, 30);
        this.rscmProducto.setBorder(new MatteBorder(0, 0, 1, 0, Colores.C_COLOR_GRIS4.getColor()));
        this.rscmProducto.setEditable(true);
        CargarDatos.comboBoxPonerDatos(this.rscmProducto, "nombreProductoEmpresa");
        this.rscmProducto.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String sNombreProductoEmpresa = rscmProducto.getSelectedItem() == null ? " " : rscmProducto.getSelectedItem().toString();
                int iPosicionProductoEmpresa = sNombreProductoEmpresa.lastIndexOf("Marca: ");
                int iPosicionProductoNombre = sNombreProductoEmpresa.lastIndexOf(", ");

                String sEmpresaProducto = sNombreProductoEmpresa.contains("Marca: ") ? (sNombreProductoEmpresa.substring(iPosicionProductoEmpresa + 7)) : (" ");
                String sProductoNombre = sNombreProductoEmpresa.contains(", ") ? (sNombreProductoEmpresa.substring(0, iPosicionProductoNombre)) : (" ");

                String sIdProducto = CargarDatos.idProducto(sProductoNombre, sEmpresaProducto);
                sIdProductoAtributo = sIdProducto;
                System.out.println("#" + sEmpresaProducto + "#" + sProductoNombre + "#");


                JComponent[] jtMisEntradas = {txtNombre, cbCargos, slPrecio, jsCambiarValor, txtAuxiliar};
                int[] iCampos = {2, 4, 3, 3, 6};
                ActualizarCargarDatos.cargarDatos(sIdProducto, 3, jtMisEntradas, iCampos);
                txtAuxiliar.setVisible(false);
                sIdProveedorAnterior = " ";//txtProveedor.getText();
                jsCambiarValor.getEditor().getComponent(0).setEnabled(false);
                if (txtNombre.isEnabled()) {
                    rscmProveedor.setEnabled(true);
                    rscmProveedor.setModel(CargarDatos.cargarComboBox("", "nombreProveedor"));
                    rscmProveedor.setSelectedIndex(posicionIdProvedor((DefaultComboBoxModel) rscmProveedor.getModel(), txtAuxiliar.getText()));
                } else {
                    rscmProveedor.setEnabled(false);
                    rscmProveedor.setModel(new DefaultComboBoxModel<String>());
                }
            }
        });
        this.kgPantalla.add(this.rscmProducto);
    }

    private void sliderConfiguracion() {
        this.slPrecio = new JSlider(1, 15000, 7500);
        this.slPrecio.setOrientation(SwingConstants.HORIZONTAL);
        this.slPrecio.setMajorTickSpacing(1000);
        this.slPrecio.setMinorTickSpacing(2);
        this.slPrecio.setPaintLabels(true);
        this.slPrecio.setPaintTicks(true);
        this.slPrecio.setBounds(60, 300, 900, 50);
        this.slPrecio.setEnabled(false);
        this.kgPantalla.add(slPrecio);
        this.slPrecio.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                cambiarValorSpinner1();
            }
        });
    }

    private void spinerConfiguracion() {
        this.jsCambiarValor = new JSpinner();
        this.jsCambiarValor.setValue(this.slPrecio.getValue());
        this.jsCambiarValor.setBounds(0, 350, 100, 30);
        this.jsCambiarValor.setBackground(Colores.C_COLOR_GRIS3.getColor());
        this.jsCambiarValor.setFont(Fuentes.FUENTES_ETIQUETAS.getFont());
        this.jsCambiarValor.setEnabled(false);
        this.kgPantalla.add(this.jsCambiarValor);
        this.jsCambiarValor.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                cambiarValorSlider1();
                int valor = (int) jsCambiarValor.getValue();
                if (valor <= 1) {
                    jsCambiarValor.setValue(1);
                } else if (valor >= 15000) {
                    jsCambiarValor.setValue(15000);
                }
            }

        });
    }

    private void cambiarValorSpinner1() {
        this.jsCambiarValor.setValue(this.slPrecio.getValue());
    }

    private void cambiarValorSlider1() {
        this.slPrecio.setValue((int) this.jsCambiarValor.getValue());
    }

    public KGradientPanel getkgPantalla() {
        return this.kgPantalla;
    }

    private int posicionIdProvedor(DefaultComboBoxModel dtm, String sValor) {
        for (int i = 0; i < dtm.getSize(); i++) {
            if (dtm.getElementAt(i).toString().contains("ID: ".concat(sValor))) {
                return i;
            }
        }
        return 0;
    }
}
