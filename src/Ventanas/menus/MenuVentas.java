package Ventanas.menus;

import Animacion.Animacion;
import Recursos.CambiarPanel;
import Recursos.Colores;
import SQL.Login;
import Ventanas.Pantallas.*;
import Ventanas.VentanaPrincipal;
import keeptoo.KGradientPanel;
import rojeru_san.RSButtonRiple;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author Daniel Ochoa
 * <p>
 * Ochoa Fecha: 05/11/2021
 * Modificaciones: Codigo para delimitar los roles
 */
public class MenuVentas implements ActionListener {

    private JPanel jpPanel;
    private KGradientPanel kgActual;
    private RSButtonRiple btnAgregar, btnEliminar, btnMostrar, btnFactura;
    private PantVentAgregar pvaNuevaPantalla;
    private PantVentVer pvvNuevaPantalla;
    private PantVentEliminar pveNuevaPantalla;
    private PantFacturaVentVer pfvvNuevaPantalla;
    private CambiarPanel cbCambiar;
    private static JPanel jpMenu;
    private Login lTrabajador;

    public MenuVentas(KGradientPanel kgActual, Login lTrabajador) {
        this.kgActual = kgActual;
        this.lTrabajador = lTrabajador;
        this.panelConfiguracion();
        this.botonesConfiguracion();
        this.activarRoles();
    }

    public static void setJpMenu(JPanel jpMenu) {
        MenuVentas.jpMenu = jpMenu;
    }

    public void panelConfiguracion() {
        this.jpPanel = new JPanel();
        this.jpPanel.setSize(120, 220);
        this.jpPanel.setBackground(Colores.C_COLOR_GRIS2.getColor());
        this.jpPanel.setLayout(null);
    }

    private void botonesConfiguracion() {
        this.btnAgregar = new RSButtonRiple();
        this.btnAgregar.setBounds(0, 10, 120, 50);
        this.btnAgregar.setBorderPainted(false);
        this.btnAgregar.setFocusPainted(false);
        this.btnAgregar.setBackground(Colores.C_COLOR_GRIS2.getColor());
        this.btnAgregar.setColorHover(Colores.C_COLOR_GRIS3.getColor());
        this.btnAgregar.setColorText(Colores.C_COLOR_NEGRO.getColor());
        this.btnAgregar.setColorTextHover(Colores.C_COLOR_GRIS2.getColor());
        this.btnAgregar.setText("Agregar 6");
        this.btnAgregar.setActionCommand("Agregar");
        this.btnAgregar.addActionListener(this);
        this.btnAgregar.setEnabled(false);
        this.jpPanel.add(this.btnAgregar);


        /*-------------------------------------------------------*/

        this.btnEliminar = new RSButtonRiple();
        this.btnEliminar.setBounds(0, 60, 120, 50);
        this.btnEliminar.setBorderPainted(false);
        this.btnEliminar.setFocusPainted(false);
        this.btnEliminar.setBackground(Colores.C_COLOR_GRIS2.getColor());
        this.btnEliminar.setColorHover(Colores.C_COLOR_GRIS3.getColor());
        this.btnEliminar.setColorText(Colores.C_COLOR_NEGRO.getColor());
        this.btnEliminar.setColorTextHover(Colores.C_COLOR_GRIS2.getColor());
        this.btnEliminar.setText("Eliminar");
        this.btnEliminar.setActionCommand("Eliminar");
        this.btnEliminar.addActionListener(this);
        this.btnEliminar.setEnabled(false);
        this.jpPanel.add(this.btnEliminar);

        /*-------------------------------------------------------*/

        this.btnMostrar = new RSButtonRiple();
        this.btnMostrar.setBounds(0, 110, 120, 50);
        this.btnMostrar.setBorderPainted(false);
        this.btnMostrar.setFocusPainted(false);
        this.btnMostrar.setBackground(Colores.C_COLOR_GRIS2.getColor());
        this.btnMostrar.setColorHover(Colores.C_COLOR_GRIS3.getColor());
        this.btnMostrar.setColorText(Colores.C_COLOR_NEGRO.getColor());
        this.btnMostrar.setColorTextHover(Colores.C_COLOR_GRIS2.getColor());
        this.btnMostrar.setText("Ver registros");
        this.btnMostrar.setActionCommand("Mostrar");
        this.btnMostrar.addActionListener(this);
        this.jpPanel.add(this.btnMostrar);

        /*-------------------------------------------------------*/

        this.btnFactura = new RSButtonRiple();
        this.btnFactura.setBounds(0, 160, 120, 50);
        this.btnFactura.setBorderPainted(false);
        this.btnFactura.setFocusPainted(false);
        this.btnFactura.setBackground(Colores.C_COLOR_GRIS2.getColor());
        this.btnFactura.setColorHover(Colores.C_COLOR_GRIS3.getColor());
        this.btnFactura.setColorText(Colores.C_COLOR_NEGRO.getColor());
        this.btnFactura.setColorTextHover(Colores.C_COLOR_GRIS2.getColor());
        this.btnFactura.setText("Ver Facturas");
        this.btnFactura.setActionCommand("btnFactura");
        this.btnFactura.addActionListener(this);
        this.btnFactura.setEnabled(false);
        this.jpPanel.add(this.btnFactura);
    }

    public JPanel getPanel() {
        return this.jpPanel;
    }

    private void btnAgregarActionPerformed() {
        this.pvaNuevaPantalla = new PantVentAgregar(this.lTrabajador);
        this.cbCambiar = new CambiarPanel(this.kgActual, this.pvaNuevaPantalla.getkgPantalla(), jpMenu);
        this.cbCambiar.actualizarPanel();
        VentanaPrincipal.hmMenu.put("btnVentas", false);
        Animacion.subir(0, -250, 1, 5, VentanaPrincipal.jpMenu);
    }

    private void btnMostrarActionPerformed() {
        this.pvvNuevaPantalla = new PantVentVer();
        this.cbCambiar = new CambiarPanel(this.kgActual, this.pvvNuevaPantalla.getkgPantalla(), jpMenu);
        this.cbCambiar.actualizarPanel();
        VentanaPrincipal.hmMenu.put("btnVentas", false);
        Animacion.subir(0, -250, 1, 5, VentanaPrincipal.jpMenu);
    }

    private void btnEliminarActionPerformed() {
        this.pveNuevaPantalla = new PantVentEliminar();
        this.cbCambiar = new CambiarPanel(this.kgActual, this.pveNuevaPantalla.getkgPantalla(), jpMenu);
        this.cbCambiar.actualizarPanel();
        VentanaPrincipal.hmMenu.put("btnVentas", false);
        Animacion.subir(0, -250, 1, 5, VentanaPrincipal.jpMenu);
    }

    private void btnFacturaActionPerformed() {
        this.pfvvNuevaPantalla = new PantFacturaVentVer();
        this.cbCambiar = new CambiarPanel(this.kgActual, this.pfvvNuevaPantalla.getkgPantalla(), jpMenu);
        this.cbCambiar.actualizarPanel();
        VentanaPrincipal.hmMenu.put("btnVentas", false);
        Animacion.subir(0, -250, 1, 5, VentanaPrincipal.jpMenu);
    }

    private void activarRoles() {
        String sRolTrabajador = this.lTrabajador.getsCargo();
        switch (sRolTrabajador) {
            case "Cajero":
            case "Gerente":
                this.btnEliminar.setEnabled(true);
                this.btnAgregar.setEnabled(true);
                this.btnFactura.setEnabled(true);
                break;
        }
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        String sAction = actionEvent.getActionCommand();
        if (sAction.equals(this.btnAgregar.getActionCommand())) {
            this.btnAgregarActionPerformed();
        } else if (sAction.equals(this.btnMostrar.getActionCommand())) {
            this.btnMostrarActionPerformed();
        } else if (sAction.equals(this.btnEliminar.getActionCommand())) {
            this.btnEliminarActionPerformed();
        } else if (sAction.equals(this.btnFactura.getActionCommand())) {
            this.btnFacturaActionPerformed();
        }
    }
}