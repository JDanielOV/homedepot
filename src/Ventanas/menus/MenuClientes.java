/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ventanas.menus;

import Animacion.Animacion;
import Recursos.CambiarPanel;
import Recursos.Colores;
import SQL.CargarDatos;
import SQL.Login;
import Ventanas.Pantallas.PantCliActualizaciones;
import Ventanas.Pantallas.PantCliAgregar;
import Ventanas.Pantallas.PantCliEliminar;
import Ventanas.Pantallas.PantCliVer;
import Ventanas.VentanaPrincipal;
import keeptoo.KGradientPanel;
import rojeru_san.RSButtonRiple;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author Daniel Ochoa
 * Ochoa Fecha: 07/11/2021
 * Modificaciones: Codigo para delimitar los roles
 */
public class MenuClientes implements ActionListener {

    private JPanel jpPanel;
    private KGradientPanel kgActual;
    private RSButtonRiple btnAgregar ,btnActualizar, btnMostrar;
    private PantCliAgregar pcaNuevaPantalla;
    private PantCliVer pcvNuevaPantalla;
    private PantCliActualizaciones pcacNuevaPantalla;
    private PantCliEliminar pceNuevaPantalla;
    private CambiarPanel cbCambiar;
    private static JPanel jpMenu;
    private Login lTrabajador;

    public MenuClientes(KGradientPanel kgActual, Login lTrabajador) {
        this.kgActual = kgActual;
        this.lTrabajador = lTrabajador;
        this.panelConfiguracion();
        this.botonesConfiguracion();
        this.activarRoles();
    }

    public static void setJpMenu(JPanel jpMenu) {
        MenuClientes.jpMenu = jpMenu;
    }

    public void panelConfiguracion() {
        this.jpPanel = new JPanel();
        this.jpPanel.setSize(120, 170);
        this.jpPanel.setBackground(Colores.C_COLOR_GRIS2.getColor());
        this.jpPanel.setLayout(null);
    }

    private void botonesConfiguracion() {
        this.btnAgregar = new RSButtonRiple();
        this.btnAgregar.setBounds(0, 10, 120, 50);
        this.btnAgregar.setBorderPainted(false);
        this.btnAgregar.setFocusPainted(false);
        this.btnAgregar.setBackground(Colores.C_COLOR_GRIS2.getColor());
        this.btnAgregar.setColorHover(Colores.C_COLOR_GRIS3.getColor());
        this.btnAgregar.setColorText(Colores.C_COLOR_NEGRO.getColor());
        this.btnAgregar.setColorTextHover(Colores.C_COLOR_GRIS2.getColor());
        this.btnAgregar.setText("Agregar");
        this.btnAgregar.setActionCommand("Agregar");
        this.btnAgregar.addActionListener(this);
        this.btnAgregar.setEnabled(false);
        this.jpPanel.add(this.btnAgregar);



        /*-------------------------------------------------------*/

        this.btnActualizar = new RSButtonRiple();
        this.btnActualizar.setBounds(0, 60, 120, 50);
        this.btnActualizar.setBorderPainted(false);
        this.btnActualizar.setFocusPainted(false);
        this.btnActualizar.setBackground(Colores.C_COLOR_GRIS2.getColor());
        this.btnActualizar.setColorHover(Colores.C_COLOR_GRIS3.getColor());
        this.btnActualizar.setColorText(Colores.C_COLOR_NEGRO.getColor());
        this.btnActualizar.setColorTextHover(Colores.C_COLOR_GRIS2.getColor());
        this.btnActualizar.setText("Actualizar");
        this.btnActualizar.setActionCommand("Actualizar");
        this.btnActualizar.addActionListener(this);
        this.btnActualizar.setEnabled(false);
        this.jpPanel.add(this.btnActualizar);

        /*-------------------------------------------------------*/

        this.btnMostrar = new RSButtonRiple();
        this.btnMostrar.setBounds(0, 110, 120, 50);
        this.btnMostrar.setBorderPainted(false);
        this.btnMostrar.setFocusPainted(false);
        this.btnMostrar.setBackground(Colores.C_COLOR_GRIS2.getColor());
        this.btnMostrar.setColorHover(Colores.C_COLOR_GRIS3.getColor());
        this.btnMostrar.setColorText(Colores.C_COLOR_NEGRO.getColor());
        this.btnMostrar.setColorTextHover(Colores.C_COLOR_GRIS2.getColor());
        this.btnMostrar.setText("Ver registros");
        this.btnMostrar.setActionCommand("btnMostrar");
        this.btnMostrar.addActionListener(this);
        this.btnMostrar.setEnabled(false);
        this.jpPanel.add(this.btnMostrar);

    }

    public JPanel getPanel() {
        return this.jpPanel;
    }

    private void btnAgregarActionPerformed() {
        this.pcaNuevaPantalla = new PantCliAgregar();
        this.cbCambiar = new CambiarPanel(this.kgActual, this.pcaNuevaPantalla.getkgPantalla(), jpMenu);
        this.cbCambiar.actualizarPanel();
        VentanaPrincipal.hmMenu.put("btnClientes", false);
        Animacion.subir(0, -250, 1, 5, VentanaPrincipal.jpMenu);
    }

    private void btnActualizarrActionPerformed() {
        this.pcacNuevaPantalla = new PantCliActualizaciones();
        this.cbCambiar = new CambiarPanel(this.kgActual, this.pcacNuevaPantalla.getkgPantalla(), jpMenu);
        this.cbCambiar.actualizarPanel();
        VentanaPrincipal.hmMenu.put("btnClientes", false);
        Animacion.subir(0, -250, 1, 5, VentanaPrincipal.jpMenu);
    }

    private void btnMostrarActionPerformed() {
        this.pcvNuevaPantalla = new PantCliVer();
        this.cbCambiar = new CambiarPanel(this.kgActual, this.pcvNuevaPantalla.getkgPantalla(), jpMenu);
        this.cbCambiar.actualizarPanel();
        VentanaPrincipal.hmMenu.put("btnClientes", false);
        Animacion.subir(0, -250, 1, 5, VentanaPrincipal.jpMenu);
    }

    private void btnEliminarActionPerformed() {
        this.pceNuevaPantalla = new PantCliEliminar();
        this.cbCambiar = new CambiarPanel(this.kgActual, this.pceNuevaPantalla.getkgPantalla(), jpMenu);
        this.cbCambiar.actualizarPanel();
        VentanaPrincipal.hmMenu.put("btnClientes", false);
        Animacion.subir(0, -250, 1, 5, VentanaPrincipal.jpMenu);
    }

    private void activarRoles() {
        String sRolTrabajador = this.lTrabajador.getsCargo();
        switch (sRolTrabajador) {
            case "Atencion a clientes":
            case "Cajero":
            case "Gerente":
                this.btnAgregar.setEnabled(true);
                this.btnMostrar.setEnabled(true);
                this.btnActualizar.setEnabled(true);
                break;
            case "Oficinista":
            case "Almacenista":
                this.btnMostrar.setEnabled(true);
                break;
        }
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        String sAction = actionEvent.getActionCommand();
        if (sAction.equals(this.btnAgregar.getActionCommand())) {
            this.btnAgregarActionPerformed();
        } else if (sAction.equals(this.btnMostrar.getActionCommand())) {
            this.btnMostrarActionPerformed();
        } else if (sAction.equals(this.btnActualizar.getActionCommand())) {
            this.btnActualizarrActionPerformed();
  
        }
    }
}
