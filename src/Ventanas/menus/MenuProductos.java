package Ventanas.menus;

import Animacion.Animacion;
import Recursos.CambiarPanel;
import Recursos.Colores;
import SQL.Login;
import Ventanas.Pantallas.*;
import Ventanas.VentanaPrincipal;
import keeptoo.KGradientPanel;
import rojeru_san.RSButtonRiple;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author Daniel Ochoa
 * Ochoa Fecha: 03/11/2021
 * Modificaciones: Codigo para delimitar los roles
 */
public class MenuProductos implements ActionListener {
    private JPanel jpPanel;
    private KGradientPanel kgActual;
    private RSButtonRiple btnAgregar, btnEliminar, btnActualizar, btnMostrar, btnFactura;
    private PantProdAgregar pcaNuevaPantalla;
    private PantProdActualizaciones ppaNuevaPantalla;
    private PantProdEliminar ppeNuevaPantalla;
    private PantProdVer ppvNuevaPantalla;
    private PantProdProveedor pppNuevaPantalla;
    private CambiarPanel cbCambiar;
    private static JPanel jpMenu;
    private Login lTrabajador;

    public MenuProductos(KGradientPanel kgActual, Login lTrabajador) {
        this.kgActual = kgActual;
        this.lTrabajador = lTrabajador;
        this.panelConfiguracion();
        this.botonesConfiguracion();
        this.activarRoles();
    }

    public static void setJpMenu(JPanel jpMenu) {
        MenuProductos.jpMenu = jpMenu;
    }

    public void panelConfiguracion() {
        this.jpPanel = new JPanel();
        this.jpPanel.setSize(120, 270);
        this.jpPanel.setBackground(Colores.C_COLOR_GRIS2.getColor());
        this.jpPanel.setLayout(null);
    }

    private void botonesConfiguracion() {
        this.btnAgregar = new RSButtonRiple();
        this.btnAgregar.setBounds(0, 10, 120, 50);
        this.btnAgregar.setBorderPainted(false);
        this.btnAgregar.setFocusPainted(false);
        this.btnAgregar.setBackground(Colores.C_COLOR_GRIS2.getColor());
        this.btnAgregar.setColorHover(Colores.C_COLOR_GRIS3.getColor());
        this.btnAgregar.setColorText(Colores.C_COLOR_NEGRO.getColor());
        this.btnAgregar.setColorTextHover(Colores.C_COLOR_GRIS2.getColor());
        this.btnAgregar.setText("Agregar 4");
        this.btnAgregar.setActionCommand("Agregar");
        this.btnAgregar.addActionListener(this);
        this.btnAgregar.setEnabled(false);
        this.jpPanel.add(this.btnAgregar);



        /*-------------------------------------------------------*/

        this.btnActualizar = new RSButtonRiple();
        this.btnActualizar.setBounds(0, 60, 120, 50);
        this.btnActualizar.setBorderPainted(false);
        this.btnActualizar.setFocusPainted(false);
        this.btnActualizar.setBackground(Colores.C_COLOR_GRIS2.getColor());
        this.btnActualizar.setColorHover(Colores.C_COLOR_GRIS3.getColor());
        this.btnActualizar.setColorText(Colores.C_COLOR_NEGRO.getColor());
        this.btnActualizar.setColorTextHover(Colores.C_COLOR_GRIS2.getColor());
        this.btnActualizar.setText("Actualizar");
        this.btnActualizar.setActionCommand("Actualizar");
        this.btnActualizar.addActionListener(this);
        this.btnActualizar.setEnabled(false);
        this.jpPanel.add(this.btnActualizar);

        /*-------------------------------------------------------*/

        this.btnEliminar = new RSButtonRiple();
        this.btnEliminar.setBounds(0, 110, 120, 50);
        this.btnEliminar.setBorderPainted(false);
        this.btnEliminar.setFocusPainted(false);
        this.btnEliminar.setBackground(Colores.C_COLOR_GRIS2.getColor());
        this.btnEliminar.setColorHover(Colores.C_COLOR_GRIS3.getColor());
        this.btnEliminar.setColorText(Colores.C_COLOR_NEGRO.getColor());
        this.btnEliminar.setColorTextHover(Colores.C_COLOR_GRIS2.getColor());
        this.btnEliminar.setText("Eliminar");
        this.btnEliminar.setActionCommand("Eliminar");
        this.btnEliminar.addActionListener(this);
        this.btnEliminar.setEnabled(false);
        this.jpPanel.add(this.btnEliminar);

        /*-------------------------------------------------------*/

        this.btnMostrar = new RSButtonRiple();
        this.btnMostrar.setBounds(0, 160, 120, 50);
        this.btnMostrar.setBorderPainted(false);
        this.btnMostrar.setFocusPainted(false);
        this.btnMostrar.setBackground(Colores.C_COLOR_GRIS2.getColor());
        this.btnMostrar.setColorHover(Colores.C_COLOR_GRIS3.getColor());
        this.btnMostrar.setColorText(Colores.C_COLOR_NEGRO.getColor());
        this.btnMostrar.setColorTextHover(Colores.C_COLOR_GRIS2.getColor());
        this.btnMostrar.setText("Ver registros");
        this.btnMostrar.setActionCommand("btnMostrar");
        this.btnMostrar.addActionListener(this);
        this.jpPanel.add(this.btnMostrar);
        
        /*-------------------------------------------------------*/

        this.btnFactura = new RSButtonRiple();
        this.btnFactura.setBounds(0, 210, 120, 50);
        this.btnFactura.setBorderPainted(false);
        this.btnFactura.setFocusPainted(false);
        this.btnFactura.setBackground(Colores.C_COLOR_GRIS2.getColor());
        this.btnFactura.setColorHover(Colores.C_COLOR_GRIS3.getColor());
        this.btnFactura.setColorText(Colores.C_COLOR_NEGRO.getColor());
        this.btnFactura.setColorTextHover(Colores.C_COLOR_GRIS2.getColor());
        this.btnFactura.setText("Proveedores");
        this.btnFactura.setActionCommand("btnFactura");
        this.btnFactura.addActionListener(this);
        this.btnFactura.setEnabled(false);
        this.jpPanel.add(this.btnFactura);

    }

    public JPanel getPanel() {
        return this.jpPanel;
    }

    private void btnAgregarActionPerformed() {
        this.pcaNuevaPantalla = new PantProdAgregar();
        this.cbCambiar = new CambiarPanel(this.kgActual, this.pcaNuevaPantalla.getkgPantalla(), jpMenu);
        this.cbCambiar.actualizarPanel();
        VentanaPrincipal.hmMenu.put("btnProductos",false);
        Animacion.subir(0, -300, 1, 5, VentanaPrincipal.jpMenu);
    }

    private void btnActualizarrActionPerformed() {
        this.ppaNuevaPantalla = new PantProdActualizaciones();
        this.cbCambiar = new CambiarPanel(this.kgActual, this.ppaNuevaPantalla.getkgPantalla(), jpMenu);
        this.cbCambiar.actualizarPanel();
        VentanaPrincipal.hmMenu.put("btnProductos",false);
        Animacion.subir(0, -300, 1, 5, VentanaPrincipal.jpMenu);
    }

    private void btnEliminarActionPerformed() {
        this.ppeNuevaPantalla = new PantProdEliminar();
        this.cbCambiar = new CambiarPanel(this.kgActual, this.ppeNuevaPantalla.getkgPantalla(), jpMenu);
        this.cbCambiar.actualizarPanel();
        VentanaPrincipal.hmMenu.put("btnProductos",false);
        Animacion.subir(0, -300, 1, 5, VentanaPrincipal.jpMenu);
    }

    private void btnMostrarActionPerformed() {
        this.ppvNuevaPantalla = new PantProdVer();
        this.cbCambiar = new CambiarPanel(this.kgActual, this.ppvNuevaPantalla.getkgPantalla(), jpMenu);
        this.cbCambiar.actualizarPanel();
        VentanaPrincipal.hmMenu.put("btnProductos",false);
        Animacion.subir(0, -300, 1, 5, VentanaPrincipal.jpMenu);
    }
    
    private void btnFacturaActionPerformed() {
        this.pppNuevaPantalla = new PantProdProveedor();
        this.cbCambiar = new CambiarPanel(this.kgActual, this.pppNuevaPantalla.getkgPantalla(), jpMenu);
        this.cbCambiar.actualizarPanel();
        VentanaPrincipal.hmMenu.put("btnProveedor", false);
        Animacion.subir(0, -300, 1, 5, VentanaPrincipal.jpMenu);
    }

    private void activarRoles() {
        String sRolTrabajador = this.lTrabajador.getsCargo();
        switch (sRolTrabajador) {
            case "Almacenista":
            case "Gerente":
                this.btnEliminar.setEnabled(true);
                this.btnAgregar.setEnabled(true);
                this.btnActualizar.setEnabled(true);
                this.btnFactura.setEnabled(true);
                break;
        }
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        String sAction = actionEvent.getActionCommand();
        if (sAction.equals(this.btnAgregar.getActionCommand())) {
            this.btnAgregarActionPerformed();
        } else if (sAction.equals(this.btnMostrar.getActionCommand())) {
            this.btnMostrarActionPerformed();
        } else if (sAction.equals(this.btnActualizar.getActionCommand())) {
            this.btnActualizarrActionPerformed();
        } else if (sAction.equals(this.btnEliminar.getActionCommand())) {
            this.btnEliminarActionPerformed();
        } else if (sAction.equals(this.btnMostrar.getActionCommand())) {
            this.btnMostrarActionPerformed();
        } else if (sAction.equals(this.btnFactura.getActionCommand())) {
            this.btnFacturaActionPerformed();
        }
    }
}
