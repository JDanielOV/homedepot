/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ventanas.menus;

import Animacion.Animacion;
import Recursos.CambiarPanel;
import Recursos.Colores;
import SQL.Login;
import Ventanas.Pantallas.PantDevAgregar;
import Ventanas.Pantallas.PantDevEliminar;
import Ventanas.Pantallas.PantDevVer;
import Ventanas.VentanaPrincipal;
import keeptoo.KGradientPanel;
import rojeru_san.RSButtonRiple;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author Juan
 * 05-11-2021
 * Agregue la funcion eliminar,actualizar y mostrar
 */
public class MenuDevoluciones implements ActionListener {
    private JPanel jpPanel;
    private final KGradientPanel kgActual;
    private RSButtonRiple btnAgregar, btnEliminar, btnMostrar;
    private PantDevAgregar pddaNuevaPantalla;
    private PantDevVer pddvNuevaPantalla;
    private PantDevEliminar pddeNuevaPantalla;
    private CambiarPanel cbCambiar;
    private static JPanel jpMenu;
    private Login lTrabajador;

    public MenuDevoluciones(KGradientPanel kgActual, Login lTrabajador) {
        this.kgActual = kgActual;
        this.lTrabajador = lTrabajador;
        this.panelConfiguracion();
        this.botonesConfiguracion();
        this.activarRoles();
    }

    public static void setJpMenu(JPanel jpMenu) {
        MenuDevoluciones.jpMenu = jpMenu;
    }

    public void panelConfiguracion() {
        this.jpPanel = new JPanel();
        this.jpPanel.setSize(120, 170);
        this.jpPanel.setBackground(Colores.C_COLOR_GRIS2.getColor());
        this.jpPanel.setLayout(null);
    }

    private void botonesConfiguracion() {
        this.btnAgregar = new RSButtonRiple();
        this.btnAgregar.setBounds(0, 10, 120, 50);
        this.btnAgregar.setBorderPainted(false);
        this.btnAgregar.setFocusPainted(false);
        this.btnAgregar.setBackground(Colores.C_COLOR_GRIS2.getColor());
        this.btnAgregar.setColorHover(Colores.C_COLOR_GRIS3.getColor());
        this.btnAgregar.setColorText(Colores.C_COLOR_NEGRO.getColor());
        this.btnAgregar.setColorTextHover(Colores.C_COLOR_GRIS2.getColor());
        this.btnAgregar.setText("Agregar 8");
        this.btnAgregar.setActionCommand("Agregar");
        this.btnAgregar.addActionListener(this);
        this.btnAgregar.setEnabled(false);
        this.jpPanel.add(this.btnAgregar);

        /*-------------------------------------------------------*/

        this.btnEliminar = new RSButtonRiple();
        this.btnEliminar.setBounds(0, 60, 120, 50);
        this.btnEliminar.setBorderPainted(false);
        this.btnEliminar.setFocusPainted(false);
        this.btnEliminar.setBackground(Colores.C_COLOR_GRIS2.getColor());
        this.btnEliminar.setColorHover(Colores.C_COLOR_GRIS3.getColor());
        this.btnEliminar.setColorText(Colores.C_COLOR_NEGRO.getColor());
        this.btnEliminar.setColorTextHover(Colores.C_COLOR_GRIS2.getColor());
        this.btnEliminar.setText("Eliminar");
        this.btnEliminar.setActionCommand("Eliminar");
        this.btnEliminar.addActionListener(this);
        this.btnEliminar.setEnabled(false);
        this.jpPanel.add(this.btnEliminar);

        /*-------------------------------------------------------*/

        this.btnMostrar = new RSButtonRiple();
        this.btnMostrar.setBounds(0, 110, 120, 50);
        this.btnMostrar.setBorderPainted(false);
        this.btnMostrar.setFocusPainted(false);
        this.btnMostrar.setBackground(Colores.C_COLOR_GRIS2.getColor());
        this.btnMostrar.setColorHover(Colores.C_COLOR_GRIS3.getColor());
        this.btnMostrar.setColorText(Colores.C_COLOR_NEGRO.getColor());
        this.btnMostrar.setColorTextHover(Colores.C_COLOR_GRIS2.getColor());
        this.btnMostrar.setText("Ver registros");
        this.btnMostrar.setActionCommand("btnMostrar");
        this.btnMostrar.addActionListener(this);
        this.jpPanel.add(this.btnMostrar);

    }

    public JPanel getPanel() {
        return this.jpPanel;
    }

    private void btnAgregarActionPerformed() {
        this.pddaNuevaPantalla = new PantDevAgregar();
        this.cbCambiar = new CambiarPanel(this.kgActual, this.pddaNuevaPantalla.getkgPantalla(), jpMenu);
        this.cbCambiar.actualizarPanel();
        VentanaPrincipal.hmMenu.put("btnDevoluciones", false);
        Animacion.subir(0, -250, 1, 5, VentanaPrincipal.jpMenu);
    }

    private void btnMostrarActionPerformed() {
        this.pddvNuevaPantalla = new PantDevVer();
        this.cbCambiar = new CambiarPanel(this.kgActual, this.pddvNuevaPantalla.getkgPantalla(), jpMenu);
        this.cbCambiar.actualizarPanel();
        VentanaPrincipal.hmMenu.put("btnDevoluciones", false);
        Animacion.subir(0, -250, 1, 5, VentanaPrincipal.jpMenu);
    }

    private void btnEliminarActionPerformed() {
        this.pddeNuevaPantalla = new PantDevEliminar();
        this.cbCambiar = new CambiarPanel(this.kgActual, this.pddeNuevaPantalla.getkgPantalla(), jpMenu);
        this.cbCambiar.actualizarPanel();
        VentanaPrincipal.hmMenu.put("btnDevoluciones", false);
        Animacion.subir(0, -250, 1, 5, VentanaPrincipal.jpMenu);
    }

    private void activarRoles() {
        String sRolTrabajador = this.lTrabajador.getsCargo();
        switch (sRolTrabajador) {
            case "Atencion a clientes":
            case "Gerente":
                this.btnEliminar.setEnabled(true);
                this.btnAgregar.setEnabled(true);
                break;
        }
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        String sAction = actionEvent.getActionCommand();
        if (sAction.equals(this.btnAgregar.getActionCommand())) {
            this.btnAgregarActionPerformed();
        } else if (sAction.equals(this.btnMostrar.getActionCommand())) {
            this.btnMostrarActionPerformed();
        } else if (sAction.equals(this.btnEliminar.getActionCommand())) {
            this.btnEliminarActionPerformed();
        }
    }
}

