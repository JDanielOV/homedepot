/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ventanas;

import Animacion.Animacion;
import Recursos.CambiarPanel;
import Recursos.Colores;
import Recursos.Fuentes;
import Recursos.Imagenes;
import SQL.Login;
import Ventanas.menus.*;
import keeptoo.KGradientPanel;
import rojeru_san.RSButtonRiple;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.HashMap;

/**
 * @author Daniel Ochoa
 * <p>
 * Ochoa 07-11-2021
 * Modificaciones: cambie el codigo de animacion ya que cambie los menus
 */
public class VentanaPrincipal extends JFrame implements ActionListener {

    private KGradientPanel kgPanelFondo, kgPanelOpcionesSuperior, kgPanelUusuario, kgPanelPrincipal;
    public static JPanel jpMenu;
    private JButton btnCerrar;
    private RSButtonRiple btnClientes, btnTrabajadores, btnProveedores, btnProductos, btnTarjetas, btnVentas, btnPedidos, btnDevoluciones;
    public static HashMap<String, Boolean> hmMenu;
    private Login lTrabajador;
    private int xx, xy;

    public VentanaPrincipal(Login lUsuario) {
        this.lTrabajador = lUsuario;
        this.frameConfiguracion();
        this.panelConfiguracion();
        this.botonesConfiguracion();
        this.labelsConfiguracion();
        hmMenu = new HashMap();
        hmMenu.put(this.btnClientes.getActionCommand(), false);
        hmMenu.put(this.btnTrabajadores.getActionCommand(), false);
        hmMenu.put(this.btnProveedores.getActionCommand(), false);
        hmMenu.put(this.btnProductos.getActionCommand(), false);
        hmMenu.put(this.btnTarjetas.getActionCommand(), false);
        hmMenu.put(this.btnVentas.getActionCommand(), false);
        hmMenu.put(this.btnPedidos.getActionCommand(), false);
        hmMenu.put(this.btnDevoluciones.getActionCommand(), false);
    }

    private void panelConfiguracion() {
        this.kgPanelFondo = new KGradientPanel();
        this.kgPanelFondo.setBounds(0, 0, 1200, 650);
        this.kgPanelFondo.setBackground(Colores.C_COLOR_TRANSPARENTE.getColor());
        this.kgPanelFondo.setkEndColor(Colores.C_COLOR_BLANCO.getColor());
        this.kgPanelFondo.setkStartColor(Colores.C_COLOR_BLANCO.getColor());
        this.kgPanelFondo.setkBorderRadius(40);
        this.kgPanelFondo.setLayout(null);
        this.getContentPane().add(this.kgPanelFondo);

        /*--------------------------------------------------------*/
        this.kgPanelOpcionesSuperior = new KGradientPanel();
        this.kgPanelOpcionesSuperior.setBounds(0, 0, 1200, 100);
        this.kgPanelOpcionesSuperior.setBackground(Colores.C_COLOR_TRANSPARENTE.getColor());
        this.kgPanelOpcionesSuperior.setkEndColor(Colores.C_COLOR_GRIS4.getColor());
        this.kgPanelOpcionesSuperior.setkStartColor(Colores.C_COLOR_GRIS3.getColor());
        this.kgPanelOpcionesSuperior.setkBorderRadius(40);
        this.kgPanelOpcionesSuperior.setLayout(null);
        this.kgPanelFondo.add(this.kgPanelOpcionesSuperior);
        this.kgPanelOpcionesSuperior.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                xx = e.getX();
                xy = e.getY();
            }
        });

        this.kgPanelOpcionesSuperior.addMouseMotionListener(new MouseAdapter() {
            @Override
            public void mouseDragged(MouseEvent e) {
                int x = e.getXOnScreen();
                int y = e.getYOnScreen();

                setLocation(x - xx, y - xy);
            }
        });

        /*-------------------------------------------------------*/
        this.kgPanelUusuario = new KGradientPanel();
        this.kgPanelUusuario.setBounds(0, 0, 150, 650);
        this.kgPanelUusuario.setBackground(Colores.C_COLOR_TRANSPARENTE.getColor());
        this.kgPanelUusuario.setkEndColor(Colores.C_COLOR_GRIS3.getColor());
        this.kgPanelUusuario.setkStartColor(Colores.C_COLOR_GRIS1.getColor());
        this.kgPanelUusuario.setkBorderRadius(40);
        this.kgPanelUusuario.setLayout(null);
        this.kgPanelFondo.add(this.kgPanelUusuario);
        this.kgPanelUusuario.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                xx = e.getX();
                xy = e.getY();
            }
        });

        this.kgPanelUusuario.addMouseMotionListener(new MouseAdapter() {
            @Override
            public void mouseDragged(MouseEvent e) {
                int x = e.getXOnScreen();
                int y = e.getYOnScreen();

                setLocation(x - xx, y - xy);
            }
        });

        /*------------------------------------------------------*/
        this.kgPanelPrincipal = new KGradientPanel();
        this.kgPanelPrincipal.setBounds(150, 100, 1090, 570);
        this.kgPanelPrincipal.setBackground(Colores.C_COLOR_TRANSPARENTE.getColor());
        this.kgPanelPrincipal.setkEndColor(Colores.C_COLOR_BLANCO.getColor());
        this.kgPanelPrincipal.setkStartColor(Colores.C_COLOR_BLANCO.getColor());
        this.kgPanelPrincipal.setkBorderRadius(40);
        this.kgPanelPrincipal.setLayout(null);
        this.kgPanelFondo.add(this.kgPanelPrincipal);

        this.jpMenu = new JPanel();
        this.jpMenu.setBounds(0, 0, 740, 570);
        this.jpMenu.setBackground(Colores.C_COLOR_TRANSPARENTE.getColor());
        this.jpMenu.setLayout(null);
        this.kgPanelPrincipal.add(this.jpMenu);
    }

    private void botonesConfiguracion() {
        this.btnCerrar = new JButton(Imagenes.I_IMAGEN_CERRAR.getImagen());
        this.btnCerrar.setBounds(1168, 0, 32, 32);
        this.btnCerrar.setBorderPainted(false);
        this.btnCerrar.setContentAreaFilled(false);
        this.btnCerrar.setFocusPainted(false);
        this.btnCerrar.setActionCommand("btnCerrar");
        this.btnCerrar.addActionListener(this);
        this.kgPanelOpcionesSuperior.add(this.btnCerrar);


        JButton btnCerrarSesion = new JButton(Imagenes.I_IMAGEN_CERRAR_SESION.getImagen());
        btnCerrarSesion.setBounds(50, 40, 32, 32);
        btnCerrarSesion.setBorderPainted(false);
        btnCerrarSesion.setContentAreaFilled(false);
        btnCerrarSesion.setFocusPainted(false);
        btnCerrarSesion.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                VentanaIngreso v = new VentanaIngreso();
                v.setVisible(true);
                dispose();
            }
        });
        this.kgPanelOpcionesSuperior.add(btnCerrarSesion);

        /*----------------------------------------*/
        this.btnClientes = new RSButtonRiple();
        this.btnClientes.setBounds(150, 30, 120, 50);
        this.btnClientes.setBorderPainted(false);
        this.btnClientes.setFocusPainted(false);
        this.btnClientes.setBackground(Colores.C_COLOR_GRIS2.getColor());
        this.btnClientes.setColorHover(Colores.C_COLOR_GRIS3.getColor());
        this.btnClientes.setColorText(Colores.C_COLOR_NEGRO.getColor());
        this.btnClientes.setColorTextHover(Colores.C_COLOR_GRIS2.getColor());
        this.btnClientes.setText("Clientes");
        this.btnClientes.setActionCommand("btnClientes");
        this.btnClientes.addActionListener(this);
        this.kgPanelOpcionesSuperior.add(this.btnClientes);

        /*----------------------------------------*/
        this.btnTrabajadores = new RSButtonRiple();
        this.btnTrabajadores.setBounds(280, 30, 120, 50);
        this.btnTrabajadores.setBorderPainted(false);
        this.btnTrabajadores.setFocusPainted(false);
        this.btnTrabajadores.setBackground(Colores.C_COLOR_GRIS2.getColor());
        this.btnTrabajadores.setColorHover(Colores.C_COLOR_GRIS3.getColor());
        this.btnTrabajadores.setColorText(Colores.C_COLOR_NEGRO.getColor());
        this.btnTrabajadores.setColorTextHover(Colores.C_COLOR_GRIS2.getColor());
        this.btnTrabajadores.setText("Trabajadores");
        this.btnTrabajadores.setActionCommand("btnTrabajadores");
        this.btnTrabajadores.addActionListener(this);
        this.kgPanelOpcionesSuperior.add(this.btnTrabajadores);

        /*----------------------------------------*/
        this.btnProveedores = new RSButtonRiple();
        this.btnProveedores.setBounds(410, 30, 120, 50);
        this.btnProveedores.setBorderPainted(false);
        this.btnProveedores.setFocusPainted(false);
        this.btnProveedores.setBackground(Colores.C_COLOR_GRIS2.getColor());
        this.btnProveedores.setColorHover(Colores.C_COLOR_GRIS3.getColor());
        this.btnProveedores.setColorText(Colores.C_COLOR_NEGRO.getColor());
        this.btnProveedores.setColorTextHover(Colores.C_COLOR_GRIS2.getColor());
        this.btnProveedores.setText("Proveedores");
        this.btnProveedores.setActionCommand("btnProveedores");
        this.btnProveedores.addActionListener(this);
        this.kgPanelOpcionesSuperior.add(this.btnProveedores);

        /*----------------------------------------*/
        this.btnProductos = new RSButtonRiple();
        this.btnProductos.setBounds(540, 30, 120, 50);
        this.btnProductos.setBorderPainted(false);
        this.btnProductos.setFocusPainted(false);
        this.btnProductos.setBackground(Colores.C_COLOR_GRIS2.getColor());
        this.btnProductos.setColorHover(Colores.C_COLOR_GRIS3.getColor());
        this.btnProductos.setColorText(Colores.C_COLOR_NEGRO.getColor());
        this.btnProductos.setColorTextHover(Colores.C_COLOR_GRIS2.getColor());
        this.btnProductos.setText("Productos");
        this.btnProductos.setActionCommand("btnProductos");
        this.btnProductos.addActionListener(this);
        this.kgPanelOpcionesSuperior.add(this.btnProductos);


        /*----------------------------------------*/
        this.btnTarjetas = new RSButtonRiple();
        this.btnTarjetas.setBounds(670, 30, 120, 50);
        this.btnTarjetas.setBorderPainted(false);
        this.btnTarjetas.setFocusPainted(false);
        this.btnTarjetas.setBackground(Colores.C_COLOR_GRIS2.getColor());
        this.btnTarjetas.setColorHover(Colores.C_COLOR_GRIS3.getColor());
        this.btnTarjetas.setColorText(Colores.C_COLOR_NEGRO.getColor());
        this.btnTarjetas.setColorTextHover(Colores.C_COLOR_GRIS2.getColor());
        this.btnTarjetas.setText("Tarjetas");
        this.btnTarjetas.setActionCommand("btnTarjetas");
        this.btnTarjetas.addActionListener(this);
        this.kgPanelOpcionesSuperior.add(this.btnTarjetas);


        /*----------------------------------------*/
        this.btnVentas = new RSButtonRiple();
        this.btnVentas.setBounds(800, 30, 120, 50);
        this.btnVentas.setBorderPainted(false);
        this.btnVentas.setFocusPainted(false);
        this.btnVentas.setBackground(Colores.C_COLOR_GRIS2.getColor());
        this.btnVentas.setColorHover(Colores.C_COLOR_GRIS3.getColor());
        this.btnVentas.setColorText(Colores.C_COLOR_NEGRO.getColor());
        this.btnVentas.setColorTextHover(Colores.C_COLOR_GRIS2.getColor());
        this.btnVentas.setText("Ventas");
        this.btnVentas.setActionCommand("btnVentas");
        this.btnVentas.addActionListener(this);
        this.kgPanelOpcionesSuperior.add(this.btnVentas);

        /*----------------------------------------*/
        this.btnPedidos = new RSButtonRiple();
        this.btnPedidos.setBounds(930, 30, 120, 50);
        this.btnPedidos.setBorderPainted(false);
        this.btnPedidos.setFocusPainted(false);
        this.btnPedidos.setBackground(Colores.C_COLOR_GRIS2.getColor());
        this.btnPedidos.setColorHover(Colores.C_COLOR_GRIS3.getColor());
        this.btnPedidos.setColorText(Colores.C_COLOR_NEGRO.getColor());
        this.btnPedidos.setColorTextHover(Colores.C_COLOR_GRIS2.getColor());
        this.btnPedidos.setText("Pedidos");
        this.btnPedidos.setActionCommand("btnPedidos");
        this.btnPedidos.addActionListener(this);
        this.kgPanelOpcionesSuperior.add(this.btnPedidos);

        /*----------------------------------------*/
        this.btnDevoluciones = new RSButtonRiple();
        this.btnDevoluciones.setBounds(1060, 30, 120, 50);
        this.btnDevoluciones.setBorderPainted(false);
        this.btnDevoluciones.setFocusPainted(false);
        this.btnDevoluciones.setBackground(Colores.C_COLOR_GRIS2.getColor());
        this.btnDevoluciones.setColorHover(Colores.C_COLOR_GRIS3.getColor());
        this.btnDevoluciones.setColorText(Colores.C_COLOR_NEGRO.getColor());
        this.btnDevoluciones.setColorTextHover(Colores.C_COLOR_GRIS2.getColor());
        this.btnDevoluciones.setText("Devolucion");
        this.btnDevoluciones.setActionCommand("btnDevoluciones");
        this.btnDevoluciones.addActionListener(this);
        this.kgPanelOpcionesSuperior.add(this.btnDevoluciones);

    }

    private void frameConfiguracion() {
        this.setSize(1200, 650);
        this.setResizable(false);
        this.setLocationRelativeTo(null);
        this.setUndecorated(true);
        this.getContentPane().setLayout(null);
        this.setBackground(Colores.C_COLOR_TRANSPARENTE.getColor());
    }


    private void btnCerrarActionPerformed() {
        System.exit(0);
    }

    private void btnClientesActionPerformed(String btnAction) {
        this.animacion(btnAction);
    }

    private void btnTrabajadoresActionPerformed(String btnAction) {
        this.animacion(btnAction);
    }

    private void btnProveedoresActionPerformed(String btnAction) {
        this.animacion(btnAction);
    }

    private void btnProductosActionPerformed(String btnAction) {
        this.animacion(btnAction);
    }

    private void btnTarjetasActionPerformed(String btnAction) {
        this.animacion(btnAction);
    }

    private void btnVentasActionPerformed(String btnAction) {
        this.animacion(btnAction);
    }

    private void btnPedidosActionPerformed(String btnAction) {
        this.animacion(btnAction);
    }

    private void btnDevolucionesActionPerformed(String btnAction) {
        this.animacion(btnAction);
    }

    private void animacion(String btnAction) {
        for (HashMap.Entry<String, Boolean> entry : this.hmMenu.entrySet()) {
            if (!entry.getKey().equals(btnAction) && entry.getValue()) {
                System.out.println("cambio");
                Animacion.subir(0, -350, 1, 5, this.jpMenu);
                this.hmMenu.put(entry.getKey(), false);
                return;
            } else if (entry.getKey().equals(btnAction) && entry.getValue()) {
                System.out.println("el mismo");
                Animacion.subir(0, -350, 1, 5, this.jpMenu);
                this.hmMenu.put(entry.getKey(), false);
                return;
            }
        }
//        this.kgPanelPrincipal = new KGradientPanel();
//        this.kgPanelPrincipal.setBounds(150, 100, 840, 570);
//        this.kgPanelPrincipal.setBackground(Colores.C_COLOR_TRANSPARENTE.getColor());
//        this.kgPanelPrincipal.setkEndColor(Colores.C_COLOR_BLANCO.getColor());
//        this.kgPanelPrincipal.setkStartColor(Colores.C_COLOR_BLANCO.getColor());
//        this.kgPanelPrincipal.setkBorderRadius(40);
//        this.kgPanelPrincipal.setLayout(null);
//        this.kgPanelFondo.add(this.kgPanelPrincipal);
        System.out.println("todo falso");
        this.hmMenu.put(btnAction, true);
        if (btnAction.equals(this.btnClientes.getActionCommand())) {
            MenuClientes mp = new MenuClientes(this.kgPanelPrincipal, this.lTrabajador);
            CambiarPanel cp = new CambiarPanel(this.jpMenu, mp.getPanel());
            cp.actualizarPanel2();
            MenuClientes.setJpMenu(this.jpMenu);
            this.jpMenu.setLocation(0, 0);
        } else if (btnAction.equals(this.btnTrabajadores.getActionCommand())) {
            MenuTrabajadores mp = new MenuTrabajadores(this.kgPanelPrincipal, this.lTrabajador);
            CambiarPanel cp = new CambiarPanel(this.jpMenu, mp.getPanel());
            cp.actualizarPanel2();
            MenuTrabajadores.setJpMenu(this.jpMenu);
            this.jpMenu.setLocation(130, 0);
        } else if (btnAction.equals(this.btnProveedores.getActionCommand())) {
            MenuProveedores mp = new MenuProveedores(this.kgPanelPrincipal, this.lTrabajador);
            CambiarPanel cp = new CambiarPanel(this.jpMenu, mp.getPanel());
            cp.actualizarPanel2();
            MenuProveedores.setJpMenu(this.jpMenu);
            this.jpMenu.setLocation(260, 0);
        } else if (btnAction.equals(this.btnProductos.getActionCommand())) {
            MenuProductos mp = new MenuProductos(this.kgPanelPrincipal, this.lTrabajador);
            CambiarPanel cp = new CambiarPanel(this.jpMenu, mp.getPanel());
            cp.actualizarPanel2();
            MenuProductos.setJpMenu(this.jpMenu);
            this.jpMenu.setLocation(390, 0);
            System.out.println(btnAction);
        } else if (btnAction.equals(this.btnTarjetas.getActionCommand())) {
            MenuTarjetas mp = new MenuTarjetas(this.kgPanelPrincipal, this.lTrabajador);
            CambiarPanel cp = new CambiarPanel(this.jpMenu, mp.getPanel());
            cp.actualizarPanel2();
            MenuTarjetas.setJpMenu(this.jpMenu);
            this.jpMenu.setLocation(520, 0);
            System.out.println(btnAction);
        } else if (btnAction.equals(this.btnVentas.getActionCommand())) {
            MenuVentas mp = new MenuVentas(this.kgPanelPrincipal, this.lTrabajador);
            CambiarPanel cp = new CambiarPanel(this.jpMenu, mp.getPanel());
            cp.actualizarPanel2();
            MenuVentas.setJpMenu(this.jpMenu);
            this.jpMenu.setLocation(650, 0);
            System.out.println(btnAction);
        } else if (btnAction.equals(this.btnPedidos.getActionCommand())) {
            MenuPedidos mp = new MenuPedidos(this.kgPanelPrincipal, this.lTrabajador);
            CambiarPanel cp = new CambiarPanel(this.jpMenu, mp.getPanel());
            cp.actualizarPanel2();
            MenuPedidos.setJpMenu(this.jpMenu);
            this.jpMenu.setLocation(780, 0);
            System.out.println(btnAction);
        } else if (btnAction.equals(this.btnDevoluciones.getActionCommand())) {
            MenuDevoluciones mp = new MenuDevoluciones(this.kgPanelPrincipal, this.lTrabajador);
            CambiarPanel cp = new CambiarPanel(this.jpMenu, mp.getPanel());
            cp.actualizarPanel2();
            MenuDevoluciones.setJpMenu(this.jpMenu);
            this.jpMenu.setLocation(910, 0);
        }

//        this.jpMenu.setLayout(null);
        Animacion.bajar(-350, 0, 1, 5, this.jpMenu);
    }

    private void labelsConfiguracion() {
        this.lTrabajador.datosUsuario();
        JLabel jlTituloImagen = new JLabel(Imagenes.I_IMAGEN_USUARIO_TRABAJADOR.getImagen());
        jlTituloImagen.setBounds(0, 200, 150, 64);
        jlTituloImagen.setHorizontalAlignment(SwingConstants.CENTER);
        jlTituloImagen.setVerticalAlignment(SwingConstants.CENTER);
        this.kgPanelUusuario.add(jlTituloImagen);

        JLabel jlIDTrabajador = new JLabel("RFC Trabajador: ".concat(this.lTrabajador.getsRFC()));
        jlIDTrabajador.setBounds(0, 270, 150, 20);
        jlIDTrabajador.setHorizontalAlignment(SwingConstants.CENTER);
        jlIDTrabajador.setVerticalAlignment(SwingConstants.CENTER);
        jlIDTrabajador.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.kgPanelUusuario.add(jlIDTrabajador);

        String sTraNombre = "Nombre: " + ("<br>") + (this.lTrabajador.getsNombre());
        JLabel jlTraNombre = new JLabel("<html><div style='text-align: center;'>" + sTraNombre + "</div></html>");
        jlTraNombre.setBounds(0, 300, 150, 70);
        jlTraNombre.setHorizontalAlignment(SwingConstants.CENTER);
        jlTraNombre.setVerticalAlignment(SwingConstants.CENTER);
        jlTraNombre.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.kgPanelUusuario.add(jlTraNombre);


        String sTraTelefono = "Telefono: " + ("<br>") + (this.lTrabajador.getsTelefono());
        JLabel jlTraTelefono = new JLabel("<html><div style='text-align: center;'>" + sTraTelefono + "</div></html>");
        jlTraTelefono.setBounds(0, 370, 150, 70);
        jlTraTelefono.setHorizontalAlignment(SwingConstants.CENTER);
        jlTraTelefono.setVerticalAlignment(SwingConstants.CENTER);
        jlTraTelefono.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.kgPanelUusuario.add(jlTraTelefono);

        String sTraCargo = "Cargo: " + ("<br>") + (this.lTrabajador.getsCargo());
        JLabel jlTraCargo = new JLabel("<html><div style='text-align: center;'>" + sTraCargo + "</div></html>");
        jlTraCargo.setBounds(0, 420, 150, 70);
        jlTraCargo.setHorizontalAlignment(SwingConstants.CENTER);
        jlTraCargo.setVerticalAlignment(SwingConstants.CENTER);
        jlTraCargo.setFont(Fuentes.FUENTE_TEXTOS.getFont());
        this.kgPanelUusuario.add(jlTraCargo);
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        String sButon = ae.getActionCommand();
        if (sButon.equals(this.btnCerrar.getActionCommand())) {
            this.btnCerrarActionPerformed();
        } else if (sButon.equals(this.btnClientes.getActionCommand())) {
            this.btnClientesActionPerformed(sButon);
        } else if (sButon.equals(this.btnTrabajadores.getActionCommand())) {
            this.btnTrabajadoresActionPerformed(sButon);
        } else if (sButon.equals(this.btnProveedores.getActionCommand())) {
            this.btnProveedoresActionPerformed(sButon);
        } else if (sButon.equals(this.btnProductos.getActionCommand())) {
            this.btnProductosActionPerformed(sButon);
        } else if (sButon.equals(this.btnTarjetas.getActionCommand())) {
            this.btnTarjetasActionPerformed(sButon);
        } else if (sButon.equals(this.btnVentas.getActionCommand())) {
            this.btnVentasActionPerformed(sButon);
        } else if (sButon.equals(this.btnPedidos.getActionCommand())) {
            this.btnPedidosActionPerformed(sButon);
        } else if (sButon.equals(this.btnDevoluciones.getActionCommand())) {
            this.btnDevolucionesActionPerformed(sButon);
        }
    }

    public KGradientPanel getKgPanelPrincipal() {
        return this.kgPanelPrincipal;
    }

}