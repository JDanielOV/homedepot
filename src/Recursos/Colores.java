/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Recursos;

import java.awt.*;

/**
 *
 * @author Daniel Ochoa
 */
public enum Colores {
    C_COLOR_TRANSPARENTE (new Color(0, 0, 0, 0)),
    C_COLOR_BLANCO (Color.white),
    C_COLOR_NEGRO (Color.BLACK),
    C_COLOR_GRIS4(new Color(153, 167, 153)),//rgb(255, 155, 106)
    C_COLOR_GRIS3(new Color(173, 194, 169)),//rgb(255, 81, 81)
    C_COLOR_GRIS2(new Color(211, 228, 205)),//rgb(22, 30, 84)
    C_COLOR_GRIS1(new Color(254, 245, 237));//rgb(136, 224, 239)
    private final Color color;

    Colores(Color color) {
        this.color = color;
    }

    public Color getColor() {
        return color;
    }
}
