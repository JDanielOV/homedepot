package Recursos;

import java.awt.*;

public enum Fuentes {
    FUENTE_TITULOS(new Font("Montserrat", Font.PLAIN, 25)),
    FUENTE_TEXTOS(new Font("Montserrat", Font.PLAIN, 15)),
    FUENTES_ETIQUETAS(new Font("Montserrat", Font.PLAIN, 18));
    private final Font fLetras;
     Fuentes(Font fLetras){
        this.fLetras=fLetras;
    }
    public Font getFont(){
         return this.fLetras;
    }
}
