/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Recursos;

import javax.swing.*;

/**
 * @author Daniel Ochoa
 * <p>
 * Ochoa Fecha: 06/11/2021
 * Modificaciones: Agrege nueva iamgen
 */
public enum Imagenes {
    I_IMAGEN_CERRAR(new ImageIcon("src/Recursos/Imgs/icons8_delete_32px.png")),
    I_IMAGEN_CLIENTE(new ImageIcon("src/Recursos/Imgs/icons8_card_payment_64px.png")),//rgb(136, 224, 239)
    I_IMAGEN_TRABAJADOR(new ImageIcon("src/Recursos/Imgs/icon8_obrero_64px.png")),
    I_IMAGEN_VENTA(new ImageIcon("src/Recursos/Imgs/icons8_buying_64px.png")),
    I_IMAGEN_PRODUCTO(new ImageIcon("src/Recursos/Imgs/icons8_product_64px.png")),
    I_IMAGEN_LOGO(new ImageIcon("src/Recursos/Imgs/the-home-depot.png")),
    I_IMAGEN_USUARIO(new ImageIcon("src/Recursos/Imgs/icons8_user_32px.png")),
    I_IMAGEN_USUARIO_TRABAJADOR(new ImageIcon("src/Recursos/Imgs/icons8_user_64px.png")),
    I_IMAGEN_CONTRASENA(new ImageIcon("src/Recursos/Imgs/icons8_password_32px.png")),
    I_IMAGEN_PROVEEDOR(new ImageIcon("src/Recursos/Imgs/icons8-proveedor_64px.png")),
    I_IMAGEN_TARJETA(new ImageIcon("src/Recursos/Imgs/icons8_tarjetas_64px.png")),
    I_IMAGEN_PEDIDO(new ImageIcon("src/Recursos/Imgs/icons8_pedido_64px.png")),
    I_IMAGEN_DEVOLUCION(new ImageIcon("src/Recursos/Imgs/icons8_devolucion_64px.png")),
    I_IMAGEN_CERRAR_SESION(new ImageIcon("src/Recursos/Imgs/icons8_close_pane_32px.png"));

    private final ImageIcon imagen;

    Imagenes(ImageIcon imagen) {
        this.imagen = imagen;
    }

    public ImageIcon getImagen() {
        return imagen;
    }
}
