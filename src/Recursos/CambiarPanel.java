package Recursos;

import keeptoo.KGradientPanel;

import javax.swing.*;

public class CambiarPanel {


    private KGradientPanel primario;
    private KGradientPanel secundario;
    private JPanel jpPrimario,jpSecundario;
    private JPanel jpMenu;

    public CambiarPanel(KGradientPanel primario, KGradientPanel secundario,JPanel jpMenu) {
        this.primario = primario;
        this.secundario = secundario;
        this.jpMenu = jpMenu;

    }
    public CambiarPanel(JPanel primario, JPanel secundario) {
        this.jpPrimario = primario;
        this.jpSecundario = secundario;

    }

    public void actualizarPanel() {
        //this.primario.updateUI();
        this.primario.removeAll();
        this.primario.add(this.jpMenu);
        this.primario.add(this.secundario);
        this.secundario.setLocation(20,20);
        this.primario.updateUI();
    }
    public void actualizarPanel2() {
        this.jpPrimario.removeAll();
        //this.primario.updateUI();
        this.jpPrimario.add(this.jpSecundario);
        this.jpSecundario.setLocation(0,0);
        this.jpPrimario.updateUI();
    }
}
