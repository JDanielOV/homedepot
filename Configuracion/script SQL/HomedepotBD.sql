create database HomeDepot;
use HomeDepot;

create table Persona(
	personaId smallint not null auto_increment,
    personaNom varchar(30) not null,
	personaApeP varchar(20) not null,
	personaApeM varchar(20) not null,
	personaEmail varchar(30) null,
	personaTel varchar(15) not null,
    primary key (personaId)
);
describe Persona;

create table Cliente(
	clienteId smallint not null auto_increment,
	personaId smallint not null,
	clienteFechaNa date not null,
    primary key (clienteId),
    foreign key (personaId) references Persona(personaId) on update cascade on delete cascade
);
describe Cliente;

create table Trabajador(
	trabRFC varchar(20) not null,
	personaId smallint not null,
	trabFechaNac date not null,
	trabDireccion varchar(100) not null,
	trabCargo varchar(40) not null,
	trabSueldo decimal(7,2)unsigned not null,
	trabContraseña varchar(20) not null,
    trabActivo boolean not null,
    primary key (trabRFC),
    foreign key (personaId) references Persona(personaId) on update cascade on delete cascade
);
describe Trabajador;

create table Proveedor(
	proveedorId smallint not null auto_increment,
	personaId smallint not null,
	proveedorEmpre varchar(30) not null,
    proveedorActivo boolean not null,
    primary key (proveedorId),
    foreign key (personaId) references Persona(personaId) on update cascade on delete cascade
);
describe Proveedor;

create table Producto(
	productoId smallint not null auto_increment,
	productoNom varchar(40) not null,
	productoPrecio decimal(7,2)unsigned not null,
    proveedorId smallint not null,
	productoDep varchar(40) not null,
	inventarioCant smallint unsigned not null,
    productoActivo boolean not null,
    primary key (productoId),
    foreign key (proveedorId) references proveedor(proveedorId) on update cascade on delete cascade
);
describe Producto;

create table Tarjeta(
	tarjeNum smallint not null auto_increment,
	clienteId smallint not null,
	trabRFC varchar(20) not null,
	tarjeDiaAlta date not null,
	tarjeExpiracion date not null,
	tarjePuntos mediumint unsigned not null,
    tarjeActivo boolean not null,
    primary key (tarjeNum),
    foreign key (clienteId) references Cliente(clienteId) on update cascade on delete cascade,
    foreign key (trabRFC) references Trabajador(trabRFC) on update cascade on delete cascade
);

/*---------------------------- 26/11/2021---------------------------------*/
create table Ventas(
	ventaNum mediumint not null auto_increment,
	clienteId  smallint not null,
    tarjeNum smallint null,
	trabRFC varchar(20) not null,
    ventaFecha date not null,
    ventaTotal decimal(10,2)unsigned not null,
    puntosGastados MEDIUMINT not null,
    efectivo DECIMAL(10,2) not null,
    cambio DECIMAL(10,2) not null,
    primary key (ventaNum),
    foreign key (tarjeNum) references Tarjeta(tarjeNum) on update cascade on delete cascade,
    foreign key (clienteId) references Cliente(clienteId) on update cascade on delete cascade,
    foreign key (trabRFC) references Trabajador(trabRFC) on update cascade on delete cascade
);
describe Ventas;

create table FacturaVenta(
	ventaNum mediumint not null,
	productoId smallint not null,
	ventaPrecio decimal(9,2)unsigned not null,
	ventaCant smallint unsigned not null,
    ventActivo boolean,
    foreign key (ventaNum) references Ventas(ventaNum) on update cascade on delete cascade,
    foreign key (productoId) references Producto(productoId) on update cascade on delete cascade
);
describe FacturaVenta;

/*---------------------------- 26/11/2021---------------------------------*/
create table Pedido(
	pedId mediumint not null auto_increment,
	trabRFC varchar(20) not null,
	proveedorId smallint not null,
    pedFecha date not null,
	pedFechaEntrega date not null,
    pedTotal decimal(10,2)unsigned not null,
    pedActivo boolean not null,
    pedLlego boolean not null,
    primary key (pedId),
    foreign key (proveedorId) references proveedor(proveedorId) on update cascade on delete cascade,
    foreign key (trabRFC) references Trabajador(trabRFC) on update cascade on delete cascade
);
describe Pedido;

create table FacturaPedido(
	pedId mediumint not null,
	productoId smallint not null,
	pedCantProduc smallint unsigned not null,
	pedCantDinero decimal(9,2)unsigned not null,
    pedFActivo boolean,
    foreign key (pedId) references Pedido(pedId) on update cascade on delete cascade,
    foreign key (productoId) references Producto(productoId) on update cascade on delete cascade
);
describe Pedido;

create table Devoluciones(
	devolucionId smallint not null auto_increment,
    ventaNum mediumint not null,
	clienteId smallint not null,
    tarjeNum smallint null,
	productoId smallint not null,
	devolucionFecha date not null,
	devolucionMotiv varchar(100) null,
	devolucionCant smallint unsigned not null,
    devolucionActivo boolean not null,
    primary key (devolucionId),
    foreign key (tarjeNum) references Tarjeta(tarjeNum) on update cascade on delete cascade,
    foreign key (ventaNum) references Ventas(ventaNum) on update cascade on delete cascade,
    foreign key (clienteId) references Cliente(clienteId) on update cascade on delete cascade,
    foreign key (productoId) references Producto(productoId) on update cascade on delete cascade
);
describe Devoluciones;

describe persona;
describe cliente;
describe trabajador;
describe proveedor;
describe producto;
describe Pedido;
describe ventas;
describe devoluciones;
describe Tarjeta;
