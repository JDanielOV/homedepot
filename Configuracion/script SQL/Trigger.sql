/*-------------------------------------------Trigger insercion venta------------------------------------------*/

DELIMITER $$
CREATE TRIGGER TinsertarFacturaVenta
AFTER INSERT ON FacturaVenta
FOR EACH ROW
BEGIN
  DECLARE idProvedor int;
  DECLARE idActivo boolean;
  DECLARE iPiezasProducto int;
set idProvedor := (select Producto.proveedorId from Producto where Producto.productoId=new.productoId);
set idActivo := (select Proveedor.proveedorActivo from Proveedor where Proveedor.proveedorId=idProvedor);
set iPiezasProducto := (select Producto.inventarioCant from Producto where Producto.productoId=new.productoId);

  UPDATE Producto set inventarioCant=inventarioCant-new.ventaCant
  where Producto.productoId=new.productoId;
  
 UPDATE Producto set productoActivo=false
  where Producto.productoId=new.productoId and (iPiezasProducto-new.ventaCant)=0 and idActivo=false;
END$$
DELIMITER ;
/*
resultadoes esperados 83 en el inventario
*/
insert into Ventas (clienteId, trabRFC, ventaFecha, ventaTotal) values (4, '1', '2021-11-15', 1200.73);
insert into FacturaVenta(ventaNum, productoId, ventaPrecio, ventaCant)values (26, 1, 1200.73, 1);

-- TRIGGER PARA AGREGAR DEVOLUCION VA A AUMENTAR EL INVENTARIO
-- disminuir las cantidades de factura ventas y disminuir total de venta
drop trigger aumentarInventario;
DELIMITER $$
CREATE TRIGGER aumentarInventario
AFTER INSERT ON Devoluciones
FOR EACH ROW
BEGIN
  DECLARE idActivo boolean;
  DECLARE Precio decimal(7,2)unsigned;
  DECLARE ventaPreci decimal(9,2)unsigned;
  set Precio := (select (Producto.productoPrecio) from producto where productoId=new.productoId);
  Update Producto set inventarioCant = inventarioCant + new.devolucionCant
  where Producto.productoId=new.productoId;
  Update FacturaVenta set ventaCant = ventaCant - new.devolucionCant
  where FacturaVenta.ventaNum=new.ventaNum and FacturaVenta.productoId=new.productoId;
  Update FacturaVenta set ventaPrecio = ventaPrecio - (Precio*new.devolucionCant)
  where FacturaVenta.ventaNum=new.ventaNum and FacturaVenta.productoId=new.productoId;
  set ventaPreci := (select (FacturaVenta.ventaPrecio) from FacturaVenta where FacturaVenta.ventaNum=new.ventaNum and FacturaVenta.productoId=new.productoId);
  Update Ventas set ventaTotal = ventaTotal - (Precio*new.devolucionCant)
  where Ventas.ventaNum=new.ventaNum and Ventas.clienteId=new.clienteId;
  
  set idActivo := (select facturaventa.ventActivo from facturaventa where FacturaVenta.ventaNum=new.ventaNum and FacturaVenta.productoId=new.productoId);
  Update FacturaVenta set ventActivo=false
  where FacturaVenta.ventaNum=new.ventaNum and FacturaVenta.productoId=new.productoId and (ventaCant=0);
END$$
DELIMITER ;


-- TRIGGER PARA ELIMINAR DEVOLUCION VA A DISMINUIR EL INVENTARIO y AUMENTAR LAS CANTIDADES EN FACTURA VENTA Y AUMENTA EL TOTAL VENTA
/*drop trigger disminuirInventario;
DELIMITER $$
CREATE TRIGGER disminuirInventario
AFTER DELETE ON Devoluciones
FOR EACH ROW
BEGIN
  DECLARE idActivo boolean;
  DECLARE Precio decimal(7,2)unsigned;
  DECLARE ventaPreci decimal(9,2)unsigned;
  set Precio := (select (Producto.productoPrecio) from producto where productoId=old.productoId);
  Update Producto set inventarioCant = inventarioCant - old.devolucionCant
  where Producto.productoId=old.productoId;
  Update FacturaVenta set ventaCant = ventaCant + old.devolucionCant
  where FacturaVenta.ventaNum=old.ventaNum and FacturaVenta.productoId=old.productoId;
  Update FacturaVenta set ventaPrecio = ventaPrecio + (Precio*old.devolucionCant)
  where FacturaVenta.ventaNum=old.ventaNum and FacturaVenta.productoId=old.productoId;
  set ventaPreci := (select (FacturaVenta.ventaPrecio) from FacturaVenta where FacturaVenta.ventaNum=old.ventaNum and FacturaVenta.productoId=old.productoId);
  Update Ventas set ventaTotal = ventaTotal + (Precio*old.devolucionCant)
  where Ventas.ventaNum=old.ventaNum and Ventas.clienteId=old.clienteId;
  
  set idActivo := (select facturaventa.ventActivo from facturaventa where FacturaVenta.ventaNum=old.ventaNum and FacturaVenta.productoId=old.productoId);
  Update FacturaVenta set ventActivo=true
  where FacturaVenta.ventaNum=old.ventaNum and FacturaVenta.productoId=old.productoId and (ventaCant>0);
END$$
DELIMITER ;

/*drop trigger actualizarTotalPedido;
DELIMITER $$
CREATE TRIGGER actualizarTotalPedido
AFTER INSERT ON FacturaPedido
FOR EACH ROW
BEGIN
  DECLARE Total decimal(9,2);
  set Total :=  (select (FacturaPedido.pedCantDinero) from facturapedido where productoId=new.productoId and pedId=new.pedId);
  UPDATE Pedido set pedTotal = pedTotal+Total where Pedido.pedId=new.pedId;
END$$
DELIMITER ;*/