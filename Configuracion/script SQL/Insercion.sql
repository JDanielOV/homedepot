use HomeDepot;

select *from Persona;
select *from Cliente;
select *from Trabajador;
select *from Proveedor;
select *from Producto;
select *from Ventas;
select *from Pedido;
select* from FacturaVenta;
select * from FacturaPedido;
select *from Devoluciones;
select *from Tarjeta;

insert into Persona (personaId, personaNom, personaApeP, personaApeM, personaEmail, personaTel) values (1, 'Doyle', 'Ludy', 'Corteney', 'dcorteney0@yale.edu', '611-725-9491');
insert into Persona (personaId, personaNom, personaApeP, personaApeM, personaEmail, personaTel) values (2, 'Domingo', 'Sallowaye', 'Shulem', 'dshulem1@csmonitor.com', '148-998-6032');
insert into Persona (personaId, personaNom, personaApeP, personaApeM, personaEmail, personaTel) values (3, 'Rafaellle', 'Battista', 'Mankor', 'rmankor2@ask.com', '623-640-1519');
insert into Persona (personaId, personaNom, personaApeP, personaApeM, personaEmail, personaTel) values (4, 'Florian', 'Armatage', 'Haggis', 'fhaggis3@clickbank.net', '227-363-4232');
insert into Persona (personaId, personaNom, personaApeP, personaApeM, personaEmail, personaTel) values (5, 'Noel', 'Rigney', 'Smorthit', 'nsmorthit4@army.mil', '900-898-0647');
insert into Persona (personaId, personaNom, personaApeP, personaApeM, personaEmail, personaTel) values (6, 'Legra', 'Roath', 'Shreeves', 'lshreeves5@cdc.gov', '275-283-2128');
insert into Persona (personaId, personaNom, personaApeP, personaApeM, personaEmail, personaTel) values (7, 'Sari', 'Olivet', 'Benedit', 'sbenedit6@theguardian.com', '989-116-1926');
insert into Persona (personaId, personaNom, personaApeP, personaApeM, personaEmail, personaTel) values (8, 'Devin', 'Beckerleg', 'Chapelle', 'dchapelle7@berkeley.edu', '907-359-5464');
insert into Persona (personaId, personaNom, personaApeP, personaApeM, personaEmail, personaTel) values (9, 'Esdras', 'Fareweather', 'Bolger', 'ebolger8@cbc.ca', '845-441-3240');
insert into Persona (personaId, personaNom, personaApeP, personaApeM, personaEmail, personaTel) values (10, 'Dolly', 'Ormesher', 'Blower', 'dblower9@uiuc.edu', '730-306-6324');
insert into Persona (personaId, personaNom, personaApeP, personaApeM, personaEmail, personaTel) values (11, 'Kara', 'Nassy', 'Hyslop', 'khyslopa@canalblog.com', '694-122-2632');
insert into Persona (personaId, personaNom, personaApeP, personaApeM, personaEmail, personaTel) values (12, 'Orelee', 'Greser', 'Bigadike', 'obigadikeb@phpbb.com', '228-717-0254');
insert into Persona (personaId, personaNom, personaApeP, personaApeM, personaEmail, personaTel) values (13, 'Herschel', 'Higford', 'Grabbam', 'hgrabbamc@posterous.com', '332-327-0089');
insert into Persona (personaId, personaNom, personaApeP, personaApeM, personaEmail, personaTel) values (14, 'See', 'Franciottoi', 'Braemer', 'sbraemerd@fema.gov', '831-585-5493');
insert into Persona (personaId, personaNom, personaApeP, personaApeM, personaEmail, personaTel) values (15, 'Mattheus', 'Gaskal', 'Darrigoe', 'mdarrigoee@economist.com', '549-130-4000');
insert into Persona (personaId, personaNom, personaApeP, personaApeM, personaEmail, personaTel) values (16, 'Gwendolyn', 'Lissandri', 'Perham', 'gperhamf@reference.com', '808-531-6748');
insert into Persona (personaId, personaNom, personaApeP, personaApeM, personaEmail, personaTel) values (17, 'Jacquette', 'Stovold', 'Michele', 'jmicheleg@creativecommons.org', '212-693-1863');
insert into Persona (personaId, personaNom, personaApeP, personaApeM, personaEmail, personaTel) values (18, 'Ibby', 'Jeandillou', 'Bowdidge', 'ibowdidgeh@businessweek.com', '527-960-7693');
insert into Persona (personaId, personaNom, personaApeP, personaApeM, personaEmail, personaTel) values (19, 'Tobie', 'Christauffour', 'Downham', 'tdownhami@quantcast.com', '804-108-7498');
insert into Persona (personaId, personaNom, personaApeP, personaApeM, personaEmail, personaTel) values (20, 'Thoma', 'McNelis', 'Gathwaite', 'tgathwaitej@msu.edu', '394-674-6720');
insert into Persona (personaId, personaNom, personaApeP, personaApeM, personaEmail, personaTel) values (21, 'Vick', 'Ehlerding', 'Tenpenny', 'vtenpennyk@privacy.gov.au', '319-185-1111');
insert into Persona (personaId, personaNom, personaApeP, personaApeM, personaEmail, personaTel) values (22, 'Arielle', 'Rowlands', 'Hardwin', 'ahardwinl@ebay.com', '920-103-6370');
insert into Persona (personaId, personaNom, personaApeP, personaApeM, personaEmail, personaTel) values (23, 'Yuri', 'Piatek', 'Chason', 'ychasonm@flickr.com', '580-282-8000');
insert into Persona (personaId, personaNom, personaApeP, personaApeM, personaEmail, personaTel) values (24, 'Ashlan', 'Janosevic', 'Makeswell', 'amakeswelln@springer.com', '779-247-5743');
insert into Persona (personaId, personaNom, personaApeP, personaApeM, personaEmail, personaTel) values (25, 'Sherri', 'Cavalier', 'Meatyard', 'smeatyardo@artisteer.com', '342-345-9189');
insert into Persona (personaId, personaNom, personaApeP, personaApeM, personaEmail, personaTel) values (26, 'Aldridge', 'Doore', 'McKelvie', 'amckelviep@google.es', '641-465-9277');
insert into Persona (personaId, personaNom, personaApeP, personaApeM, personaEmail, personaTel) values (27, 'Malinde', 'Melloy', 'Necrews', 'mnecrewsq@usda.gov', '942-445-4618');
insert into Persona (personaId, personaNom, personaApeP, personaApeM, personaEmail, personaTel) values (28, 'Humfrey', 'Lars', 'Dines', 'hdinesr@icq.com', '706-649-1147');
insert into Persona (personaId, personaNom, personaApeP, personaApeM, personaEmail, personaTel) values (29, 'Anitra', 'Talby', 'Byatt', 'abyatts@slashdot.org', '419-273-5546');
insert into Persona (personaId, personaNom, personaApeP, personaApeM, personaEmail, personaTel) values (30, 'Koralle', 'Murkitt', 'Meneghelli', 'kmeneghellit@vk.com', '122-900-6065');
insert into Persona (personaId, personaNom, personaApeP, personaApeM, personaEmail, personaTel) values (31, 'Eziechiele', 'Nickoles', 'Hedgeley', 'ehedgeleyu@cnet.com', '433-498-2034');
insert into Persona (personaId, personaNom, personaApeP, personaApeM, personaEmail, personaTel) values (32, 'Viva', 'de Guise', 'Smee', 'vsmeev@google.fr', '810-561-2374');
insert into Persona (personaId, personaNom, personaApeP, personaApeM, personaEmail, personaTel) values (33, 'Lucais', 'Boules', 'Boud', 'lboudw@globo.com', '796-746-1272');
insert into Persona (personaId, personaNom, personaApeP, personaApeM, personaEmail, personaTel) values (34, 'Honoria', 'Dongate', 'Bisgrove', 'hbisgrovex@foxnews.com', '196-687-6852');
insert into Persona (personaId, personaNom, personaApeP, personaApeM, personaEmail, personaTel) values (35, 'Fedora', 'Vannini', 'Spragg', 'fspraggy@posterous.com', '120-552-4358');
insert into Persona (personaId, personaNom, personaApeP, personaApeM, personaEmail, personaTel) values (36, 'Selie', 'Woollard', 'Vitet', 'svitetz@archive.org', '768-622-3762');
insert into Persona (personaId, personaNom, personaApeP, personaApeM, personaEmail, personaTel) values (37, 'Carie', 'Hierro', 'Hollingby', 'chollingby10@usatoday.com', '187-994-9598');
insert into Persona (personaId, personaNom, personaApeP, personaApeM, personaEmail, personaTel) values (38, 'Sophia', 'Entreis', 'Hake', 'shake11@discuz.net', '549-294-7542');
insert into Persona (personaId, personaNom, personaApeP, personaApeM, personaEmail, personaTel) values (39, 'Alena', 'Phidgin', 'Simonnot', 'asimonnot12@mtv.com', '138-184-9575');
insert into Persona (personaId, personaNom, personaApeP, personaApeM, personaEmail, personaTel) values (40, 'Bryon', 'Errichi', 'Trenam', 'btrenam13@usatoday.com', '453-326-6454');
insert into Persona (personaId, personaNom, personaApeP, personaApeM, personaEmail, personaTel) values (41, 'Lucille', 'Lomaz', 'Balfe', 'lbalfe14@hao123.com', '371-135-1970');
insert into Persona (personaId, personaNom, personaApeP, personaApeM, personaEmail, personaTel) values (42, 'Winfield', 'Ollerearnshaw', 'Cogger', 'wcogger15@vk.com', '564-126-0734');
insert into Persona (personaId, personaNom, personaApeP, personaApeM, personaEmail, personaTel) values (43, 'Hagen', 'Weetch', 'Fidal', 'hfidal16@eepurl.com', '468-470-3995');
insert into Persona (personaId, personaNom, personaApeP, personaApeM, personaEmail, personaTel) values (44, 'Pearle', 'Wiltshear', 'Bestiman', 'pbestiman17@geocities.com', '177-694-1032');
insert into Persona (personaId, personaNom, personaApeP, personaApeM, personaEmail, personaTel) values (45, 'Imojean', 'Brashaw', 'Beville', 'ibeville18@posterous.com', '697-729-0251');
insert into Persona (personaId, personaNom, personaApeP, personaApeM, personaEmail, personaTel) values (46, 'Doug', 'Stallworth', 'Slocombe', 'dslocombe19@engadget.com', '697-536-7928');
insert into Persona (personaId, personaNom, personaApeP, personaApeM, personaEmail, personaTel) values (47, 'Arabella', 'Barry', 'Puttnam', 'aputtnam1a@smugmug.com', '242-308-5472');
insert into Persona (personaId, personaNom, personaApeP, personaApeM, personaEmail, personaTel) values (48, 'Antoinette', 'Dederich', 'Foxall', 'afoxall1b@ocn.ne.jp', '791-274-3901');
insert into Persona (personaId, personaNom, personaApeP, personaApeM, personaEmail, personaTel) values (49, 'Keary', 'Blasiak', 'Antonoyev', 'kantonoyev1c@google.co.uk', '736-587-2152');
insert into Persona (personaId, personaNom, personaApeP, personaApeM, personaEmail, personaTel) values (50, 'Sancho', 'Brouncker', 'Garriock', 'sgarriock1d@sogou.com', '821-326-0096');
insert into Persona (personaId, personaNom, personaApeP, personaApeM, personaEmail, personaTel) values (51, 'Gene', 'Spinola', 'Cannam', 'gcannam1e@bing.com', '302-754-8273');
insert into Persona (personaId, personaNom, personaApeP, personaApeM, personaEmail, personaTel) values (52, 'Blythe', 'Stenbridge', 'Austin', 'baustin1f@webs.com', '732-773-7244');
insert into Persona (personaId, personaNom, personaApeP, personaApeM, personaEmail, personaTel) values (53, 'Terri-jo', 'Burde', 'Giacopini', 'tgiacopini1g@marketwatch.com', '707-842-9996');
insert into Persona (personaId, personaNom, personaApeP, personaApeM, personaEmail, personaTel) values (54, 'Violetta', 'Aberhart', 'Kettleson', 'vkettleson1h@economist.com', '950-201-5012');
insert into Persona (personaId, personaNom, personaApeP, personaApeM, personaEmail, personaTel) values (55, 'Reina', 'Sunners', 'Hansana', 'rhansana1i@behance.net', '974-911-0309');
insert into Persona (personaId, personaNom, personaApeP, personaApeM, personaEmail, personaTel) values (56, 'Ransom', 'McCall', 'Van Bruggen', 'rvanbruggen1j@imdb.com', '390-986-7137');
insert into Persona (personaId, personaNom, personaApeP, personaApeM, personaEmail, personaTel) values (57, 'Jackquelin', 'Metson', 'Whittingham', 'jwhittingham1k@blogger.com', '113-840-7828');
insert into Persona (personaId, personaNom, personaApeP, personaApeM, personaEmail, personaTel) values (58, 'Dorice', 'Tremmel', 'Pennycook', 'dpennycook1l@geographic.com', '320-465-4040');
insert into Persona (personaId, personaNom, personaApeP, personaApeM, personaEmail, personaTel) values (59, 'Monah', 'Cunde', 'Hubbins', 'mhubbins1m@google.com.hk', '326-705-3128');
insert into Persona (personaId, personaNom, personaApeP, personaApeM, personaEmail, personaTel) values (60, 'Shamus', 'Digan', 'Rosenstiel', 'srosenstiel1n@stanford.edu', '502-641-6809');
insert into Persona (personaId, personaNom, personaApeP, personaApeM, personaEmail, personaTel) values (61, 'Gram', 'Kemell', 'Hebden', 'ghebden1o@jiathis.com', '341-307-2450');
insert into Persona (personaId, personaNom, personaApeP, personaApeM, personaEmail, personaTel) values (62, 'Woodrow', 'Redborn', 'Mont', 'wmont1p@house.gov', '621-184-1219');
insert into Persona (personaId, personaNom, personaApeP, personaApeM, personaEmail, personaTel) values (63, 'Brianna', 'Dennis', 'Riseborough', 'briseborough1q@usnews.com', '773-811-0744');
insert into Persona (personaId, personaNom, personaApeP, personaApeM, personaEmail, personaTel) values (64, 'Richart', 'Dupoy', 'Wey', 'rwey1r@wufoo.com', '176-807-6172');
insert into Persona (personaId, personaNom, personaApeP, personaApeM, personaEmail, personaTel) values (65, 'Duke', 'Lago', 'Ingolotti', 'dingolotti1s@redcross.org', '605-877-6311');
insert into Persona (personaId, personaNom, personaApeP, personaApeM, personaEmail, personaTel) values (66, 'Caritta', 'Ellph', 'Attenborrow', 'cattenborrow1t@home.pl', '971-359-8706');
insert into Persona (personaId, personaNom, personaApeP, personaApeM, personaEmail, personaTel) values (67, 'Rowena', 'Kraut', 'Antat', 'rantat1u@pagesperso-orange.fr', '755-570-8952');
insert into Persona (personaId, personaNom, personaApeP, personaApeM, personaEmail, personaTel) values (68, 'Benji', 'Harkes', 'Stiebler', 'bstiebler1v@java.com', '465-927-1562');
insert into Persona (personaId, personaNom, personaApeP, personaApeM, personaEmail, personaTel) values (69, 'Sibbie', 'Burgise', 'Depka', 'sdepka1w@jugem.jp', '468-156-8842');
insert into Persona (personaId, personaNom, personaApeP, personaApeM, personaEmail, personaTel) values (70, 'Judah', 'Simla', 'Vigus', 'jvigus1x@sourceforge.net', '726-329-1995');
insert into Persona (personaId, personaNom, personaApeP, personaApeM, personaEmail, personaTel) values (71, 'Jilli', 'Bootman', 'Golsworthy', 'jgolsworthy1y@digg.com', '893-728-2478');
insert into Persona (personaId, personaNom, personaApeP, personaApeM, personaEmail, personaTel) values (72, 'Damon', 'Whitty', 'Batie', 'dbatie1z@mac.com', '781-640-7739');
insert into Persona (personaId, personaNom, personaApeP, personaApeM, personaEmail, personaTel) values (73, 'Elke', 'Coleby', 'Mundow', 'emundow20@google.com.br', '998-573-2573');
insert into Persona (personaId, personaNom, personaApeP, personaApeM, personaEmail, personaTel) values (74, 'August', 'Emblin', 'Cabell', 'acabell21@networksolutions.com', '360-597-9097');
insert into Persona (personaId, personaNom, personaApeP, personaApeM, personaEmail, personaTel) values (75, 'Jamie', 'Everingham', 'McKendry', 'jmckendry22@jugem.jp', '602-795-6223');



select * from cliente;
insert into Cliente (clienteId, personaId, clienteFechaNa) values (1, 1, '1990-05-24');
insert into Cliente (clienteId, personaId, clienteFechaNa) values (2, 2, '1990-04-06');
insert into Cliente (clienteId, personaId, clienteFechaNa) values (3, 3, '1991-06-03');
insert into Cliente (clienteId, personaId, clienteFechaNa) values (4, 4, '1990-12-08');
insert into Cliente (clienteId, personaId, clienteFechaNa) values (5, 5, '1980-11-12');
insert into Cliente (clienteId, personaId, clienteFechaNa) values (6, 6, '1980-01-01');
insert into Cliente (clienteId, personaId, clienteFechaNa) values (7, 7, '1990-06-24');
insert into Cliente (clienteId, personaId, clienteFechaNa) values (8, 8, '1980-01-15');
insert into Cliente (clienteId, personaId, clienteFechaNa) values (9, 9, '1970-05-05');
insert into Cliente (clienteId, personaId, clienteFechaNa) values (10, 10, '1996-05-21');
insert into Cliente (clienteId, personaId, clienteFechaNa) values (11, 11, '2001-02-09');
insert into Cliente (clienteId, personaId, clienteFechaNa) values (12, 12, '2000-02-13');
insert into Cliente (clienteId, personaId, clienteFechaNa) values (13, 13, '2000-02-24');
insert into Cliente (clienteId, personaId, clienteFechaNa) values (14, 14, '2001-09-13');
insert into Cliente (clienteId, personaId, clienteFechaNa) values (15, 15, '2000-03-09');
insert into Cliente (clienteId, personaId, clienteFechaNa) values (16, 16, '1990-04-25');
insert into Cliente (clienteId, personaId, clienteFechaNa) values (17, 17, '1994-09-12');
insert into Cliente (clienteId, personaId, clienteFechaNa) values (18, 18, '1998-06-03');
insert into Cliente (clienteId, personaId, clienteFechaNa) values (19, 19, '1998-03-09');
insert into Cliente (clienteId, personaId, clienteFechaNa) values (20, 20, '1974-06-04');
insert into Cliente (clienteId, personaId, clienteFechaNa) values (21, 21, '1969-09-08');
insert into Cliente (clienteId, personaId, clienteFechaNa) values (22, 22, '1998-03-07');
insert into Cliente (clienteId, personaId, clienteFechaNa) values (23, 23, '1997-04-03');
insert into Cliente (clienteId, personaId, clienteFechaNa) values (24, 24, '1990-07-23');
insert into Cliente (clienteId, personaId, clienteFechaNa) values (25, 25, '1967-04-12');





select * from trabajador;

insert into Trabajador (trabRFC, personaId, trabFechaNac, trabDireccion, trabCargo, trabSueldo, trabContraseña, trabActivo) values (1, 26, '1990-02-18', '967 Stoughton Park', 'Atencion a clientes', 9564.17, 'ZR2XdwLksRM5', 1);
insert into Trabajador (trabRFC, personaId, trabFechaNac, trabDireccion, trabCargo, trabSueldo, trabContraseña, trabActivo) values (2, 27, '1996-05-10', '3 Killdeer Way', 'Cajero', 2669.62, 'mHNV5Qg', 1);
insert into Trabajador (trabRFC, personaId, trabFechaNac, trabDireccion, trabCargo, trabSueldo, trabContraseña, trabActivo) values (3, 28, '1968-06-27', '132 School Place', 'Almacenista', 2317.11, 'ihU3UTtbBz', 1);
insert into Trabajador (trabRFC, personaId, trabFechaNac, trabDireccion, trabCargo, trabSueldo, trabContraseña, trabActivo) values (4, 29, '1986-11-05', '09766 Steensland Lane', 'Gerente', 3149.68, '0SkO2S', 1);
insert into Trabajador (trabRFC, personaId, trabFechaNac, trabDireccion, trabCargo, trabSueldo, trabContraseña, trabActivo) values (5, 30, '1990-09-15', '626 Nancy Terrace', 'Oficinista', 8568.2, 'AMOhjtPme', 1);
insert into Trabajador (trabRFC, personaId, trabFechaNac, trabDireccion, trabCargo, trabSueldo, trabContraseña, trabActivo) values (6, 31, '1990-01-07', '616 Kings Court', 'Gerente', 9060.08, '4jYXKEc', 1);
insert into Trabajador (trabRFC, personaId, trabFechaNac, trabDireccion, trabCargo, trabSueldo, trabContraseña, trabActivo) values (7, 32, '2000-08-23', '370 Lien Drive', 'Atencion a clientes', 5362.42, 'Yxpf1LBlB', 1);
insert into Trabajador (trabRFC, personaId, trabFechaNac, trabDireccion, trabCargo, trabSueldo, trabContraseña, trabActivo) values (8, 33, '2001-03-06', '7 Pawling Circle', 'Almacenista', 8920.84, 'kFfCTK6R6itp', 1);
insert into Trabajador (trabRFC, personaId, trabFechaNac, trabDireccion, trabCargo, trabSueldo, trabContraseña, trabActivo) values (9, 34, '2000-12-30', '9 Brickson Park Drive', 'Cajero', 9071.56, 'taxuv28JtQf1', 1);
insert into Trabajador (trabRFC, personaId, trabFechaNac, trabDireccion, trabCargo, trabSueldo, trabContraseña, trabActivo) values (10, 35, '1990-02-12', '98451 Anderson Parkway', 'Cajero', 7438.18, 'ldsEyDx', 1);
insert into Trabajador (trabRFC, personaId, trabFechaNac, trabDireccion, trabCargo, trabSueldo, trabContraseña, trabActivo) values (11, 36, '1990-08-21', '02 Ramsey Pass', 'Oficinista', 3049.15, 'zWELu1gxj', 1);
insert into Trabajador (trabRFC, personaId, trabFechaNac, trabDireccion, trabCargo, trabSueldo, trabContraseña, trabActivo) values (12, 37, '1997-12-13', '33177 Waubesa Avenue', 'Atencion a clientes', 7576.3, 'pgIJY8UvZ', 1);
insert into Trabajador (trabRFC, personaId, trabFechaNac, trabDireccion, trabCargo, trabSueldo, trabContraseña, trabActivo) values (13, 38, '1986-12-01', '28 Crescent Oaks Place', 'Cajero', 8706.84, 'Rd2rF8toG5', 1);
insert into Trabajador (trabRFC, personaId, trabFechaNac, trabDireccion, trabCargo, trabSueldo, trabContraseña, trabActivo) values (14, 39, '1986-09-04', '3353 Vahlen Park', 'Almacenista', 5091.33, '77g09IOQS3', 1);
insert into Trabajador (trabRFC, personaId, trabFechaNac, trabDireccion, trabCargo, trabSueldo, trabContraseña, trabActivo) values (15, 40, '1993-08-29', '5 Farmco Point', 'Atencion a clientes', 5270.23, 'kEfEooWI8Ym', 1);
insert into Trabajador (trabRFC, personaId, trabFechaNac, trabDireccion, trabCargo, trabSueldo, trabContraseña, trabActivo) values (16, 41, '1997-10-27', '46534 Forest Pass', 'Cajero', 8468.36, 'MTJgvUQ', 1);
insert into Trabajador (trabRFC, personaId, trabFechaNac, trabDireccion, trabCargo, trabSueldo, trabContraseña, trabActivo) values (17, 42, '2000-04-21', '9 Goodland Trail', 'Oficinista', 6010.74, 'bFnvQKiql', 1);
insert into Trabajador (trabRFC, personaId, trabFechaNac, trabDireccion, trabCargo, trabSueldo, trabContraseña, trabActivo) values (18, 43, '2000-06-25', '32 Center Road', 'Cajero', 2161.63, '30CjVb', 1);
insert into Trabajador (trabRFC, personaId, trabFechaNac, trabDireccion, trabCargo, trabSueldo, trabContraseña, trabActivo) values (19, 44, '2001-01-11', '1956 Northview Parkway', 'Almacenista', 7689.38, 'HZXZsO',1);
insert into Trabajador (trabRFC, personaId, trabFechaNac, trabDireccion, trabCargo, trabSueldo, trabContraseña, trabActivo) values (20, 45, '2000-06-11', '48 Sugar Road', 'Cajero', 7323.7, 'AXocsxUhpRxZ',1);
insert into Trabajador (trabRFC, personaId, trabFechaNac, trabDireccion, trabCargo, trabSueldo, trabContraseña, trabActivo) values (21, 46, '1968-02-08', '6 Hauk Center', 'Atencion a clientes', 8315.43, 'ey1GEAZxqU',1);
insert into Trabajador (trabRFC, personaId, trabFechaNac, trabDireccion, trabCargo, trabSueldo, trabContraseña, trabActivo) values (22, 47, '1965-06-08', '19 Riverside Plaza', 'Cajero', 5448.3, 'O1Mwhf',1);
insert into Trabajador (trabRFC, personaId, trabFechaNac, trabDireccion, trabCargo, trabSueldo, trabContraseña, trabActivo) values (23, 48, '1998-04-05', '38132 Lighthouse Bay Point', 'Oficinista', 7706.55, 'Cy7HUiO6LWfz',1);
insert into Trabajador (trabRFC, personaId, trabFechaNac, trabDireccion, trabCargo, trabSueldo, trabContraseña, trabActivo) values (24, 49, '1990-12-06', '77741 Ronald Regan Crossing', 'Gerente', 2924.77, 'XAatTBi',1);
insert into Trabajador (trabRFC, personaId, trabFechaNac, trabDireccion, trabCargo, trabSueldo, trabContraseña, trabActivo) values (25, 50, '1979-07-23', '705 Corry Place', 'Cajero', 2885.27, 'SXrz7UEuvMX6',1);




select * from Proveedor;
insert into Proveedor (proveedorId, personaId, proveedorEmpre, proveedorActivo) values (1, 51, 'Thoughtmix', 1);
insert into Proveedor (proveedorId, personaId, proveedorEmpre, proveedorActivo) values (2, 52, 'Brightdog',1);
insert into Proveedor (proveedorId, personaId, proveedorEmpre, proveedorActivo) values (3, 53, 'Topiclounge',1);
insert into Proveedor (proveedorId, personaId, proveedorEmpre, proveedorActivo) values (4, 54, 'InnoZ',1);
insert into Proveedor (proveedorId, personaId, proveedorEmpre, proveedorActivo) values (5, 55, 'Mydeo',1);
insert into Proveedor (proveedorId, personaId, proveedorEmpre, proveedorActivo) values (6, 56, 'Skidoo',1);
insert into Proveedor (proveedorId, personaId, proveedorEmpre, proveedorActivo) values (7, 57, 'Skimia',1);
insert into Proveedor (proveedorId, personaId, proveedorEmpre, proveedorActivo) values (8, 58, 'Flipopia',1);
insert into Proveedor (proveedorId, personaId, proveedorEmpre, proveedorActivo) values (9, 59, 'Bubbletube',1);
insert into Proveedor (proveedorId, personaId, proveedorEmpre, proveedorActivo) values (10, 60, 'Gabcube',1);
insert into Proveedor (proveedorId, personaId, proveedorEmpre, proveedorActivo) values (11, 61, 'Babblestorm',1);
insert into Proveedor (proveedorId, personaId, proveedorEmpre, proveedorActivo) values (12, 62, 'Kwimbee',1);
insert into Proveedor (proveedorId, personaId, proveedorEmpre, proveedorActivo) values (13, 63, 'Yakijo',1);
insert into Proveedor (proveedorId, personaId, proveedorEmpre, proveedorActivo) values (14, 64, 'Centimia',1);
insert into Proveedor (proveedorId, personaId, proveedorEmpre, proveedorActivo) values (15, 65, 'Twitternation',1);
insert into Proveedor (proveedorId, personaId, proveedorEmpre, proveedorActivo) values (16, 66, 'Dynabox',1);
insert into Proveedor (proveedorId, personaId, proveedorEmpre, proveedorActivo) values (17, 67, 'Dabvine',1);
insert into Proveedor (proveedorId, personaId, proveedorEmpre, proveedorActivo) values (18, 68, 'Brightbean',1);
insert into Proveedor (proveedorId, personaId, proveedorEmpre, proveedorActivo) values (19, 69, 'Jaxnation',1);
insert into Proveedor (proveedorId, personaId, proveedorEmpre, proveedorActivo) values (20, 70, 'Tazzy',1);
insert into Proveedor (proveedorId, personaId, proveedorEmpre, proveedorActivo) values (21, 71, 'Vitz',1);
insert into Proveedor (proveedorId, personaId, proveedorEmpre, proveedorActivo) values (22, 72, 'Meedoo',1);
insert into Proveedor (proveedorId, personaId, proveedorEmpre, proveedorActivo) values (23, 73, 'Eamia',1);
insert into Proveedor (proveedorId, personaId, proveedorEmpre, proveedorActivo) values (24, 74, 'Quinu',1);
insert into Proveedor (proveedorId, personaId, proveedorEmpre, proveedorActivo) values (25, 75, 'Skilith',1);




insert into Tarjeta (tarjeNum, clienteId, trabRFC, tarjeDiaAlta, tarjeExpiracion, tarjePuntos,tarjeActivo) values (1, 13, 17, '2020-05-27', '2023-05-27', 781.5726,1);
insert into Tarjeta (tarjeNum, clienteId, trabRFC, tarjeDiaAlta, tarjeExpiracion, tarjePuntos, tarjeActivo) values (2, 11, 14, '2021-03-14', '2024-03-14', 0,1);
insert into Tarjeta (tarjeNum, clienteId, trabRFC, tarjeDiaAlta, tarjeExpiracion, tarjePuntos, tarjeActivo) values (3, 9, 15, '2020-01-09', '2023-01-09', 3592,1);
insert into Tarjeta (tarjeNum, clienteId, trabRFC, tarjeDiaAlta, tarjeExpiracion, tarjePuntos, tarjeActivo) values (4, 4, 15, '2021-09-21', '2024-09-21', 5581,1);
insert into Tarjeta (tarjeNum, clienteId, trabRFC, tarjeDiaAlta, tarjeExpiracion, tarjePuntos, tarjeActivo) values (5, 16, 2, '2021-05-14', '2024-05-14', 35,1);
insert into Tarjeta (tarjeNum, clienteId, trabRFC, tarjeDiaAlta, tarjeExpiracion, tarjePuntos, tarjeActivo) values (6, 17, 7, '2020-10-02', '2023-10-02', 0,1);
insert into Tarjeta (tarjeNum, clienteId, trabRFC, tarjeDiaAlta, tarjeExpiracion, tarjePuntos, tarjeActivo) values (7, 7, 25, '2020-03-27', '2023-03-27', 405,1);
insert into Tarjeta (tarjeNum, clienteId, trabRFC, tarjeDiaAlta, tarjeExpiracion, tarjePuntos, tarjeActivo) values (8, 2, 2, '2020-01-16', '2023-01-16', 0,1);
insert into Tarjeta (tarjeNum, clienteId, trabRFC, tarjeDiaAlta, tarjeExpiracion, tarjePuntos, tarjeActivo) values (9, 8, 7, '2020-05-31', '2023-05-31', 0,1);
insert into Tarjeta (tarjeNum, clienteId, trabRFC, tarjeDiaAlta, tarjeExpiracion, tarjePuntos, tarjeActivo) values (10, 24, 25, '2019-12-14', '2022-12-14', 2795,1);
insert into Tarjeta (tarjeNum, clienteId, trabRFC, tarjeDiaAlta, tarjeExpiracion, tarjePuntos, tarjeActivo) values (11, 4, 2, '2020-08-06', '2023-08-06', 5581,1);
insert into Tarjeta (tarjeNum, clienteId, trabRFC, tarjeDiaAlta, tarjeExpiracion, tarjePuntos, tarjeActivo) values (12, 14, 22, '2020-05-19', '2020-05-19', 976,1);
insert into Tarjeta (tarjeNum, clienteId, trabRFC, tarjeDiaAlta, tarjeExpiracion, tarjePuntos, tarjeActivo) values (13, 23, 21, '2021-02-27', '2024-02-27', 251,1);
insert into Tarjeta (tarjeNum, clienteId, trabRFC, tarjeDiaAlta, tarjeExpiracion, tarjePuntos, tarjeActivo) values (14, 5, 3, '2020-07-22', '2023-07-22', 0,1);
insert into Tarjeta (tarjeNum, clienteId, trabRFC, tarjeDiaAlta, tarjeExpiracion, tarjePuntos, tarjeActivo) values (15, 15, 25, '2020-03-03', '2023-03-03', 0,1);
insert into Tarjeta (tarjeNum, clienteId, trabRFC, tarjeDiaAlta, tarjeExpiracion, tarjePuntos, tarjeActivo) values (16, 19, 6, '2020-08-14', '2023-08-14', 0,1);
insert into Tarjeta (tarjeNum, clienteId, trabRFC, tarjeDiaAlta, tarjeExpiracion, tarjePuntos, tarjeActivo) values (17, 15, 16, '2020-02-15', '2023-02-15', 0,1);
insert into Tarjeta (tarjeNum, clienteId, trabRFC, tarjeDiaAlta, tarjeExpiracion, tarjePuntos, tarjeActivo) values (18, 11, 4, '2021-05-09', '2021-05-09', 0,1);
insert into Tarjeta (tarjeNum, clienteId, trabRFC, tarjeDiaAlta, tarjeExpiracion, tarjePuntos, tarjeActivo) values (19, 7, 5, '2021-08-24', '2021-08-24', 405,1);
insert into Tarjeta (tarjeNum, clienteId, trabRFC, tarjeDiaAlta, tarjeExpiracion, tarjePuntos, tarjeActivo) values (20, 10, 24, '2020-11-12', '2023-11-12', 402,1);
insert into Tarjeta (tarjeNum, clienteId, trabRFC, tarjeDiaAlta, tarjeExpiracion, tarjePuntos, tarjeActivo) values (21, 11, 3, '2020-07-04', '2023-07-04', 0,1);
insert into Tarjeta (tarjeNum, clienteId, trabRFC, tarjeDiaAlta, tarjeExpiracion, tarjePuntos, tarjeActivo) values (22, 13, 23, '2020-08-03', '2023-08-03', 781,1);
insert into Tarjeta (tarjeNum, clienteId, trabRFC, tarjeDiaAlta, tarjeExpiracion, tarjePuntos, tarjeActivo) values (23, 21, 4, '2020-07-10', '2020-07-10',0,1);
insert into Tarjeta (tarjeNum, clienteId, trabRFC, tarjeDiaAlta, tarjeExpiracion, tarjePuntos, tarjeActivo) values (24, 9, 2, '2021-09-04', '2024-09-04', 3592,1);
insert into Tarjeta (tarjeNum, clienteId, trabRFC, tarjeDiaAlta, tarjeExpiracion, tarjePuntos, tarjeActivo) values (25, 13, 19, '2021-01-25', '2024-01-25', 781,1);


insert into Producto (productoId, productoNom, productoPrecio, proveedorId, productoDep, inventarioCant,productoActivo) values (1, 'Calentador', 1200.73, 1, 'Plomeria', 84,1);
insert into Producto (productoId, productoNom, productoPrecio, proveedorId, productoDep, inventarioCant,productoActivo) values (2, 'Cisterna', 972.51, 2, 'Plomeria', 900,1);
insert into Producto (productoId, productoNom, productoPrecio, proveedorId, productoDep, inventarioCant,productoActivo) values (3, 'Bomba de agua', 166.3, 3, 'Plomeria', 932,1);
insert into Producto (productoId, productoNom, productoPrecio, proveedorId, productoDep, inventarioCant,productoActivo) values (4, 'Grifo de cocina', 849.31, 4, 'Plomeria', 582,1);
insert into Producto (productoId, productoNom, productoPrecio, proveedorId, productoDep, inventarioCant,productoActivo) values (5, 'Tuberia', 183.46, 5, 'Plomeria', 419,1);
insert into Producto (productoId, productoNom, productoPrecio, proveedorId, productoDep, inventarioCant,productoActivo) values (6, 'Minisplit', 2855.99, 6, 'Ventilacion y calefacion', 389,1);
insert into Producto (productoId, productoNom, productoPrecio, proveedorId, productoDep, inventarioCant,productoActivo) values (7, 'Ventilador', 449.99, 7, 'Ventilacion y calefacion', 693,1);
insert into Producto (productoId, productoNom, productoPrecio, proveedorId, productoDep, inventarioCant,productoActivo) values (8, 'Ventilador de techo', 685.59,8, 'Ventilacion y calefacion', 40,1);
insert into Producto (productoId, productoNom, productoPrecio, proveedorId, productoDep, inventarioCant,productoActivo) values (9, 'Refrigerador', 410.09, 9, 'Linea blanca y cocinas', 539,1);
insert into Producto (productoId, productoNom, productoPrecio, proveedorId, productoDep, inventarioCant,productoActivo) values (10, 'Campanas de cocina', 140.04, 10, 'Linea blanca y cocinas', 767,1);
insert into Producto (productoId, productoNom, productoPrecio, proveedorId, productoDep, inventarioCant,productoActivo) values (11, 'Parrillas', 99.55, 11, 'Linea blanca y cocinas', 641,1);
insert into Producto (productoId, productoNom, productoPrecio, proveedorId, productoDep, inventarioCant,productoActivo) values (12, 'Horno', 616.92, 12, 'Linea blanca y cocinas', 397,1);
insert into Producto (productoId, productoNom, productoPrecio, proveedorId, productoDep, inventarioCant,productoActivo) values (13, 'Estufa', 525.14, 13, 'Linea blanca y cocinas', 475,1);
insert into Producto (productoId, productoNom, productoPrecio, proveedorId, productoDep, inventarioCant,productoActivo) values (14, 'Microondas', 126.89, 14, 'Linea blanca y cocinas', 442,1);
insert into Producto (productoId, productoNom, productoPrecio, proveedorId, productoDep, inventarioCant,productoActivo) values (15, 'Triturador', 1167.56, 15, 'Linea blanca y cocinas', 130,1);
insert into Producto (productoId, productoNom, productoPrecio, proveedorId, productoDep, inventarioCant,productoActivo) values (16, 'Licuadora', 592.56, 16, 'Linea blanca y cocinas', 30,1);
insert into Producto (productoId, productoNom, productoPrecio, proveedorId, productoDep, inventarioCant,productoActivo) values (17, 'Regadera', 50.24, 17, 'Baños', 153,1);
insert into Producto (productoId, productoNom, productoPrecio, proveedorId, productoDep, inventarioCant,productoActivo) values (18, 'Llaves para baño', 960.23, 18, 'Baños', 338,1);
insert into Producto (productoId, productoNom, productoPrecio, proveedorId, productoDep, inventarioCant,productoActivo) values (19, 'Sanitario', 750.04, 19, 'Baños', 864,1);
insert into Producto (productoId, productoNom, productoPrecio, proveedorId, productoDep, inventarioCant,productoActivo) values (20, 'Caja de herramientas', 84.25, 20, 'Herramientas', 828,1);
insert into Producto (productoId, productoNom, productoPrecio, proveedorId, productoDep, inventarioCant,productoActivo) values (21, 'Gabinete', 719.56, 21, 'Herramientas', 60,1);
insert into Producto (productoId, productoNom, productoPrecio, proveedorId, productoDep, inventarioCant,productoActivo) values (22, 'Compresor', 358.29, 22, 'Herramientas', 215,1);
insert into Producto (productoId, productoNom, productoPrecio, proveedorId, productoDep, inventarioCant,productoActivo) values (23, 'Martillo', 130.82, 23, 'Herramientas', 922,1);
insert into Producto (productoId, productoNom, productoPrecio, proveedorId, productoDep, inventarioCant,productoActivo) values (24, 'Puerta de seguridad', 751.74, 24, 'Puertas y ventanas', 437,1);
insert into Producto (productoId, productoNom, productoPrecio, proveedorId, productoDep, inventarioCant,productoActivo) values (25, 'Protector de ventana', 657.1, 25, 'Puertas y ventanas', 572,1);


select*from Ventas;
insert into Ventas (ventaNum, clienteId, tarjeNum, trabRFC, ventaFecha, ventaTotal, puntosGastados, efectivo, cambio) values (1, 13, 1, '1', '2021-9-26', 5547.21, 0, 5547.21, 0);
insert into Ventas (ventaNum, clienteId, tarjeNum, trabRFC, ventaFecha, ventaTotal, puntosGastados, efectivo, cambio) values (2, 11, 2, '2', '2021-10-12', 2197.52, 0, 2197.52, 0);
insert into Ventas (ventaNum, clienteId, tarjeNum, trabRFC, ventaFecha, ventaTotal, puntosGastados, efectivo, cambio) values (3, 9, 3, '1', '2021-06-29', 5775.43, 0, 5775.43, 0); 
insert into Ventas (ventaNum, clienteId, tarjeNum, trabRFC, ventaFecha, ventaTotal, puntosGastados, efectivo, cambio) values (4, 4, 4, '12', '2021-05-26', 1349.97, 0, 13450, 0.03); 
insert into Ventas (ventaNum, clienteId, tarjeNum, trabRFC, ventaFecha, ventaTotal, puntosGastados, efectivo, cambio) values (5, 16, 5,'21', '2021-11-13', 184.73, 0, 185, 0.23); 
insert into Ventas (ventaNum, clienteId, tarjeNum, trabRFC, ventaFecha, ventaTotal, puntosGastados, efectivo, cambio) values (6, 17, 6, '15', '2021-09-17', 719.56, 0, 720, 0.44); 
insert into Ventas (ventaNum, clienteId, tarjeNum, trabRFC, ventaFecha, ventaTotal, puntosGastados, efectivo, cambio) values (7, 7, 7, '8', '2021-07-19', 657.10, 0, 660, 2.90); 
insert into Ventas (ventaNum, clienteId, tarjeNum, trabRFC, ventaFecha, ventaTotal, puntosGastados, efectivo, cambio) values (8, 2, 8, '9', '2021-06-12', 1172.99, 0, 1173, 0.01); 
insert into Ventas (ventaNum, clienteId, tarjeNum, trabRFC, ventaFecha, ventaTotal, puntosGastados, efectivo, cambio) values (9, 8,  9, '22', '2021-05-22', 99.55, 0, 100, 0.45); 
insert into Ventas (ventaNum, clienteId, tarjeNum, trabRFC, ventaFecha, ventaTotal, puntosGastados, efectivo, cambio) values (10, 24, 10, '12', '2021-04-11',1268.9, 0, 1268.9, 0); 
insert into Ventas (ventaNum, clienteId, tarjeNum, trabRFC, ventaFecha, ventaTotal, puntosGastados, efectivo, cambio) values (11, 4, 11, '11', '2021-03-21', 7206.32, 0, 7206.32, 0); 
insert into Ventas (ventaNum, clienteId, tarjeNum, trabRFC, ventaFecha, ventaTotal, puntosGastados, efectivo, cambio) values (12, 14, 12, '10', '2021-03-25', 28559.9, 0, 28559.9, 0); 
insert into Ventas (ventaNum, clienteId, tarjeNum, trabRFC, ventaFecha, ventaTotal, puntosGastados, efectivo, cambio) values (13, 23, 13, '1', '2021-02-24', 525.14, 0, 525.14, 0); 
insert into Ventas (ventaNum, clienteId, tarjeNum, trabRFC, ventaFecha, ventaTotal, puntosGastados, efectivo, cambio) values (14, 5, 14, '2', '2021-10-23', 4049.91, 0, 4049.91, 0); 
insert into Ventas (ventaNum, clienteId, tarjeNum, trabRFC, ventaFecha, ventaTotal, puntosGastados, efectivo, cambio) values (15, 15, 15, '17', '2021-01-12', 3280.72, 0, 3281, 0.28); 
insert into Ventas (ventaNum, clienteId, tarjeNum, trabRFC, ventaFecha, ventaTotal, puntosGastados, efectivo, cambio) values (16, 19, 16, '18', '2021-03-09', 4147.92, 0, 4147.92, 0); 
insert into Ventas (ventaNum, clienteId, tarjeNum, trabRFC, ventaFecha, ventaTotal, puntosGastados, efectivo, cambio) values (17, 15, 17, '13', '2021-04-06', 2880.69, 0, 2890, 9.31); 
insert into Ventas (ventaNum, clienteId, tarjeNum, trabRFC, ventaFecha, ventaTotal, puntosGastados, efectivo, cambio) values (18, 11, 18, '2', '2021-03-05', 1850.76, 0, 1850.76, 0); 
insert into Ventas (ventaNum, clienteId, tarjeNum, trabRFC, ventaFecha, ventaTotal, puntosGastados, efectivo, cambio) values (19, 7,  19, '3', '2021-08-04', 298.65, 0, 298.65, 0); 
insert into Ventas (ventaNum, clienteId, tarjeNum, trabRFC, ventaFecha, ventaTotal, puntosGastados, efectivo, cambio) values (20, 10, 20, '4', '2021-07-03', 261.64, 0, 262, 0.36); 
insert into Ventas (ventaNum, clienteId, tarjeNum, trabRFC, ventaFecha, ventaTotal, puntosGastados, efectivo, cambio) values (21, 11, 21, '5', '2021-10-02', 1433.16, 0, 1433.16, 0); 
insert into Ventas (ventaNum, clienteId, tarjeNum, trabRFC, ventaFecha, ventaTotal, puntosGastados, efectivo, cambio) values (22, 13,  22, '1', '2021-11-01', 2628.4, 0, 2628.4, 0); 
insert into Ventas (ventaNum, clienteId, tarjeNum, trabRFC, ventaFecha, ventaTotal, puntosGastados, efectivo, cambio) values (23, 21, 23, '2', '2021-05-21', 1945.02, 0, 1945.02, 0); 
insert into Ventas (ventaNum, clienteId, tarjeNum, trabRFC, ventaFecha, ventaTotal, puntosGastados, efectivo, cambio) values (24, 9, 24, '19', '2021-08-24', 410.09, 0, 410.09, 0); 
insert into Ventas (ventaNum, clienteId, tarjeNum, trabRFC, ventaFecha, ventaTotal, puntosGastados, efectivo, cambio) values (25, 13, 25, '23', '2021-01-11', 1260.61, 0, 1260.61, 0);




select*from FacturaVenta;
insert into FacturaVenta(ventaNum, productoId, ventaPrecio, ventaCant,ventActivo) values (1,1, 3602.19, 3,1);
insert into FacturaVenta(ventaNum, productoId, ventaPrecio, ventaCant,ventActivo) values (1,2, 1945.02, 2,1);
insert into FacturaVenta(ventaNum, productoId, ventaPrecio, ventaCant,ventActivo) values (2,3, 498.9, 3,1);
insert into FacturaVenta(ventaNum, productoId, ventaPrecio, ventaCant,ventActivo) values (2,4, 1698.62, 2,1);
insert into FacturaVenta(ventaNum, productoId, ventaPrecio, ventaCant,ventActivo) values (3,2, 972.51, 1,1);
insert into FacturaVenta(ventaNum, productoId, ventaPrecio, ventaCant,ventActivo) values (3,1, 4802.92, 4,1);
insert into FacturaVenta(ventaNum, productoId, ventaPrecio, ventaCant,ventActivo) values (4,7, 1349.97, 3,1);
insert into FacturaVenta(ventaNum, productoId, ventaPrecio, ventaCant,ventActivo) values (5,17, 100.48, 2,1);
insert into FacturaVenta(ventaNum, productoId, ventaPrecio, ventaCant,ventActivo) values (5,20, 84.25, 1,1);
insert into FacturaVenta(ventaNum, productoId, ventaPrecio, ventaCant,ventActivo) values (6,21, 719.56, 1,1);
insert into FacturaVenta(ventaNum, productoId, ventaPrecio, ventaCant,ventActivo) values (7,25, 657.10, 1,1);
insert into FacturaVenta(ventaNum, productoId, ventaPrecio, ventaCant,ventActivo) values (8,20, 421.25, 5,1);
insert into FacturaVenta(ventaNum, productoId, ventaPrecio, ventaCant,ventActivo) values (8,24, 751.74, 1,1);
insert into FacturaVenta(ventaNum, productoId, ventaPrecio, ventaCant,ventActivo) values (9,11, 99.55, 1,1);
insert into FacturaVenta(ventaNum, productoId, ventaPrecio, ventaCant,ventActivo) values (10,14, 1268.9, 10,1);
insert into FacturaVenta(ventaNum, productoId, ventaPrecio, ventaCant,ventActivo) values (11,15, 7005.36, 6,1);
insert into FacturaVenta(ventaNum, productoId, ventaPrecio, ventaCant,ventActivo) values (11,17, 200.96, 4,1);
insert into FacturaVenta(ventaNum, productoId, ventaPrecio, ventaCant,ventActivo) values (12,6, 28559.9, 10,1);
insert into FacturaVenta(ventaNum, productoId, ventaPrecio, ventaCant,ventActivo) values (13,13, 525.14, 1,1);
insert into FacturaVenta(ventaNum, productoId, ventaPrecio, ventaCant,ventActivo) values (14,7, 4049.91, 9,1);
insert into FacturaVenta(ventaNum, productoId, ventaPrecio, ventaCant,ventActivo) values (15,9, 3280.72, 8,1);
insert into FacturaVenta(ventaNum, productoId, ventaPrecio, ventaCant,ventActivo) values (16,15, 4147.92, 7,1);
insert into FacturaVenta(ventaNum, productoId, ventaPrecio, ventaCant,ventActivo) values (17,18, 2880.69, 3,1);
insert into FacturaVenta(ventaNum, productoId, ventaPrecio, ventaCant,ventActivo) values (18,12, 1850.76, 3,1);
insert into FacturaVenta(ventaNum, productoId, ventaPrecio, ventaCant,ventActivo) values (19,11, 298.65, 3,1);
insert into FacturaVenta(ventaNum, productoId, ventaPrecio, ventaCant,ventActivo) values (20,23, 261.64, 2,1);
insert into FacturaVenta(ventaNum, productoId, ventaPrecio, ventaCant,ventActivo) values (21,22, 1433.16, 4,1);
insert into FacturaVenta(ventaNum, productoId, ventaPrecio, ventaCant,ventActivo) values (22,25, 2628.4, 4,1);
insert into FacturaVenta(ventaNum, productoId, ventaPrecio, ventaCant,ventActivo) values (23,2, 1945.02, 2,1);
insert into FacturaVenta(ventaNum, productoId, ventaPrecio, ventaCant,ventActivo) values (24,9, 410.09, 1,1);
insert into FacturaVenta(ventaNum, productoId, ventaPrecio, ventaCant,ventActivo) values (25,9, 410.09, 1,1);
insert into FacturaVenta(ventaNum, productoId, ventaPrecio, ventaCant,ventActivo) values (25,19, 750.04, 1,1);
insert into FacturaVenta(ventaNum, productoId, ventaPrecio, ventaCant,ventActivo) values (25,17, 100.48, 2,1);




select*from pedido;
/**-----------------camabir-----------------------------*/
insert into Pedido (pedId, trabRFC, proveedorId, pedFecha, pedFechaEntrega, pedTotal,pedActivo,pedLlego) values (1, 13, 1, '2021-04-20', '2021-10-12',3602.19,1,0);
insert into Pedido (pedId, trabRFC, proveedorId, pedFecha, pedFechaEntrega, pedTotal,pedActivo,pedLlego) values (2, 8, 2, '2021-01-21', '2021-10-20',2917.53,1,0);
insert into Pedido (pedId, trabRFC, proveedorId, pedFecha, pedFechaEntrega, pedTotal,pedActivo,pedLlego) values (3, 18, 3,  '2020-12-12', '2021-10-14',1663,1,0);
insert into Pedido (pedId, trabRFC, proveedorId, pedFecha, pedFechaEntrega, pedTotal,pedActivo,pedLlego) values (4, 8, 4, '2020-11-13', '2021-10-29',25479.3,1,0);
insert into Pedido (pedId, trabRFC, proveedorId, pedFecha, pedFechaEntrega, pedTotal,pedActivo,pedLlego) values (5, 15, 5, '2021-01-02', '2021-11-21',366.92,1,0);
insert into Pedido (pedId, trabRFC, proveedorId, pedFecha, pedFechaEntrega, pedTotal,pedActivo,pedLlego) values (6, 12, 6, '2020-12-25', '2021-11-14',285599,1,0);
insert into Pedido (pedId, trabRFC, proveedorId, pedFecha, pedFechaEntrega, pedTotal,pedActivo,pedLlego) values (7, 9, 7, '2021-09-29', '2021-10-15',4499.9,1,0);
insert into Pedido (pedId, trabRFC, proveedorId, pedFecha, pedFechaEntrega, pedTotal,pedActivo,pedLlego) values (8, 20, 8, '2020-06-28', '2021-10-13',3427.95,1,0);
insert into Pedido (pedId, trabRFC, proveedorId, pedFecha, pedFechaEntrega, pedTotal,pedActivo,pedLlego) values (9, 21, 9, '2021-03-22', '2021-10-16',4100.9,1,0);
insert into Pedido (pedId, trabRFC, proveedorId, pedFecha, pedFechaEntrega, pedTotal,pedActivo,pedLlego) values (10, 7, 10, '2021-10-09', '2021-10-18',1400.4,1,0);
insert into Pedido (pedId, trabRFC, proveedorId, pedFecha, pedFechaEntrega, pedTotal,pedActivo,pedLlego) values (11, 13, 11, '2020-02-10', '2021-11-29', 597.3,1,0);
insert into Pedido (pedId, trabRFC, proveedorId, pedFecha, pedFechaEntrega, pedTotal,pedActivo,pedLlego) values (12, 24, 12, '2021-04-21', '2021-11-16',6169.2,1,0);
insert into Pedido (pedId, trabRFC, proveedorId, pedFecha, pedFechaEntrega, pedTotal,pedActivo,pedLlego) values (13, 5, 13, '2020-11-27', '2021-11-23', 5251.4,1,0);
insert into Pedido (pedId, trabRFC, proveedorId, pedFecha, pedFechaEntrega, pedTotal,pedActivo,pedLlego) values (14, 2, 14, '2021-08-13', '2021-11-18',1142.01,1,0);
insert into Pedido (pedId, trabRFC, proveedorId, pedFecha, pedFechaEntrega, pedTotal,pedActivo,pedLlego) values (15, 21, 15, '2020-10-04', '2021-10-19',9340.48,1,0);
insert into Pedido (pedId, trabRFC, proveedorId, pedFecha, pedFechaEntrega, pedTotal,pedActivo,pedLlego) values (16, 13, 16, '2020-01-11', '2021-10-29',4147.92,1,0);
insert into Pedido (pedId, trabRFC, proveedorId, pedFecha, pedFechaEntrega, pedTotal,pedActivo,pedLlego) values (17, 24, 17, '2021-06-15', '2021-10-18',150.72,1,0);
insert into Pedido (pedId, trabRFC, proveedorId, pedFecha, pedFechaEntrega, pedTotal,pedActivo,pedLlego) values (18, 10, 18, '2020-11-26', '2021-11-19',2880.69,1,0);
insert into Pedido (pedId, trabRFC, proveedorId, pedFecha, pedFechaEntrega, pedTotal,pedActivo,pedLlego) values (19, 22, 19, '2020-09-22', '2021-10-12',2250.12,1,0);
insert into Pedido (pedId, trabRFC, proveedorId, pedFecha, pedFechaEntrega, pedTotal,pedActivo,pedLlego) values (20, 9, 20, '2021-09-10', '2021-11-22',1685,1,0);
insert into Pedido (pedId, trabRFC, proveedorId, pedFecha, pedFechaEntrega, pedTotal,pedActivo,pedLlego) values (21, 25, 21, '2021-07-07', '2021-11-13',28782.4,1,0);
insert into Pedido (pedId, trabRFC, proveedorId, pedFecha, pedFechaEntrega, pedTotal,pedActivo,pedLlego) values (22, 23, 22, '2020-01-16', '2021-11-16',14331.6,1,0);
insert into Pedido (pedId, trabRFC, proveedorId, pedFecha, pedFechaEntrega, pedTotal,pedActivo,pedLlego) values (23, 5, 23, '2021-01-27', '2021-11-08',2616.4,1,0);
insert into Pedido (pedId, trabRFC, proveedorId, pedFecha, pedFechaEntrega, pedTotal,pedActivo,pedLlego) values (24, 16, 24, '2020-06-02', '2021-10-13',7517.4,1,0);
insert into Pedido (pedId, trabRFC, proveedorId, pedFecha, pedFechaEntrega, pedTotal,pedActivo,pedLlego) values (25, 20, 25, '2021-04-12', '2021-10-21', 65710,1,0);


select*from FacturaPedido;
insert into FacturaPedido(pedId, productoId, pedCantProduc, pedCantDinero,pedFActivo) values (1,1, 3, 3602.19,1);
insert into FacturaPedido(pedId, productoId, pedCantProduc, pedCantDinero,pedFActivo) values (2,2, 3, 2917.53,1);
insert into FacturaPedido(pedId, productoId, pedCantProduc, pedCantDinero,pedFActivo) values (3,3, 10, 1663,1);
insert into FacturaPedido(pedId, productoId, pedCantProduc, pedCantDinero,pedFActivo) values (4,4, 30, 25479.3,1);
insert into FacturaPedido(pedId, productoId, pedCantProduc, pedCantDinero,pedFActivo) values (5,5, 2, 366.92,1);
insert into FacturaPedido(pedId, productoId, pedCantProduc, pedCantDinero,pedFActivo) values (6,6, 100, 285599,1);
insert into FacturaPedido(pedId, productoId, pedCantProduc, pedCantDinero,pedFActivo) values (7,7, 10, 4499.9,1);
insert into FacturaPedido(pedId, productoId, pedCantProduc, pedCantDinero,pedFActivo) values (8,8, 5, 3427.95,1);
insert into FacturaPedido(pedId, productoId, pedCantProduc, pedCantDinero,pedFActivo) values (9,9, 10, 4100.9,1);
insert into FacturaPedido(pedId, productoId, pedCantProduc, pedCantDinero,pedFActivo) values (10,10, 10, 1400.4,1);
insert into FacturaPedido(pedId, productoId, pedCantProduc, pedCantDinero,pedFActivo) values (11,11, 6,597.3,1);
insert into FacturaPedido(pedId, productoId, pedCantProduc, pedCantDinero,pedFActivo) values (12,12, 10, 6169.2,1);
insert into FacturaPedido(pedId, productoId, pedCantProduc, pedCantDinero,pedFActivo) values (13,13, 10, 5251.4,1);
insert into FacturaPedido(pedId, productoId, pedCantProduc, pedCantDinero,pedFActivo) values (14,14, 9, 1142.01,1);
insert into FacturaPedido(pedId, productoId, pedCantProduc, pedCantDinero,pedFActivo) values (15,15, 8, 9340.48,1);
insert into FacturaPedido(pedId, productoId, pedCantProduc, pedCantDinero,pedFActivo) values (16,16, 7, 4147.92,1);
insert into FacturaPedido(pedId, productoId, pedCantProduc, pedCantDinero,pedFActivo) values (17,17, 3, 150.72,1);
insert into FacturaPedido(pedId, productoId, pedCantProduc, pedCantDinero,pedFActivo) values (18,18, 3, 2880.69,1);
insert into FacturaPedido(pedId, productoId, pedCantProduc, pedCantDinero,pedFActivo) values (19,19, 3, 2250.12,1);
insert into FacturaPedido(pedId, productoId, pedCantProduc, pedCantDinero,pedFActivo) values (20,20, 20, 1685,1);
insert into FacturaPedido(pedId, productoId, pedCantProduc, pedCantDinero,pedFActivo) values (21,21, 40, 28782.4,1);
insert into FacturaPedido(pedId, productoId, pedCantProduc, pedCantDinero,pedFActivo) values (22,22, 40, 14331.6,1);
insert into FacturaPedido(pedId, productoId, pedCantProduc, pedCantDinero,pedFActivo) values (23,23, 20, 2616.4,1);
insert into FacturaPedido(pedId, productoId, pedCantProduc, pedCantDinero,pedFActivo) values (24,24, 10, 7517.4,1);
insert into FacturaPedido(pedId, productoId, pedCantProduc, pedCantDinero,pedFActivo) values (25,25, 100,65710,1);


insert into Devoluciones (devolucionId, ventaNum, clienteId, tarjeNum, productoId, devolucionFecha, devolucionMotiv, devolucionCant,devolucionActivo) values (1, 1, 13,1, 1, '2021-10-22', 'Producto roto', 23,1);
insert into Devoluciones (devolucionId, ventaNum, clienteId, tarjeNum, productoId, devolucionFecha, devolucionMotiv, devolucionCant,devolucionActivo) values (2, 2, 11,2, 3, '2021-10-28', 'No le gustó al cliente', 3,1);
insert into Devoluciones (devolucionId, ventaNum, clienteId, tarjeNum, productoId, devolucionFecha, devolucionMotiv, devolucionCant,devolucionActivo) values (3, 3, 9,3, 2, '2021-07-03',  'Una polea floja', 10,1);
insert into Devoluciones (devolucionId, ventaNum, clienteId, tarjeNum, productoId, devolucionFecha, devolucionMotiv, devolucionCant,devolucionActivo) values (4, 4,4,4, 7, '2021-06-10',  'Gotea mucho', 8,1);
insert into Devoluciones (devolucionId, ventaNum, clienteId, tarjeNum, productoId, devolucionFecha, devolucionMotiv, devolucionCant,devolucionActivo) values (5, 5,16,5, 17, '2021-11-19',  'Producto roto', 10,1);
insert into Devoluciones (devolucionId, ventaNum, clienteId, tarjeNum, productoId, devolucionFecha, devolucionMotiv, devolucionCant,devolucionActivo) values (6, 6, 17,6,21, '2021-09-19',  'Producto roto', 5,1);
insert into Devoluciones (devolucionId, ventaNum, clienteId, tarjeNum, productoId, devolucionFecha, devolucionMotiv, devolucionCant,devolucionActivo) values (7, 7,7,7, 25, '2021-08-01',  'Producto roto', 5,1);
insert into Devoluciones (devolucionId, ventaNum, clienteId, tarjeNum, productoId, devolucionFecha, devolucionMotiv, devolucionCant,devolucionActivo) values (8, 8,2,8, 20, '2021-06-28',  'Mal soldado', 7,1);
insert into Devoluciones (devolucionId, ventaNum, clienteId, tarjeNum, productoId, devolucionFecha, devolucionMotiv, devolucionCant,devolucionActivo) values (9, 9,8,9, 11, '2021-05-29',  'Temperatura incorrecta', 5,1);
insert into Devoluciones (devolucionId, ventaNum, clienteId, tarjeNum, productoId, devolucionFecha, devolucionMotiv, devolucionCant,devolucionActivo) values (10, 10, 24,10, 14, '2021-04-17', 'Motor deteriorado', 5,1);
insert into Devoluciones (devolucionId, ventaNum, clienteId, tarjeNum, productoId, devolucionFecha, devolucionMotiv, devolucionCant,devolucionActivo) values (11, 11,4,11, 15, '2021-03-22', 'Motor deteriorado', 9,1);
insert into Devoluciones (devolucionId, ventaNum, clienteId, tarjeNum, productoId, devolucionFecha, devolucionMotiv, devolucionCant,devolucionActivo) values (12,12,14,12 , 6, '2021-03-30',  'Calentamiento desigual', 1,1);
insert into Devoluciones (devolucionId, ventaNum, clienteId, tarjeNum, productoId, devolucionFecha, devolucionMotiv, devolucionCant,devolucionActivo) values (13,13, 23,13, 13, '2021-02-26', 'Flama muy baja', 2,1);
insert into Devoluciones (devolucionId, ventaNum, clienteId, tarjeNum, productoId, devolucionFecha, devolucionMotiv, devolucionCant,devolucionActivo) values (14, 14,5,14, 7, '2021-10-29',  'Las teclas del panel no responden', 1,1);
insert into Devoluciones (devolucionId, ventaNum, clienteId, tarjeNum, productoId, devolucionFecha, devolucionMotiv, devolucionCant,devolucionActivo) values (15, 15,15,15, 9, '2021-01-24', 'Falla del motor eléctrico', 3,1);
insert into Devoluciones (devolucionId, ventaNum, clienteId, tarjeNum, productoId, devolucionFecha, devolucionMotiv, devolucionCant,devolucionActivo) values (16, 16, 19,16,15, '2021-03-27', 'El motor suena extraño', 9,1);
insert into Devoluciones (devolucionId, ventaNum, clienteId, tarjeNum, productoId, devolucionFecha, devolucionMotiv, devolucionCant,devolucionActivo) values (17,17, 15,17, 18, '2021-04-28', 'Falta de llaves', 3,1);
insert into Devoluciones (devolucionId, ventaNum, clienteId, tarjeNum, productoId, devolucionFecha, devolucionMotiv, devolucionCant,devolucionActivo) values (18, 18,11,18, 12, '2021-04-09', 'No es la forma deseada', 2,1);
insert into Devoluciones (devolucionId, ventaNum, clienteId, tarjeNum, productoId, devolucionFecha, devolucionMotiv, devolucionCant,devolucionActivo) values (19,19, 7,19, 11, '2021-08-19', 'Venia cuarteado', 1,1);
insert into Devoluciones (devolucionId, ventaNum, clienteId, tarjeNum, productoId, devolucionFecha, devolucionMotiv, devolucionCant,devolucionActivo) values (20, 20,10,20, 23, '2021-07-10', 'Los seguros venian rotos', 2,1);
insert into Devoluciones (devolucionId, ventaNum, clienteId, tarjeNum, productoId, devolucionFecha, devolucionMotiv, devolucionCant,devolucionActivo) values (21, 21,11,21, 22, '2021-10-29', 'No es la forma deseada', 1,1);
insert into Devoluciones (devolucionId, ventaNum, clienteId, tarjeNum, productoId, devolucionFecha, devolucionMotiv, devolucionCant,devolucionActivo) values (22, 22,13,22, 25, '2021-11-16', 'Producto roto', 7,1);
insert into Devoluciones (devolucionId, ventaNum, clienteId, tarjeNum, productoId, devolucionFecha, devolucionMotiv, devolucionCant,devolucionActivo) values (23, 23,21,23, 2, '2021-05-30', 'Retorno de líquido', 4,1);
insert into Devoluciones (devolucionId, ventaNum, clienteId, tarjeNum, productoId, devolucionFecha, devolucionMotiv, devolucionCant,devolucionActivo) values (24, 24,9,24, 9, '2021-08-28', 'Falta de seguros', 5,1);
insert into Devoluciones (devolucionId, ventaNum, clienteId, tarjeNum, productoId, devolucionFecha, devolucionMotiv, devolucionCant,devolucionActivo) values (25, 25,13,25, 9, '2021-01-17', 'Mal soldado', 1,1);




