-- Vista clientes
CREATE VIEW Vista_Cliente
AS
select Cliente.clienteId as 'ID_Cliente', CONCAT(Persona.personaNom,' ', Persona.personaApeP, ' ', Persona.personaApeM) as 'Nombre', Persona.personaEmail as 'Email', Persona.personaTel as 'Telefono', Cliente.clienteFechaNa as 'Fecha_Nacimiento'
from Persona
INNER JOIN Cliente ON Persona.personaId=Cliente.personaId
order by Cliente.clienteId asc;

SELECT * FROM Vista_Cliente;

-- Vista Trabajadores
CREATE VIEW Vista_Trabajador
AS
select Trabajador.trabRFC as 'RFC_Trabajador', CONCAT(Persona.personaNom,' ', Persona.personaApeP, ' ', Persona.personaApeM) as 'Nombre', Persona.personaEmail as 'Email', Persona.personaTel as 'Telefono', Trabajador.trabFechaNac as 'Fecha_Nacimiento',
Trabajador.trabDireccion as 'Direccion', Trabajador.trabCargo as 'Cargo', Trabajador.trabSueldo as 'Sueldo', Trabajador.trabActivo as 'Activo'
from Persona
INNER JOIN Trabajador ON Persona.personaId=Trabajador.personaId;

SELECT RFC_Trabajador,Nombre,Email,Telefono,Fecha_Nacimiento,Direccion,Cargo,Sueldo FROM Vista_Trabajador;
SELECT * FROM Vista_Trabajador where Activo=1;

CREATE VIEW Vista_TrabajadorCont
AS
select Trabajador.trabRFC as 'RFC', Trabajador.trabContraseña as 'Contraseña', Trabajador.trabCargo as 'Cargo'
from Persona
INNER JOIN Trabajador ON Persona.personaId=Trabajador.personaId;

SELECT * FROM Vista_TrabajadorCont;

CREATE VIEW Vista_Proveedor
AS 
select Proveedor.proveedorId as 'ID_Proveedor', CONCAT(Persona.personaNom,' ', Persona.personaApeP, ' ', Persona.personaApeM) as 'Nombre', Persona.personaEmail as 'Email', Persona.personaTel as 'Telefono', Proveedor.proveedorEmpre as 'Proveedor_Empresa', Proveedor.proveedorActivo as 'Activo'
from Persona
INNER JOIN Proveedor ON Persona.personaId=Proveedor.personaId
order by Proveedor.proveedorId asc;

SELECT ID_Proveedor,Nombre,Email,Telefono,Proveedor_Empresa FROM Vista_Proveedor;
SELECT * FROM Vista_Proveedor where Activo=1;

CREATE VIEW Vista_Producto
AS
select Producto.productoId as 'ID_Producto', Producto.productoNom as 'Nombre', Producto.productoPrecio as 'Precio', Producto.productoDep as 'Departamento', Producto.inventarioCant as 'Cantidad',Producto.proveedorId as 'ID_Proveedor',Proveedor.proveedorEmpre as 'Empresa', Producto.productoActivo as 'Activo'
from Producto
INNER JOIN Proveedor ON Producto.proveedorId=Proveedor.proveedorId
order by Producto.productoId asc;

SELECT ID_Producto,Nombre,Precio,Departamento,Cantidad,ID_Proveedor,Empresa FROM Vista_Producto;
SELECT * FROM Vista_Producto where Activo=1;

CREATE VIEW Vista_Tarjeta
AS
select Tarjeta.tarjeNum as 'Numero_de_Tarjeta',Tarjeta.clienteId as 'Id_Cliente', CONCAT(Persona.personaNom,' ', Persona.personaApeP, ' ', Persona.personaApeM) as 'Cliente',Tarjeta.tarjeDiaAlta as 'Fecha_de_alta', Tarjeta.tarjeExpiracion as 'Fecha_de_expiracion', Tarjeta.tarjePuntos as 'Puntos', Tarjeta.tarjeActivo as 'Activo'
from Persona
INNER JOIN Cliente ON Persona.personaId=Cliente.personaId
INNER JOIN Tarjeta ON Cliente.clienteId=Tarjeta.clienteId
order by Tarjeta.tarjeNum asc;

SELECT Numero_de_Tarjeta,Cliente,Fecha_de_alta,Fecha_de_expiracion,Puntos FROM Vista_Tarjeta;
SELECT * FROM Vista_Tarjeta where Activo=1;

CREATE VIEW Vista_Venta
AS
select Ventas.ventaNum as 'Venta_Numero', Ventas.trabRFC as 'Trabajador_RFC',Ventas.clienteId as 'ID_Cliente', Ventas.ventaFecha as 'Fecha', Ventas.ventaTotal as 'Total',Ventas.tarjeNum as 'ID_Tarjeta', Ventas.puntosGastados as 'Puntos_Gastados', Ventas.efectivo as 'Efectivo', Ventas.cambio as 'Cambio'
from Ventas
order by Ventas.ventaNum asc;
SELECT * FROM Vista_Venta;

/**------------------------- 27/11/2021---------------------**/
CREATE VIEW Vista_Pedidos
AS
select Pedido.pedId as 'ID_Pedido', Pedido.trabRFC 'RFC_Trabajador', Pedido.proveedorId as 'ID_Proveedor', Pedido.pedFecha as 'Fecha_de_Pedido', Pedido.pedFechaEntrega as 'Fecha_de_Entrega', Pedido.pedTotal as 'Total' ,Pedido.pedActivo as 'Activo',Pedido.pedLlego as 'Entregado'
from Pedido
order by Pedido.pedId asc;

SELECT ID_Pedido,RFC_Trabajador,ID_Proveedor,Fecha_de_Pedido,Fecha_de_Entrega,Total FROM Vista_Pedidos;
SELECT * FROM Vista_Pedidos where Activo=1;

CREATE VIEW Vista_FacturaVenta
AS
select FacturaVenta.ventaNum as 'Venta_Numero', Producto.productoNom as 'Producto', FacturaVenta.ventaPrecio as 'Precio_Total', FacturaVenta.ventaCant as 'Cantidad',Producto.productoId as 'ID_Producto', FacturaVenta.ventActivo as 'Activo'
from Producto
INNER JOIN FacturaVenta ON Producto.productoId=FacturaVenta.productoId
order by FacturaVenta.ventaNum asc;
SELECT Venta_Numero, Producto,Precio_Total,Cantidad,ID_Producto FROM Vista_FacturaVenta;
SELECT * FROM Vista_FacturaVenta;

CREATE VIEW Vista_FacturaPedido
AS
select FacturaPedido.pedId as 'ID_Pedido', Producto.productoNom as 'Producto', FacturaPedido.pedCantProduc as 'Cantidad', FacturaPedido.pedCantDinero as 'Precio_Total', FacturaPedido.pedFActivo as 'Activo'
from Producto
INNER JOIN FacturaPedido ON Producto.productoId=FacturaPedido.productoId
order by FacturaPedido.pedId asc;
SELECT ID_Pedido,Producto,Cantidad,Precio_Total FROM Vista_FacturaPedido;
SELECT * FROM Vista_FacturaPedido;

/**------------------------- 27/11/2021---------------------**/
-- Vista para ver productos que tiene un pedido en especifico
CREATE VIEW Vista_PedProducto
AS
select FacturaPedido.pedId as 'ID_Pedido', FacturaPedido.productoId as 'ID_Producto', Producto.productoNom as 'Nombre', FacturaPedido.pedCantDinero as 'Precio_Total', Producto.productoDep as 'Departamento', FacturaPedido.pedCantProduc as 'Cantidad',  FacturaPedido.pedFActivo as 'Activo',Pedido.pedLlego as 'Entregado'
from Producto
INNER JOIN FacturaPedido ON Producto.productoId=FacturaPedido.productoId
INNER JOIN Pedido ON FacturaPedido.pedId=Pedido.pedId
order by FacturaPedido.productoId asc;

SELECT * FROM Vista_PedProducto;

-- Vista devoluciones
CREATE VIEW Vista_Devolucion
AS
select Devoluciones.devolucionId as 'ID_Devolucion', Devoluciones.ventaNum as 'Numero_Venta', Devoluciones.clienteId as 'ID_Cliente', Devoluciones.productoId as 'ID_Producto',Devoluciones.tarjeNum as 'ID_Tarjeta',
Devoluciones.devolucionFecha as 'Fecha', Devoluciones.devolucionMotiv as 'Motivo', Devoluciones.devolucionCant as 'Cantidad', Devoluciones.devolucionActivo as 'Activo'
from Devoluciones
order by Devoluciones.devolucionId asc;

SELECT ID_Devolucion,Numero_Venta,ID_Cliente,ID_Producto,ID_Tarjeta,Fecha,Motivo,Cantidad FROM Vista_Devolucion;
SELECT * FROM Vista_Devolucion where Activo=1;

-- Vista proveedor producto
CREATE VIEW Vista_ProveedorProd
AS
select distinct Producto.proveedorId as 'ID_Proveedor', concat(Persona.personaNom,' ', Persona.personaApeP, ' ', Persona.personaApeM) as 'Proveedor', Proveedor.proveedorEmpre as 'Empresa',
Producto.productoNom as 'Producto', Producto.productoDep as 'Departamento'
from Proveedor
INNER JOIN Persona ON Proveedor.personaId = Persona.personaId
INNER JOIN Producto ON Proveedor.proveedorId = Producto.proveedorId
order by Proveedor.proveedorId asc;

SELECT * FROM Vista_ProveedorProd;