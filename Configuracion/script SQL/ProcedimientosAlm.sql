-- AGREGAR PERSONA
DELIMITER $$
USE homedepot$$
CREATE PROCEDURE agregarPersona (
in nombre  varchar(30),
in apellidoP varchar(20),
in apellidoM varchar(20),
in email varchar(30),
in telefono varchar(15))

BEGIN
insert into Persona (personaNom, personaApeP, personaApeM, personaEmail, personaTel) values (nombre,apellidoP,apellidoM,email,telefono);
END$$
DELIMITER ;
call agregarPersona('Juan','Perez','Perez','dhwduw@gmail.com','512-745-9491');

-- AGREGAR TRABAJADOR
use homedepot;
DROP procedure IF EXISTS agregarTrabajador;

DELIMITER $$
USE homedepot$$
CREATE PROCEDURE agregarTrabajador (
in nombre  varchar(30),
in apellidoP varchar(20),
in apellidoM varchar(20),
in email varchar(30),
in telefono varchar(15),
in id varchar(20),
in fechaNac date,
in direccion varchar(100),
in cargo varchar(40),
in sueldo decimal(7,2),
in contraseña varchar(20),
in activo boolean)

BEGIN
DECLARE IdPersonaNuevo int;
call agregarPersona(nombre, apellidoP, apellidoM, email, telefono);
set IdPersonaNuevo := (select max(Persona.personaId) from Persona);
insert into Trabajador (trabRFC, personaId, trabFechaNac, trabDireccion, trabCargo, trabSueldo, trabContraseña,trabActivo) values (id,IdPersonaNuevo,fechaNac,direccion,cargo,sueldo,contraseña,activo);
END$$

DELIMITER ;

call agregarTrabajador('Carlos','Perez','Perez','ccq1uw@gmail.com','512-335-9451',26, '1990-02-18', '937 Stoughton Park', 'Atencion a clientes', 9564.17, 'ZLksRM5',1);

-- AGREGAR PROVEEDOR
DELIMITER $$
USE homedepot$$
CREATE PROCEDURE agregarProveedor (
in nombre  varchar(30),
in apellidoP varchar(20),
in apellidoM varchar(20),
in email varchar(30),
in telefono varchar(15),
in empresa varchar(30),
in activo boolean)

BEGIN
DECLARE IdPersonaNuevo int;
call agregarPersona(nombre, apellidoP, apellidoM, email, telefono);
set IdPersonaNuevo := (select max(Persona.personaId) from Persona);
insert into Proveedor (personaId, proveedorEmpre, proveedorActivo) values (IdPersonaNuevo,empresa,activo);
END$$
DELIMITER ;
call agregarProveedor ('Miguel','Lopez','Perez','mud123@gmail.com','945-745-9421','PPP',1);

-- AGREGAR PRODUCTO 
DELIMITER $$
USE homedepot$$
CREATE PROCEDURE agregarProducto (
in producto varchar(40),
in precio decimal(7,2)unsigned,
in proveedor smallint,
in departamento varchar(40),
in inventario smallint unsigned,
in activo boolean)

BEGIN
insert into Producto (productoNom, productoPrecio,proveedorId, productoDep, inventarioCant,productoActivo) values (producto,precio,proveedor,departamento,inventario,activo);
END$$
DELIMITER ;
call agregarProducto ('Bote',50,1,'Linea blanca y cocinas',200,1);



DROP procedure IF EXISTS selectVistas;
DROP procedure IF EXISTS selectBuscar;

-- BUSCAR VISTAS
DELIMITER $$
USE homedepot$$
CREATE PROCEDURE selectVistas (in ope varchar(13))
BEGIN
     case ope
     when 'Cliente' then
		SELECT * FROM Vista_Cliente;
     when 'Trabajador' then
        SELECT RFC_Trabajador,Nombre,Email,Telefono,Fecha_Nacimiento,Direccion,Cargo,Sueldo FROM Vista_Trabajador where Activo=1;
	 when 'Proveedor' then
        SELECT ID_Proveedor,Nombre,Email,Telefono,Proveedor_Empresa FROM Vista_Proveedor where Activo=1;
     when 'Producto' then
        SELECT ID_Producto,Nombre,Precio,Departamento,Cantidad,ID_Proveedor,Empresa FROM Vista_Producto where Activo=1;
     when 'Tarjetas' then
        SELECT Numero_de_Tarjeta,Id_Cliente,Cliente,Fecha_de_alta,Fecha_de_expiracion,Puntos FROM Vista_Tarjeta where Activo=1;
     when 'Venta' then
        SELECT * FROM Vista_Venta;
     when 'Pedidos' then
        SELECT ID_Pedido,RFC_Trabajador,ID_Proveedor,Fecha_de_Pedido,Fecha_de_Entrega,Total FROM Vista_Pedidos where Activo=1;
      when 'PedidoProductosId' then
        SELECT ID_Pedido,ID_Producto,Nombre,Precio_Total,Departamento,Cantidad FROM Vista_PedProducto where Activo=1;
     when 'FacturaVenta' then
        SELECT Venta_Numero, Producto,Precio_Total,Cantidad,ID_Producto FROM Vista_FacturaVenta where Activo=1;
     when 'FacturaPedido' then
        SELECT ID_Pedido,Producto,Cantidad,Precio_Total FROM Vista_FacturaPedido where Activo=1;
     when 'Devolucion' then 
        SELECT ID_Devolucion,Numero_Venta,ID_Cliente,ID_Producto,ID_Tarjeta,Fecha,Motivo,Cantidad FROM Vista_Devolucion  where Activo=1;
     when 'ProvProd' then
        SELECT * FROM Vista_ProveedorProd;
	 end case;
END$$

DELIMITER ;

call selectVistas('Devolucion');
call selectVistas('Venta');

-- BUSCAR ID O NOMBRES EN LAS VISTAS
/**------------------------- 27/11/2021---------------------**/
DELIMITER $$
USE homedepot$$
CREATE PROCEDURE selectBuscar (in id varchar(50), in ope varchar(50))
BEGIN
     case ope
     when 'Cliente' then
		SELECT * FROM Vista_Cliente where ID_Cliente Like id;
	 when 'ClienteNombre' then
		SELECT * FROM Vista_Cliente where Nombre Like id;
     when 'Trabajador' then
        SELECT * FROM Vista_Trabajador where RFC_Trabajador Like id and Activo=1;
     when 'TrabajadorNombre' then
        SELECT * FROM Vista_Trabajador where Nombre Like id and Activo=1;
     when 'TrabajadorContraseña' then
        SELECT * FROM Vista_TrabajadorCont where RFC Like id;
	when 'TrabajadorTodos' then
        SELECT * FROM Vista_Trabajador where RFC_Trabajador Like id ;
	 when 'Proveedor' then
        SELECT * FROM Vista_Proveedor where ID_Proveedor Like id and Activo=1;
	 when 'ProveedorNombre' then
        SELECT * FROM Vista_Proveedor where Nombre Like id and Activo=1;
	 when 'ProveedorEmpresa' then
        SELECT * FROM Vista_Proveedor where Proveedor_Empresa Like id and Activo=1;
     when 'Producto' then
        SELECT * FROM Vista_Producto where ID_Producto Like id and Activo=1;
	 when 'ProductoNombre' then
        SELECT * FROM Vista_Producto where Nombre Like id and Activo=1;
     when 'ProductoIdCualquiera' then
        SELECT * FROM Vista_Producto where ID_Producto Like id;
	 when 'ProductoNombreCualquiera' then
        SELECT * FROM Vista_Producto where Nombre Like id;
     when 'Tarjetas' then
        SELECT * FROM Vista_Tarjeta where Numero_de_Tarjeta Like id and Activo=1;
	 when 'TarjetasCliente' then
        SELECT * FROM Vista_Tarjeta where Cliente Like id and Activo=1;
     when 'Venta' then
        SELECT * FROM Vista_Venta where Venta_Numero Like id;
     when 'Pedidos' then
        SELECT * FROM Vista_Pedidos where ID_Pedido Like id and Activo=1;
     when 'FacturaVenta' then
        SELECT * FROM Vista_FacturaVenta where Venta_Numero Like id and Activo=1;
     when 'PedidoProductos' then
        SELECT * FROM Vista_PedProducto where ID_Pedido Like id and Activo=1 and Entregado=false;
	 when 'PedidoProductosId' then
        SELECT * FROM Vista_PedProducto where ID_Pedido Like id;
	 when 'FacturaPedido' then
        SELECT * FROM Vista_FacturaPedido where ID_Pedido Like id  and Activo=1;
     when 'Devolucion' then 
        SELECT * FROM Vista_Devolucion where ID_Devolucion Like id  and Activo=1;
	 when 'ProveedorProducto' then
		SELECT * FROM Vista_ProveedorProd where ID_Proveedor Like id;
	 end case;
END$$

DELIMITER ;

call selectBuscar('1%','Cliente');
call selectBuscar('D%','ClienteNombre');
call selectBuscar('1%','Trabajador');
call selectBuscar('E%','TrabajadorNombre');
call selectBuscar('1%','Proveedor');
call selectBuscar('S%','ProveedorNombre');
call selectBuscar('1%','Producto');
call selectBuscar('C%','ProductoNombre');
call selectBuscar('1%','Tarjetas');
call selectBuscar('D%','TarjetasCliente');
call selectBuscar('1%','Venta');
call selectBuscar('1%','Pedidos');
call selectBuscar('2%','FacturaVenta');
call selectBuscar('2%','PedidoProductos');
call selectBuscar('1%','FacturaPedido');
call selectBuscar('3%','Devolucion');

-- SABER LA CANTIDAD QUE SE A VENDIDO DE UN PRODUCTO
DELIMITER $$
USE homedepot$$
CREATE PROCEDURE contarProductoVenta(in nombre varchar(40),out numero varchar(20))
BEGIN
     SELECT 
     sum(FacturaVenta.ventaCant) INTO numero FROM FacturaVenta
     INNER JOIN producto ON FacturaVenta.productoId = producto.productoId
     where Producto.ProductoNom=nombre;
END$$

DELIMITER ;

use homedepot;
call contarProductoVenta('Refrigerador',@numero);
call contarProductoVenta('Cisterna',@numero);
SELECT @numero AS Productos_VendidosTotal;

/**----------------------22/11/2021-----------------------------------------***/
DELIMITER $$
USE homedepot$$
CREATE PROCEDURE selectPantallasActualizaciones (in iVista varchar(40), in vIdVista varchar(40))
BEGIN
     case iVista
     when 0 then
		SELECT * FROM Vista_Cliente where  ID_Cliente=vIdVista;
	 when 1 then
		SELECT RFC_Trabajador,Nombre,Email,Telefono,Fecha_Nacimiento,Direccion,Cargo,Sueldo FROM Vista_Trabajador where  RFC_Trabajador=vIdVista and activo = true;
     when 2 then
        SELECT ID_Proveedor,Nombre,Email,Telefono,Proveedor_Empresa FROM Vista_Proveedor where  ID_Proveedor=vIdVista and activo = true;
     when 3 then
        SELECT ID_Producto,Nombre,Precio,Departamento,Cantidad,ID_Proveedor,Empresa FROM Vista_Producto where  ID_Producto=vIdVista and activo = true;
	 when 4 then
        SELECT ID_Pedido,RFC_Trabajador,ID_Proveedor,Fecha_de_Pedido,Fecha_de_Entrega,Total FROM Vista_Pedidos where  ID_Pedido=vIdVista and activo = true and Entregado=false;
	 when 5 then
        SELECT * FROM Vista_Venta where Venta_Numero=vIdVista;
	 end case;
END$$
DELIMITER ;


call selectPantallasActualizaciones(0, '1');





-- AGREGAR CLIENTE
DELIMITER $$
USE homedepot$$
CREATE PROCEDURE agregarCliente (
in nombre  varchar(30),
in apellidoP varchar(20),
in apellidoM varchar(20),
in email varchar(30),
in telefono varchar(15),
in FechaNa date
)
BEGIN
DECLARE IdPersonaNuevo int;
call agregarPersona(nombre, apellidoP, apellidoM, email, telefono);
set IdPersonaNuevo := (select max(Persona.personaId) from Persona);
insert into Cliente(personaId, clienteFechaNa) values (IdPersonaNuevo, FechaNa);
END$$
DELIMITER ;

call agregarCliente('Alfredo','Hernandez','Gomez','ALHGO@gmail.com','2211334455','1991-12-12');





-- AGREGAR TARJETA
DELIMITER $$
USE homedepot$$
CREATE PROCEDURE agregarTarjeta (
in NclienteId  smallint,
in NtrabRFC varchar(20),
in NtarjeDiaAlta date,
in NtarjeExpiracion date,
in NtarjePuntos mediumint,
in activo boolean
)
BEGIN
insert into Tarjeta (clienteId,trabRFC, tarjeDiaAlta, tarjeExpiracion, tarjePuntos,tarjeActivo) values (NclienteId,NtrabRFC, NtarjeDiaAlta, NtarjeExpiracion, NtarjePuntos,activo);
END$$
DELIMITER ;
call agregarTarjeta(2,'1','2021-11-12','2024-11-12', 0,1);