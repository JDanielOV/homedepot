/**-------------------------------Eliminar Trabajador--------------------------**/

DELIMITER $$
USE homedepot$$
CREATE PROCEDURE eliminarTrabajador(
	in id varchar(20)
    )
BEGIN 
Update Trabajador set
trabActivo=false
where trabRFC=id;
END$$

DELIMITER ;


/**-------------------------------Eliminar tarjeta--------------------------**/

DELIMITER $$
USE homedepot$$
CREATE PROCEDURE eliminarTarjeta(
	in id mediumint
    )
BEGIN 
Update Tarjeta set
tarjeActivo=false
where tarjeNum=id;
END$$

DELIMITER ;

/**-------------------------------Eliminar Devolucion--------------------------**/
DELIMITER $$
USE homedepot$$
CREATE PROCEDURE eliminarDevolucion(
	in id smallint,
    in idProducto smallint,
    in cantidad smallint,
    in numVenta mediumint,
    in cliente smallint,
    in NTarje smallint
    )
BEGIN 
DECLARE idActivo boolean;
DECLARE Precio decimal(7,2)unsigned;
DECLARE ventaPreci decimal(9,2)unsigned;
  set Precio := (select (Producto.productoPrecio) from producto where productoId=idProducto);
Update Devoluciones set
devolucionActivo=false
where devolucionId=id;
call darPuntos(cantidad,NTarje,idProducto);

Update Producto set inventarioCant = inventarioCant - cantidad
where Producto.productoId=idProducto;

Update FacturaVenta set ventaCant = ventaCant + cantidad
where FacturaVenta.ventaNum=numVenta and FacturaVenta.productoId=idProducto;

Update FacturaVenta set ventaPrecio = (ventaPrecio + (Precio*cantidad))
where FacturaVenta.ventaNum=numVenta and FacturaVenta.productoId=idProducto;

set ventaPreci := (select (FacturaVenta.ventaPrecio) from FacturaVenta where FacturaVenta.ventaNum=numVenta and FacturaVenta.productoId=idProducto);
Update Ventas set ventaTotal = (ventaTotal + (Precio*cantidad))
where Ventas.ventaNum=numVenta and Ventas.clienteId=cliente;
  
set idActivo := (select facturaventa.ventActivo from facturaventa where FacturaVenta.ventaNum=numVenta and FacturaVenta.productoId=idProducto);

Update FacturaVenta set ventActivo=true
where FacturaVenta.ventaNum=numVenta and FacturaVenta.productoId=idProducto and (ventaCant>0);
END$$

DELIMITER ;
call agregarDevolucion(4,4,7,'2021-11-15', 'Producto roto',1,null,1);
call eliminarDevolucion(44,7,1,4,4,null);
call eliminarDevolucion(45,1,50,29,1,"null");

/**-------------------------------Eliminar pedido--------------------------**/

DELIMITER $$
USE homedepot$$
CREATE PROCEDURE eliminarPedido(
	in id mediumint
    )
BEGIN 
Update Pedido set
pedActivo=false
where pedId=id and pedLlego=false;
call eliminarFacturaPed(id);
END$$

DELIMITER ;

call eliminarPedido(26);

/**-------------------------------Eliminar facturaPedido--------------------------**/

DELIMITER $$
USE homedepot$$
CREATE PROCEDURE eliminarFacturaPed(
	in id mediumint
    )
BEGIN 
Update FacturaPedido set
pedFActivo=false
where pedId=id;
END$$

DELIMITER ;

/**-------------------------------Eliminar proveedor--------------------------**/
DELIMITER $$
USE homedepot$$
CREATE PROCEDURE eliminarProveedor(
	in id smallint(20)
    )
BEGIN 
Update Proveedor set
proveedorActivo=false
where ProveedorId=id;
END$$

DELIMITER ;

/**-------------------------------Eliminar producto--------------------------**/

DELIMITER $$
USE homedepot$$
CREATE PROCEDURE eliminarProducto(
	in id smallint
    )
BEGIN 
Update Producto set
productoActivo=false
where productoId=id;
END$$

DELIMITER ;