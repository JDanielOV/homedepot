/*--------------------------------Actualizar Persona------------------------*/
DELIMITER $$
USE homedepot$$
CREATE PROCEDURE actualizarPersona(
	in personaIdP smallint,
	in personaNom varchar(30),
	in personaApeP varchar(20),
	in personaApeM varchar(20),
	in personaEmail varchar(30),
	in personaTel varchar(15))
BEGIN 
Update Persona set
personaNom=personaNom,
personaApeP=personaApeP,
personaApeM=personaApeM,
personaEmail=personaEmail,
personaTel=personaTel
where personaId=personaIdP;
END$$
DELIMITER ;

call actualizarPersona(2, 'antonio manuel', 'benitez', 'varela', 'abv@outlook.com','2213334444');


DELIMITER $$
USE homedepot$$
CREATE PROCEDURE actualizarCliente(
	in id varchar(20),
	in personaNom varchar(30),
	in personaApeP varchar(20),
	in personaApeM varchar(20),
	in personaEmail varchar(30),
	in personaTel varchar(15),
    in clienteFechaNa date)
BEGIN 
DECLARE IdPersona int;
SET IdPersona := (select personaId from Cliente WHERE clienteId = id);
call actualizarPersona(IdPersona, personaNom, personaApeP, personaApeM, personaEmail, personaTel);
Update Cliente set
clienteFechaNa=clienteFechaNa
where clienteId=id;
END$$

DELIMITER ;

call actualizarCliente(2, 'Maria Esperanza', 'Muñoz', 'Bautista', 'memb@hotmail.com','22-1-3334444', '1990-04-16');

DELIMITER $$
USE homedepot$$
CREATE PROCEDURE actualizarTrabajador(
    in id varchar(20),
	in personaNom varchar(30),
	in personaApeP varchar(20),
	in personaApeM varchar(20),
	in personaEmail varchar(30),
	in personaTel varchar(15),
    in fechaNac date,
    in direccion varchar(100),
    in cargo varchar(40),
    in sueldo decimal(7,2),
    in contraseña varchar(20))
BEGIN 
DECLARE IdPersona int;
SET IdPersona := (select personaId from Trabajador WHERE trabRFC = id);
call actualizarPersona(IdPersona, personaNom, personaApeP, personaApeM, personaEmail, personaTel);
Update Trabajador set
trabFechaNac=fechaNac,
trabDireccion=direccion,
trabCargo=cargo,
trabSueldo=sueldo,
trabContraseña=contraseña
where trabRFC=id;
END$$

DELIMITER ;

call actualizarTrabajador(25, 'Juana', 'Palacios', 'Bautista', 'jp19b@hotmail.com','11-1-6334944','1990-03-28', '968 Stoughton Park', 'Atencion a clientes', 9500.17, '12345');

/*-------------------------------------------------------------30/11/2021*/
DELIMITER $$
USE homedepot$$
CREATE PROCEDURE actualizarProducto(
	in id smallint,
	in NproductoNom varchar(40),
	in NproductoPrecio decimal(7,2),
	in NproveedorId smallint,
	in NproductoDep varchar(40),
    in NproductoActivo boolean
    )
BEGIN 
Update Producto set
productoNom=NproductoNom,
productoPrecio=NproductoPrecio,
proveedorId=NproveedorId,
productoDep=NproductoDep,
productoActivo=NproductoActivo
where productoId=id;
END$$
DELIMITER ;

/**-------------------------nombreProductoIdProd 26/11/2021---------------------**/
DELIMITER $$
USE homedepot$$
CREATE PROCEDURE nombreProductoIdProdDevolucion(
   in venta mediumint
)
BEGIN
	SELECT CONCAT(Nombre,', Marca ',Empresa,', ID: ',Vista_FacturaVenta.ID_Producto) FROM Vista_Producto 
    INNER JOIN Vista_FacturaVenta ON Vista_Producto.ID_Producto=Vista_FacturaVenta.ID_Producto
    WHERE Venta_Numero Like venta;
END$$
DELIMITER ;
call nombreProductoIdProdDevolucion(1);

/**-------------------------nombreProductoIdProveedorV 26/11/2021---------------------**/
DELIMITER $$
USE homedepot$$
CREATE PROCEDURE `nombreProductoIdProveedorV`(
   in proveedor varchar(40)
)
BEGIN
	SELECT CONCAT(Nombre,', ID: ',ID_Producto) FROM Vista_Producto 
    WHERE ID_Proveedor Like proveedor;
END$$
DELIMITER ;
call nombreProductoIdProveedorV(1);

/**-------------------------nombreProveedorId 26/11/2021---------------------**/
DELIMITER $$
USE homedepot$$
CREATE PROCEDURE `nombreProveedor`(
   in proveedor varchar(50)
)
BEGIN
	SELECT CONCAT(Proveedor_Empresa,', ',Nombre,', ID: ',ID_Proveedor) FROM Vista_Proveedor 
    WHERE Proveedor_Empresa Like proveedor AND Activo=true;
END$$
DELIMITER ;
call nombreProveedor("T");

/**-------------------------TrabajdorFechaNombre 21/11/2021---------------------**/

DELIMITER $$
USE homedepot$$
CREATE PROCEDURE TrabajdorFechaNombre(
	in personaNom varchar(40),
    in fechaNac date
    )
BEGIN 
	select RFC_Trabajador from Vista_Trabajador where Nombre=personaNom and Fecha_Nacimiento=fechaNac;
END$$
DELIMITER ;

DELIMITER $$
USE homedepot$$
CREATE PROCEDURE actualizarProveedor(
	in id smallint,
	in personaNom varchar(30),
	in personaApeP varchar(20),
	in personaApeM varchar(20),
	in personaEmail varchar(30),
	in personaTel varchar(15),
    in empresa varchar(30))
BEGIN 
DECLARE IdPersona int;
SET IdPersona := (select personaId from Proveedor WHERE proveedorId = id);
call actualizarPersona(IdPersona, personaNom, personaApeP, personaApeM, personaEmail, personaTel);
Update Proveedor set
proveedorEmpre=empresa
where proveedorId=id;
END$$

DELIMITER ;
call actualizarProveedor(26,'Juan', 'Marquez', 'Bautista', 'jjjmb@hotmail.com','123-123-4567','EMparedado');

/**-------------------------actualizarPedido 26/11/2021---------------------**/
DELIMITER $$
USE homedepot$$
CREATE PROCEDURE actualizarPedido(
	in id mediumint,
    in NpedFechaEntrega date,
    in NpedTotal decimal(10,2),
    in entrega boolean)
BEGIN 
Update Pedido set
pedFechaEntrega=NpedFechaEntrega, pedTotal=NpedTotal, pedLlego=entrega
where pedId=id;
END$$

DELIMITER ;

/**-------------------------actualizarPedidoTotal 26/11/2021---------------------**/
DELIMITER $$
USE homedepot$$
CREATE PROCEDURE actualizarPedidoTotal(
	in id mediumint,
    in NpedTotal decimal(10,2))
BEGIN 
Update Pedido set
pedTotal=NpedTotal
where pedId=id;
END$$

DELIMITER ;

/*DELIMITER $$
USE homedepot$$
CREATE PROCEDURE nombreProductoIdProdPedido(
   in nombreProducto varchar(40),
   in pedido mediumint
)
BEGIN
	SELECT CONCAT(Nombre,', ID_Producto: ',ID_Producto,', Marca ',Empresa,', ID_Proveedor= ',ID_Proveedor) FROM Vista_Producto 
    INNER JOIN Vista_FacturaPedido ON Vista_Producto.Nombre=Vista_FacturaPedido.Producto
    WHERE Nombre LIKE nombreProducto && ID_Pedido Like pedido;
END$$
DELIMITER ;
call nombreProductoIdProdPedido('c%',1);*/

DELIMITER $$
USE homedepot$$
CREATE PROCEDURE llegoPedido(
    in NproductoId smallint,
    in NpedCant smallint,
    in entrega boolean)
BEGIN 
Update Producto set
inventarioCant=inventarioCant+NpedCant
where productoId=NproductoId and entrega=true;
END$$

DELIMITER ;

DELIMITER $$
USE homedepot$$
CREATE PROCEDURE actualizarFacturaPedido(
	in id mediumint,
	in NproductoId smallint,
    in NpedPrecio decimal(9,2),
	in NpedCant smallint,
    in activo boolean,
    in entrega boolean)
BEGIN 
Update FacturaPedido set
productoId=NproductoId,pedCantProduc=NpedCant, pedCantDinero=NpedPrecio, pedFActivo=activo
where pedId=id and productoId=NproductoId;
call llegoPedido(NproductoId,NpedCant,entrega);
END$$

DELIMITER ;

-- call actualizarPedido(25,"2021-11-26",65710.00,true);
-- call actualizarFacturaPedido(25,25,65710.00,100,1,true);

DELIMITER $$
USE homedepot$$
CREATE PROCEDURE eliminarFacturaPedido(
	in id mediumint,
	in NproductoId smallint)
BEGIN 
Update FacturaPedido set
pedCantProduc=0, pedCantDinero=0,pedFActivo=0
where pedId=id and productoId=NproductoId;
END$$

DELIMITER ;



 


/*-----------------------------nombreTrabajador 27/11/2021---------------------------------*/
DELIMITER $$
USE homedepot$$
CREATE PROCEDURE `nombreTrabajadorRFC`(
	in nombreTrabajador varchar(40)
    )
BEGIN 
	SELECT CONCAT(Nombre,',     RFC: ',RFC_Trabajador) FROM Vista_Trabajador WHERE Nombre LIKE nombreTrabajador AND Activo=true;
END$$
DELIMITER ;

/*-----------------------------combinarTarjetas 01/12/2021---------------------------------*/
DELIMITER $$
USE homedepot$$
CREATE PROCEDURE combinarTarjetas(
in idTarjetaOrigen smallint,
in idTarjetaDestino smallint
)
BEGIN
Declare PuntosTarjeOrigen smallint;
SET PuntosTarjeOrigen := (select tarjePuntos from tarjeta WHERE tarjeNum = idTarjetaOrigen);
Update Tarjeta set
tarjePuntos=tarjePuntos-PuntosTarjeOrigen
where tarjeNum=idTarjetaOrigen;
Update Tarjeta set
tarjePuntos=tarjePuntos+PuntosTarjeOrigen
where tarjeNum=idTarjetaDestino;
END$$
DELIMITER ;