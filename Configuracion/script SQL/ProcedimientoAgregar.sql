/*-----------------------------quitarPuntosCompra 26/11/2021---------------------------------*/
DELIMITER $$
USE homedepot$$
CREATE  PROCEDURE `quitarPuntosCompra`(
	in NtarjeNum smallint,
    in NtarjePuntos smallint 
    )
BEGIN 
	UPDATE Tarjeta set tarjePuntos=tarjePuntos-NtarjePuntos
  where Tarjeta.tarjeNum=NtarjeNum;
END$$
DELIMITER ;

/*-----------------------------quitarPuntos 06/12/2021---------------------------------*/
DELIMITER $$
CREATE PROCEDURE quitarPuntos(in cantidad smallint unsigned,in idTarje smallint, in idProd smallint)
BEGIN
  DECLARE Precio decimal(7,2)unsigned;
  DECLARE PuntosActual mediumint;
  set Precio := (select (Producto.productoPrecio) from producto where productoId=idProd);
  set PuntosActual := (select Tarjeta.tarjePuntos from tarjeta where Tarjeta.tarjeNum=idTarje);
  Update Tarjeta set tarjePuntos = (tarjePuntos-(cantidad*Precio)*0.03)
  where idTarje IS NOT NULL AND Tarjeta.tarjeNum=idTarje and ((cantidad*Precio)*0.03)<=PuntosActual;
   Update Tarjeta set tarjePuntos = 0
  where idTarje IS NOT NULL AND Tarjeta.tarjeNum=idTarje and ((cantidad*Precio)*0.03)>PuntosActual;
END$$
DELIMITER ;
-- AGREGAR DEVOLUCION PROCEDIMIENTOS ALMACENADOS 26-11-2021
DELIMITER $$
USE homedepot$$
CREATE PROCEDURE agregarDevolucion (
in NventaNum  smallint,
in NclienteId  smallint,
in NproductoId  smallint,
in Nfecha date,
in Nmotivo varchar(100),
in Ncantidad smallint unsigned,
in NTarje smallint,
in activo boolean
)
BEGIN
insert into Devoluciones (ventaNum, clienteId,tarjeNum, productoId, devolucionFecha, devolucionMotiv, devolucionCant,devolucionActivo) values (NventaNum,NclienteId,NTarje,NproductoId, Nfecha, Nmotivo, Ncantidad,activo);
call quitarPuntos(Ncantidad,NTarje,NproductoId);
Update Producto set productoActivo=true
  where Producto.productoId=NproductoId and (Ncantidad>0);
END$$
DELIMITER ;
call agregarDevolucion(1, 4, 1, '2021-11-15', 'Producto roto', 1,4,1);

-- Eliminar devolucion procedimientos almacenados
DELIMITER $$
CREATE PROCEDURE darPuntos(in cantidad smallint unsigned,in idTarje smallint, in idProd smallint)
BEGIN
  DECLARE Precio decimal(7,2)unsigned;
  set Precio := (select (Producto.productoPrecio) from producto where productoId=idProd);
  Update Tarjeta set tarjePuntos = (tarjePuntos+(cantidad*Precio)*0.03)
  where  idTarje IS NOT NULL AND Tarjeta.tarjeNum=idTarje;
END$$
DELIMITER ;

-- Eliminar devolucion
/*DELIMITER $$
USE homedepot$$
CREATE PROCEDURE eliminarDevolucion (in id smallint,in NproductoId smallint, in Ncantidad smallint unsigned,
in NTarje smallint)
BEGIN
SET sql_safe_updates=0;
DELETE FROM Devoluciones WHERE devolucionId = id;
SET sql_safe_updates=1;
call darPuntos(Ncantidad,NTarje,NproductoId);
END$$

DELIMITER ;
call eliminarDevolucion(37,1,1,4);*/


/*-----------------------------Procedimiento agregar venta 26/11/2021---------------------------------*/
DELIMITER $$
USE homedepot$$
CREATE  PROCEDURE `agregarVenta`(
in NclienteId  smallint,
in NtrabRFC varchar(20),
in NventaFecha date,
in NventaTotal decimal(10,2),
in NidTarjeta smallint,
in NpuntosGastados mediumint,
in Nefectivo decimal(10,2)unsigned,
in NCambio decimal(10,2)unsigned
)
BEGIN
insert into Ventas (clienteId,tarjeNum,trabRFC, ventaFecha, ventaTotal,puntosGastados,efectivo,cambio) values (NclienteId,NidTarjeta,NtrabRFC, NventaFecha, NventaTotal,NpuntosGastados,Nefectivo,NCambio);
call puntosVenta(NidTarjeta, NventaTotal);
call quitarPuntosCompra(NidTarjeta,NpuntosGastados);
END$$

DELIMITER;


/*-----------------------------Procedimiento agregar venta sin tarjeta---------------------------------*/
/*
DELIMITER $$
USE homedepot$$
CREATE PROCEDURE agregarVentaSinTarjeta (
in NclienteId  smallint,
in NtrabRFC varchar(20),
in NventaFecha date,
in NventaTotal decimal(10,2),
in NidTarjeta smallint
)
BEGIN
insert into Ventas (clienteId,trabRFC, ventaFecha, ventaTotal) values (NclienteId,NtrabRFC, NventaFecha, NventaTotal);
END$$
DELIMITER ;
*/
/*-----------------------------Procedimiento agregar FacturaVenta---------------------------------*/
DELIMITER $$
USE homedepot$$
CREATE PROCEDURE agregarFacturaVenta (
in NproductoId smallint,
in NventaPrecio decimal(9,2),
in NventaCant smallint,
in activo boolean
)
BEGIN
DECLARE IdVentaNueva int;
set IdVentaNueva := (select max(Ventas.ventaNum) from Ventas);
insert into FacturaVenta(ventaNum, productoId, ventaPrecio, ventaCant,ventActivo)values (IdVentaNueva, NproductoId, NventaPrecio, NventaCant,activo);
END$$
DELIMITER ;

/*-----------------------------Procedimiento aumtentar puntos en la venta---------------------------------*/
DELIMITER $$
USE homedepot$$
CREATE PROCEDURE puntosVenta (
in NidTarjeta smallint,
in NventaTotal decimal(10,2)
)
BEGIN
        UPDATE Tarjeta set tarjePuntos=tarjePuntos+(NventaTotal*0.03)
		where  NidTarjeta IS NOT NULL AND Tarjeta.tarjeNum=NidTarjeta; 

END$$
DELIMITER ;

/*5689 puntps
77 inventario
27 venta
tarjeta 4 cliente 4
*/

call agregarVenta(4, '1', '2021-11-15', 3602.19, 4 );
call agregarFacturaVenta(1, 3602.19, 3,1);

/*-----------------------------Procedimiento agregar pedido---------------------------------*/
DELIMITER $$
USE homedepot$$
CREATE PROCEDURE agregarPedido(
in NtrabRFC varchar(20),
in NproveedorId  smallint,
in NpedFecha date,
in NpedFechaEntrega date,
in NpedTotal decimal(10,2),
in activo boolean,
in entrega boolean
)
BEGIN
insert into Pedido (trabRFC, proveedorId, pedFecha, pedFechaEntrega, pedTotal,pedActivo,pedLlego) values (NtrabRFC,NproveedorId, NpedFecha,NpedFechaEntrega, NpedTotal,activo,entrega);
END$$
DELIMITER ;

/*-----------------------------Procedimiento agregar FacturaPedido---------------------------------*/
DELIMITER $$
USE homedepot$$
CREATE PROCEDURE agregarFacturaPedido (
in NproductoId smallint,
in NpedPrecio decimal(9,2),
in NpedCant smallint,
in activo boolean
)
BEGIN
DECLARE IdPedidoNueva int;
set IdPedidoNueva := (select max(Pedido.pedId) from Pedido);
insert into FacturaPedido(pedId, productoId, pedCantProduc, pedCantDinero,pedFActivo)values (IdPedidoNueva, NproductoId, NpedCant, NpedPrecio,activo);
END$$
DELIMITER ;

call agregarPedido(13, 1, '2021-04-20', '2021-10-12',3602.19,1);
call agregarFacturaPedido(1, 3, 3602.19,1);

DELIMITER $$
USE homedepot$$
CREATE PROCEDURE agregarFacturaPedidoActualizar (
in id smallint,
in NproductoId smallint,
in NpedPrecio decimal(9,2),
in NpedCant smallint,
in activo boolean
)
BEGIN
insert into FacturaPedido(pedId, productoId, pedCantProduc, pedCantDinero,pedFActivo)values (id, NproductoId, NpedCant, NpedPrecio,activo);
END$$
DELIMITER ;



/*----------------------------------Ver Existencia Producto proveedor----------------------------*/

DELIMITER $$
USE homedepot$$
CREATE PROCEDURE ExistenciaProducto (
in productoNom varchar(40),
in proveedorId smallint
)
BEGIN
 select*from Vista_Producto where Nombre=productoNom and ID_Proveedor=proveedorId and Activo=true;
END$$
DELIMITER ;


/*----------------------------------Ver Existencia Trabajador----------------------------*/

DELIMITER $$
USE homedepot$$
CREATE PROCEDURE ExistenciaTrabajador(
	in rfc varchar(20),
	in personaNom varchar(40),
    in fechaNac date
    )
BEGIN 
	select * from Vista_Trabajador where Nombre=personaNom and Fecha_Nacimiento=fechaNac and rfc=RFC_Trabajador and activo=true;
END$$
DELIMITER ;


/*----------------------------------Ver despido Trabajador----------------------------*/

DELIMITER $$
USE homedepot$$
CREATE PROCEDURE DespidoTrabajador(
	in rfc varchar(20),
	in personaNom varchar(40),
    in fechaNac date
    )
BEGIN 
	select * from Vista_Trabajador where Nombre=personaNom and Fecha_Nacimiento=fechaNac and rfc=RFC_Trabajador and activo=false;
END$$
DELIMITER ;




/*----------------------------------Ver contratar de nuevo Trabajador----------------------------*/

DELIMITER $$
USE homedepot$$
CREATE PROCEDURE contratarDeNuevoTrabajador(
	in id varchar(20),
	in personaNom varchar(30),
	in personaApeP varchar(20),
	in personaApeM varchar(20),
	in personaEmail varchar(30),
	in personaTel varchar(15),
    in fechaNac date,
    in direccion varchar(100),
    in cargo varchar(40),
    in sueldo decimal(7,2),
    in contraseña varchar(20))
BEGIN 
DECLARE IdPersona int;
SET IdPersona := (select personaId from Trabajador WHERE trabRFC = id);
call actualizarPersona(IdPersona, personaNom, personaApeP, personaApeM, personaEmail, personaTel);
Update Trabajador set
trabFechaNac=fechaNac,
trabDireccion=direccion,
trabCargo=cargo,
trabSueldo=sueldo,
trabContraseña=contraseña,
trabActivo=true
where trabRFC=id;
END$$
DELIMITER ;



/**-------------------------TrabajdorFechaNombre 22/11/2021---------------------**/

DELIMITER $$
USE homedepot$$
CREATE PROCEDURE TrabajdorFechaNombreActivos(
	in personaNom varchar(40),
    in fechaNac date
    )
BEGIN 
	select RFC_Trabajador from Vista_Trabajador where Nombre=personaNom and Fecha_Nacimiento=fechaNac and activo=true;
END$$
DELIMITER ;








/*-----------------------------puntosTarjeta 26/11/2021---------------------------------*/
DELIMITER $$
USE homedepot$$
CREATE PROCEDURE `puntosTarjeta`(
	in idTarjeta mediumint
    )
BEGIN 
	SELECT Puntos FROM Vista_Tarjeta WHERE Numero_de_Tarjeta LIKE idTarjeta AND activo=true;
END$$
DELIMITER ;




/*-----------------------------puntosTarjeta 26/11/2021---------------------------------*/
DELIMITER $$
USE homedepot$$
CREATE PROCEDURE `nombreClienteIDComprovacion`(
	in nombreCliente varchar(40),
    in idCliente varchar(40)
    )
BEGIN 
	SELECT * FROM Vista_Cliente WHERE Nombre LIKE nombreCliente AND ID_Cliente LIKE idCliente;
END$$
DELIMITER ;


/*-----------------------------nombreProductoEmpresa 26/11/2021---------------------------------*/
DELIMITER $$
USE homedepot$$
CREATE  PROCEDURE `nombreProductoEmpresa`(
	in nombreProducto varchar(40)
    )
BEGIN 
	SELECT CONCAT(Nombre,', Marca: ',Empresa) FROM Vista_Producto WHERE Nombre LIKE nombreProducto AND Activo=true;
END$$
DELIMITER ;


/*-----------------------------nombreClienteIDCliente 26/11/2021---------------------------------*/
DELIMITER $$
USE homedepot$$
CREATE PROCEDURE `nombreClienteIDCliente`(
	in nombreCliente varchar(40)
    )
BEGIN 
	SELECT CONCAT(Nombre,',     ID: ',ID_Cliente) FROM Vista_Cliente WHERE Nombre LIKE nombreCliente;
END$$
DELIMITER ;


/*-----------------------------idTarjeta 26/11/2021---------------------------------*/
DELIMITER $$
USE homedepot$$
CREATE PROCEDURE `idTarjeta`(
	in idCliente varchar(40)
    )
BEGIN 
	SELECT Numero_de_Tarjeta FROM Vista_Tarjeta WHERE Id_Cliente LIKE idCliente AND Activo=true;
END$$
DELIMITER ;
call idTarjeta(13);

/*-----------------------------idProducto 26/11/2021---------------------------------*/
DELIMITER $$
USE homedepot$$
CREATE  PROCEDURE `idProducto`(
	in nombreProducto varchar(40),
    in nombreEmpresa varchar(30) 
    )
BEGIN 
	SELECT ID_Producto FROM Vista_Producto WHERE Nombre LIKE nombreProducto AND Activo=true AND Empresa=nombreEmpresa;
END$$
DELIMITER ;

/*-----------------------------idProveedorPedido 27/11/2021---------------------------------*/
DELIMITER $$
USE homedepot$$
CREATE PROCEDURE `idProveedorPed`(
	in idPedido varchar(40)
    )
BEGIN 
	SELECT ID_Proveedor FROM Vista_Pedidos WHERE ID_Pedido LIKE idPedido AND Activo=true AND Entregado=false;
END$$
DELIMITER ;
call idProveedorPed(26);






/*-----------------------------reporteVenta 01/12/2021---------------------------------*/
DELIMITER $$
USE homedepot$$
CREATE PROCEDURE `reporteVenta`(
	in idVenta mediumint
    )
BEGIN 
select 
    Vista_Cliente.Nombre,
    Vista_Trabajador.Nombre, 
    Vista_Venta.Venta_Numero, 
    (select ID_Tarjeta from Vista_Venta WHERE Vista_Venta.Venta_Numero=idVenta) as 'ID Tarjeta',
    Vista_Venta.Total, 
    Vista_Venta.Efectivo, 
    Vista_Venta.Cambio, 
    (Vista_Venta.Total*0.03) as 'Puntos Dados', 
    Vista_Venta.Puntos_Gastados, 
	(select Puntos from Vista_Tarjeta WHERE Numero_de_Tarjeta=(
		select ID_Tarjeta from Vista_Venta WHERE Vista_Venta.Venta_Numero=idVenta)) as 'Puntos' 
from Vista_Venta
INNER JOIN Vista_Cliente ON Vista_Cliente.ID_Cliente=Vista_Venta.ID_Cliente
INNER JOIN Vista_Trabajador ON Vista_Trabajador.RFC_Trabajador=Vista_Venta.Trabajador_RFC
WHERE Vista_Venta.Venta_Numero=idVenta;
END$$
DELIMITER ;
call reporteVenta(26);



/*-----------------------------ventaMasReciente 01/12/2021---------------------------------*/
DELIMITER $$
USE homedepot$$
CREATE PROCEDURE `ventaMasReciente`(
    )
BEGIN 
	SELECT max(Venta_Numero) from Vista_Venta;
END$$
DELIMITER ;
call ventaMasReciente();
